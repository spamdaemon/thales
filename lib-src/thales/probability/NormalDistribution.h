#ifndef _THALES_PROBABILITY_NORMALDISTRIBUTION_H
#define _THALES_PROBABILITY_NORMALDISTRIBUTION_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <cmath>

namespace thales {
  namespace probability {
    /**
     * This class provides a namespace for the normal distribution.
     */
    class NormalDistribution {

      /**
       * Create a new normal distribution with mean =0 and variance=1
       */
    public:
      NormalDistribution() throws();

      /**
       * Create a new normal distribution.
       * @param mean the mean of the distribution
       * @param variance the variance
       */
    public:
      NormalDistribution(double mean, double variance) throws();

      /**
       * Get the mean of this distribution.
       * @return the mean of this distribution
       */
    public:
      inline double mean() const throws() { return _mean; }

      /**
       * Get the stddev of this distribution.
       * @return the standard deviation of this distribution
       */
    public:
      inline double stddev() const throws() { return ::std::sqrt(_variance); }

      /**
       * Get the variance of this distribution.
       * @return the variance of this distribution
       */
    public:
      inline double variance() const throws() { return _variance; }

      /**
       * Evaluate the probability density function at the specified value.
       * The mean and sigma are assumed to be 0 and 1, respectively.
       * @param x a value
       * @return the density function at x
       */
    public:
      double pdf(double x) const throws();

      /**
       * Evaluate the cumulative density function at the specified value.
       * @param x a value
       * @return the density function at x
       */
    public:
      double cdf(double x) const throws();

     /**
       * Integrate the pdf over the interval minX to maxX.
       * @param minX the lower integration bound
       * @param maxX the upper integration bound
       * @return the cimulative density between minX and maxX
       */
    public:
      double cdf(double minX, double maxX) const throws();
      
      /**
       * Draw a random number from this distribution.
       * @return a random number in a distribution.
       */
    public:
      double drawRandom() const throws();
      
      /** The mean of this distribution */
    private:
      double _mean;

      /** The standard deviation */
    private:
      double _variance;
    };
  }
}

#endif
