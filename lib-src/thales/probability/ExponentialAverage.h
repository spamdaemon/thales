#ifndef _THALES_PROBABILITY_EXPONENTIALAVERAGE_
#define _THALES_PROBABILITY_EXPONENTIALAVERAGE_

#include <stdexcept>
#include <memory>
#include <cstdlib>

namespace thales {
   namespace probability {

      /**
       * This class implements an exponentially decaying moving average
       */
      template<class T> class ExponentialAverage
      {
            struct Operator
            {
                  /** The sum function */
                  inline void add(T& lhs, const T& rhs)
                  {
                     lhs += rhs;
                  }

                  /** The mul function */
                  inline void mul(T& lhs, double rhs)
                  {
                     lhs *= rhs;
                  }
            };

            /**
             * Create a new moving average
             * @param mNew the multiplier for a new sample
             * @exception ::std::runtime_error if 0>= mNew or mNew > 1
             */
         public:
            ExponentialAverage(double mNew)
                  : _initialized(false), _new(mNew), _old((1.0 - mNew) / mNew)
            {
               if (!::std::isfinite(_old) || _old < 0 || _new <= 0 || _new > 1) {
                  throw ::std::runtime_error("Invalid multipliers");
               }
            }

            /**
             * Add a new sample.
             * @param t the new sample
             * @return the  total average
             */
         public:
            void update(const T& t) throw()
            {
               if (_initialized) {
                  _op.mul(_average, _old);
                  _op.add(_average, t);
                  _op.mul(_average, _new);
               }
               else {
                  _average = t;
                  _initialized = true;
               }
            }

            /**
             * Get the current average.
             * @return the current average
             */
         public:
            const T& average() const throw()
            {
               return _average;
            }

            /** True if we've at least 1 value */
         private:
            bool _initialized;

            /** The current average */
         private:
            T _average;

            /** The 'new' multipliers */
         private:
            double _new;

            /** The 'old' multiplier */
         private:
            double _old;

            /** The operator */
         private:
            Operator _op;
      };

   }
}

#endif
