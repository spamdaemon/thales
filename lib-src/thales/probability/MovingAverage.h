#ifndef _THALES_PROBABILITY_MOVINGAVERAGE_
#define _THALES_PROBABILITY_MOVINGAVERAGE_

#include <memory>

namespace thales {
   namespace probability {

      /**
       * This class implements a moving average
       */
      template<class T> class MovingAverage
      {
            struct Operator
            {
                  /** The sum function */
                  inline void add(T& lhs, const T& rhs)
                  {
                     lhs += rhs;
                  }

                  /** The sum function */
                  inline void sub(T& lhs, const T& rhs)
                  {
                     lhs -= rhs;
                  }

                  /** The div function */
                  inline void div(T& lhs, double rhs)
                  {
                     lhs /= rhs;
                  }

                  /** The mul function */
                  inline void mul(T& lhs, double rhs)
                  {
                     lhs *= rhs;
                  }
            };

            /**
             * Create a new moving average
             * @param sz the size of the window
             */
         public:
            MovingAverage(size_t sz)
                  : _size(sz), _count(0), _history(new T[sz])
            {
            }

            /**
             * Add a new sample.
             * @param t the new sample
             * @return the  total average
             */
         public:
            void update(const T& t) throw()
            {
               const size_t i = _count % _size;
               _op.mul(_average, _count);
               if (_count >= _size) {
                  _op.sub(_average, _history[i]);
               }
               ++_count;
               _op.add(_average, t);
               _op.div(_average, ::std::min(_size, _count));
               _history[i] = t;
            }

            /**
             * Add a new sample using the move operator.
             * @param t a new sample
             * @return the total average
             */
         public:
            void update(const T&& t) throw()
            {
               const size_t i = _count % _size;
               _op.mul(_average, _count);
               if (_count >= _size) {
                  _op.sub(_average, _history[i]);
               }
               ++_count;
               _op.add(_average, t);
               _op.div(_average, ::std::min(_size, _count));
               _history[i] = ::std::move(t);
            }

            /**
             * Get the current average.
             * @return the current average
             */
         public:
            const T& average() const throw()
            {
               return _average;
            }

            /** The current average */
         private:
            T _average;

            /** The size */
         private:
            const size_t _size;

            /** The total number of items added so far */
         private:
            size_t _count;

            /** The history */
         private:
            ::std::unique_ptr< T[]> _history;

            /** The operator */
         private:
            Operator _op;
      };

   }
}

#endif
