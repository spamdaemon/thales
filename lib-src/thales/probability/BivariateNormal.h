#ifndef _THALES_PROBABILITY_BIVARIATENORMAL_H
#define _THALES_PROBABILITY_BIVARIATENORMAL_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <cmath>
#include <cassert>
#include <stdexcept>

namespace thales {
  namespace probability {

    /**
     * This BivariateNormal represents instances of bivariate normal distributions. A bivariate
     * normal distribution is defined by the following pdf:
     * <pre>
     *    PDF(x,y) = exp(-0.5*(dx^2 + dy^2 - 2*r*dx*dy)/(2*PI*sqrt(1-r*r))
     *
     *    where
     *
     *      dx = (x-mx)/sigmaX
     *      dy = (y-my)/sigmaY
     *      mx,my : the mean of the distribution 
     *      sigmaX, sigmaY : the standard deviation of the variates
     *      r : the correlation between the variates
     * </pre>
     * One of the nice features of the BivariateNormal distribution is that any instanceof of
     * a bivariate normal distribution can be transformed into the standard normal distribution.
     * Once in normal form, algorithms can often be reduced to functions of a single variate.
     * <p>
     * The class provides methods to evaluate the PDF, and the CDF (cumulate distribution function)
     * for various values of x and y. Since the CDF cannot be computed in a closed form, approximations
     * are needed. For this reason, there is no single perfect approximation that suits all 
     * purposes and algorithms using the bivariate normal distribution have been factored out
     * into separate methods. <br>
     * Two functors, BivariateNormal::CDF and BivariateNormal::ApproximateCDF,
     * have been provided and can be used with the external algorithms. ApproximateCDF provides fairly
     * good results and is generally at least twice as fast CDF, but suffers from precision problems
     * for larger values of the correlation. For most applications, the ApproximateCDF may be more
     * appropriate though if only a ballpark estimate is needed.
     * <p>
     * @see NormalDistribution
     */
    class BivariateNormal 
    {
      /**
       * A functor to compute the the cumulative distribution 
       * function of a standard bivariate 
       * normal distribution with a given rho.
       * @param x the upper integration limit for the first variate
       * @param y the upper integration limit for the second variate
       * @param rho the correlation coefficient
       */
    public:
      struct CDF {
      public:
	double operator()(double x, double y, double rho) const throws();
      };

      /**
       * A functor to compute an approximation of the cumulative distribution 
       * function of a standard bivariate 
       * normal distribution with a given rho.
       * @param x the upper integration limit for the first variate
       * @param y the upper integration limit for the second variate
       * @param rho the correlation coefficient
       */
    public:
      struct ApproximateCDF {
      public:
	double operator()(double x, double y, double rho) const throws();
      };

      /**
       * Create a bivariate normal distribution
       * @param x the mean in x
       * @param y the mean in y
       * @param sx the std. deviation in x
       * @param sy the std. deviation in y
       * @param rho the correlation coefficient 
       * @pre sx >0
       * @pre sy >0
       * @pre -1 <= rho <= 1
       */
    public:
      BivariateNormal (double x, double y, double sx, double sy, double rho) throws();
      

      /**
       * Create a bivariate normal distribution with a mean of 0,0 and a variances of 1.
       * @param rho the correlation coefficient 
       * @pre -1 <= rho <= 1
       */
    public:
      BivariateNormal (double rho=0) throws();

      /**
       * Decompose a 2D covariance into a bivariate normal distribution.
       * @param x the x-component of the mean
       * @param y the y-component of the mean
       * @param xx the xx-component of the covariance matrix
       * @param yy the yy-component of the covariance matrix
       * @param xy the xy-component (or yx-component) of the covariance matrix
       * @return a bivariate normal distribution
       * @throws ::std::exception an exception if the xx*yy - xy*xy <= 0
       */
    public:
      static BivariateNormal decomposeGaussian(double x, double y, double xx, double yy, double xy) throws (::std::exception);

      /**
       * Decompose a 2D covariance into a bivariate normal distribution.
       * @param xx the xx-component of the covariance matrix
       * @param yy the yy-component of the covariance matrix
       * @param xy the xy-component (or yx-component) of the covariance matrix
       * @return a bivariate normal distribution
       * @throws ::std::exception an exception if the xx*yy - xy*xy <= 0
       */
    public:
      static BivariateNormal decomposeCovariance(double xx, double yy, double xy) throws (::std::exception)
      { return decomposeGaussian(0,0,xx,yy,xy); }

      /**
       * Create a covariance matrix corresponding to this bivariate normal
       * @param xx the xx-component of the covariance matrix (output)
       * @param yy the yy-component of the covariance matrix (output)
       * @param xy the xy-component (or yx-component) of the covariance matrix (output)
       */
    public:
      void createCovariance (double& xx, double& yy, double& xy) const throws();

      /**
       * Get the mean of the x-variate
       * @return the mean of the variate x
       */
    public:
      inline double meanX() const throws() { return _mean[0]; }

      /**
       * Get the mean of the y-variate
       * @return the mean of the variate y
       */
    public:
      inline double meanY() const throws() { return _mean[1]; }

      /**
       * Get the stddev of the x-variate.
       * @return the standard deviation of the x-variate
       */
    public:
      inline double stddevX() const throws() { return _stddev[0]; }

      /**
       * Get the stddev of the y-variate.
       * @return the standard deviation of the y-variate
       */
    public:
      inline double stddevY() const throws() { return _stddev[1]; }

      /**
       * Get the variance of the x-variate.
       * @return the variance the the x-variate
       */
    public:
      inline double varianceX() const throws() { return _stddev[0]*_stddev[0]; }

      /**
       * Get the variance of the y-variate.
       * @return the variance the the y-variate
       */
    public:
      inline double varianceY() const throws() { return _stddev[1]*_stddev[1]; }

      /**
       * Get the correlation coefficient.
       * @return the correlation between x and y, which is a number between -1 and 1.
       */
    public:
      inline double correlation() const throws() { return _rho; }

      /**
       * Transform a point from this normal distribution into one in the standard
       * normal distribution.
       * @param x the vertex to be transformed 
       * @param y the vertex to be transformed 
       * @param tx the x-coordinate of the transformed vertex
       * @param ty the y-coordinate of the transformed vertex
       */
    public:
      inline void transform(double x, double y, double& tx, double& ty) const  throws()
      {
	const double divX = ::std::sqrt(2+2*_rho);
	const double divY = ::std::sqrt(2-2*_rho);
	
	const double tmpX = (x - _mean[0]) / _stddev[0];
	const double tmpY = (y - _mean[1]) / _stddev[1];
	
	tx = (tmpX + tmpY)/divX;
	ty = (tmpY - tmpX)/divY;
      }
      
      /**
       * Evaluate the probability density function at the specified value. 
       * If the rho==-1 then 0 is returned, if rho==1 then the 
       * NormalDistribution::pdf(max(x,y)) is returned.
       *
       * @param x a value
       * @param y the y-value
       * @return the density function at x,y given correlation rho
       */
    public:
      double pdf(double x, double y) const throws();
      
      /**
       * Evaluate the cumulative density function at the specified value. 
       * Return the probability that a point p=x0,y0 has coordinates
       * less than x and y. This is also known as the lower-left tail.
       * <p>
       * This algorithm is based on the Fortran Algorithm by Genz:
       * <pre>
       *    Alan Genz
       *    Department of Mathematics
       *    Washington State University
       *    Pullman, WA 99164-3113
       *    Email : alangenz@wsu.edu
       * </pre>
       * @param x a value
       * @param y a value
       * @return P(x0 < x and y0 < y)
       */
    public:
      double cdf(double x, double y) const throws();
      
      /**
       * Compute an approximation of the cdf. This method may be faster
       * but has larger errors at abs(rho) > 9.
       * Return the probability that a point p=x0,y0 has coordinates
       * less than x and y.
       * The algorithm used is that of Mee and Owen:
       * <pre>
       *   A SIMPLE APPROXIMATION FOR BIVARIATE NORMAL PROBABILITIES
       *         by Robert W. Mee and D. B. Owen
       *     Technical Report No. 169
       *     Department of Statistics ONR Contract
       *     December, 1982
       * </pre>
       * This algorithm is not particularly accurate for correlations
       * beyond .9 in magnitude.
       * @param x a value
       * @param y a value
       * @return P(x0 < x and y0 < y)
       */
    public:
      double approximateCDF(double x, double y) const throws();
      
      /** The mean */
    private:
      double  _mean[2];
      
      /** The standard deviations */
    private:
      double _stddev[2];

      /** The correlation coefficient */
    private:
      double _rho;

      /** True if this is a standard bivariate */
    private:
      bool _isStandard;
    };
    
    /**
     * Compute the cdf of a standard bivariate normal distribution 
     * over the triangle (0,0),(s1,t1),(s2,t2). The normal distribution
     * is assumed to have mean (0,0), variance (1,1), and the given rho.
     * <p>
     * This algorithm is due to 
     * <pre>
     *   HANDBOOK OF MATHEMATICAL FUNCTIONS
     *     Milton Abramovitz, Irene Stegun
     *    p. 956
     * </pre>
     * <p>
     * This function serves as a primitive for the signedPolygonCDF() functions. 
     * @note the resulting probability will be negative if the triangle
     *       (0,0),(s1,t1),(s2,t2) is clockwise oriented.
     *
     * @param s1 the first variate of the first point
     * @param t1 the second variate of the first point
     * @param s2 the first variate of the second point
     * @param t2 the second variate of the second point
     * @param cdf a function/functor that computes cdf(h,k,rho) 
     * @return the <em>signed</em> probability contained within the triangle (0,0),(s1,t1),(s2,t2).
     * @see BivariateNormal::transform()
     */
    template <class CDF> 
      double signedTriangleCDF (double s1, double t1, double s2, double t2, const CDF& cdf=CDF()) throws()
      {
	const double PI = 3.14159265358979323846;
	const double denom = ::std::sqrt((s2-s1)*(s2-s1) + (t2-t1)*(t2-t1));
	if (denom < 1E-8) { //FIXME: hardcoded constant!
	  // repeated points!
	  return 0;
	}
	const double h = (t2*s1 - t1*s2)/denom;
	if (::std::abs(h) < 1E-8) { //FIXME: hardcoded constant!
	  return 0;
	}

	double cdfx[2];
	double asinx[2];
	
	// NOTE: Abramovitz, Stegun use cdf(h,0,rho), since they use the upper-right tail of the distribution
	//       rather than the lower-left tail as this is more customary.
	{
	  const double k = (s1*(s2-s1) + t1*(t2-t1))/denom;
	  const double a = k/h;  // cannot simplify due to sign of h
	  const double rho = -a/::std::sqrt(1+a*a);
	  assert(-1 <= rho && rho <= 1 && "Quotient should be less than 1 in magnitude");
	  asinx[0] = ::std::asin(rho);
	  cdfx[0] = cdf(-h,0,rho);
	}
	
	{
	  const double k = (s2*(s2-s1) + t2*(t2-t1))/denom;
	  const double a = k/h;  // cannot simplify due to sign of h
	  const double rho = -a/::std::sqrt(1+a*a);
	  assert(-1 <= rho && rho <= 1 && "Quotient should be less than 1 in magnitude");
	  asinx[1] = ::std::asin(rho);
	  cdfx[1] = cdf(-h,0,rho);
	}
	
	return (cdfx[1]-cdfx[0]) - (asinx[1] - asinx[0])/(2*PI);
      }

      /**
       * Compute the cdf of a given BivariateNormal distribution over a polygon. 
       * The coordinates are specified in an array of size 2*n. The x-coordinate
       * of the i'th point is at 2*i, and the corresponding y-coordinate
       * is at 2*i+1.
       * <p>
       * The probability will be returned as a signed probability, where
       * counter-clockwise orientated polygons will have a positive probability
       * and clockwise oriented polygons will have a negative probability.
       * <p>
       * This function is related to the calculation of the area of a polygon.
       * 
       * @param dist the bivariate distribution
       * @param n the number of vertices in the polygon
       * @param v the coordinates of a vertex.
       * @param cdf the cumulative distribution function 
       * @return the signed probability over the polygon.
       * @throws ::std::exception if the polygon is invalid
       */
    template <class CDF>
      double signedPolygonCDF (const BivariateNormal& dist, size_t n, const double* v, const CDF& cdf=CDF()) throws (::std::exception)
      {
	double p=0;
	size_t nPoints=0;
	if (n>2) {
	  const double *const vEnd = v + 2*n;
	  double s2,t2;
	  // all points must be transformed into a standard bivariate normal distribution
	  dist.transform(vEnd[-2],vEnd[-1],s2,t2);
	  while(v!=vEnd) {
	    double s1 = s2;
	    double t1 = t2;
	    s2= *v++;
	    t2= *v++;
	    dist.transform(s2,t2,s2,t2);
	    if (s1!=s2 || t1!=t2) {
	      p += signedTriangleCDF(s1,t1,s2,t2,cdf);
	      ++nPoints;
	    }
	  }
	}
	if (nPoints < 3) {
	  throw ::std::invalid_argument("Invalid polygon specified");
	}
	return p;
      }

      /**
       * Compute the cdf of a standard bivariate normal distribution
       * over a polygon. The coordinates
       * are specified in an array of size 2*n. The x-coordinate
       * of the i'th point is at 2*i, and the corresponding y-coordinate
       * is at 2*i+1.
       * <p>
       * The probability will be returned as a signed probability, where
       * counter-clockwise orientated polygons will have a positive probability
       * and clockwise oriented polygons will have a negative probability.
       * <p>
       * This function is related to the calculation of the area of a polygon.
       * 
       * @param n the number of vertices in the polygon
       * @param v the coordinates of a vertex.
       * @param cdf the cumulative distribution function 
       * @return the signed probability over the polygon.
       * @throws ::std::exception if the polygon is invalid
       */
    template <class CDF>
      double signedPolygonCDF (size_t n, const double* v, const CDF& cdf=CDF()) throws (::std::exception)
      {
	double p=0;
	size_t nPoints=0;
	if (n>2) {
	  const double *const vEnd = v + 2*n;
	  double s2=vEnd[-2];
	  double t2=vEnd[-1];
	  while(v!=vEnd) {
	    double s1 = s2;
	    double t1 = t2;
	    s2= *v++;
	    t2= *v++;
	    if (s1!=s2 || t1!=t2) {
	      p += signedTriangleCDF(s1,t1,s2,t2,cdf);
	      ++nPoints;
	    }
	  }
	}

	if (nPoints < 3) {
	  throw ::std::invalid_argument("Invalid polygon specified");
	}
	return p;
      }


  }
}

#endif
