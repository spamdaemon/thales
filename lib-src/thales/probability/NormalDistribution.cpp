#include <thales/probability/NormalDistribution.h>
#include <cmath>
#include <math.h>

#include <random>

using namespace ::std;

namespace thales {
   namespace probability {
      namespace {
         static const double PI = 3.14159265358979323846;
      }

      NormalDistribution::NormalDistribution() throws()
            : _mean(0), _variance(1)
      {
      }

      NormalDistribution::NormalDistribution(double xmean, double xvar) throws()
            : _mean(xmean), _variance(xvar)
      {
         assert(xvar >= 0);
      }

      double NormalDistribution::pdf(double x) const throws()
      {
         double dx = (x - _mean);
         return exp(-dx * dx / (2 * _variance)) / sqrt(2 * _variance * PI);
      }

      double NormalDistribution::cdf(double x) const throws()
      {
         double denom = sqrt(2 * _variance);
         return 0.5 * (1 + ::erf((x - _mean) / denom));
      }

      double NormalDistribution::cdf(double minX, double maxX) const throws()
      {
         double denom = sqrt(2 * _variance);
         return 0.5 * (::erf((maxX - _mean) / denom) - erf((minX - _mean) / denom));
      }

      double NormalDistribution::drawRandom() const throws()
      {
         static normal_distribution< > dist;
         static mt19937 eng;
         return dist(eng);

      }

   }
}
