#include <thales/probability/BivariateNormal.h>
#include <cassert>
#include <cmath>
#include <stdexcept>
#include <iostream>

#include <math.h>

using namespace ::std;

namespace thales {
  namespace probability {

    inline double sqr(double x) { return x*x; }

    /**
     * Most of G. West/ss attention is focused on the difficulties of numerically 
     * evaluating bivariate and higher variate integrals. There, a big problem is well
     * known to be accuracy near the singular points corr = +-1.
     *
     * For this reason as I mentioned before I think it is a good idea to use different 
     * numerical methods for different values of the correlations. For the bivariate case,
     * I use Owen's method (including Borth's modification), Curnow/Dunnett, and Drezner.
     * The choice of the method for different values of the correlation is taken from the 
     * paper of Sowden and Ashford, and then modified further. A list of references 
     * I use is as follows:
     * 1. Borth, D., A Modification of Owen's Method for Computing the Bivariate Normal Integral. Applied Statistics Vol 22, No 1, 1973. Pp. 82-85.
     * 2. Sowden, R. and Ashford, J., Computation of the Bivariate Normal Integral. Appl. Statistics, Vol 18, 1969, Pp. 169-180.
     * 3. Curnow, R. and Dunnett, C., The Numerical Evaluation of Certain Multivariate Normal Integrals. Ann. Math. Statist., 1962. Pp. 571-579.
     * 4. Owen, D.B., Tables for Computing Bivariate Normal Probabilities. Ann. Math. Statist., Vol 27, 1956. Pp 1075-1090.
     * 5. Drezner, Z., Computation of the Bivariate Normal Integral. Mathematics of Computation, Vol 32, 1978, Pp 277-279.
     */
    
    namespace {
      static const double PI = 3.14159265358979323846;

      /**
       * Compute the cumulative distribution function using the erf
       * function.
       * @param x param the value of x for which to compute the cdf
       * @return the probability of that y < x
       */
      static inline double x_cdf(double x)
      {
	double denom = sqrt(2);
	return 0.5*(1+::erf(x/denom));
      }
      
      /**
       * Compute the cumulative distribution function using the erf
       * function.
       * @param x param the value of x for which to compute the cdf
       * @param m the mean of the distribution
       * @parma var the variance of the distribution
       * @return the probability of that y < x
       */
      static inline double x_cdf(double x, double m, double var)
      {
	double denom = sqrt(2*var);
	return 0.5*(1+::erf((x-m)/denom));
      }
      
      /**
       * Return the value of the probability density function
       * @param x the value at which to evaluate the pdf
       * @return the value of the pdf at x
       */
      static inline double x_pdf(double x)
      {
	return exp(-0.5 * x*x)/sqrt(2*PI);
      }
      
      /**
       * Compute the lower-left tail probabilities of a normal distribution.
       * @param X the upper integration limit x
       * @param Y the upper integration limit for y
       * @param rho the correlation coefficient for x and y
       * @return the probability P(x < x && y < Y)
       */
      static double genz(double X, double Y, double rho)
      {
	static const double x1[] = { -0.9324695142031522,-0.6612093864662647,-0.2386191860831970 };
	static const double W1[] = { 0.1713244923791705,0.3607615730481384, 0.4679139345726904 };

	static const double x2[] = { -0.9815606342467191,-0.9041172563704750,-0.7699026741943050,-0.5873179542866171,-0.3678314989981802,-0.1252334085114692 };
	static const double W2[] = { 0.04717533638651177,0.1069393259953183,0.1600783285433464,0.2031674267230659,0.2334925365383547,0.2491470458134029 };

	static const double x3[] = { -0.9931285991850949,-0.9639719272779138,-0.9122344282513259,-0.8391169718222188,-0.7463319064601508,-0.6360536807265150,-0.5108670019508271,-0.3737060887154196,-0.2277858511416451,-0.07652652113349733 };
	static const double W3[] = { 0.01761400713915212,0.04060142980038694,0.06267204833410906,0.08327674157670475,0.1019301198172404,0.1181945319615184,0.1316886384491766,0.1420961093183821,0.1491729864726037,0.1527533871307259 };
	;
 
	// because Genz computes P(x > X && y > Y) we need to change the variables
	const double h = -X; 
	double k = -Y; 
	const double r= rho;
	
	const double* x;
	const double *W;
  
	size_t nLG;
	// depending on the magnitude of rho, we need to use more or 
	// fewer points to compute an accurate probability
	if (abs(r) < .3) {
	  x = x1;
	  W = W1;
	  nLG = 3;
	}
	else if (abs(r) < .75) {
	  x = x2;
	  W = W2;
	  nLG = 6;
	}
	else {
	  x = x3;
	  W = W3;
	  nLG = 10;
	}

	double hk = h*k;
	double res = 0;
	if (abs(r) < .925) {
	  double hs = 0.5*(h*h + k*k);
	  double asr = 0.5*::std::asin(r);
	  for (size_t i=0;i<nLG;++i) {
	    double sn = sin(asr*(1-x[i]));
	    res += W[i] * exp((sn * hk - hs)/(1-sn*sn));
	    sn = sin(asr*(1+x[i]));
	    res += W[i] * exp((sn * hk - hs)/(1-sn*sn));      
	  }
	  res = res*asr/(2*PI) + x_cdf(-h)*x_cdf(-k); 
	}
	else {
	  if (r < 0) {
	    k = -k;
	    hk = -hk;
	  }
	  if (abs(r) < 1) {
	    double as = (1-r*r);
	    double a= sqrt(as);
	    double bs = (h - k)*(h-k);
	    double c = 0.125 * (4-hk);
	    double d=  0.0625 * (12-hk);
	    double asr = -0.5*(bs/as + hk);
	    if (asr > -100) {
	      res = a*exp(asr)*(1 - c*(bs-as)*(1-d*bs/5)/3 + c*d*as*as/5);
	    }
	    if (-hk < 100) {
	      double b = sqrt(bs);
	      res -= exp(-0.5*hk)*sqrt(2*PI)*x_cdf(-b/a)*b*(1-c*bs*(1-d*bs/5)/3);
	    }
	    a = 0.5*a;
	    for (size_t i=0;i<nLG;++i) {
	
	      {
		double xs = sqr(a * (1 - x[i]));
		double rs = sqrt(1-xs);
		asr = -0.5*(bs/xs + hk);
		if (asr > -100) {
		  res += a*W[i]*exp(asr)*(exp(-hk*(1-rs)/(2*(1+rs)))/rs - ( 1+c*xs*(1+d*xs)));
		}
	      }

	      {
		double xs = sqr(a * (1 + x[i]));
		double rs = sqrt(1-xs);
		asr = -0.5*(bs/xs + hk);
		if (asr > -100) {
		  res += a*W[i]*exp(asr)*(exp(-hk*(1-rs)/(2*(1+rs)))/rs - ( 1+c*xs*(1+d*xs)));
		}
	      }
	    }
	    res /= -2*PI;
	  }
	  if (r>0) {
	    res += x_cdf(-max(h,k));
	  }
	  else if (r<0) {
	    res = max(0.0,x_cdf(-h) - x_cdf(-k)) - res;
	  }

	}
	return res;

      }



      static double bivarcumnorm(double a, double b, double r)
      {
	static const double x[] = { 0.04691008, 0.23076534, 0.5, 0.76923466, 0.95308992 };
	static const double W[] = { 0.018854042, 0.038088059, 0.0452707394, 0.038088059, 0.018854042 };

	double h1=a;
	double h2=b;
	double h12= 0.5*(h1*h1+h2*h2);
	if (abs(r) >= .7) {
	  double r2 = 1-r*r;
	  double r3 = sqrt(r2);
	  if (r<0) {
	    h2 = -h2;
	  }
	  double h3 = h1*h2;
	  double h7 = exp(-0.5*h3);
	  double LH = 0;
	  if (abs(r) < 1) {
	    double h6 = abs(h1-h2);
	    double h5 = 0.5*h6*h6;
	    h6 = h6 / r3;
	    double AA = 0.5 - 0.125*h3;
	    double ab = 3-2*AA*h5;
	    LH = 0.13298076 * h6 * ab * 
	      (1 - x_cdf(h6)) - exp(-h5 / r2) * 
	      (ab + AA * r2) * 0.053051647;
	    for (int i=0;i<5;++i) {
	      double r1=r3*x[i];
	      double rr = r1*r1;
	      r2 = sqrt(1-rr);
	      LH = LH - W[i]*exp(-h5/rr)*(exp(-h3/(1+r2)) / r2/h7 -1 - AA * rr);
	    }
	  }
	  double res = LH * r3*h7 + x_cdf(min(h1,h2));
	  if (r<0) {
	    res = x_cdf(h1) - res;
	  }
	  return res;
	}
	else {
	  double h3 = h1*h2;
	  double LH = 0;
	  if (r != 0.0) {
	    for (int i=0;i<5;++i) {
	      double r1 = r * x[i];
	      double r2 = 1-r1*r1;
	      LH = LH + W[i] * exp((r1*h3 - h12)/r2) / sqrt(r2);
	    }
	  }
	  double res = x_cdf(h1) * x_cdf(h2) + r*LH;
	  return res;
	}
      }

      static inline double x_cdf2 (double h, double k, double p)
      { 
	return genz(h,k,p);
      }

      static inline double x_approximateCDF(double h, double k, double rho)
      {
        if (abs(h) < abs(k)) {
          // increase accuracy by swapping h and k
          swap(h,k);
        }
        double res;
        if (h>0) {
          const double pdf_h = x_pdf(-h);
          const double cdf_h = x_cdf(-h);
          const double m  = pdf_h < 1E-5 ?  0 : (rho*pdf_h / cdf_h); //FIXME: cdf(h) might be close to 0
          const double V = max(0.0,1+rho*h*m - m*m); //max to ensure that the square is positive
          const double B = cdf_h * x_cdf(k,m,V);
          res = x_cdf(k) - B;
        }
        else {
          const double pdf_h = x_pdf(h);
          const double cdf_h = x_cdf(h);
          const double m  = pdf_h < 1E-6 ?  0 : (-rho*pdf_h / cdf_h); //FIXME: cdf(h) might be close to 0
          const double V = max(0.0,1+rho*h*m - m*m); //max to ensure that the square is positive
          const double B = cdf_h * x_cdf(k,m,V);
          res = B;
        }
        return res;
      }

    }


    double BivariateNormal::CDF::operator()(double x, double y, double r) const throws()
    {
      return x_cdf2(x,y,r); 
    }

    double BivariateNormal::ApproximateCDF::operator()(double x, double y, double rho) const throws()
    { return x_approximateCDF(x,y,rho); }

    BivariateNormal::BivariateNormal (double x, double y, double sx, double sy, double rho) throws()
    : _rho(rho),_isStandard(x==0 && y==0 && sx == 1 && sy==1)
    {
      assert(sx>0);
      assert(sy>0);
      assert(_rho >= -1);
      assert(_rho <= 1);
      
      _mean[0] = x;
      _mean[1] = y;
      _stddev[0] = sx;
      _stddev[1] = sy;
    }
      
    BivariateNormal::BivariateNormal (double rho) throws()
    : _rho(rho), _isStandard(true)
    {
      assert(_rho >= -1);
      assert(_rho <= 1);
      _mean[0] = _mean[1] = 0;
      _stddev[0] = _stddev[1] = 1;
    }

    double BivariateNormal::pdf (double x, double y)  const throws()
    { 
      const double tmp = 1-_rho*_rho;
      if (tmp==0) {
	if (_rho<0) {
	  return 0;
	}
	else {
	  return x_pdf(max(x,y));
	}
      }

      const double dx = (x-_mean[0])/_stddev[0];
      const double dy = (y-_mean[1])/_stddev[1];
      return exp(-0.5*(dx*dx + dy*dy - 2*_rho*dx*dy)/tmp)/(2*PI*sqrt(tmp));
    }
    
    double BivariateNormal::cdf (double h, double k) const throws()
    {
      if (!_isStandard) {
	transform(h,k,h,k);
      }
      return x_cdf2(h,k,_rho); 
    }

    double BivariateNormal::approximateCDF(double h, double k) const throws()
    {
      if (!_isStandard) {
	transform(h,k,h,k);
      }
      return  x_approximateCDF(h,k,_rho);
    }


    BivariateNormal BivariateNormal::decomposeGaussian(double x, double y, double xx, double yy, double xy) throws(::std::exception)
    {
      const double mx = sqrt(xx);
      const double my = sqrt(yy);
      const double rho = xy / (mx*my);
      
      return BivariateNormal(x,y,mx,my,rho);
    }

    void BivariateNormal::createCovariance (double& xx, double& yy, double& xy) const throws()
    {
      xy = stddevX()*stddevY()*correlation();
      xx = varianceX();
      yy = varianceY();
   }
    
  }
}
