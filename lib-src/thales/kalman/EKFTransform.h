#ifndef _THALES_KALMAN_EKFTRANSFORM_H
#define _THALES_KALMAN_EKFTRANSFORM_H

#ifndef _THALES_KALMAN_GAUSSIANTRANSFORM_H
#include <thales/kalman/GaussianTransform.h>
#endif

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

#include <typeinfo>

namespace thales {
  namespace kalman {
    class Gaussian;
    class CoordinateFrame;

    /**
     * The EKFTransform is just a tagging interface to indicate
     * that the transform uses linearization around the mean for its transformations.
     */
    class EKFTransform : public GaussianTransform {
          CANOPY_BOILERPLATE_PREVENT_COPYING(EKFTransform);
      
    protected:
      EKFTransform() throws();

      /** Destroy this transform */
    public:
      ~EKFTransform() throws();

    };

  }
}

#endif
