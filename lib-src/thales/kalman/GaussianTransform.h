#ifndef _THALES_KALMAN_GAUSSIANTRANSFORM_H
#define _THALES_KALMAN_GAUSSIANTRANSFORM_H

#ifndef _THALES_KALMAN_TRANSFORMNOTFOUND_H
#include <thales/kalman/TransformNotFound.h>
#endif

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

#include <typeinfo>

namespace thales {
   namespace kalman {
      class Gaussian;
      class CoordinateFrame;

      /**
       * The GaussianTransform is used in the conversion of
       * one Gaussian into another. It essentially provides
       * the <em>H matrix</em> in the Kalman filter equations, i.e.
       * it is the matrix that maps the Kalman filter's state into
       * the space of the measurement.
       */
      class GaussianTransform : public ::timber::Counted
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(GaussianTransform);

         protected:
            inline GaussianTransform() throws()
            {
            }

            /** Destroy this transform */
         public:
            ~GaussianTransform() throws();

            /**
             * Get an identity transform for the specified frame.
             * @param frame  a coordinate frame
             * @return an identity transform for the specified frame
             */
         public:
            static ::timber::Reference< GaussianTransform> identityTransform(const CoordinateFrame& frame) throws();

            /**
             * Register a new transform. The transform will be registered by the address of the two
             * type_info object. It will also be registered by the names of the classes which
             * will be used as a backup, in case the same coordinate class is given separate instances
             * of the same type_info object.
             *
             * @param from the type of the from-domain
             * @param to the type of the to-domain
             * @param t the transform itself
             * @return true if the transform was registered, false otherwise
             */
         public:
            static bool registerTransform(const ::std::type_info& from, const ::std::type_info& to,
                  const ::timber::Reference< GaussianTransform>& t) throws();

            /**
             * Find a transform.
             * @param from a coordinate from which to transform
             * @param to a coordinate frame into which to transform
             */
         public:
            static ::timber::Reference< GaussianTransform> find(const CoordinateFrame& from,
                  const CoordinateFrame& to) throws (TransformNotFound);

            /**
             * Apply this transform to a mean and covariance into the specified frame. If
             * the transform is an identity transform, then this method may
             * return false and <em>not update</em> toMean, toCovariance, H
             * and HP. This can improve performance for identity transformations
             * quite a bit.
             *
             * @param from the original frame
             * @param x the original mean
             * @param P the original covariance
             * @param to the target frame
             * @param toX the new mean in the target frame (output)
             * @param toP the covariance in the target frame (output, nullptr if not desired)
             * @param HP the product of H*P (output, nullptr if not desired)
             * @return true if H is not an identity matrix, false if it is
             */
         public:
            virtual bool apply(const CoordinateFrame& from, const ::newton::Vector< double>& x,
                  const ::newton::Matrix< double>& P, const CoordinateFrame& to, ::newton::Vector< double>& toX,
                  ::newton::Matrix< double>* toP, ::newton::Matrix< double>* HP) const throws (::std::exception) = 0;
      };

   }
}

#endif
