#ifndef _THALES_KALMAN_LIKELIHOOD_H
#define _THALES_KALMAN_LIKELIHOOD_H

#ifndef _THALES_H
#include <thales/thales.h>
#endif

namespace thales {
  namespace kalman {

    /**
     * This class represents the likelihood between a measurement and a filter estimate.
     * Larger values are better. 
     */
    class Likelihood  {

      /**
       * Create a new likelihood from a value in log-space.
       * @param logLikelihood a likelihood in the natural log space
       */
    public:
      explicit inline Likelihood (double logLikelihood) throws (::std::exception)
	: _lambda(logLikelihood) 
      { assert(logLikelihood==logLikelihood); }

      /**
       * Default constructor.
       * This is equivalent to <code>Likelihood(0)</code>.
       */
    public:
      inline Likelihood() throws()
	: _lambda(0) {}

      /**
       * Get the value for this likelihood. Remember that the value is
       * in log-space
       * @return the numeric value for this likelihood
       */
    public:
      inline double value() const throws()
      { return _lambda; }

      /**
       * Compare two likelihoods for equality.
       * @param lambda a likelihood
       * @return true if this likelihood equals lambda
       */
    public:
      inline bool operator==(const Likelihood& lambda) const throws()
      { return _lambda == lambda._lambda; }
      
 
      /**
       * Compare two likelihoods for in-equality.
       * @param lambda a likelihood
       * @return true if this likelihood is less than lambda.
       */
    public:
      inline bool operator<(const Likelihood& lambda) const throws()
      { return _lambda < lambda._lambda; }

      /**
       * Multiply this likelihood by a probability. 
       * @param p a probability (must be between 0 and 1)
       * @return this likelihood
       * @throws ::std::invalid_argument if p==0.0
       */
    public:
      Likelihood& operator*=(double p) throws (::std::exception);

      /**
       * Multiply this likelihood by a probability.
       * @param p a probability (must be between 0 and 1)
       * @return a new likelihood
       */
    public:
      inline Likelihood operator*(double p) const throws (::std::exception)
      { Likelihood q(_lambda); q *= p; return q; }

      /**
       * Increase this likelihood.
       * @param q a likelihood.
       */
    public:
      inline Likelihood& operator+=(const  Likelihood& q) throws()
      { _lambda += q._lambda; return *this; }

      /**
       * Add two likelihoods. 
       * @param q a likelihood.
       */
    public:
      inline Likelihood operator+(const  Likelihood& q) const throws()
      { return Likelihood(_lambda+q._lambda); }

      /** The likelihood in log space*/
    private:
      double _lambda;
    };
      
  }
}
#endif
