#include <thales/kalman/TransformNotFound.h>

using namespace ::timber;

namespace thales {
  namespace kalman {
 
    TransformNotFound::TransformNotFound(const CoordinateFrame& , const CoordinateFrame& )  throws()
    : ::std::runtime_error("Transform not found ")
    {}
  }
}
