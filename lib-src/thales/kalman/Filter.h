#ifndef _THALES_KALMAN_FILTER_H
#define _THALES_KALMAN_FILTER_H

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _THALES_KALMAN_GAUSSIAN_H
#include <thales/kalman/Gaussian.h>
#endif

#ifndef _THALES_KALMAN_TIME_H
#include <thales/kalman/Time.h>
#endif

namespace thales {
  namespace kalman {

    class Prediction;
    class ProcessModel;
    class FilterUpdate;

    /**
     * This is the base class for all kinds of Kalman filters variations,
     * such as IMM and VSMM, etc. 
     */
    class Filter : public ::timber::Counted {
          CANOPY_BOILERPLATE_PREVENT_COPYING(Filter);

      /** The default constructor */
    protected:
      inline Filter() throws() {}
      
      /** The destructor */
    public:
      virtual ~Filter() throws() = 0;


      /**
       * Create a Filter with the specified initial state.
       * @param tm an absolute time
       * @param initial state
       * @param model the model for this filter.
       * @param z a measurement or 0 if not available
       */
    public:
      static ::timber::Reference<Filter> createFilter (const Time& tm,
						       const ::timber::Reference<Gaussian>& initial, 
						       const ::timber::Reference<ProcessModel>& model, 
						       const ::timber::Pointer<Gaussian>& z = ::timber::Pointer<Gaussian>()) throws();
      
      /**
       * Get the current filter time.
       * @return the time of this filter.
       */
    public:
      virtual Time time() const throws() = 0;

      /**
       * Get the current state of this filter.
       * @return a Gaussian probability distribution.
       */
    public:
      virtual ::timber::Reference<Gaussian> state() const throws() = 0;

      /**
       * Predict this filter forward to the specified time.
       * @param absTime a time value
       * @throws ::std::exception if the filter cannot be predicted
       */
    public:
      virtual ::timber::Reference<Prediction> predict(const Time& absTime) const throws (::std::exception) = 0;

      /**
       * Create a new filter that performs an update of this filter
       * with the specified state. This method usually fails, if the the current
       * filter state cannot be mapped into the measurement's space.
       * @param z the measurement 
       * @return a new filter
       * @throws ::std::exception if this filter could not be updated.
       */
    public:
      virtual ::timber::Reference<Filter> update(const ::timber::Reference<Gaussian>& z) const throws (::std::exception) = 0;

      /**
       * Prepare this filter for an update with the specified gaussian. The purpose of this
       * method is to provide a way to efficently query the association likelihood, and also compute
       * the Mahalanobis distance between the measuremetn and this filter's state.
       * @param z a measurement
       * @return an object that prepares the update
       */
    public:
      virtual ::timber::Reference<FilterUpdate> prepareUpdate(const ::timber::Reference<Gaussian>& z) const throws (::std::exception) = 0;

    };
      
  }
}
#endif
