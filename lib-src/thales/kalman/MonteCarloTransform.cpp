#include <thales/kalman/MonteCarloTransform.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/GaussianTransform.h>
#include <thales/kalman/CoordinateFrame.h>

#include <vector>

using namespace ::std;
using namespace ::timber;
using namespace ::newton;

namespace thales {
  namespace kalman {
    namespace {
    
      class Transform : public GaussianTransform {

      public:
	Transform(const Reference<GaussianTransform>& t, size_t nSamples) throws()
	: _transform(t),_nSamples(nSamples)
	{}
	
	~Transform() throws()
	{}
      
	bool apply (const CoordinateFrame& from,
		    const Vector<double>& x, const Matrix<double>& P,
		    const CoordinateFrame& to,
		    Vector<double>& toX, 
		    Matrix<double>* toP,
		    Matrix<double>* HP) const  throws (::std::exception)
	{
	  vector<Vector<double> > samples = Gaussian::randomSamples(x,P,_nSamples);
	  vector<Vector<double> > transformed(samples.size());
	  Vector<double> y;
	  Vector<double> y0;
	  _transform->apply(from,x,P,to,y0,0,0);

	  for (size_t i=0;i<samples.size();++i) {
	    _transform->apply(from,samples[i],P,to,y,0,0);
	    transformed[i].swap(y);
	  }
	
	  // compute the mean
	  y = Vector<double>(to.dimension());
	  for (size_t i=0;i<samples.size();++i) {
	    assert(transformed[i].dimension()==to.dimension());
	    y += to.difference(transformed[i],y0);
	  }
	  y /= samples.size();
	  y = to.difference(y,-y0);

	  if (toP || HP) {
	    // subtract the means form the samples
	    for (size_t i=0;i<samples.size();++i) {
	      transformed[i] = to.difference(transformed[i],y);
	      samples[i] = from.difference(samples[i],x);
	    }
	
	    // compute the variance
	    if (toP) {
	      Matrix<double> newP(to.dimension());
	      for (size_t i=0;i<samples.size();++i) {
		for (size_t j=0;j<to.dimension();++j) {
		  for (size_t k=0;k<=j;++k) {
		    newP(j,k) += transformed[i](j)*transformed[i](k);
		  }
		}
	      }
	      for (size_t j=0;j<to.dimension();++j) {
		for (size_t k=0;k<=j;++k) {
		  newP(j,k) /= samples.size();
		  newP(k,j) = newP(j,k);
		}
	      }
	      toP->swap(newP);
	    }
	
	    if (HP) {
	      Matrix<double> newHP(to.dimension());
	      for (size_t i=0;i<samples.size();++i) {
		for (size_t j=0;j<to.dimension();++j) {
		  for (size_t k=0;k<from.dimension();++k) {
		    newHP(j,k) += transformed[i](j)*samples[i](k);
		  }
		}
	      }
	      for (size_t j=0;j<to.dimension();++j) {
		for (size_t k=0;k<from.dimension();++k) {
		  newHP(j,k) /= samples.size();
		}
	      }
	      HP->swap(newHP);
	    }
	  }
	  toX.swap(y);
	  return true;
	}

	/** The gaussian used for the monte carlo simulation */
      private:
	Reference<GaussianTransform> _transform;
	
	/** The number of samples to generate */
      private:
	const size_t _nSamples;
      };
    }

    Reference<GaussianTransform> MonteCarloTransform::create (const Reference<GaussianTransform>& t,size_t nSamples) throws()
    { return new Transform(t,nSamples); }

    Reference<Gaussian> MonteCarloTransform::transform (const Reference<GaussianTransform>& t,const Reference<Gaussian>& ge, const Reference<CoordinateFrame>& toFrame, size_t nSamples) throws()
    {
      Vector<double> x;
      Matrix<double> P;
      create(t,nSamples)->apply(*ge->frame(),ge->mean(),ge->covariance().matrix(),*toFrame,x,&P,0);

      return Gaussian::create(toFrame,::std::move(x),::std::move(P));
    }

    
  }
}    
