#ifndef _THALES_KALMAN_PREDICTION_H
#define _THALES_KALMAN_PREDICTION_H

#ifndef _THALES_KALMAN_FILTER_H
#include <thales/kalman/Filter.h>
#endif

namespace thales {
  namespace kalman {
 

    /**
     * This is the base class for all kinds of Kalman filters variations,
     * such as IMM and VSMM, etc. 
     */
    class Prediction : public Filter {
          CANOPY_BOILERPLATE_PREVENT_COPYING(Prediction);

      /** The default constructor */
    protected:
      inline Prediction() throws() {}
      
      /** The destructor */
    public:
      virtual ~Prediction() throws() = 0;

      /** 
       * The source of this prediction.
       * @return the filter that was predicted
       */
    public:
      virtual ::timber::Reference<Filter> sourceFilter() const throws() = 0;
    };
    
  }
}
#endif
