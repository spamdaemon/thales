#ifndef _THALES_KALMAN_TRANSFORMNOTFOUND_H
#define _THALES_KALMAN_TRANSFORMNOTFOUND_H

#include <exception>

#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#include <thales/kalman/CoordinateFrame.h>
#endif


namespace thales {
  namespace kalman {
 

    /**
     * This exception is thrown when a transform from one
     * coordinate frame to another cannot be found.
     */
    class TransformNotFound : public ::std::runtime_error {

      /**
       * Create a new exception.
       * @param from the coordinate frame from which the transformation was attempted
       * @param to the destination frame
       */
    public:
      TransformNotFound(const CoordinateFrame& from, const CoordinateFrame& to)  throws();

      /** Destructor */
    public:
      ~TransformNotFound() throw() {}

    };
  }
}


#endif
