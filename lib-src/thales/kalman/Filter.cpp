#include <thales/kalman/Filter.h>
#include <thales/kalman/kalman.h>
#include <thales/kalman/ProcessModel.h>
#include <thales/kalman/Prediction.h>
#include <thales/kalman/FilterUpdate.h>
#include <cassert>

using namespace ::std;
using namespace ::timber;

namespace thales {
  namespace kalman {

    namespace {
      
      class DefaultFilterUpdate : public FilterUpdate 
      {
      public:
	DefaultFilterUpdate (const Reference<Filter>& source, const Reference<Gaussian>& z) throws (::std::exception)
	: _pre(source), _measurement(z), _post(source->update(z))
	{
	  Reference<GaussianTransform> tx = GaussianTransform::find(*source->state()->frame(),*z->frame());
	  _lambda = source->state()->likelihood(z,*tx);
	  _distance2 = source->state()->mahalanobisDistance2(z,*tx);
	}
	
      public:
	~DefaultFilterUpdate() throws() 
	{}

	::timber::Reference<Filter> preUpdateFilter() const throws() { return _pre; }

      /**
       * Get the measurement.
       * @return the measurement with which the preUpdateFilter() is updated.
       */
    public:
        ::timber::Reference<Gaussian> measurement() const throws() { return _measurement; }

      /**
       * Get the post-update filter that results from this update.
       * @return the filter that results from an update
       */
    public:
	::timber::Reference<Filter> postUpdateFilter() const throws() {return _post; }

      /**
       * Compute the square of the Mahalanobis distance between this update and its measurement.
       * @return the square of the Mahalanobis distance between the measurement and original filter.
       */
    public:
	double mahalanobisDistance2() const throws() { return _distance2; }

      /**
       * Get the likelihood that measurements is associated with the original filter.
       * @return the likelihood
       */
    public:
	Likelihood likelihood() const throws() { return _lambda; }
	
      private:
	Reference<Filter> _pre;
	Reference<Gaussian> _measurement;
	Reference<Filter> _post;
	Likelihood _lambda;
	double _distance2;
      };

      class DefaultPrediction : public Prediction {
	
      public:
	DefaultPrediction(const Time& absTime, 
			  const Reference<ProcessModel>& pmodel,
			  const Reference<Filter>& filter) throws()
	:  _time(absTime),
	  _state(pmodel->predict(filter->state(),(absTime-filter->time()))),
	  _model(pmodel),
	  _source(filter)
	{
	}
	
      public:
	~DefaultPrediction() throws()
	{}
	
	Reference<Filter> sourceFilter() const throws()
	{ return _source; }
	
	Reference<Prediction> predict(const Time& absTime) const throws (::std::exception)
	{
	  return new DefaultPrediction(absTime,_model,_source);
	}
	
	Reference<Filter> update(const Reference<Gaussian>& z) const throws(::std::exception)
	{ 
	  Reference<GaussianTransform> tx = GaussianTransform::find(*_state->frame(),*z->frame());
	  return Filter::createFilter(_time,_state->doKalmanUpdate(z,*tx),_model,z); 
	}

	Reference<FilterUpdate> prepareUpdate(const Reference<Gaussian>& z) const throws(::std::exception)
	{
	  return new DefaultFilterUpdate(const_cast<DefaultPrediction*>(this),z);
	}

	Time time() const throws() 
	{ return _time; }
    
	Reference<Gaussian> state() const throws()
	{ return _state; }

 	/** The time */
      private:
	Time _time;

	/** The initial state */
      private:
	Reference<Gaussian> _state;

	/** The model */
      private:
	Reference<ProcessModel> _model;
	
	/** The optional measurment */
      private:
	Pointer<Gaussian> _measurement;
 
	/** The filter */
      private:
	Reference<Filter> _source;

    };


      class DefaultFilter : public Filter {

	/** Create a default filter */
      public:
	DefaultFilter(const Time& absTime, 
		      const Reference<Gaussian>& initial, 
		      const Reference<ProcessModel>& pmodel, 
		      const Pointer<Gaussian>& z) throws()
	: _time(absTime),
	  _state(initial),
	  _model(pmodel),
	  _measurement(z)
	{}

	~DefaultFilter() throws()
	{}

	Time time() const throws() 
	{ return _time; }
    
	Reference<Gaussian> state() const throws()
	{ return _state; }

	Reference<Prediction> predict(const Time& dt) const throws(::std::exception)
	{
	  assert(refCount()>0);
	  return new DefaultPrediction(dt,_model,const_cast<DefaultFilter*>(this)); 
	}

	Reference<Filter> update(const Reference<Gaussian>& z) const throws(::std::exception)
	{
	  Reference<GaussianTransform> tx = GaussianTransform::find(*_state->frame(),*z->frame());
	  return Filter::createFilter(_time,_state->doKalmanUpdate(z,*tx),_model,z); 
	}

	Reference<FilterUpdate> prepareUpdate(const Reference<Gaussian>& z) const throws(::std::exception)
	{
	  return new DefaultFilterUpdate(const_cast<DefaultFilter*>(this),z);
	}


	/** The time */
      private:
	Time _time;

	/** The initial state */
      private:
	Reference<Gaussian> _state;

	/** The model */
      private:
	Reference<ProcessModel> _model;
	
	/** The optional measurment */
      private:
	Pointer<Gaussian> _measurement;
      };

    }

    Filter::~Filter() throws()
    {}

    Reference<Filter> Filter::createFilter (const Time& tm, 
					    const Reference<Gaussian>& initial, 
					    const Reference<ProcessModel>& model, 
					    const Pointer<Gaussian>& z) throws ()
    {
      return new DefaultFilter(tm,initial,model,z);
    }

  }
}
