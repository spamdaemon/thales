#include <thales/kalman/Covariance.h>
#include <timber/logging.h>
#include <iostream>

namespace thales {
   namespace kalman {
      namespace {
         static void makeSymmetricWithDelta(::newton::Matrix< double>& m,
               double eps = 1.0 / 1024) throws (::std::exception)
         {
            if (!m.isSquare()) {
               throw ::std::invalid_argument("Not a square matrix");
            }

            for (size_t i = 0; i < m.rowSize(); ++i) {
               for (size_t j = 0; j < i; ++j) {
                  if (m(i, j) != m(j, i)) {
                     if (::std::abs(m(i, j) - m(j, i)) / ::std::max(::std::abs(m(i, j)), ::std::abs(m(j, i))) > eps) {
                        ::timber::logging::LogEntry("thales.kalman").warn() << "Matrix not symmetric:" << m
                              << ::timber::logging::doLog;
                        throw ::std::invalid_argument("Not a symmetric matrix");
                     }
                     else {
                        // force symmetry
                        double avg = 0.5 * (m(i, j) + m(j, i));
                        m(i, j) = m(j, i) = avg;
                     }
                  }
               }
            }

            // check for a negative determinant. we'll allow a degenerate matrix
            // because it can be convenient to do so; note: if the determinant is
            // 0, the the matrix is not positive-definite!
            if (m.determinant() < -1E-4) { // need to take error into account
               ::timber::logging::LogEntry("thales.kalman").warn() << "Matrix not positive-definite: " << m
                     << ::timber::logging::doLog;
               throw ::std::invalid_argument("Not a positive-definite matrix");
            }
         }
      }

      Covariance::Covariance(size_t dim) throws(::std::exception)
            : _matrix(::newton::Matrix< double>::identity(dim))
      {
      }

       Covariance::Covariance(::newton::Matrix< double> cvar) throws(::std::exception)
            : _matrix(::std::move(cvar))
      {
         makeSymmetricWithDelta(_matrix);
      }

      Covariance Covariance::createCovariance2D(::newton::Matrix< double>&& matrix) throws (::std::exception)
      {
         if (matrix.rowSize() != matrix.colSize() && matrix.rowSize() != 2) {
            throw ::std::invalid_argument("Not a square matrix of dimension 2");
         }
         return Covariance(::std::move(matrix));
      }

      Covariance Covariance::createCovariance2D(double xx, double yy, double xy) throws (::std::exception)
      {
         if (xx * yy - xy * xy < 0) {
            throw ::std::invalid_argument("Invalid covariance (determinant < 0)");
         }

         Covariance cvar(2);
         cvar._matrix(0, 0) = xx;
         cvar._matrix(1, 1) = yy;
         cvar._matrix(0, 1) = cvar._matrix(1, 0) = xy;
         return ::std::move(cvar);
      }

      Covariance::~Covariance() throws()
      {
      }

      bool Covariance::decompose(double xx, double yy, double xy, double& mx, double& mn, double& angle) throws()
      {
         const double det = xx * yy - xy * xy;

         if (det <= 0.0) {
            return false;
         }

         double theta = ::std::atan2((2.0 * xy), (xx - yy)) / 2.0;
         double cosTheta = ::std::cos(theta);
         double sinTheta = ::std::sin(theta);
         double cos2Theta = cosTheta * cosTheta;
         double sin2Theta = sinTheta * sinTheta;
         double sincosTheta = 2 * sinTheta * cosTheta;

         // compute the major and minor axes
         double p1 = det / (yy * cos2Theta - xy * sincosTheta + xx * sin2Theta);
         double p2 = det / (yy * sin2Theta + xy * sincosTheta + xx * cos2Theta);

         if (p1 < 0 || p2 < 0) {
            return false;
         }

         mx = ::std::sqrt(p1);
         mn = ::std::sqrt(p2);
         angle = theta;
         return true;
      }

      void Covariance::recompose(double maj, double min, double theta, double& xx, double& yy, double& xy) throws()
      {
         const double cosTheta = ::std::cos(theta);
         const double sinTheta = ::std::sin(theta);

         // rotate the the points (maj,0) and (0,min) by theta
         const double a00 = cosTheta * maj;
         const double a10 = sinTheta * maj;
         const double a01 = -sinTheta * min;
         const double a11 = cosTheta * min;

         // multiple A by transpose(A)
         xx = a00 * a00 + a01 * a01;
         yy = a10 * a10 + a11 * a11;
         xy = a00 * a10 + a11 * a01;
      }

      Covariance Covariance::recompose(double maj, double min, double theta) throws()
      {
         Covariance res(2);
         recompose(maj, min, theta, res._matrix(0, 0), res._matrix(1, 1), res._matrix(0, 1));
         res._matrix(1, 0) = res._matrix(0, 1);
         return res;
      }

      ::newton::Matrix< double>& Covariance::makeSymmetric(::newton::Matrix< double>& P) throws()
      {
         size_t n = P.rowSize();
         assert(n == P.colSize());
         for (size_t i = 1; i < n; ++i) {
            for (size_t j = 0; j < i; ++j) {
               double tmp = 0.5 * (P(i, j) + P(j, i));
               P(i, j) = P(j, i) = tmp;
            }
         }
         return P;
      }

   }
}

::std::ostream& operator<<(::std::ostream& out, const ::thales::kalman::Covariance& cvar)
{
   const ::newton::Matrix< double>& m = cvar.matrix();
   out << "Determinant : " << m.determinant() << ::std::endl << m;

   return out;
}
