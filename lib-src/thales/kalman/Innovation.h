#ifndef _THALES_KALMAN_INNOVATION_H
#define _THALES_KALMAN_INNOVATION_H

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

#ifndef _NEWTON_CHOLESKY_H
#include <newton/Cholesky.h>
#endif

#ifndef _THALES_KALMAN_COVARIANCE_H
#include <thales/kalman/Covariance.h>
#endif

#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#include <thales/kalman/CoordinateFrame.h>
#endif

#ifndef _THALES_KALMAN_LIKELIHOOD_H
#include <thales/kalman/Likelihood.h>
#endif

#ifndef _THALES_KALMAN_GAUSSIANTRANSFORM_H
#include <thales/kalman/GaussianTransform.h>
#endif

#include <iosfwd>

namespace thales {
  namespace kalman {

    /**
     * The Innovation is a quantity determined by two Gaussians. The innovation
     * is computed by differencing the means of the two Gaussians and summing the
     * covariances:
     * <code>
     *   Matrix<double> S = (H*covariance()*Ht + z.covariance())
     *   Vector<double> dz = z.mean() - H*mean()
     * </code>
     */
    class Innovation {
          CANOPY_BOILERPLATE_PREVENT_COPYING(Innovation);

      /** 
       * A constructor.
       * @param zFrame the coordiate frame of the measurement space
       */
    private:
      Innovation (const ::timber::Reference<CoordinateFrame>& zFrame) throws();

      /** 
       * Create a new innovation.
       * @param z a Gaussian determining the target coordinate frame
       * @param Hp the transform that maps the predicted state into the coordinate frame of z.
       * @param p a Gaussian that is normally a predicted state
       * @throws ::std::exception if the gaussian could not be created
       * @throws ::std::invalid_argument if mean.dimension()!=covar.dimension()
       */
    public:
      Innovation(const Gaussian& z, const GaussianTransform& Hp, const Gaussian& p) throws (::std::exception);

      /** The destructor */
    public:
      ~Innovation() throws();
      
      /**
       * Create an innovation conditionally. If the mahalanobis distance of 
       * between the measurement z and the estimate state p is too large,
       * then a null-pointer is returned.
       * @param z a Gaussian determining the target coordinate frame
       * @param Hp the transform that maps the predicted state into the coordinate frame of z.
       * @param p a Gaussian that is normally a predicted state
       * @param maxDistance the maximum mahalanobis distance between a and p
       * @return a pointer to an innovation
       */
    public:
      static ::std::unique_ptr<Innovation> create(const Gaussian& z, 
						const GaussianTransform& Hp, 
						const Gaussian& p,
						double maxDistance) throws (::std::exception);
      /**
       * Get the dimension of the space of this gaussian
       * @return the dimension of the space for this gaussian
       */
    public:
      inline size_t dimension() const throws()
      { return _mean.dimension(); }

      /**
       * Get the state vector.
       * @return mean for this distribution
       */
    public:
      const ::newton::Vector<double>& mean() const throws()
      { return _mean; }

      /**
       * Get the i'th value of the mean.
       * This is just syntactic sugar for mean()(i)
       * @param i an index
       * @return the i'th mean value
       */
    public:
      inline double operator()(size_t i) const throws()
      { return _mean(i); }

      /**
       * Get the matrix HP, where P is the original predicted
       * matrix and H is the matrix that maps a covariance in P
       * into another frame.
       */
    public:
      inline const ::newton::Matrix<double>& HP () const throws()
      { return _HP; }

      /**
       * Get the covariance of for this innovation. This
       * method will use compute the covariance as 
       * <code>cholesky().originalMatrix()</code>.
       * @return the covariance matrix
       */
    public:
      Covariance covariance() const throws();
      
      /**
       * Get the cholesky decomposition of the original innovation matrix.
       * Operations may be more efficient if executed with the cholesky.
       * @return the cholesky decomposition.
       */
    public:
      const ::newton::Cholesky<double> cholesky() const throws()
      { return _cholesky; }
      
      /**
       * Get the coordinate frame.
       * @return the coordinate frame
       */
    public:
      inline const ::timber::Reference<CoordinateFrame>& frame() const throws()
      { return _frame; }

      /**
       * The Mahalanobis distances is simply a function of this innovation.
       * @return the square of the Mahalanobis distance
       */
    public:
      inline double mahalanobisDistance2() const throws()
      { return _mahalanobisDistance2; }
      
      /**
       * The likelihood of this innovation can Get the likelihood that measurements is associated with the original filter. The 
       * likelihood is obtained by computing the following function:
       * <pre>
       * <code>
       *   Matrix<double> S = (H*covariance()*Ht + z.covariance())
       *   double lambda = -0.5*(mahalanobisDistance2(*this,z,H) + log_e((2*PI)^z->dimension * S.determinant())
       * </code>
       * </pre>
       * Basically, we're computing the negative log-likelihood function. Most of the time, the standard likelihood
       * calculation will result in negative numbers in log-space.
       * A description of a multi-variate Gaussian distribution can be found <a href="http://en.wikipedia.org/wiki/Multivariate_normal_distribution">here</a>.
       * @return likelihood of this innovation
       * @note The resulting likelihood values are usually negative in the log-likelihood calculation! 
       */
    public:
      inline Likelihood likelihood() const throws()
      { return _likelihood; }

      /** The cholesky decomposition of the innovation */
    private:
      ::newton::Cholesky<double> _cholesky;

      /** The mean vector */
    private:
      ::newton::Vector<double> _mean;

      /** The (HP).transpose() matrix */
    private:
      ::newton::Matrix<double> _HP;

      /** The coordinate frame */
    private:
      ::timber::Reference<CoordinateFrame> _frame;

      /** The mahalanobis distance */
    private:
      double _mahalanobisDistance2;

      /** The determinant of the cholesky */
    private:
      double _determinant;

      /** The likelihood */
    private:
      Likelihood _likelihood;
    };
  }
}
#endif
