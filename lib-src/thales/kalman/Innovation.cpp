#include <thales/kalman/Innovation.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/GaussianTransform.h>
#include <newton/Matrix.h>
#include <cmath>

#ifndef THALES_USE_ALLOCA
#define THALES_USE_ALLOCA 0
#endif

#if THALES_USE_ALLOCA == 1
#include <string.h>
#include <alloca.h>
#endif

using namespace ::std;
using namespace ::timber;
using namespace ::newton;


namespace thales {
  namespace kalman {
    namespace {
      static const double PI = 3.14159265358979323846;

      static bool doCholesky (const Matrix<double>& A, const Vector<double>& dx, double maxDist,
			      double& outDeterminant, double &outMHD2, Cholesky<double>& outCholesky)
      {
	assert(A.isSquare());
	assert(A(0,0)>0);

	double testMHD = maxDist*maxDist;
	const double L00 = A(0,0);

	double mhd2 = (dx(0)*dx(0))/L00;
	// compute A(0,0) and if we immeditately reject it, then we don't even bother
	// initializing the Cholesky matrix
	if (mhd2 > testMHD) {
	  return false;
	}

	const size_t nr = A.rowSize();
	const size_t nc = nr;

	// initialize the cholesky
	Matrix<double> L(nr,nc);

	// we're going to use the first row of the cholesky 
	// as a scratch pad for the y vector!!! Before returning the cholesky
	// matrix, we'll just have to update it
	double det = 1;
	double dist2  = 0;

	for (size_t k=0;k<nc;++k) {
	  // compute the diagonal entry
	  double y;
	  {
	    double  tmp = A(k,k);
	    double x = dx(k);

	    for (size_t j=0;j<k;++j) {
	      const double& l_kj = L(k,j);
	      tmp -= l_kj * l_kj;
	      x -= l_kj * L(0,j); // L(0,j) is y(j)
	    }
	    if (tmp <= 0.0) {
	      throw ::std::logic_error("Matrix is not positive-definite");
	    }

	    // update the mahalanobis distance and 
	    // test it
	    dist2 += (x*x)/tmp;

	    if (dist2>testMHD) {
	      return false;
	    }
	    tmp = ::std::sqrt(tmp);
	    L(k,k) = tmp;
	    y = x/tmp;
	    
	    det *= tmp;
	  }
	  
	  // compute the lower-diagonal entries
	  for (size_t i=k+1;i<nr;++i) {
	    double tmp = A(i,k);
	    assert(A(k,i)==tmp && "Matrix is not symmetric");
	    for (size_t j=0;j<k;++j) {
	      tmp -= L(i,j)*L(k,j);
	    }
	    tmp /= L(k,k);
	    L(i,k) = tmp;
	  }
	  L(0,k) = y; // here we use the scratch space
	}

	// fix the upper row of the matrix L which was used as scratch space
	L(0,0) = sqrt(L00);
	for (size_t j=1;j<nc;++j) {
	  L(0,j) = 0;
	}

	// generate the return parameters
	outCholesky.swap(L);
	outMHD2 = dist2;
	outDeterminant = det;
	return true;
      }
    }
    
    Innovation::Innovation(const Reference<CoordinateFrame>& zFrame) throws()
    : _frame(zFrame)
    {}

    unique_ptr<Innovation> Innovation::create(const Gaussian& z, 
					    const GaussianTransform& Hp, 
					    const Gaussian& p,
					    double maxDistance) throws (::std::exception)
    {
      Vector<double> Hx;
      Matrix<double> HPHt,HP;
      Cholesky<double> L;
      double det;
      double mhd2;
      
      unique_ptr<Innovation> res;

      const Matrix<double>& R = z.covariance().matrix();
      if (Hp.apply(*p.frame(),p.mean(),p.covariance().matrix(),
		    *z.frame(),Hx,&HPHt,&HP)) {
	z.frame()->difference(z.mean(),Hx).swap(Hx);
	HPHt += R;

	// do the Cholesky conditionally
	if (!doCholesky(HPHt,Hx,maxDistance,det,mhd2,L)) {
	  return res;
	}
      }
      else {
	// HP is obviously just the covariance matrix
	// HPHt is the same as HP, but below we're using HP
	// so ignore HPHt
	HP = p.covariance().matrix();
	HP += R;
	z.frame()->difference(z.mean(),p.mean()).swap(Hx);

	// do the Cholesky conditionally
	if (!doCholesky(HP,Hx,maxDistance,det,mhd2,L)) {
	  return res;
	}
      }

      res.reset(new Innovation(z.frame()));
      res->_cholesky.swap(L);
      res->_mean.swap(Hx);
      res->_HP.swap(HP);
      res->_mahalanobisDistance2 = mhd2;
      res->_determinant = det;
      res->_likelihood = Likelihood(-0.5*(mhd2 + static_cast<double>(res->_mean.dimension()) * log(2*PI)) - log(det));
      
      return res;
    }


   Innovation::Innovation(const Gaussian& z, const GaussianTransform& HF, const Gaussian& p) throws(exception)
    : _mean(z.dimension()), _frame(z.frame())
    {
      Vector<double> Hx;
      Matrix<double> HPHt;
      const Matrix<double>& R = z.covariance().matrix();
      if (HF.apply(*p.frame(),p.mean(),p.covariance().matrix(),
		   *z.frame(),Hx,&HPHt,&_HP)) {
	
	_frame->difference(z.mean(),Hx).swap(_mean);
	HPHt += R;
	
	::newton::Cholesky<double>(HPHt).swap(_cholesky);
      }
      else {
	// an identity transform is being used
	_frame->difference(z.mean(),p.mean()).swap(_mean);

	// HP is obviously just the covariance matrix
	_HP = p.covariance().matrix();
	
	::newton::Cholesky<double>(p.covariance().matrix()+R).swap(_cholesky);
      }
      _determinant = _cholesky.determinant();
      assert(_cholesky.determinant()>0 && "Matrix was not positive definite");

      {
#if THALES_USE_ALLOCA == 1
	size_t allocaSize = _mean.dimension()*sizeof(double);
	double* dataBuffer = reinterpret_cast<double*>(alloca(allocaSize));
	memset(dataBuffer,0,allocaSize);
	Vector<double> tmp = Vector<double>::createWithBuffer(_mean.dimension(),dataBuffer);
#else
	Vector<double> tmp(_mean.dimension());
#endif
	// do the forward substitution on the cholesky, which is equivalent to 
	// computing _cholesky.matrix().inverse()*_mean
	_cholesky.doForwardSubstitution(_mean,tmp);
	_mahalanobisDistance2 = tmp*tmp;
      }

      _likelihood = Likelihood(-0.5*(_mahalanobisDistance2 + static_cast<double>(_mean.dimension()) * log(2*PI)) - log(_determinant));
    }
    
    Innovation::~Innovation() throws()
    {}


    Covariance Innovation::covariance() const throws()
    { return _cholesky.originalMatrix(); }
   
  }
}
