#include <thales/kalman/GaussianTransform.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/frame/Polar_2D_P.h>
#include <thales/kalman/frame/Polar_2D_PV.h>
#include <thales/kalman/frame/Rdot_2D.h>
#include <thales/kalman/frame/RRdot_2D.h>
#include <thales/kalman/frame/Cartesian_2D_Relative_PRdot.h>
#include <thales/kalman/frame/ProjectivePlane3D_P.h>
#include <thales/kalman/frame/Spherical_P.h>

#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Rdot_2D.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__RRdot_2D.h>

#include <thales/kalman/transform/Cartesian_3D_Inertial_PV__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/transform/Cartesian_3D_Inertial_PV__Cartesian_3D_Inertial_P.h>
#include <thales/kalman/transform/Cartesian_3D_Inertial_P__Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/transform/Cartesian_3D_Inertial_PV__Spherical_P.h>
#include <thales/kalman/transform/Cartesian_3D_Inertial_P__Spherical_P.h>
#include <thales/kalman/transform/Cartesian_3D_Inertial_P__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/transform/Spherical_P__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/transform/Spherical_P__Cartesian_3D_Inertial_P.h>
#include <thales/kalman/transform/Spherical_P__Cartesian_3D_Inertial_PV.h>

#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__ProjectivePlane3D_P.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Cartesian_2D_Relative_PRdot.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Polar_2D_PV.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Polar_2D_P.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_P__Cartesian_2D_Relative_PRdot.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_P__ProjectivePlane3D_P.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_P__Polar_2D_P.h>

#include <thales/kalman/transform/ProjectivePlane3D_P__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_P__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/transform/Polar_2D_PV__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/transform/Polar_2D_P__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/transform/Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/transform/ProjectivePlane3D_P__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/transform/Polar_2D_P__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/transform/Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_P.h>

#include <thales/kalman/Gaussian.h>
#include <timber/logging.h>
#include <canopy/mt/MutexGuard.h>
#include <canopy/mt/Mutex.h>

#include <map>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;
using namespace ::newton;

namespace thales {
   namespace kalman {
      namespace {

         struct IdentityTransform : public GaussianTransform
         {
               ~IdentityTransform() throws()
               {
               }

               Pointer< Gaussian> inverseTransform(const Reference< Gaussian>& ge,
                     const Reference< CoordinateFrame>&) const throws()
               {
                  return ge;
               }

               bool apply(const CoordinateFrame&, const ::newton::Vector< double>&, const ::newton::Matrix< double>&,
                     const CoordinateFrame&, ::newton::Vector< double>&, ::newton::Matrix< double>*,
                     ::newton::Matrix< double>*) const throws (::std::exception)
               {
                  return false; // the identity transform will not perform any updates at all
               }
         };

         typedef map< pair< const type_info*, const type_info*>, Reference< GaussianTransform> > TransformRegistry;

         struct StaticTransformRegistry : public TransformRegistry
         {
               StaticTransformRegistry()
                     : _identity(new IdentityTransform()), _logger("thales.kalman.GaussianTransform")
               {
                  registerTransform(typeid(Cartesian_2D_Inertial_PV), typeid(Cartesian_2D_Inertial_PV), _identity);

                  registerTransform(typeid(Cartesian_3D_Inertial_PV), typeid(Cartesian_3D_Inertial_PV), _identity);

                  registerTransform(typeid(Cartesian_3D_Inertial_P), typeid(Cartesian_3D_Inertial_P), _identity);

                  registerTransform(typeid(Cartesian_2D_Inertial_P), typeid(Cartesian_2D_Inertial_P), _identity);

                  registerTransform(typeid(Cartesian_3D_Inertial_PV), typeid(Cartesian_3D_Inertial_P),
                        Cartesian_3D_Inertial_PV__Cartesian_3D_Inertial_P::create());

                  registerTransform(typeid(Cartesian_3D_Inertial_PV), typeid(Cartesian_2D_Inertial_P),
                        Cartesian_3D_Inertial_PV__Cartesian_2D_Inertial_P::create());

                  registerTransform(typeid(Cartesian_3D_Inertial_P), typeid(Cartesian_3D_Inertial_PV),
                        Cartesian_3D_Inertial_P__Cartesian_3D_Inertial_PV::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_PV), typeid(Cartesian_2D_Inertial_P),
                        Cartesian_2D_Inertial_PV__Cartesian_2D_Inertial_P::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_P), typeid(Cartesian_2D_Inertial_PV),
                        Cartesian_2D_Inertial_P__Cartesian_2D_Inertial_PV::create());

                  registerTransform(typeid(Cartesian_3D_Inertial_P), typeid(Cartesian_2D_Inertial_P),
                        Cartesian_3D_Inertial_P__Cartesian_2D_Inertial_P::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_PV), typeid(Polar_2D_PV),
                        Cartesian_2D_Inertial_PV__Polar_2D_PV::create());

                  registerTransform(typeid(Polar_2D_PV), typeid(Cartesian_2D_Inertial_PV),
                        Polar_2D_PV__Cartesian_2D_Inertial_PV::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_PV), typeid(Polar_2D_P),
                        Cartesian_2D_Inertial_PV__Polar_2D_P::create());

                  registerTransform(typeid(Polar_2D_P), typeid(Cartesian_2D_Inertial_PV),
                        Polar_2D_P__Cartesian_2D_Inertial_PV::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_PV), typeid(Rdot_2D),
                        Cartesian_2D_Inertial_PV__Rdot_2D::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_PV), typeid(RRdot_2D),
                        Cartesian_2D_Inertial_PV__RRdot_2D::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_P), typeid(Polar_2D_P),
                        Cartesian_2D_Inertial_P__Polar_2D_P::create());

                  registerTransform(typeid(Polar_2D_P), typeid(Cartesian_2D_Inertial_P),
                        Polar_2D_P__Cartesian_2D_Inertial_P::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_PV), typeid(Cartesian_2D_Relative_PRdot),
                        Cartesian_2D_Inertial_PV__Cartesian_2D_Relative_PRdot::create());

                  registerTransform(typeid(Cartesian_2D_Relative_PRdot), typeid(Cartesian_2D_Inertial_PV),
                        Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV::create());

                  registerTransform(typeid(Cartesian_2D_Relative_PRdot), typeid(Cartesian_2D_Inertial_P),
                        Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_P::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_P), typeid(Cartesian_2D_Relative_PRdot),
                        Cartesian_2D_Inertial_P__Cartesian_2D_Relative_PRdot::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_PV), typeid(ProjectivePlane3D_P),
                        Cartesian_2D_Inertial_PV__ProjectivePlane3D_P::create());

                  registerTransform(typeid(ProjectivePlane3D_P), typeid(Cartesian_2D_Inertial_PV),
                        ProjectivePlane3D_P__Cartesian_2D_Inertial_PV::create());

                  registerTransform(typeid(Cartesian_2D_Inertial_P), typeid(ProjectivePlane3D_P),
                        Cartesian_2D_Inertial_P__ProjectivePlane3D_P::create());

                  registerTransform(typeid(ProjectivePlane3D_P), typeid(Cartesian_2D_Inertial_P),
                        ProjectivePlane3D_P__Cartesian_2D_Inertial_P::create());

                  registerTransform(typeid(ProjectivePlane3D_P), typeid(Cartesian_2D_Inertial_P),
                        ProjectivePlane3D_P__Cartesian_2D_Inertial_P::create());

                  registerTransform(typeid(Cartesian_3D_Inertial_P), typeid(Spherical_P),
                        Cartesian_3D_Inertial_P__Spherical_P::create());

                  registerTransform(typeid(Cartesian_3D_Inertial_PV), typeid(Spherical_P),
                        Cartesian_3D_Inertial_PV__Spherical_P::create());

                  registerTransform(typeid(Spherical_P), typeid(Cartesian_2D_Inertial_P),
                        Spherical_P__Cartesian_2D_Inertial_P::create());
                  registerTransform(typeid(Spherical_P), typeid(Cartesian_3D_Inertial_P),
                        Spherical_P__Cartesian_3D_Inertial_P::create());

                  registerTransform(typeid(Spherical_P), typeid(Cartesian_3D_Inertial_PV),
                        Spherical_P__Cartesian_3D_Inertial_PV::create());

               }

               bool registerTransform(const type_info& from, const type_info& to,
                     const Reference< GaussianTransform>& t) throws()
               {
                  typedef map< pair< string, string>, Reference< GaussianTransform> > XForms;

                  const string first(from.name());
                  const string second(to.name());

                  ::canopy::mt::MutexGuard< > GUARD(_mutex);

                  // insert by typeinfo
                  insert(make_pair(make_pair(&from, &to), t));

                  pair< XForms::iterator, bool> istat = _transformsByString.insert(
                        make_pair(make_pair(first, second), t));
                  if (!istat.second) {
                     // not inserted, replace all instances of those with the new one
                     istat.first->second = t; // replace it

                     // replace any previously set transforms
                     for (iterator i = begin(); i != end(); ++i) {
                        if (first == i->first.first->name() && second == i->first.second->name()) {
                           i->second = t;
                        }
                     }

                     LogEntry("thales.kalman.GaussianTransform").debugging()
                           << "Replaced a previously registered transform " << typeid(*t).name() << " from " << first
                           << " to " << second << doLog;

                  }
                  else {
                     LogEntry("thales.kalman.GaussianTransform").debugging() << "Registered a new transform "
                           << typeid(*t).name() << " from " << first << " to " << second << doLog;
                  }
                  return true;
               }

               Pointer< GaussianTransform> findTransform(const CoordinateFrame& from,
                     const CoordinateFrame& to) throws (exception)
               {
                  // we'll always return the identity transform for the same instances
                  if (&from == &to) {
                     return _identity;
                  }

                  const type_info& fromInfo = typeid(from);
                  const type_info& toInfo = typeid(to);

                  // a protected section
                  {
                     // do this before the mutex lock to reduce the amount of work
                     // being performed while the mutex is locked
                     const TransformRegistry::key_type key(&fromInfo, &toInfo);

                     ::canopy::mt::MutexGuard< > GUARD(_mutex); // should lock for concurrent read
                     const TransformRegistry::const_iterator i = find(key);
                     if (i != end()) {
                        return i->second;
                     }
                  }

                  // if we did not find a transform then check if we can use an identity transform
                  // this will be done with a mutex having been locked
                  if (from.equals(to)) {
                     // only use an identity transform if the two coordinate frames are the same
                     // for instance, we cannot transform a polar coordinate frame into another
                     // polar coordinate frame via an identity transform, if the two centers are different.
                     return _identity;
                  }

                  // if not found by typeid, then check by name, because there may be problems if
                  // new transforms are registered by different modules
                  const string fromName(fromInfo.name());
                  const string toName(toInfo.name());

                  // create a new locked section
                  Pointer< GaussianTransform> t;
                  {
                     ::canopy::mt::MutexGuard< > GUARD(_mutex); // should lock for concurrent read
                     map< pair< string, string>, Reference< GaussianTransform> >::const_iterator j =
                           _transformsByString.find(make_pair(fromName, toName));
                     if (j != _transformsByString.end()) {
                        t = j->second;
                     }
                  }

                  if (t) {
                     if (_logger.isLoggable(Level::INFO)) {
                        Log("thales.kalman.GaussianTransform").info(
                              "Transform not found by type_info, but by name; registering by type_info");
                     }
                     // since we've found the transform, we register the real types so that the next lookup
                     // will be fast
                     registerTransform(fromInfo, toInfo, t);
                     return t;
                  }

                  if (_logger.isLoggable(Level::WARN)) {
                     LogEntry(_logger).warn() << "Transform not found from " << fromName << " to " << toName << doLog;
                  }
                  return ::timber::Pointer< GaussianTransform>();
               }

               /** A mutex to protected access */
            private:
               ::canopy::mt::Mutex _mutex;

               /** An identity transform, cached, so that we don't have to keep allocating it */
            private:
               const Reference< GaussianTransform> _identity;

               /** The registry for transforms by string names, just in case */
            private:
               map< pair< string, string>, Reference< GaussianTransform> > _transformsByString;

               /** A logger */
            private:
               mutable Log _logger;
         }
         ;

         /**
          * Get a transform registry
          * @return a transform registry
          */
         static StaticTransformRegistry& getRegistry()
         {
            // ensure this is only created once (need to have gcc for this)
            static StaticTransformRegistry REGISTRY;

            return REGISTRY;
         }

      }

      GaussianTransform::~GaussianTransform() throws()
      {
      }

      Reference< GaussianTransform> GaussianTransform::identityTransform(const CoordinateFrame& frame) throws()
      {
         try {
            return find(frame, frame);
         }
         catch (const exception& e) {
            Log("thales.kalman.GaussianTransform").caught("Caught a completely unexpected exception", e);
            return new IdentityTransform();
         }
      }

      bool GaussianTransform::registerTransform(const type_info& from, const type_info& to,
            const Reference< GaussianTransform>& t) throws()
      {
         return getRegistry().registerTransform(from, to, t);
      }

      Reference< GaussianTransform> GaussianTransform::find(const CoordinateFrame& from,
            const CoordinateFrame& to) throws (TransformNotFound)
      {
         ::timber::Pointer< GaussianTransform> tx = getRegistry().findTransform(from, to);
         if (tx) {
            return tx;
         }
         throw TransformNotFound(from, to);
      }
   }
}

