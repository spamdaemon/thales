#ifndef _THALES_KALMAN_FRAME_CARTESIAN_3D_INERTIAL_P__CARTESIAN_2D_INERTIAL_P_H
#define _THALES_KALMAN_FRAME_CARTESIAN_3D_INERTIAL_P__CARTESIAN_2D_INERTIAL_P_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
   namespace kalman {

      class GaussianTransform;
      namespace transform {

         /**
          * This transform converts a 3D inertial position into a 2D inertial position
          * on the z=0 plane
          */
         class Cartesian_3D_Inertial_P__Cartesian_2D_Inertial_P
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_3D_Inertial_P__Cartesian_2D_Inertial_P);
               Cartesian_3D_Inertial_P__Cartesian_2D_Inertial_P() = delete;

               /**
                * Get a cartesian coordinate frame.
                * @return a coordinate frame
                */
            public:
               static ::timber::Reference< GaussianTransform> create() throws();
         };

      }
   }
}

#endif
