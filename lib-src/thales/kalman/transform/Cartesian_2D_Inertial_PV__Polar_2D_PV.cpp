#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Polar_2D_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Polar_2D_PV.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <timber/logging.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>

using namespace newton;
using namespace timber;
using namespace timber::logging;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform()
	  {}
	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {
	    dynamic_cast<const Cartesian_2D_Inertial_PV&>(from);
	    const Polar_2D_PV& polar = dynamic_cast<const Polar_2D_PV&>(to);

	    Vector<double> Hx(3);
	    Matrix<double> H(3,4);

	    const double dx = x(0) - polar.centerX();
	    const double dy = x(1) - polar.centerY();
	    
	    const double r2  = dx*dx+dy*dy;
	    if (r2==0.0) {
	      throw ::std::runtime_error("Mean is at the center of the polar coordinate system");
	    }

	    const double r  = ::std::sqrt(r2);
	    const double az = ::std::atan2(dx,dy);
	    const double tmp = x(2)*dx + x(3)*dy;
	    const double rr = tmp / r;
	    Hx(0) = az;
	    Hx(1) = r;
	    Hx(2) = rr;

	    if (toP || xHP) {
	      const double daz_dx = dy/r2;
	      const double daz_dy = -dx/r2;
	      
	      const double dr_dx = dx/r;
	      const double dr_dy = dy/r;
	      
	      const double drr_dx = (r*x(2) - tmp*dr_dx)/r2;
	      const double drr_dy = (r*x(3) - tmp*dr_dy)/r2;
	      const double drr_dvx = dx/r;
	      const double drr_dvy = dy/r;
	      
	      H(0,0) = daz_dx;
	      H(0,1) = daz_dy;
	      H(1,0) = dr_dx;
	      H(1,1) = dr_dy;
	      H(2,0) = drr_dx;
	      H(2,1) = drr_dy;
	      H(2,2) = drr_dvx;
	      H(2,3) = drr_dvy;
	      
	      Matrix<double> HP = H*P;
	    
	      if (toP) {
		Matrix<double> HPHt = HP.multiplyTranspose(H);
		Covariance::makeSymmetric(HPHt);
		toP->swap(HPHt);
	      }
	      if (xHP) {
		xHP->swap(HP);
	      }
	    }

	    toX.swap(Hx);


	    return true;
	  }
	};
      }

      Reference<GaussianTransform> Cartesian_2D_Inertial_PV__Polar_2D_PV::create() throws()
      {	return new Transform(); }

    }
  }
}

