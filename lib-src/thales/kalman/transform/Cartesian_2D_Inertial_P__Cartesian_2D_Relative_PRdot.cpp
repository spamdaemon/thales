#include <thales/kalman/transform/Cartesian_2D_Inertial_P__Cartesian_2D_Relative_PRdot.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Relative_PRdot.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <timber/logging.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>

using namespace newton;
using namespace timber;
using namespace timber::logging;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform(double maxSpeed) 
	    :_maxSpeed(0)
	  {}
	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {

	    dynamic_cast<const Cartesian_2D_Inertial_P&>(from);
	    const Cartesian_2D_Relative_PRdot& polar = dynamic_cast<const Cartesian_2D_Relative_PRdot&>(to);

	    Vector<double> Hx(3);

	    const double dx = x(0) - polar.centerX();
	    const double dy = x(1) - polar.centerY();

	    const double r2  = dx*dx+dy*dy;
	    if (r2==0.0) {
	      throw ::std::runtime_error("Mean is at the center of the polar coordinate system");
	    }
	    
	    const double r  = ::std::sqrt(r2);
	    const double tmp = x(2)*dx + x(3)*dy;
	    const double rr = tmp / r;
	    Hx(0) = dx;
	    Hx(1) = dy;
	    Hx(2) = 0;

	    // compute the jacobian
	    if (toP) {
	      Matrix<double> HPHt(3,3);
	      HPHt(0,0) = P(0,0);
	      HPHt(1,0) = HPHt(0,1) = P(0,1);
	      HPHt(1,1) = P(1,1);
	      HPHt(2,2) = _maxSpeed*_maxSpeed;
	      toP->swap(HPHt);
	    }
	    if (xHP) {
	      Matrix<double> HP(3,2);
	      HP(0,0) = P(0,0);
	      HP(1,0) = HP(0,1) = P(0,1);
	      HP(1,1) = P(1,1);
	      xHP->swap(HP);
	    }
	    toX.swap(Hx);
	    
	    return true;
	  }

	private:
	  double _maxSpeed;
	};
      }

      Reference<GaussianTransform> Cartesian_2D_Inertial_P__Cartesian_2D_Relative_PRdot::create(double maxSpeed) throws()
      {	return new Transform(maxSpeed); }

    }
  }
}

