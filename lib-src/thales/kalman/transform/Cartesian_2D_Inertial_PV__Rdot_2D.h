#ifndef _THALES_KALMAN_FRAME_CARTESIAN_2D_INERTIAL_PV__RDOT_2D_H
#define _THALES_KALMAN_FRAME_CARTESIAN_2D_INERTIAL_PV__RDOT_2D_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
  namespace kalman {
    class GaussianTransform;
    namespace transform {

      /**
       * This class represents an inertial _2D cartesian coordinate frame 
       * with position. 
       */
      class Cartesian_2D_Inertial_PV__Rdot_2D {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_2D_Inertial_PV__Rdot_2D);
	Cartesian_2D_Inertial_PV__Rdot_2D() = delete;

	/**
	 * Get a transform that maps an intertial 2D estimate's velocity
	 * into a range-rate.
	 * @return a coordinate transform
	 */
      public:
	static ::timber::Reference<GaussianTransform> create() throws();
      };

    }
  }
}

#endif
