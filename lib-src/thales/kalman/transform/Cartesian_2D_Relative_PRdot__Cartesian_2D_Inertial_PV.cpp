#include <thales/kalman/transform/Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Relative_PRdot.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <timber/logging.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>

using namespace newton;
using namespace timber;
using namespace timber::logging;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform(double maxSpeed)
	    : _maxSpeed(maxSpeed) 
	  {}
	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {

	    const Cartesian_2D_Relative_PRdot& polar = dynamic_cast<const Cartesian_2D_Relative_PRdot&>(from);
       dynamic_cast<const Cartesian_2D_Inertial_PV&>(to);

	    Vector<double> Hx(4);
	    double r = sqrt(x(0)*x(0) + x(1)*x(1));
	    if (r==0) {
	      throw ::std::runtime_error("Mean is at the center of the polar coordinate system");
	    }
	      
	    Hx(0) = polar.centerX() + x(0);
	    Hx(1) = polar.centerY() + x(1);
	    Hx(2) = x(2) * x(0) / r;
	    Hx(3) = x(2) * x(1) / r;

	    // compute the jacobian
	    if (toP || xHP) {
	      Matrix<double> H(4,3);

	      H(0,0) = 1;
	      H(1,1) = 1;
	      
	      // derivative of : g(x)/f(x)
	      // g(x)/f(x) = -g(x)/f2(x)*df(x) + dg(x)*f(x)/f2(x) = f*dg/f2 - g*df/f2 = (f*dg - g*df)/f2

	      H(2,0) = ((x(2)*r) - (x(2)*x(0)  *  x(0)/r))/(r*r);
	      H(2,1) = (- (x(2)*x(0)  *  x(1)/r))/(r*r);
	      H(2,2) = x(0)/r;
	      H(3,0) = ((x(2)*r) - (x(2)*x(1)  *  x(1)/r))/(r*r);
	      H(3,1) = (- (x(2)*x(1)  *  x(0)/r))/(r*r);
	      H(3,2) = x(1)/r;
	      
	      Matrix<double> HP = H*P;
	      
	      if (toP) {
		Matrix<double> HPHt = HP.multiplyTranspose(H);
		Covariance::makeSymmetric(HPHt);
		
		double speed = ::std::max(_maxSpeed - x(2),1.0);
		double vy = speed * x(0)/r;
		double vx = -speed * x(1)/r;

		HPHt(2,2) += vx*vx;
		HPHt(3,3) += vy*vy;
		HPHt(2,3) += vx*vy;
		HPHt(3,2) = HPHt(2,3);
		
		toP->swap(HPHt);
	      }
	      if (xHP) {
		xHP->swap(HP);
	      }
	    }
	    toX.swap(Hx);

	    return true;
	  }
	  
	private:
	  const double _maxSpeed;
	};
      }

      Reference<GaussianTransform> Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV::create(double maxSpeed) throws()
      {	return new Transform(maxSpeed); }

    }
  }
}

