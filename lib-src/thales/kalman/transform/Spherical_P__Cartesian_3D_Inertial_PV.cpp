#include <thales/kalman/transform/Spherical_P__Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/frame/Spherical_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <timber/logging.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>

using namespace newton;
using namespace timber;
using namespace timber::logging;
using namespace thales::kalman::frame;

namespace thales {
   namespace kalman {
      namespace transform {
         namespace {
            struct Transform : public EKFTransform
            {
                  Transform(double maxSpeed)
                        : _maxSpeed(maxSpeed)
                  {
                  }
                  ~Transform() throws()
                  {
                  }

                  bool apply(const CoordinateFrame& from, const Vector< double>& mean, const Matrix< double>& P,
                        const CoordinateFrame& to, Vector< double>& toX, Matrix< double>* toP,
                        Matrix< double>* xHP) const throws (::std::exception)
                  {
                     const Spherical_P& spherical = dynamic_cast< const Spherical_P&>(from);
                     dynamic_cast< const Cartesian_3D_Inertial_PV&>(to);

                     const double az = mean(0);
                     const double el = mean(1);
                     const double r = mean(2);

                     double cosAz = ::std::cos(az);
                     double sinAz = ::std::sin(az);
                     double cosEl = ::std::cos(el);
                     double sinEl = ::std::sin(el);

                     // note: due to use of azimuth, we need to swap cos and sin!
                     double x = spherical.centerX() + sinAz * (r * cosEl);
                     double y = spherical.centerY() + cosAz * (r * cosEl);
                     double z = spherical.centerZ() + r * sinEl;

                     Vector< double> Hx(x, y, z, 0, 0, 0);

                     if (xHP || toP) {
                        Matrix< double> H(6, 3);

                        // compute the jacobian
                        const double dx_daz = (r * cosEl) * cosAz;
                        const double dx_del = -(r * sinEl) * sinAz;
                        const double dx_dr = cosEl * sinAz;
                        const double dy_daz = -(r * cosEl) * sinAz;
                        const double dy_del = -(r * sinEl) * cosAz;
                        const double dy_dr = cosEl * cosAz;
                        const double dz_daz = 0;
                        const double dz_del = (r * cosEl);
                        const double dz_dr = sinEl;

                        H(0, 0) = dx_daz;
                        H(0, 1) = dx_del;
                        H(0, 2) = dx_dr;
                        H(1, 0) = dy_daz;
                        H(1, 1) = dy_del;
                        H(1, 2) = dy_dr;
                        H(2, 0) = dz_daz;
                        H(2, 1) = dz_del;
                        H(2, 2) = dz_dr;

                        Matrix< double> HP = H * P;

                        if (toP) {
                           Matrix< double> HPHt = HP.multiplyTranspose(H);
                           Covariance::makeSymmetric(HPHt);
                           HPHt(3, 3) = HPHt(4, 4) = HPHt(5, 5) = _maxSpeed * _maxSpeed;
                           toP->swap(HPHt);
                        }
                        if (xHP) {
                           xHP->swap(HP);
                        }
                     }

                     toX.swap(Hx);
                     return true;
                  }
               private:
                  const double _maxSpeed;
            };
         }

         Reference< GaussianTransform> Spherical_P__Cartesian_3D_Inertial_PV::create(double maxSpeed) throws()
         {
            return new Transform(maxSpeed);
         }

      }
   }
}

