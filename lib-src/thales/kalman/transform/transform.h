#ifndef _THALES_KALMAN_TRANSFORM_H
#define _THALES_KALMAN_TRANSFORM_H


namespace thales {
  namespace kalman {

    /**
     * This namespace defines various transformations between coordinate frames.
     * Almost all possible combinations of all known coordinates frame are given.
     */
    namespace transform {
    }
  }
}

#endif
