#ifndef _THALES_KALMAN_FRAME_SPHERICAL_P__CARTESIAN_2D_INERTIAL_P_H
#define _THALES_KALMAN_FRAME_SPHERICAL_P__CARTESIAN_2D_INERTIAL_P_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
   namespace kalman {
      class GaussianTransform;
      namespace transform {

         /**
          * This class represents an inertial _2D cartesian coordinate frame
          * with position.
          */
         class Spherical_P__Cartesian_2D_Inertial_P
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Spherical_P__Cartesian_2D_Inertial_P);
               Spherical_P__Cartesian_2D_Inertial_P() = delete;

               /**
                * Get a transform from cartesian space into polar space.
                * @return a coordinate frame transform
                */
            public:
               static ::timber::Reference< GaussianTransform> create() throws();

         };

      }
   }
}

#endif
