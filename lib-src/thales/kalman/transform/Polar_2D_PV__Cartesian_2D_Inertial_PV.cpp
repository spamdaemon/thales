#include <thales/kalman/transform/Polar_2D_PV__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Polar_2D_PV.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <timber/logging.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>
#include <iostream>

using namespace newton;
using namespace timber;
using namespace timber::logging;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform(double maxSpeed)
	    : _maxSpeed(maxSpeed) 
	  {}
	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const Vector<double>& mean, const Matrix<double>& P,
		      const CoordinateFrame& to,
		      Vector<double>& toX, 
		      Matrix<double>* toP,
		      Matrix<double>* xHP) const  throws (::std::exception)
	  {
	    const Polar_2D_PV& polar = dynamic_cast<const Polar_2D_PV&>(from);
       dynamic_cast<const Cartesian_2D_Inertial_PV&>(to);

	    double x = polar.centerX() + ::std::sin(mean(0))* mean(1);
	    double y = polar.centerY() + ::std::cos(mean(0))* mean(1);
	    double vx = ::std::sin(mean(0))*mean(2);
	    double vy = ::std::cos(mean(0))*mean(2);

	    Vector<double> Hx(x,y,vx,vy);

	    if (toP || xHP) {

	      Matrix<double> H(4,3);
	      H(0,0) = mean(1)* ::std::cos(mean(0));
	      H(1,0) = -mean(1)* ::std::sin(mean(0));
	      H(2,0) = mean(2)* ::std::cos(mean(0)); // vy
	      H(3,0) = -mean(2)* ::std::sin(mean(0)); // -vx

	      H(0,1) = ::std::sin(mean(0));
	      H(1,1) = ::std::cos(mean(0));
	      H(2,1) = 0;
	      H(3,1) = 0;
	      
	      H(0,2) = 0;
	      H(1,2) = 0;
	      H(2,2) = ::std::sin(mean(0));
	      H(3,2) = ::std::cos(mean(0));

	      Matrix<double> HP = H*P;
	      if (toP) {
		Matrix<double> HPHt = HP.multiplyTranspose(H);
		Covariance::makeSymmetric(HPHt);

		// we need to compute an orthogonal error matrix for the 
		// velocity covariance
		double speed = ::std::max(_maxSpeed - mean(2),.5);
		
		double orthoVy = H(2,2) * speed;
		double orthoVx = -H(3,2)* speed;
		
		HPHt(2,2) += orthoVx*orthoVx;
		HPHt(2,3) += orthoVx*orthoVy;
		HPHt(3,3) += orthoVy*orthoVy;
		HPHt(3,2) = HPHt(2,3); // symmetic op
		
		toP->swap(HPHt);
	      }
	      if (xHP) {
		xHP->swap(HP);
	      }
	    }
	    toX.swap(Hx);
	    return true;
	  }
	private:
	  const double _maxSpeed;
	};
      }

      Reference<GaussianTransform> Polar_2D_PV__Cartesian_2D_Inertial_PV::create(double maxSpeed) throws()
      {	return new Transform(maxSpeed); }

    }
  }
}

