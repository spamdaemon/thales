#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Rdot_2D.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Rdot_2D.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>

using namespace newton;
using namespace timber;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform()
	  {}

	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {
	    dynamic_cast<const Cartesian_2D_Inertial_PV&>(from);
	    const Rdot_2D& polar = dynamic_cast<const Rdot_2D&>(to);

	    const double dx = x(0) - polar.centerX();
	    const double dy = x(1) - polar.centerY();

	    const double r2  =  dx*dx+dy*dy;
	    if (r2==0.0) {
	      throw ::std::runtime_error("Mean is at the center of the polar coordinate system");
	    }

	    const double r  = ::std::sqrt(r2);
	    const double tmp = x(2)*dx + x(3)*dy;
	    const double rr = tmp / r;
	    Vector<double> Hx(1);
	    Hx(0) = rr;
	    
	    if (toP || xHP) {
	      Matrix<double> H(1,4);
	      const double dr_dx = dx/r;
	      const double dr_dy = dy/r;
	      
	      const double drr_dx = (r*x(2) - tmp*dr_dx)/r2;
	      const double drr_dy = (r*x(3) - tmp*dr_dy)/r2;
	      const double drr_dvx = dr_dx;
	      const double drr_dvy = dr_dy;
	      
	      // the X and Y components transform directly
	      H(0,0) = drr_dx;
	      H(0,1) = drr_dy;
	      H(0,2) = drr_dvx;
	      H(0,3) = drr_dvy;
	      
	      Matrix<double> HP = H*P;
	      // a 1x1 matrix is always symmetric!
	      if (toP) {
		Matrix<double> HPHt = HP.multiplyTranspose(H);
		assert(HPHt.rowSize()==1);
		toP->swap(HPHt);
	      }
	      if (xHP) {
		xHP->swap(HP);
	      }
	    }
	    
	    toX.swap(Hx);

	    return true;
	  }
	};
      }

      Reference<GaussianTransform> Cartesian_2D_Inertial_PV__Rdot_2D::create() throws()
      {	return new Transform(); }

    }
  }
}

