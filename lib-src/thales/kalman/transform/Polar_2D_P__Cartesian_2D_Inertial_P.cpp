#include <thales/kalman/transform/Polar_2D_P__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Polar_2D_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>
#include <iostream>

using namespace newton;
using namespace timber;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform()
	  {}
	  
	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {
	    const Polar_2D_P& polar = dynamic_cast<const Polar_2D_P&>(from);
       dynamic_cast<const Cartesian_2D_Inertial_P&>(to);

	    const double az = x(0);
	    const double r = x(1);

	    double cosAz = ::std::cos(az);
	    double sinAz = ::std::sin(az);

	    // note: due to use of azimuth, we need to swap cos and sin!
	    double px = polar.centerX() + sinAz* r;
	    double py = polar.centerY() + cosAz* r;

 
	    Vector<double> Hx(px,py);
	    
	    if (xHP || toP) {
	      Matrix<double> H(2,2);
	      
	      // compute the jacobian
	      double dx_dr  = sinAz;
	      double dy_dr  = cosAz;
	      double dx_daz = r* cosAz;
	      double dy_daz = -r* sinAz;
	      
	      H(0,0) = dx_daz;
	      H(0,1) = dx_dr;
	      H(1,0) = dy_daz;
	      H(1,1) = dy_dr;
	      
	      Matrix<double> HP = H*P;
	      
	      if (toP) {
		Matrix<double> HPHt = HP.multiplyTranspose(H);
		const double tmp = 0.5*(HPHt(0,1)+HPHt(1,0));
		HPHt(0,1) = HPHt(1,0) = tmp;
		toP->swap(HPHt);
	      }
	      if (xHP) {
		xHP->swap(HP);
	      }
	    }
	    
	    toX.swap(Hx);

	    return true;
	  }

	};
      }

      Reference<GaussianTransform> Polar_2D_P__Cartesian_2D_Inertial_P::create() throws()
      {
	return new Transform(); 
      }

    }
  }
}

