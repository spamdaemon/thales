#ifndef _THALES_KALMAN_FRAME_CARTESIAN_2D_INERTIAL_P__PROJECTIVEPLANE3D_P_H
#define _THALES_KALMAN_FRAME_CARTESIAN_2D_INERTIAL_P__PROJECTIVEPLANE3D_P_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
  namespace kalman {
    class GaussianTransform;
    namespace transform {

      /**
       * This class represents an inertial _2D cartesian coordinate frame 
       * with position. 
       */
      class Cartesian_2D_Inertial_P__ProjectivePlane3D_P {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_2D_Inertial_P__ProjectivePlane3D_P);
	Cartesian_2D_Inertial_P__ProjectivePlane3D_P() = delete;
	
	/**
	 * Get a transform from cartesian space into polar space.
	 * @return a coordinate frame transform
	 */
      public:
	static ::timber::Reference<GaussianTransform> create() throws();
	
	/**
	 * Get a transform from cartesian space into polar space.
	 * @param doThrow if true throw an exception if the mean is behind the projection plane
	 * @return a coordinate frame transform
	 */
      public:
	static ::timber::Reference<GaussianTransform> create(bool doThrow) throws();
	
      };
      
    }
  }
}

#endif
