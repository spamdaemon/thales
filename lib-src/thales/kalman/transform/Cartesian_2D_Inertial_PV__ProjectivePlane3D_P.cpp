#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__ProjectivePlane3D_P.h>
#include <thales/kalman/transform/ProjectivePlane3D_P__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/ProjectivePlane3D_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>
#include <iostream>

using namespace newton;
using namespace timber;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform()
	  {}

	  
	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {
	    dynamic_cast<const Cartesian_2D_Inertial_PV&>(from);
	    const ProjectivePlane3D_P& plane = dynamic_cast<const ProjectivePlane3D_P&>(to);

	    const Vector<double>& fp = plane.focalPoint();
	    const Matrix<double>& R = plane.rotationMatrix();
	    
	    double px = x(0) - fp(0);
	    double py = x(1) - fp(1);
	    double pz = - fp(2);
	    
	    double Rx = R(0,0)*px + R(0,1)*py + R(0,2)*pz;
	    double Ry = R(1,0)*px + R(1,1)*py + R(1,2)*pz;
	    double Rz = R(2,0)*px + R(2,1)*py + R(2,2)*pz;

	    

	    if (Rz <= plane.focalLength()) {
	      throw ::std::runtime_error("Could not project point; not in front of the projection plane");
	    }
	    const double ratio = plane.focalLength() / Rz;
	    Vector<double> Hx(Rx*ratio,Ry*ratio);
	    
	    // compute a, b, and c as follows:
	    // let c be the 
	    
	    if (xHP || toP) {
	      Matrix<double> H(2,4);
	      
	      // compute the jacobian
	      
	      const double dRx_dx = R(0,0);
	      const double dRx_dy = R(0,1);
	      const double dRy_dx = R(1,0);
	      const double dRy_dy = R(1,1);
	      const double dRz_dx = R(2,0);
	      const double dRz_dy = R(2,1);

	      H(0,0) = (ratio/Rz)* (dRx_dx*Rz - Rx*dRz_dx);
	      H(0,1) = (ratio/Rz)* (dRx_dy*Rz - Rx*dRz_dy);
	      H(1,0) = (ratio/Rz)* (dRy_dx*Rz - Ry*dRz_dx);
	      H(1,1) = (ratio/Rz)* (dRy_dy*Rz - Ry*dRz_dy);

	      // remaining entries are 0, since velocity does not contribute
	      // to position

	      Matrix<double> HP = H*P;
	      
	      if (toP) {
		Matrix<double> HPHt = HP.multiplyTranspose(H);
		const double tmp = 0.5*(HPHt(0,1)+HPHt(1,0));
		HPHt(0,1) = HPHt(1,0) = tmp;
		toP->swap(HPHt);
	      }
	      if (xHP) {
		xHP->swap(HP);
	      }
	    }
	    
	    toX.swap(Hx);

	    return true;
	  }
	};
      }

      Reference<GaussianTransform> Cartesian_2D_Inertial_PV__ProjectivePlane3D_P::create() throws()
      {
	return new Transform(); 
      }

    }
  }
}

