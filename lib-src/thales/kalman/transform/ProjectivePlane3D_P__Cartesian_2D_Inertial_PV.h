#ifndef _THALES_KALMAN_FRAME_PROJECTIVEPLANE3D_P__CARTESIAN_2D_INERTIAL_PV_H
#define _THALES_KALMAN_FRAME_PROJECTIVEPLANE3D_P__CARTESIAN_2D_INERTIAL_PV_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
  namespace kalman {
    class GaussianTransform;
    namespace transform {

      /**
       * This class represents an inertial _2D cartesian coordinate frame 
       * with position. 
       */
      class ProjectivePlane3D_P__Cartesian_2D_Inertial_PV {
            CANOPY_BOILERPLATE_PREVENT_COPYING(ProjectivePlane3D_P__Cartesian_2D_Inertial_PV);
	ProjectivePlane3D_P__Cartesian_2D_Inertial_PV() = delete;
	
	/**
	 * Get a transform from cartesian space into polar space.
	 * @param maxSpeed the maximum assumed speed
	 * @return a coordinate frame transform
	 */
      public:
	static ::timber::Reference<GaussianTransform> create(double maxSpeed=45) throws();

      };

    }
  }
}

#endif
