#include <thales/kalman/transform/Cartesian_2D_Inertial_P__Polar_2D_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Polar_2D_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>

using namespace newton;
using namespace timber;
using namespace thales::kalman::frame;

namespace thales {
   namespace kalman {
      namespace transform {
         namespace {
            struct Transform : public EKFTransform
            {
                  Transform()
                  {
                  }

                  ~Transform() throws()
                  {
                  }

                  bool apply(const CoordinateFrame& from, const ::newton::Vector< double>& x,
                        const ::newton::Matrix< double>& P, const CoordinateFrame& to, ::newton::Vector< double>& toX,
                        ::newton::Matrix< double>* toP, ::newton::Matrix< double>* xHP) const throws (::std::exception)
                  {
                     dynamic_cast< const Cartesian_2D_Inertial_P&>(from);
                     const Polar_2D_P& polar = dynamic_cast< const Polar_2D_P&>(to);

                     const double dx = x(0) - polar.centerX();
                     const double dy = x(1) - polar.centerY();

                     const double r2 = dx * dx + dy * dy;
                     if (r2 == 0.0) {
                        throw ::std::runtime_error("Mean is at the center of the polar coordinate system");
                     }

                     const double r = ::std::sqrt(r2);
                     const double az = ::std::atan2(dx, dy);
                     Vector< double> Hx(az, r);

                     if (xHP || toP) {
                        Matrix< double> H(2, 2);

                        // compute the jacobian

                        const double daz_dx = dy / r2;
                        const double daz_dy = -dx / r2;

                        const double dr_dx = dx / r;
                        const double dr_dy = dy / r;

                        H(0, 0) = daz_dx;
                        H(0, 1) = daz_dy;
                        H(1, 0) = dr_dx;
                        H(1, 1) = dr_dy;

                        Matrix< double> HP = H * P;

                        if (toP) {
                           Matrix< double> HPHt = HP.multiplyTranspose(H);
                           const double tmp = 0.5 * (HPHt(0, 1) + HPHt(1, 0));
                           HPHt(0, 1) = HPHt(1, 0) = tmp;
                           toP->swap(HPHt);
                        }
                        if (xHP) {
                           xHP->swap(HP);
                        }
                     }

                     toX.swap(Hx);

                     return true;
                  }
            };
         }

         Reference< GaussianTransform> Cartesian_2D_Inertial_P__Polar_2D_P::create() throws()
         {
            return new Transform();
         }

      }
   }
}

