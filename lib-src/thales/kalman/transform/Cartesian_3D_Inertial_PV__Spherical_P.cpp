#include <thales/kalman/transform/Cartesian_3D_Inertial_PV__Spherical_P.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/frame/Spherical_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>

using namespace newton;
using namespace timber;
using namespace thales::kalman::frame;

namespace thales {
   namespace kalman {
      namespace transform {
         namespace {
            struct Transform : public EKFTransform
            {
                  Transform()
                  {
                  }
                  ~Transform() throws()
                  {
                  }

                  bool apply(const CoordinateFrame& from, const ::newton::Vector< double>& mean,
                        const ::newton::Matrix< double>& P, const CoordinateFrame& to, ::newton::Vector< double>& toX,
                        ::newton::Matrix< double>* toP, ::newton::Matrix< double>* xHP) const throws (::std::exception)
                  {
                     dynamic_cast< const Cartesian_3D_Inertial_PV&>(from);
                     const Spherical_P& spherical = dynamic_cast< const Spherical_P&>(to);

                     const double x = mean(0) - spherical.centerX();
                     const double y = mean(1) - spherical.centerY();
                     const double z = mean(2) - spherical.centerZ();

                     const double rr = x * x + y * y;
                     const double R = rr + z * z;
                     if (rr == 0.0 || R == 0.0) {
                        throw ::std::runtime_error("Mean is at the center of the polar coordinate system");
                     }

                     const double r = ::std::sqrt(R);
                     const double az = ::std::atan2(x, y);
                     const double el = ::std::asin(z / r);
                     Vector< double> Hx(az, el, r);

                     if (xHP || toP) {
                        Matrix< double> H(3, 6);

                        const double daz_dx = y / rr;
                        const double daz_dy = -x / rr;
                        const double daz_dz = 0;

                        // compute the jacobian
                        const double EL = sqrt(1 - (z * z) / R);
                        const double del_dx = ((-z) / (r * R * EL)) * x;
                        const double del_dy = ((-z) / (r * R * EL)) * y;
                        const double del_dz = (R - z * z) / (r * R * EL);

                        const double dr_dx = x / r;
                        const double dr_dy = y / r;
                        const double dr_dz = z / r;

                        H(0, 0) = daz_dx;
                        H(0, 1) = daz_dy;
                        H(0, 2) = daz_dz;
                        H(1, 0) = del_dx;
                        H(1, 1) = del_dy;
                        H(1, 2) = del_dz;
                        H(2, 0) = dr_dx;
                        H(2, 1) = dr_dy;
                        H(2, 2) = dr_dz;

                        Matrix< double> HP = H * P;

                        if (toP) {
                           Matrix< double> HPHt = HP.multiplyTranspose(H);
                           Covariance::makeSymmetric(HPHt);
                           toP->swap(HPHt);
                        }
                        if (xHP) {
                           xHP->swap(HP);
                        }
                     }

                     toX.swap(Hx);

                     return true;
                  }
            };
         }

         Reference< GaussianTransform> Cartesian_3D_Inertial_PV__Spherical_P::create() throws()
         {
            return new Transform();
         }

      }
   }
}

