#include <thales/kalman/transform/Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Relative_PRdot.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <timber/logging.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>

using namespace newton;
using namespace timber;
using namespace timber::logging;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform()
	  {}
	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {
	    const Cartesian_2D_Relative_PRdot& polar = dynamic_cast<const Cartesian_2D_Relative_PRdot&>(from);
	    dynamic_cast<const Cartesian_2D_Inertial_P&>(to);

	    Vector<double> Hx(2);
	    Hx(0) = x(0)+polar.centerX();
	    Hx(1) = x(1)+polar.centerY();

	    if (toP) {
	      Matrix<double> HPHt(2,2);
	      HPHt(0,0) = P(0,0);
	      HPHt(1,1) = P(1,1);
	      HPHt(0,1) = HPHt(1,0) = P(0,1);
	      toP->swap(HPHt);
	    }

	    if (xHP) {
	      Matrix<double> HP(2,3);
	      HP(0,0)=P(0,0);
	      HP(0,1)=HP(1,0) = P(0,1);
	      HP(1,1) = P(1,1);
	      HP(0,2) = P(0,2);
	      HP(1,2) = P(1,2);
	      xHP->swap(HP); 
	    }

	    toX.swap(Hx);

	    return true;
	  }

	};
      }

      Reference<GaussianTransform> Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_P::create() throws()
      {	return new Transform(); }

    }
  }
}

