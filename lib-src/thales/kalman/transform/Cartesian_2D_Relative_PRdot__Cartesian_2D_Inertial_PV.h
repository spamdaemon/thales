#ifndef _THALES_KALMAN_FRAME_CARTESIAN_2D_RELATIVE_PRDOT__CARTESIAN_2D_INERTIAL_PV_H
#define _THALES_KALMAN_FRAME_CARTESIAN_2D_RELATIVE_PRDOT__CARTESIAN_2D_INERTIAL_PV_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
  namespace kalman {
    class GaussianTransform;
    namespace transform {

      /**
       * This class represents an inertial _2D cartesian coordinate frame 
       * with position. 
       */
      class Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV);
	Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV() = delete;

	/**
	 * Get a transform. The maximum speed will 
	 * be used to determine a velocity error when mapping from
	 * polar to cartesian space.
	 *
	 * The default speed of 45m/s is the equivalent
	 * to about 100 mph or 165 kph.
	 * @param maxSpeed the maximum speed in meters per second
	 * @return a coordinate transform
	 */
      public:
	static ::timber::Reference<GaussianTransform> create(double maxSpeed=45.0) throws();
      };

    }
  }
}

#endif
