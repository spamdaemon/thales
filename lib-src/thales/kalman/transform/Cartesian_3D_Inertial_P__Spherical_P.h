#ifndef _THALES_KALMAN_FRAME_CARTESIAN_3D_INERTIAL_P__SPHERICAL_P_H
#define _THALES_KALMAN_FRAME_CARTESIAN_3D_INERTIAL_P__SPHERICAL_P_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
   namespace kalman {
      class GaussianTransform;
      namespace transform {

         /**
          * This class represents an inertial _2D cartesian coordinate frame
          * with position.
          */
         class Cartesian_3D_Inertial_P__Spherical_P
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_3D_Inertial_P__Spherical_P);
               Cartesian_3D_Inertial_P__Spherical_P() = delete;

               /**
                * Get a transform from cartesian space into polar space.
                * @return a coordinate frame transform
                */
            public:
               static ::timber::Reference< GaussianTransform> create() throws();

         };

      }
   }
}

#endif
