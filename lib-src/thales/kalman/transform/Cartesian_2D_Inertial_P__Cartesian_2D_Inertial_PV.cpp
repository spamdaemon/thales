#include <thales/kalman/transform/Cartesian_2D_Inertial_P__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>

using namespace newton;
using namespace timber;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform(double maxSpeed)
	    : _maxSpeed(maxSpeed)
	  {}

	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {
	    dynamic_cast<const Cartesian_2D_Inertial_P&>(from);
	    dynamic_cast<const Cartesian_2D_Inertial_PV&>(to);

	    Vector<double> Hx(4);

	    Hx(0) = x(0);
	    Hx(1) = x(1);
	    Hx(2) = 0;
	    Hx(3) = 0;
	    if (toP) {
	      
	      Matrix<double> HPHt(4,4);
	      HPHt(0,0) = P(0,0);
	      HPHt(0,1) = HPHt(1,0) = P(0,1);
	      HPHt(1,1) = P(1,1);
	      HPHt(3,3) = HPHt(2,2) = _maxSpeed*_maxSpeed;
	      toP->swap(HPHt);
	    }

	    if (xHP) {
	      Matrix<double> HP(4,2);
	      HP(0,0) = P(0,0);
	      HP(0,1) = HP(1,0) = P(0,1);
	      HP(1,1) = P(1,1);
	      xHP->swap(HP);
	    }
	    
	    toX.swap(Hx);

	    return true;
	  }

	private:
	  const double _maxSpeed;
	};
      }

      Reference<GaussianTransform> Cartesian_2D_Inertial_P__Cartesian_2D_Inertial_PV::create(double maxSpeed) throws()
      {	return new Transform(maxSpeed); }

    }
  }
}

