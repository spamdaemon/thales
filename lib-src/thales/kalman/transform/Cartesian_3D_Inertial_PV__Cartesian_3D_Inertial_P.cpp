#include <thales/kalman/transform/Cartesian_3D_Inertial_PV__Cartesian_3D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>

using namespace newton;
using namespace timber;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform()
	  {}

	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {
	    dynamic_cast<const Cartesian_3D_Inertial_PV&>(from);
	    dynamic_cast<const Cartesian_3D_Inertial_P&>(to);

	    Vector<double> Hx(3);

	    Hx(0) = x(0);
	    Hx(1) = x(1);
	    Hx(2) = x(2);

	    if (toP) {
	      Matrix<double> HPHt(3,3);
	      HPHt(0,0) = P(0,0);
	      HPHt(0,1) = HPHt(1,0) = P(0,1);
	      HPHt(0,2) = HPHt(2,0) = P(0,2);
	      HPHt(1,2) = HPHt(2,1) = P(1,2);
	      HPHt(1,1) = P(1,1);
	      HPHt(2,2) = P(2,2);
	      toP->swap(HPHt);
	    }

	    if (xHP) {
	      Matrix<double> HP(3,6);
	      HP(0,0) = P(0,0);
	      HP(1,1) = P(1,1);
	      HP(2,2) = P(2,2);
	      HP(0,1) = HP(1,0) = P(0,1);
	      HP(0,2) = HP(2,0) = P(0,2);
	      HP(1,2) = HP(2,1) = P(1,2);

	      HP(0,3) = P(0,3);
	      HP(0,4) = P(0,4);
	      HP(0,5) = P(0,5);
	      HP(1,3) = P(1,3);
	      HP(1,4) = P(1,4);
	      HP(1,5) = P(1,5);
	      HP(2,3) = P(2,3);
	      HP(2,4) = P(2,4);
	      HP(2,5) = P(2,5);

	      xHP->swap(HP);
	    }
	    
	    toX.swap(Hx);

	    return true;
	  }

	};
      }

      Reference<GaussianTransform> Cartesian_3D_Inertial_PV__Cartesian_3D_Inertial_P::create() throws()
      {	return new Transform(); }

    }
  }
}

