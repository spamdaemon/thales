#ifndef _THALES_KALMAN_FRAME_CARTESIAN_3D_INERTIAL_P__CARTESIAN_3D_INERTIAL_PV_H
#define _THALES_KALMAN_FRAME_CARTESIAN_3D_INERTIAL_P__CARTESIAN_3D_INERTIAL_PV_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif


namespace thales {
  namespace kalman {

    class GaussianTransform;
    namespace transform {

      /**
       * This class represents an inertial _3D cartesian coordinate frame 
       * with position. 
       */
      class Cartesian_3D_Inertial_P__Cartesian_3D_Inertial_PV {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_3D_Inertial_P__Cartesian_3D_Inertial_PV);
	Cartesian_3D_Inertial_P__Cartesian_3D_Inertial_PV() = delete;

	/**
	 * Get a cartesian coordinate frame.
	 * @return a coordinate frame
	 */
      public:
	static ::timber::Reference<GaussianTransform> create(double maxSpeed=45.0) throws();
      };

    }
  }
}

#endif
