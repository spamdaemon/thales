#include <thales/kalman/transform/Cartesian_3D_Inertial_P__Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>

using namespace newton;
using namespace timber;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform(double maxSpeed)
	    : _maxSpeed(maxSpeed)
	  {}

	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {
	    dynamic_cast<const Cartesian_3D_Inertial_P&>(from);
	    dynamic_cast<const Cartesian_3D_Inertial_PV&>(to);

	    Vector<double> Hx(3);

	    Hx(0) = x(0);
	    Hx(1) = x(1);
	    Hx(2) = x(2);
	    Hx(3) = Hx(4) = Hx(5) = 0;

	    if (toP) {
	      Matrix<double> HPHt(6,6);
	      HPHt(0,0) = P(0,0);
	      HPHt(0,1) = HPHt(1,0) = P(0,1);
	      HPHt(0,2) = HPHt(2,0) = P(0,2);
	      HPHt(1,2) = HPHt(2,1) = P(1,2);
	      HPHt(1,1) = P(1,1);
	      HPHt(2,2) = P(2,2);

	      HPHt(3,3) = HPHt(4,4) = HPHt(5,5) = _maxSpeed*_maxSpeed;
	      toP->swap(HPHt);
	    }

	    if (xHP) {
	      Matrix<double> HP(6,3);
	      HP(0,0) = P(0,0);
	      HP(0,1) = HP(1,0) = P(0,1);
	      HP(0,2) = HP(2,0) = P(0,2);
	      HP(1,2) = HP(2,1) = P(1,2);
	      HP(1,1) = P(1,1);
	      HP(2,2) = P(2,2);
	      
	      xHP->swap(HP);
	    }
	    
	    toX.swap(Hx);

	    return true;
	  }
	  
	private:
	  const double _maxSpeed;
	};
      }

      Reference<GaussianTransform> Cartesian_3D_Inertial_P__Cartesian_3D_Inertial_PV::create(double maxSpeed) throws()
      {	return new Transform(maxSpeed); }

    }
  }
}

