#ifndef _THALES_KALMAN_FRAME_CARTESIAN_2D_INERTIAL_PV__RRDOT_2D_H
#define _THALES_KALMAN_FRAME_CARTESIAN_2D_INERTIAL_PV__RRDOT_2D_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
  namespace kalman {
    class GaussianTransform;
    namespace transform {

      /**
       * This class represents an inertial _2D cartesian coordinate frame 
       * with position. 
       */
      class Cartesian_2D_Inertial_PV__RRdot_2D {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_2D_Inertial_PV__RRdot_2D);
	Cartesian_2D_Inertial_PV__RRdot_2D() = delete;

	/**
	 * Get a transform. 
	 *
	 * @return a coordinate transform
	 */
      public:
	static ::timber::Reference<GaussianTransform> create() throws();
      };

    }
  }
}

#endif
