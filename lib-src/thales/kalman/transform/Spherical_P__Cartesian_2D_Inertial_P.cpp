#include <thales/kalman/transform/Spherical_P__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Spherical_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>
#include <iostream>

using namespace newton;
using namespace timber;
using namespace thales::kalman::frame;

namespace thales {
   namespace kalman {
      namespace transform {
         namespace {
            struct Transform : public EKFTransform
            {
                  Transform()
                  {
                  }

                  ~Transform() throws()
                  {
                  }

                  bool apply(const CoordinateFrame& from, const ::newton::Vector< double>& mean,
                        const ::newton::Matrix< double>& P, const CoordinateFrame& to, ::newton::Vector< double>& toX,
                        ::newton::Matrix< double>* toP, ::newton::Matrix< double>* xHP) const throws (::std::exception)
                  {
                     const Spherical_P& spherical = dynamic_cast< const Spherical_P&>(from);
                     dynamic_cast< const Cartesian_2D_Inertial_P&>(to);

                     const double az = mean(0);
                     const double el = mean(1);
                     const double r = mean(2);

                     double cosAz = ::std::cos(az);
                     double sinAz = ::std::sin(az);
                     double cosEl = ::std::cos(el);
                     double sinEl = ::std::sin(el);

                     // note: due to use of azimuth, we need to swap cos and sin!
                     double x = spherical.centerX() + sinAz * (r * cosEl);
                     double y = spherical.centerY() + cosAz * (r * cosEl);


                     Vector< double> Hx(x, y);

                     if (xHP || toP) {
                        Matrix< double> H(2, 3);

                        // compute the jacobian
                        const double dx_daz =  (r * cosEl) * cosAz;
                        const double dx_del = -(r * sinEl) * sinAz;
                        const double dx_dr = cosEl * sinAz;
                        const double dy_daz = -(r * cosEl) * sinAz;
                        const double dy_del = -(r * sinEl) * cosAz;
                        const double dy_dr = cosEl * cosAz;

                        H(0, 0) = dx_daz;
                        H(0, 1) = dx_del;
                        H(0, 2) = dx_dr;
                        H(1, 0) = dy_daz;
                        H(1, 1) = dy_del;
                        H(1, 2) = dy_dr;

                        Matrix< double> HP = H * P;

                        if (toP) {
                           Matrix< double> HPHt = HP.multiplyTranspose(H);
                           Covariance::makeSymmetric(HPHt);
                           toP->swap(HPHt);
                        }
                        if (xHP) {
                           xHP->swap(HP);
                        }
                     }

                     toX.swap(Hx);

                     return true;
                  }

            };
         }

         Reference< GaussianTransform> Spherical_P__Cartesian_2D_Inertial_P::create() throws()
         {
            return new Transform();
         }

      }
   }
}

