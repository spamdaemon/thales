#include <thales/kalman/transform/ProjectivePlane3D_P__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/ProjectivePlane3D_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/EKFTransform.h>
#include <newton/Matrix.h>
#include <newton/Vector.h>
#include <cmath>
#include <iostream>

using namespace newton;
using namespace timber;
using namespace thales::kalman::frame;

namespace thales {
  namespace kalman {
    namespace transform {
      namespace {
	struct Transform : public EKFTransform
	{
	  Transform(double maxSpeed)
	    : _maxSpeed(maxSpeed)
	  {}

	  
	  ~Transform() throws() {}

	  bool apply (const CoordinateFrame& from,
		      const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
		      const CoordinateFrame& to,
		      ::newton::Vector<double>& toX, 
		      ::newton::Matrix<double>* toP,
		      ::newton::Matrix<double>* xHP) const  throws (::std::exception)
	  {
	    const ProjectivePlane3D_P& zFrame = dynamic_cast<const ProjectivePlane3D_P&>(from);
       dynamic_cast<const Cartesian_2D_Inertial_PV&>(to);
	    
	    // the point on the plane
	    double px = x(0);
	    double py = x(1);

	    const Vector<double>& FP = zFrame.focalPoint();
	    const Vector<double>& N  = zFrame.planeNormal();
	    const Vector<double>& X  = zFrame.xAxis();
	    const Vector<double>& Y  = zFrame.yAxis();

	    // (Vx,Vy,Vz) is the 3D vector from the focal point to the point on the projection plane
	    double Vx = zFrame.focalLength()*N(0) + px*X(0) + py*Y(0);
	    double Vy = zFrame.focalLength()*N(1) + px*X(1) + py*Y(1);
	    double Vz = zFrame.focalLength()*N(2) + px*X(2) + py*Y(2);

	    if (Vz>=-.0001) {
	      throw ::std::runtime_error("Point cannot be mapped onto the ground plane z=0");
	    }

	    const double Fx = FP(0);
	    const double Fy = FP(1);
	    const double Fz = FP(2);
	    const double c = -Fz/Vz;
	    
	    Vector<double> Hx( Fx + c * Vx,Fy + c * Vy,0,0);
	    if (toP || xHP) {
	      // compute some partial derivatives that will be needed to 
	      // compute the Jacobian
	      double dVx_px = X(0);
	      double dVx_py = Y(0);
	      double dVy_px = X(1);
	      double dVy_py = Y(1);
	      double dVz_px = X(2);
	      double dVz_py = Y(2);
	      double dc_px = Fz*dVz_px/(Vz*Vz);
	      double dc_py = Fz*dVz_py/(Vz*Vz);
	      
	      double dx_px = dc_px*Vx + c*dVx_px;
	      double dx_py = dc_py*Vx + c*dVx_py;
	      double dy_px = dc_px*Vy + c*dVy_px;
	      double dy_py = dc_py*Vy + c*dVy_py;
	      Matrix<double> H(4,2);
	      H(0,0) = dx_px;
	      H(0,1) = dx_py;
	      H(1,0) = dy_px;
	      H(1,1) = dy_py;
	      // the remaining entries are 0

	      Matrix<double> HP = H*P;
	      
	      if (toP) {
		Matrix<double> tmpP  = HP.multiplyTranspose(H);
		double tmp = tmpP(0,1) + tmpP(1,0);
		tmpP(0,1) = tmpP(1,0) = 0.5*tmp;

		// we need to add in the maximum velocity 
		tmpP(2,2) = tmpP(3,3) = _maxSpeed*_maxSpeed;

		toP->swap(tmpP);
	      }
	      if (xHP) {
		xHP->swap(HP);
	      }
	    }
	    toX.swap(Hx);
	    
	    return true;
	  }
	  
	private:
	  const double _maxSpeed;
	};
      }

      Reference<GaussianTransform> ProjectivePlane3D_P__Cartesian_2D_Inertial_PV::create(double maxSpeed) throws()
      {
	return new Transform(maxSpeed); 
      }

    }
  }
}

