#ifndef _THALES_KALMAN_TIME_H
#define _THALES_KALMAN_TIME_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
  namespace kalman {

    /**
     * This type is used for time calculations in the kalman filter package. The time units
     * are essentially arbitary, but processModels assume certain units. 
     */
    typedef double Time;
    
  }
}

#endif
