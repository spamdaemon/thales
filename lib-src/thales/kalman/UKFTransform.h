#ifndef _THALES_KALMAN_UKFTRANSFORM_H
#define _THALES_KALMAN_UKFTRANSFORM_H

#ifndef _THALES_KALMAN_GAUSSIANTRANSFORM_H
#include <thales/kalman/GaussianTransform.h>
#endif

#ifndef _THALES_KALMAN_UNSCENTED_TRANSFORM_H
#include <thales/kalman/unscented/Transform.h>
#endif

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

#include <typeinfo>

namespace thales {
  namespace kalman {
    class Gaussian;
    class CoordinateFrame;
      
    /**
     * The UKFTransform provides all the functionality to determine
     * the necessary components for a extended Kalman filter.
     */
    class UKFTransform : public GaussianTransform {
          CANOPY_BOILERPLATE_PREVENT_COPYING(UKFTransform);

    protected:
      UKFTransform() throws();

      /** Destroy this transform */
    public:
      ~UKFTransform() throws();
      
      /**
       * Create a UKFTransform from another GaussianTransform. This method allows
       * playing with the UKFTransform, but this is not the preferred way of creating
       * a UKFTransform. This method uses a default unscented transform.
       * @param from the coordinate from which which to transform
       * @param f a gaussian transform
       */
    public:
      static ::timber::Reference<UKFTransform> create (const ::timber::Reference<CoordinateFrame>& from, const ::timber::Reference<GaussianTransform>& f) throws();

      
      /**
       * Create a UKFTransform from another GaussianTransform. This method allows
       * playing with the UKFTransform, but this is not the preferred way of creating
       * a UKFTransform.
       * @param t an unscented transform
       * @param f a gaussian transform
       */
    public:
      static ::timber::Reference<UKFTransform> create (const ::thales::kalman::unscented::Transform& t, const ::timber::Reference<GaussianTransform>& f) throws();

      /**
       * Create a UKF transform.
       * @param t an unscented transform
       * @param func the transformation function to be used
       */
    public:
      template <class F>
	static ::timber::Reference<UKFTransform> createUKF (const ::thales::kalman::unscented::Transform& t, F func) throws()
	{
	  struct Impl : public UKFTransform {
	    Impl(const ::thales::kalman::unscented::Transform& xt, F xfunc) throws()
	      : _transform(xt), _function(xfunc) {}
	    ~Impl() throws() {}

	    bool apply (const CoordinateFrame& from,
			const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
			const CoordinateFrame& to,
			::newton::Vector<double>& toX, 
			::newton::Matrix<double>* toP,
			::newton::Matrix<double>* HP) const  throws (::std::exception)
	    {
	      _transform.transform(_function,from,x,P,to,toX,toP,HP);
	      return true;
	    }

	  private:
	    const ::thales::kalman::unscented::Transform _transform;
	    const F _function;
	  };
	  
	  UKFTransform* trans = new Impl(t,func);
	  return ::timber::Reference<UKFTransform>(trans);
	}
    };

  }
}
#endif
