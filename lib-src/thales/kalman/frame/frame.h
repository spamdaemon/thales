#ifndef _THALES_KALMAN_FRAME_H
#define _THALES_KALMAN_FRAME_H


namespace thales {
  namespace kalman {

    /**
     * This namespace defines various types of coordinate frames. The naming of the classes
     * uses some variation of Hungarian notation and therefore deviates from the normal coding
     * standard. The following naming scheme is used: <code> &lt;type&gt;_&lt;dimension&gt;(_&lt;parameters&gt;)+</code>.
     * Types of coordinatesframes are, for example, Cartesian and Polar. A parameter of a Cartesian coordinate frame
     * may be that it is inertial and not relative. Usally, the letter <em>P</em> stands for <code>Position</code>, the letter <em>V</em> stands for <code>Velocity</code>, and <em>A</em> stands for <code>Acceleration</code>, indicating that a coordinate frame has the respective components.
     */
    namespace frame {
    }
  }
}

#endif
