#include <thales/kalman/frame/Polar_2D_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>

namespace thales {
  namespace kalman {
    namespace frame {

      Polar_2D_PV::Polar_2D_PV(double cx, double cy) throws()
      : _cx(cx),_cy(cy)
      {}

      Polar_2D_PV::~Polar_2D_PV() throws()
      {}
      
      ::timber::Reference<Polar_2D_PV> Polar_2D_PV::create(double cx, double cy) throws()
      {
	return new Polar_2D_PV(cx,cy);
      }

      bool Polar_2D_PV::equals(const CoordinateFrame& f) const throws()
      {
	if (CoordinateFrame::equals(f)) {
	  const Polar_2D_PV& pf = dynamic_cast<const Polar_2D_PV&>(f);
	  return pf._cx == _cx && pf._cy == _cy;
	}
	return false;
      }

      ::newton::Vector<double> Polar_2D_PV::difference(const ::newton::Vector<double>& x, const ::newton::Vector<double>& y) const throws()
      {
	const double PI = 3.14159265358979323846;

	::newton::Vector<double> res(x-y);
	if (res(0) > PI) {
	  res(0) -= 2*PI;
	}
	else if (res(0) < -PI) {
	  res(0) += 2*PI;
	}
	return res;
      }

      size_t Polar_2D_PV::dimension() const throws()
      {
	return 3;
      }
      ::timber::Pointer<CartesianInertial> Polar_2D_PV::getInertialFrame() const throws()
      { return Cartesian_2D_Inertial_PV::create(); }

    }
  }
}
