#ifndef _THALES_KALMAN_FRAME_POLAR_2D_PV_H
#define _THALES_KALMAN_FRAME_POLAR_2D_PV_H

#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#include <thales/kalman/CoordinateFrame.h>
#endif

namespace thales {
   namespace kalman {
      namespace frame {

         /**
          * This class represents an 2D polar coordinate frame
          * with position and velocity. The components of this frame are
          * <ol>
          * <li>angle (radians)
          * <li>range
          * <li>range rate (velocity along the range vector only)
          * </ol>
          */
         class Polar_2D_PV : public CoordinateFrame
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Polar_2D_PV);

               /**
                * Constructor.
                * @param cx the x-coordinate of the center
                * @param cy the y-coordinate of the center
                */
            private:
               Polar_2D_PV(double cx, double cy) throws();

               /** Destructor */
            public:
               ~Polar_2D_PV() throws();

               /**
                * Get the x-coordinate of the center
                * @return the x-coordinate of the center
                */
            public:
               inline double centerX() const throws()
               {
                  return _cx;
               }

               /**
                * Get the y-coordinate of the center
                * @return the y-coordinate of the center
                */
            public:
               inline double centerY() const throws()
               {
                  return _cy;
               }

               /**
                * Get a polar coordinate frame.
                * @param cx the x-coordinate of the center
                * @param cy the y-coordinate of the center
                * @return a coordinate frame
                */
            public:
               static ::timber::Reference< Polar_2D_PV> create(double cx, double cy) throws();

               bool equals(const CoordinateFrame& f) const throws();
               ::newton::Vector< double> difference(const ::newton::Vector< double>& x,
                     const ::newton::Vector< double>& y) const throws();
               size_t dimension() const throws();
               ::timber::Pointer< CartesianInertial> getInertialFrame() const throws();

               /** The center */
            private:
               double _cx, _cy;
         };

      }
   }
}

#endif
