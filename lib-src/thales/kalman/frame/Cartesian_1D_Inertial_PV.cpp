#include <thales/kalman/frame/Cartesian_1D_Inertial_PV.h>

namespace thales {
  namespace kalman {
    namespace frame {
      Cartesian_1D_Inertial_PV::Cartesian_1D_Inertial_PV() throws()
      : CartesianInertial(1,2)
      {}

      Cartesian_1D_Inertial_PV::~Cartesian_1D_Inertial_PV() throws()
      {}
      
      ::timber::Reference<Cartesian_1D_Inertial_PV> Cartesian_1D_Inertial_PV::create() throws()
      {
	return new Cartesian_1D_Inertial_PV();
      }

    }
  }
}
