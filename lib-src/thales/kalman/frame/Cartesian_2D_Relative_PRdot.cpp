#include <thales/kalman/frame/Cartesian_2D_Relative_PRdot.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>

namespace thales {
  namespace kalman {
    namespace frame {

      Cartesian_2D_Relative_PRdot::Cartesian_2D_Relative_PRdot(double cx, double cy) throws()
      : _cx(cx),_cy(cy)
      {}

      Cartesian_2D_Relative_PRdot::~Cartesian_2D_Relative_PRdot() throws()
      {}
      
      ::timber::Reference<Cartesian_2D_Relative_PRdot> Cartesian_2D_Relative_PRdot::create(double cx, double cy) throws()
      {
	return new Cartesian_2D_Relative_PRdot(cx,cy);
      }

      bool Cartesian_2D_Relative_PRdot::equals(const CoordinateFrame& f) const throws()
      {
	if (CoordinateFrame::equals(f)) {
	  const Cartesian_2D_Relative_PRdot& pf = dynamic_cast<const Cartesian_2D_Relative_PRdot&>(f);
	  return pf._cx == _cx && pf._cy == _cy;
	}
	return false;
      }

      size_t Cartesian_2D_Relative_PRdot::dimension() const throws()
      {	return 3; }
      
      ::timber::Pointer<CartesianInertial> Cartesian_2D_Relative_PRdot::getInertialFrame() const throws()
      { return Cartesian_2D_Inertial_PV::create(); }

    }
  }
}
