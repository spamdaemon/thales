#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>

namespace thales {
  namespace kalman {
    namespace frame {
      Cartesian_2D_Inertial_P::Cartesian_2D_Inertial_P() throws()
      : Cartesian_Inertial_P(2)
      {}

      Cartesian_2D_Inertial_P::~Cartesian_2D_Inertial_P() throws()
      {}
      
      ::timber::Reference<Cartesian_2D_Inertial_P> Cartesian_2D_Inertial_P::create() throws()
      {
	return new Cartesian_2D_Inertial_P();
      }


    }
  }
}
