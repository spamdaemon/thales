#include <thales/kalman/frame/Cartesian_3D_Inertial_PV.h>

namespace thales {
  namespace kalman {
    namespace frame {
      Cartesian_3D_Inertial_PV::Cartesian_3D_Inertial_PV() throws()
      : Cartesian_Inertial_PV(3)
      {}

      Cartesian_3D_Inertial_PV::~Cartesian_3D_Inertial_PV() throws()
      {}
      
      ::timber::Reference<Cartesian_3D_Inertial_PV> Cartesian_3D_Inertial_PV::create() throws()
      {
	return new Cartesian_3D_Inertial_PV();
      }

    }
  }
}
