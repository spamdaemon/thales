#ifndef _THALES_KALMAN_FRAME_RRDOT_2D_H
#define _THALES_KALMAN_FRAME_RRDOT_2D_H

#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#include <thales/kalman/CoordinateFrame.h>
#endif

namespace thales {
   namespace kalman {
      namespace frame {

         /**
          * This coordinate frame represents a range and range rate.
          */
         class RRdot_2D : public CoordinateFrame
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(RRdot_2D);

               /**
                * Constructor.
                * @param cx the x-coordinate of the center
                * @param cy the y-cooridinate of the center
                */
            private:
               RRdot_2D(double cx, double cy) throws();

               /** Destructor */
            public:
               ~RRdot_2D() throws();

               /**
                * Get the x-coordinate of the center
                * @return the x-coordinate of the center
                */
            public:
               inline double centerX() const throws()
               {
                  return _cx;
               }

               /**
                * Get the y-coordinate of the center
                * @return the y-coordinate of the center
                */
            public:
               inline double centerY() const throws()
               {
                  return _cy;
               }

               /**
                * Get a polar coordinate frame.
                * @param cx the x-coordinate of the center
                * @param cy the y-cooridinate of the center
                * @return a coordinate frame
                */
            public:
               static ::timber::Reference< RRdot_2D> create(double cx, double cy) throws();

               size_t dimension() const throws();
               bool equals(const CoordinateFrame& f) const throws();

               /** The center */
            private:
               double _cx, _cy;
         };

      }
   }
}

#endif
