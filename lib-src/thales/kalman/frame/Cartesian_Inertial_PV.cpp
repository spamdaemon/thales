#include <thales/kalman/frame/Cartesian_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>

namespace thales {
  namespace kalman {
    namespace frame {
      Cartesian_Inertial_PV::Cartesian_Inertial_PV(size_t n) throws()
      : CartesianInertial(n,2*n)
      {
      }

      Cartesian_Inertial_PV::~Cartesian_Inertial_PV() throws()
      {}
      
      ::timber::Reference<Cartesian_Inertial_PV> Cartesian_Inertial_PV::create(size_t n) throws()
      {
	switch(n) {
	case 2:
	  return Cartesian_2D_Inertial_PV::create();
	case 3:
	  return Cartesian_3D_Inertial_PV::create();
	default:
	  return new Cartesian_Inertial_PV(n);
	}
      }

    }
  }
}
