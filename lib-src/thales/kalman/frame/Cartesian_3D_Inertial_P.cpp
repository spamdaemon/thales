#include <thales/kalman/frame/Cartesian_3D_Inertial_P.h>

namespace thales {
  namespace kalman {
    namespace frame {
      Cartesian_3D_Inertial_P::Cartesian_3D_Inertial_P() throws()
      : Cartesian_Inertial_P(3)
      {}
      
      Cartesian_3D_Inertial_P::~Cartesian_3D_Inertial_P() throws()
      {}
      
      ::timber::Reference<Cartesian_3D_Inertial_P> Cartesian_3D_Inertial_P::create() throws()
      {
	return new Cartesian_3D_Inertial_P();
      }


    }
  }
}
