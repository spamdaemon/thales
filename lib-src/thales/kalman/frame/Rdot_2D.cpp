#include <thales/kalman/frame/Rdot_2D.h>

namespace thales {
  namespace kalman {
    namespace frame {

      Rdot_2D::Rdot_2D(double cx, double cy) throws()
      : _cx(cx),_cy(cy)
      {}

      Rdot_2D::~Rdot_2D() throws()
      {}
      
      ::timber::Reference<Rdot_2D> Rdot_2D::create(double cx, double cy) throws()
      {
	return new Rdot_2D(cx,cy);
      }

      bool Rdot_2D::equals(const CoordinateFrame& f) const throws()
      {
	if (CoordinateFrame::equals(f)) {
	  const Rdot_2D& pf = dynamic_cast<const Rdot_2D&>(f);
	  return pf._cx == _cx && pf._cy == _cy;
	}
	return false;
      }

      size_t Rdot_2D::dimension() const throws()
      {
	return 1;
      }

    }
  }
}
