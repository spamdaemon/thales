#ifndef _THALES_KALMAN_FRAME_CARTESIAN_1D_INERTIAL_P_H
#define _THALES_KALMAN_FRAME_CARTESIAN_1D_INERTIAL_P_H

#ifndef _THALES_KALMAN_CARTESIANINERTIAL_H
#include <thales/kalman/CartesianInertial.h>
#endif

namespace thales {
   namespace kalman {
      namespace frame {

         /**
          * This class represents an inertial _1D cartesian coordinate frame
          * with position.
          */
         class Cartesian_1D_Inertial_P : public CartesianInertial
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_1D_Inertial_P);

               /** Default constructor */
            private:
               Cartesian_1D_Inertial_P() throws();

               /** Destructor */
            public:
               ~Cartesian_1D_Inertial_P() throws();

               /**
                * Get a cartesian coordinate frame.
                * @return a coordinate frame
                */
            public:
               static ::timber::Reference< Cartesian_1D_Inertial_P> create() throws();
         };

      }
   }
}

#endif
