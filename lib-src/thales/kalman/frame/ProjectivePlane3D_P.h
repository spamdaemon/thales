#ifndef _THALES_KALMAN_FRAME_PROJECTIVEPLANE3D_P_H
#define _THALES_KALMAN_FRAME_PROJECTIVEPLANE3D_P_H

#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#include <thales/kalman/CoordinateFrame.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

namespace thales {
   namespace kalman {
      namespace frame {

         /**
          * This coordinate frame is plane in 3D with with a focal point a given distance
          * away from the plane.  The inertial frame is a 3D cartesian frame, but the
          * dimension of this plane is, of course, 2.
          */
         class ProjectivePlane3D_P : public CoordinateFrame
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(ProjectivePlane3D_P);

               /**
                * Create a new projective plane. The distance of the focal point to the plane
                * must be greater than 0.
                * @param fp the focal point
                * @param f the distance of the plane from the focal point
                * @param n the plane normal
                * @param x the x-axis vector
                * @pre f>0
                * @throws ::std::exception if the plane could not be set up
                */
            public:
               ProjectivePlane3D_P(const ::newton::Vector< double>& fp, double f, const ::newton::Vector< double>& n,
                     const ::newton::Vector< double>& x) throws (::std::exception);

               /** Destructor */
            public:
               ~ProjectivePlane3D_P() throws();

               /**
                * Create a projective plane in terms of points rather than vectors. This reflects
                * basically how a camera might be set up.
                * @param f the focal length
                * @param fp the focal point or position
                * @param center the location of a point that appears at (0,0) on the plane
                * @param right the point that appears on the positive x-axis (+,0)
                * @return a projection plane
                * @throws ::std::exception if the plane could not be set up
                */
            public:
               static ::timber::Reference< ProjectivePlane3D_P> create(double f, const ::newton::Vector< double>& fp,
                     const ::newton::Vector< double>& center,
                     const ::newton::Vector< double>& right) throws (::std::exception);

               /**
                * Get the focal length.
                * @return the distance of the focal point from the projection plane
                */
            public:
               inline double focalLength() const throws()
               {
                  return _focalDistance;
               }

               /**
                * Get the focal point.
                * @return the focal point in the inertial coordinate frame.
                */
            public:
               inline const ::newton::Vector< double>& focalPoint() const throws()
               {
                  return _focalPoint;
               }

               /**
                * Get the plane normal
                * @return the plane normal
                */
            public:
               inline const ::newton::Vector< double>& planeNormal() const throws()
               {
                  return _planeNormal;
               }

               /**
                * Get the normal vector representing the positive y-axis in the plane
                * @return the y-axis vector
                */
            public:
               inline const ::newton::Vector< double>& yAxis() const throws()
               {
                  return _yAxis;
               }

               /**
                * Get the normal vector representing the positive x-axis in the plane
                * @return the x-axis vector
                */
            public:
               inline const ::newton::Vector< double>& xAxis() const throws()
               {
                  return _xAxis;
               }

               /**
                * Get the rotation matrix that rotates around the focal point until
                * the projection plane is equal to z=focalLength().
                * @return a rotation matrix
                */
            public:
               inline const ::newton::Matrix< double>& rotationMatrix() const throws()
               {
                  return _rotation;
               }

               /**
                * Project a 3D point onto the plane. The point must lie on the positive
                * side of the normal and thus have a distance to the focal point that is larger
                * than the focalLength().
                * @param pt a 3D point
                * @param res the resulting 2D point
                * @return true if the point was projected, false otherwise
                */
            public:
               bool projectPoint(const ::newton::Vector< double>& pt, ::newton::Vector< double>& res) const throws();

               /**
                * Project a 3D point onto the plane. The point must lie on the positive
                * side of the normal and thus have a distance to the focal point that is larger
                * than the focalLength().
                * @param x x-coordinate of point to be projected
                * @param y y-coordinate of point to be projected
                * @param z z-coordinate of point to be projected
                * @param px x-coordinate of projected point (on the projection plane)
                * @param py y-coordinate of projected point (on the projection plane)
                * @return true if the point was projected, false otherwise
                */
            public:
               bool projectPoint(double x, double y, double z, double& px, double& py) const throws();

               /**
                * Get the 3D location of a point that lies on the projection plane.
                * @param x x-coordinate of a point in the projection plane coordinates
                * @param y y-coordinate of a point in the projection plane coordinates
                * @return the 3D point that is the equivalent to the 2D projection point
                */
            public:
               ::newton::Vector< double> computePoint(double x, double y) const throws();

               bool equals(const CoordinateFrame& f) const throws();
               ::timber::Pointer< CartesianInertial> getInertialFrame() const throws();
               size_t dimension() const throws();

               /** The focal point in the inertial coordinate frame */
            private:
               const ::newton::Vector< double> _focalPoint;

               /** The normal of the focal plane */
            private:
               const ::newton::Vector< double> _planeNormal;

               /** The plane's x-axis */
            private:
               const ::newton::Vector< double> _xAxis;

               /** The plane's y-axis */
            private:
               const ::newton::Vector< double> _yAxis;

               /** The rotation matrix */
            private:
               ::newton::Matrix< double> _rotation;

               /** The distance of the plane from the focal point */
            private:
               const double _focalDistance;
         };
      }
   }
}

#endif
