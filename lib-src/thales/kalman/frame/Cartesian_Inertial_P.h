#ifndef _THALES_KALMAN_FRAME_CARTESIAN_INERTIAL_P_H
#define _THALES_KALMAN_FRAME_CARTESIAN_INERTIAL_P_H

#ifndef _THALES_KALMAN_CARTESIANINERTIAL_H
#include <thales/kalman/CartesianInertial.h>
#endif

namespace thales {
   namespace kalman {
      namespace frame {

         /**
          * This class represents an inertial  cartesian coordinate frame
          * with position in a given dimension D.
          */
         class Cartesian_Inertial_P : public CartesianInertial
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_Inertial_P);

               /**
                * Constructor. The number of components in the state space
                * for this frame is 2*n.
                * @param n the size of the embedding dimension
                **/
            protected:
               Cartesian_Inertial_P(size_t n) throws();

               /** Destructor */
            public:
               ~Cartesian_Inertial_P() throws();

               /**
                * Get a cartesian coordinate frame. For certain dimensions,
                * specialized classes may be returned.
                * @param n the dimension
                * @pre n>0
                * @return a coordinate frame
                */
            public:
               static ::timber::Reference< Cartesian_Inertial_P> create(size_t n) throws();
         };

      }
   }
}

#endif
