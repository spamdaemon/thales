#include <thales/kalman/frame/RRdot_2D.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>

namespace thales {
  namespace kalman {
    namespace frame {

      RRdot_2D::RRdot_2D(double cx, double cy) throws()
      : _cx(cx),_cy(cy)
      {}

      RRdot_2D::~RRdot_2D() throws()
      {}
      
      ::timber::Reference<RRdot_2D> RRdot_2D::create(double cx, double cy) throws()
      {
	return new RRdot_2D(cx,cy);
      }

      bool RRdot_2D::equals(const CoordinateFrame& f) const throws()
      {
	if (CoordinateFrame::equals(f)) {
	  const RRdot_2D& pf = dynamic_cast<const RRdot_2D&>(f);
	  return pf._cx == _cx && pf._cy == _cy;
	}
	return false;
      }

      size_t RRdot_2D::dimension() const throws()
      {
	return 3;
      }
    }
  }
}
