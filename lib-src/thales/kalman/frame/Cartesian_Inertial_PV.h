#ifndef _THALES_KALMAN_FRAME_CARTESIAN_INERTIAL_PV_H
#define _THALES_KALMAN_FRAME_CARTESIAN_INERTIAL_PV_H

#ifndef _THALES_KALMAN_CARTESIANINERTIAL_H
#include <thales/kalman/CartesianInertial.h>
#endif

namespace thales {
   namespace kalman {
      namespace frame {

         /**
          * This class represents an inertial  cartesian coordinate frame
          * with position and velocity in a given dimension D. The first D
          * components represent position, and the last D components
          * represent the velocity.
          */
         class Cartesian_Inertial_PV : public CartesianInertial
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_Inertial_PV);

               /**
                * Constructor. The number of components in the state space
                * for this frame is 2*n.
                * @param n the size of the embedding dimension
                **/
            protected:
               Cartesian_Inertial_PV(size_t n) throws();

               /** Destructor */
            public:
               ~Cartesian_Inertial_PV() throws();

               /**
                * Get a cartesian coordinate frame. For certain dimensions,
                * specialized classes may be returned.
                * @param n the dimension
                * @pre n>0
                * @return a coordinate frame
                */
            public:
               static ::timber::Reference< Cartesian_Inertial_PV> create(size_t n) throws();
         };

      }
   }
}

#endif
