#include <thales/kalman/frame/ProjectivePlane3D_P.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_P.h>
#include <cassert>

using namespace ::std;
using namespace ::newton;
using namespace ::timber;

namespace thales {
  namespace kalman {
    namespace frame {
      namespace {
	static Vector<double> orthogonalVector(const Vector<double>& a, const Vector<double>& b)
	{
	  Vector<double> res(3);
	  res(0) = a(1)*b(2) - a(2)*b(1);
	  res(1) = a(2)*b(0) - a(0)*b(2);
	  res(2) = a(0)*b(1) - a(1)*b(0);
	  
	  return res;
	}
	
	static Vector<double> normalize(const Vector<double>& v)
	{ return v/v.length(); }

	
      }

      ProjectivePlane3D_P::ProjectivePlane3D_P(const Vector<double>& fp, double f, const Vector<double>& n, const Vector<double>& x) throws(::std::exception)
      : _focalPoint(fp),
	_planeNormal(n/n.length()),
	_xAxis(x/x.length()),
	_yAxis(normalize(orthogonalVector(x,n))),
	_rotation(3,3),_focalDistance(f)
      {
	if (!(f>0)) {
	  throw ::std::runtime_error("Invalid focal length");
	}
	
	// build the rotation matrix here
	_rotation(0,0) = _xAxis(0);
	_rotation(0,1) = _xAxis(1);
	_rotation(0,2) = _xAxis(2);
	_rotation(1,0) = _yAxis(0);
	_rotation(1,1) = _yAxis(1);
	_rotation(1,2) = _yAxis(2);
	_rotation(2,0) = _planeNormal(0);
	_rotation(2,1) = _planeNormal(1);
	_rotation(2,2) = _planeNormal(2);
      }

      Reference<ProjectivePlane3D_P> ProjectivePlane3D_P::create(double f, 
								 const Vector<double>& fp,
								 const Vector<double>& center,
								 const Vector<double>& right) throws (::std::exception)
      {
	Vector<double> n= center-fp;
	Vector<double> x= right-center;
	// at this point, x is not orthogonal, so we make it orthogonal
	// by first findind the y-vector which will be orthogonal to both x and n
	// and the resetting x by computing the orthogonal vector of n and y
	Vector<double> y= orthogonalVector(x,n);
	x = orthogonalVector(n,y);
	return new ProjectivePlane3D_P(fp,f,n,x);
      }


      bool ProjectivePlane3D_P::projectPoint (double x, double y, double z, double& px, double& py) const throws()
      {
	x -= _focalPoint(0);
	y -= _focalPoint(1);
	z -= _focalPoint(2);

	double pz = _rotation(2,0)*x + _rotation(2,1)*y + _rotation(2,2)*z;
	if (::std::abs(pz) < 1E-8) {
	  // too close to focal point, but we can just project into 0,0
	  px = 0;
	  py = 0;
	}
	else {
	  const double c = _focalDistance / pz;
	  
	  px = c*(_rotation(0,0)*x + _rotation(0,1)*y + _rotation(0,2)*z);
	  py = c*(_rotation(1,0)*x + _rotation(1,1)*y + _rotation(1,2)*z);
	}
	return true;
      }


      bool ProjectivePlane3D_P::projectPoint (const Vector<double>& pt, Vector<double>& res) const throws()
      { 
	double px,py;
	if (!projectPoint(pt(0),pt(1),pt(2),px,py)) {
	  return false;
	}
	if (res.dimension()!=2) {
	  Vector<double>(px,py).swap(res);
	}
	else {
	  res(0) = px;
	  res(1) = py;
	}
	return true;
      }

      Vector<double> ProjectivePlane3D_P::computePoint (double x, double y) const throws()
      {
	double px = _focalPoint(0) + _focalDistance*_planeNormal(0) + x*_xAxis(0) + y*_yAxis(0);
	double py = _focalPoint(1) + _focalDistance*_planeNormal(1) + x*_xAxis(1) + y*_yAxis(1);
	double pz = _focalPoint(2) + _focalDistance*_planeNormal(2) + x*_xAxis(2) + y*_yAxis(2);

	return Vector<double>(px,py,pz);
      }


      ProjectivePlane3D_P::~ProjectivePlane3D_P() throws()
      {}

      bool ProjectivePlane3D_P::equals(const CoordinateFrame& f) const throws()
      {
	const ProjectivePlane3D_P* pf = dynamic_cast<const ProjectivePlane3D_P*>(&f);
	return pf!=0 && 
	  pf->_focalDistance==_focalDistance &&
	  pf->_planeNormal == _planeNormal &&
	  pf->_focalPoint==_focalPoint;
      }

      Pointer<CartesianInertial> ProjectivePlane3D_P::getInertialFrame() const throws()
      { return Cartesian_3D_Inertial_P::create(); }

      size_t ProjectivePlane3D_P::dimension() const throws()
      { return 2; }

    }
  }
}
