#ifndef _THALES_KALMAN_FRAME_CARTESIAN_3D_INERTIAL_P_H
#define _THALES_KALMAN_FRAME_CARTESIAN_3D_INERTIAL_P_H

#ifndef _THALES_KALMAN_FRAME_CARTESIAN_INERTIAL_P_H
#include <thales/kalman/frame/Cartesian_Inertial_P.h>
#endif

namespace thales {
   namespace kalman {
      namespace frame {

         /**
          * This class represents an inertial 3D cartesian coordinate frame
          * with position.
          */
         class Cartesian_3D_Inertial_P : public Cartesian_Inertial_P
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_3D_Inertial_P);

               /** Default constructor */
            private:
               Cartesian_3D_Inertial_P() throws();

               /** Destructor */
            public:
               ~Cartesian_3D_Inertial_P() throws();

               /**
                * Get a cartesian coordinate frame.
                * @return a coordinate frame
                */
            public:
               static ::timber::Reference< Cartesian_3D_Inertial_P> create() throws();
         };

      }
   }
}

#endif
