#include <thales/kalman/frame/Polar_2D_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>

namespace thales {
  namespace kalman {
    namespace frame {

      Polar_2D_P::Polar_2D_P(double cx, double cy) throws()
      : _cx(cx),_cy(cy)
      {}

      Polar_2D_P::~Polar_2D_P() throws()
      {}

      ::timber::Reference<Polar_2D_P> Polar_2D_P::create(double cx, double cy) throws()
      {
	return new Polar_2D_P(cx,cy);
      }

      bool Polar_2D_P::equals(const CoordinateFrame& f) const throws()
      {
	if (CoordinateFrame::equals(f)) {
	  const Polar_2D_P& pf = dynamic_cast<const Polar_2D_P&>(f);
	  return pf._cx == _cx && pf._cy == _cy;
	}
	return false;
      }

      ::newton::Vector<double> Polar_2D_P::difference(const ::newton::Vector<double>& x, const ::newton::Vector<double>& y) const throws()
      {
	const double PI = 3.14159265358979323846;

	::newton::Vector<double> res(x-y);
	if (res(0) > PI) {
	  res(0) -= 2*PI;
	}
	else if (res(0) < -PI) {
	  res(0) += 2*PI;
	}
	return res;
      }

      size_t Polar_2D_P::dimension() const throws()
      {
	return 2;
      }

      ::timber::Pointer<CartesianInertial> Polar_2D_P::getInertialFrame() const throws()
      { return Cartesian_2D_Inertial_P::create(); }
    }
  }
}
