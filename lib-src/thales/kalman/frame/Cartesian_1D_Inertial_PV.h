#ifndef _THALES_KALMAN_FRAME_CARTESIAN_1D_INERTIAL_PV_H
#define _THALES_KALMAN_FRAME_CARTESIAN_1D_INERTIAL_PV_H

#ifndef _THALES_KALMAN_CARTESIANINERTIAL_H
#include <thales/kalman/CartesianInertial.h>
#endif

namespace thales {
  namespace kalman {
    namespace frame {

      /**
       * This class represents an inertial _1D cartesian coordinate frame 
       * with position. 
       */
      class Cartesian_1D_Inertial_PV : public CartesianInertial {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_1D_Inertial_PV);

	/** Default constructor */
      private:
	Cartesian_1D_Inertial_PV () throws();
	
	/** Destructor */
      public:
	~Cartesian_1D_Inertial_PV() throws();

	/**
	 * Get a cartesian coordinate frame.
	 * @return a coordinate frame
	 */
      public:
	static ::timber::Reference<Cartesian_1D_Inertial_PV> create() throws();
      };

    }
  }
}

#endif
