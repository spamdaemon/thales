#include <thales/kalman/frame/Spherical_P.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_P.h>

namespace thales {
   namespace kalman {
      namespace frame {

         Spherical_P::Spherical_P(double cx, double cy, double cz) throws()
               : _cx(cx), _cy(cy), _cz(cz)
         {
         }

         Spherical_P::~Spherical_P() throws()
         {
         }

         ::timber::Reference< Spherical_P> Spherical_P::create(double cx, double cy, double cz) throws()
         {
            return new Spherical_P(cx, cy, cz);
         }

         bool Spherical_P::equals(const CoordinateFrame& f) const throws()
         {
            if (CoordinateFrame::equals(f)) {
               const Spherical_P& pf = dynamic_cast< const Spherical_P&>(f);
               return pf._cx == _cx && pf._cy == _cy && pf._cz == _cz;
            }
            return false;
         }

         ::newton::Vector< double> Spherical_P::difference(const ::newton::Vector< double>& x,
               const ::newton::Vector< double>& y) const throws()
         {
            const double PI = 3.14159265358979323846;

            ::newton::Vector< double> res(x - y);
            if (res(0) > PI) {
               res(0) -= 2 * PI;
            }
            else if (res(0) < -PI) {
               res(0) += 2 * PI;
            }
            return res;
         }

         size_t Spherical_P::dimension() const throws()
         {
            return 3;
         }

         ::timber::Pointer< CartesianInertial> Spherical_P::getInertialFrame() const throws()
         {
            return Cartesian_3D_Inertial_P::create();
         }
      }
   }
}
