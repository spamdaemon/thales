#include <thales/kalman/frame/Cartesian_1D_Inertial_P.h>

namespace thales {
  namespace kalman {
    namespace frame {
      Cartesian_1D_Inertial_P::Cartesian_1D_Inertial_P() throws()
      : CartesianInertial(1,1)
      {}

      Cartesian_1D_Inertial_P::~Cartesian_1D_Inertial_P() throws()
      {}
      
      ::timber::Reference<Cartesian_1D_Inertial_P> Cartesian_1D_Inertial_P::create() throws()
      {
	return new Cartesian_1D_Inertial_P();
      }


    }
  }
}
