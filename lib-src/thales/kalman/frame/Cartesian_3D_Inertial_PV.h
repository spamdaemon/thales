#ifndef _THALES_KALMAN_FRAME_CARTESIAN_3D_INERTIAL_PV_H
#define _THALES_KALMAN_FRAME_CARTESIAN_3D_INERTIAL_PV_H

#ifndef _THALES_KALMAN_FRAME_CARTESIAN_INERTIAL_PV_H
#include <thales/kalman/frame/Cartesian_Inertial_PV.h>
#endif

namespace thales {
   namespace kalman {
      namespace frame {

         /**
          * This class represents an inertial 3D cartesian coordinate frame
          * with position and velocity.
          */
         class Cartesian_3D_Inertial_PV : public Cartesian_Inertial_PV
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_3D_Inertial_PV);

               /** Default constructor */
            private:
               Cartesian_3D_Inertial_PV() throws();

               /** Destructor */
            public:
               ~Cartesian_3D_Inertial_PV() throws();

               /**
                * Get a cartesian coordinate frame.
                * @return a coordinate frame
                */
            public:
               static ::timber::Reference< Cartesian_3D_Inertial_PV> create() throws();
         };

      }
   }
}

#endif
