#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>

namespace thales {
  namespace kalman {
    namespace frame {
      Cartesian_2D_Inertial_PV::Cartesian_2D_Inertial_PV() throws()
      : Cartesian_Inertial_PV(2)
      {}

      Cartesian_2D_Inertial_PV::~Cartesian_2D_Inertial_PV() throws()
      {}
      
      ::timber::Reference<Cartesian_2D_Inertial_PV> Cartesian_2D_Inertial_PV::create() throws()
      {
	return new Cartesian_2D_Inertial_PV();
      }

    }
  }
}
