#include <thales/kalman/frame/Cartesian_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>

namespace thales {
  namespace kalman {
    namespace frame {
      Cartesian_Inertial_P::Cartesian_Inertial_P(size_t n) throws()
      : CartesianInertial(n,n)
      {
      }

      Cartesian_Inertial_P::~Cartesian_Inertial_P() throws()
      {}
      
      ::timber::Reference<Cartesian_Inertial_P> Cartesian_Inertial_P::create(size_t n) throws()
      {
	switch(n) {
	case 2:
	  return Cartesian_2D_Inertial_P::create();
	case 3:
	  return Cartesian_3D_Inertial_P::create();
	default:
	  return new Cartesian_Inertial_P(n);
	}
      }

    }
  }
}
