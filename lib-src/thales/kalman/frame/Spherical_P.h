#ifndef _THALES_KALMAN_FRAME_SPHERICAL_P_H
#define _THALES_KALMAN_FRAME_SPHERICAL_P_H

#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#include <thales/kalman/CoordinateFrame.h>
#endif

namespace thales {
   namespace kalman {
      namespace frame {

         /**
          * This class represents an 3D spherical coordinate frame
          * with position. The components of this frame are. The elevatation
          * is computed with respect to plane in which the azimuth angle is defined
          * and points in the direction of the z-axis.
          * <ol>
          * <li>azimuth angle (radians)
          * <li>elevation angle (radians, range -PI/2 to PI/2)
          * <li>range  (positive)
          * </ol>
          */
         class Spherical_P : public CoordinateFrame
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Spherical_P);

               /**
                * Constructor.
                * @param cx the x-coordinate of the center
                * @param cy the y-coordinate of the center
                * @param cz the z-coordinate of the center
                */
            private:
               Spherical_P(double cx, double cy, double cz) throws();

               /** Destructor */
            public:
               ~Spherical_P() throws();

               /**
                * Get the x-coordinate of the center
                * @return the x-coordinate of the center
                */
            public:
               inline double centerX() const throws()
               {
                  return _cx;
               }

               /**
                * Get the y-coordinate of the center
                * @return the y-coordinate of the center
                */
            public:
               inline double centerY() const throws()
               {
                  return _cy;
               }

               /**
                * Get the z-coordinate of the center
                * @return the z-coordinate of the center
                */
            public:
               inline double centerZ() const throws()
               {
                  return _cz;
               }

               /**
                * Get a polar coordinate frame.
                * @param cx the x-coordinate of the center
                * @param cy the y-coordinate of the center
                * @param cz the z-coordinate of the center
                * @return a coordinate frame
                */
            public:
               static ::timber::Reference< Spherical_P> create(double cx, double cy, double cz) throws();

               bool equals(const CoordinateFrame& f) const throws();
               ::newton::Vector< double> difference(const ::newton::Vector< double>& x,
                     const ::newton::Vector< double>& y) const throws();
               size_t dimension() const throws();
               ::timber::Pointer< CartesianInertial> getInertialFrame() const throws();

               /** The center */
            private:
               double _cx, _cy, _cz;
         };

      }
   }
}

#endif
