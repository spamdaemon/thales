#ifndef _THALES_KALMAN_FRAME_CARTESIAN_2D_RELATIVE_PRDOTV_H
#define _THALES_KALMAN_FRAME_CARTESIAN_2D_RELATIVE_PRDOT_H

#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#include <thales/kalman/CoordinateFrame.h>
#endif

namespace thales {
  namespace kalman {
    namespace frame {

      /**
       * This class represents an relative 2D cartesian coordinate frame 
       * with cartesian position and a velocity along a range vector. The 
       * components of this frame are:
       * <ol>
       * <li>x 
       * <li>y
       * <li>velocity along the vector (x-centerx), y-centery
       * </ol> 
       */
      class Cartesian_2D_Relative_PRdot : public CoordinateFrame {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Cartesian_2D_Relative_PRdot);

	/** Default constructor */
      private:
	Cartesian_2D_Relative_PRdot (double cx, double cy) throws();
	
	/** Destructor */
      public:
	~Cartesian_2D_Relative_PRdot() throws();

	/**
	 * Get a cartesian coordinate frame.
	 * @return a coordinate frame
	 */
      public:
	static ::timber::Reference<Cartesian_2D_Relative_PRdot> create(double cx, double cy) throws();

	/**
	 * Get the x-coordinate of the center
	 * @return the x-coordinate of the center
	 */
      public:
	inline double centerX() const throws() { return _cx; }

	/**
	 * Get the y-coordinate of the center
	 * @return the y-coordinate of the center
	 */
      public:
	inline double centerY() const throws() { return _cy; }

	bool equals(const CoordinateFrame& f) const throws();
	size_t dimension() const throws();
	::timber::Pointer<CartesianInertial> getInertialFrame() const throws();

	/** The center */
      private:
	double _cx,_cy;
      };

    }
  }
}

#endif
