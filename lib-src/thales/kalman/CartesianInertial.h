#ifndef _THALES_KALMAN_CARTESIANINERTIAL_H
#define _THALES_KALMAN_CARTESIANINERTIAL_H

#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#include <thales/kalman/CoordinateFrame.h>
#endif

namespace thales {
  namespace kalman {

    /**
     * This is the base class for all inertial Cartesian coordinate frames.
     * This inertial frame uses two types of dimensions:
     * <ol>
     *   <li>cartesianDimension() defines the dimension of the underlying space of this frame. 
     *   <li>dimension() returns the number of components used to represent an object in this coordinate system.
     * </ol>
     * The extra components for instances of this coordinate frame represent usually other linear <em>things</em>,
     * such as velocity or acceleration. However, the first cartesianDimension() components represent a cartesian
     * location.
     */
    class CartesianInertial : public CoordinateFrame {
      CANOPY_BOILERPLATE_PREVENT_COPYING(CartesianInertial);

      /** Destructor */
    public:
      ~CartesianInertial() throws();

      /**
       * Create an new instance of an inertial cartesian coordinate frame.
       * @param cd the cartesian dimension
       * @param sd the space dimension
       * @pre cd > 0
       * @pre sd >= cd
       */
    protected:
      CartesianInertial(size_t cd,  size_t sd) throws();

      /**
       * Get the dimension of the space.
       * @return the dimension of the space
       */
    public:
      size_t dimension() const throws();

      /**
       * Get the cartesian dimension of this frame.
       * @return the cartesian dimension of this frame
       */
    public:
      inline size_t cartesianDimension() const throws()
      { return _cartesianDimension; }

       /**
       * Get the inertial frame.
       * @return this frame
       */
    public:
      ::timber::Pointer<CartesianInertial> getInertialFrame() const throws();

      /** The size of the cartesian space */
    private:
      const size_t _cartesianDimension;

      /** The size of the space */
    private:
      const size_t _spaceDimension;

    };
  }
}

#endif
