#include <thales/kalman/Gaussian.h>
#include <thales/kalman/GaussianTransform.h>
#include <thales/kalman/CartesianInertial.h>
#include <thales/kalman/Innovation.h>
#include <thales/probability/NormalDistribution.h>
#include <timber/AtomicPointer.h>
#include <newton/Matrix.h>
#include <timber/logging.h>

#include <cmath>
#include <iostream>

#ifndef THALES_USE_ALLOCA
#define THALES_USE_ALLOCA 0
#endif

#if THALES_USE_ALLOCA == 1
#include <string.h>
#include <alloca.h>
#endif

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::newton;

namespace thales {
   namespace kalman {

      static const char* LOGGER = "thales.kalman.Gaussian";

      class Gaussian::PreparedKalmanUpdate : public PreparedUpdate
      {
         public:
            PreparedKalmanUpdate(const Reference< Gaussian>& source, const Reference< Gaussian>& z,
                  unique_ptr< Innovation> innov) throws (exception)
                  : _pre(source), _measurement(z), _innovation(move(innov))
            {
               assert(_innovation.get() && "No innovation specified");
            }

         public:
            ~PreparedKalmanUpdate() throws()
            {
            }

            ::timber::Reference< Gaussian> preUpdate() const throws()
            {
               return _pre;
            }

            /**
             * Get the measurement.
             * @return the measurement with which the preUpdateFilter() is updated.
             */
         public:
            ::timber::Reference< Gaussian> measurement() const throws()
            {
               return _measurement;
            }

            /**
             * Get the post-update filter that results from this update.
             * @return the filter that results from an update
             */
         public:
            ::timber::Reference< Gaussian> postUpdate() const throws(::std::exception)
            {
               Pointer< Gaussian> post = _post.get();
               if (!post) {
                  post = _pre->doKalmanUpdate(*_innovation);
                  if (!_post.setOnce(post)) {
                     post = _post.get();
                  }
               }
               return post;
            }

            /**
             * Compute the square of the Mahalanobis distance between this update and its measurement.
             * @return the square of the Mahalanobis distance between the measurement and original filter.
             */
         public:
            double mahalanobisDistance2() const throws()
            {
               return _innovation->mahalanobisDistance2();
            }

            /**
             * Get the likelihood that measurements is associated with the original filter.
             * @return the likelihood
             */
         public:
            Likelihood likelihood() const throws()
            {
               return _innovation->likelihood();
            }

         private:
            Reference< Gaussian> _pre;
            Reference< Gaussian> _measurement;
            mutable ::timber::AtomicPointer< Gaussian> _post;
            unique_ptr< Innovation> _innovation;
      };

      Gaussian::Gaussian(const Reference< CoordinateFrame>& f) throws()
            : _mean(f->dimension()), _covariance(f->dimension()), _frame(f)
      {
      }

      Gaussian::Gaussian(const Reference< CoordinateFrame>& f, Vector< double>  center,   Covariance  covar
            ) throws(exception)
            : _mean(::std::move(center)), _covariance(::std::move(covar)), _frame(f)
      {
         if (_mean.dimension() != _covariance.dimension()) {
            throw invalid_argument("Invalid dimensions");
         }
         if (_mean.dimension() != f->dimension()) {
            throw invalid_argument("Invalid coordinate frame");
         }
      }

      Gaussian::~Gaussian() throws()
      {
      }

      Reference< Gaussian> Gaussian::create(const Reference< CoordinateFrame>& xframe) throws()
      {
         return new Gaussian(xframe);
      }

      Reference< Gaussian> Gaussian::create(const Reference< CoordinateFrame>& xframe, Vector< double>  xmean,
           Covariance xcovar) throws (::std::exception)
      {
         return new Gaussian(xframe, ::std::move(xmean), ::std::move(xcovar));
      }

      Pointer< Gaussian> Gaussian::findMeasurement(const Reference< Gaussian>& prior,
            const Reference< Gaussian>& post) throws()
      {
         assert(prior->_frame->equals(*post->_frame));
         // Gain calculation is:
         //  K = P*Ht*(H*P*Ht + R)^-1
         // given H=I :
         //  K = P*(P+R)^-1  <--> K^-1 = (P+R)*P^-1 = I+R*P^-1
         //
         // Post state Q is
         //  Q = (I-KH)*P = (I-K)*P
         //  Q*P^-1 = I-K
         //  I-Q*P^-1 = K   <--> K^-1 = (I-Q*P^-1)^-1 = I+R*P^-1
         //  (inv(I-Q*inv(P)) -I) * P = R
         // Also:
         //  K = I-Q*inv(P) = P*inv(P) - Q*inv(P) = (P-Q)*inv(P)
         //  --> inv(K) = P*inv(P-Q)
         //  --> R = inv(K)*P-P = P*inv(P-Q)*P - P
         //
         // Check:
         //  Q = (I-K)*P = (I-P*inv(P+R))*P
         //  I-Q*inv(P) = P*inv(P+R)
         //  inv(I-Q*inv(P)) = (P+R)*inv(P)
         //  inv(I-Q*inv(P))*P = P+R
         //  inv(I-Q*inv(P))*P - P = R --> R = inv(K)*P - P

         // PX=Q --> X = inv(P)*Q = t(t(Q)*t(inv(P))) = t(Q*inv(P)) = Q*inv(P)
         // --> solving PX=Q for X is teh same as computing Q*inv(P)
         // --> since P is symmetric, we can use Cholesky!
         //  K = I-Q*inv(P) = I-Q*inv(ch(P)*t(ch(P))) = I-Q*inv(t(ch(P)))*inv(ch(P))

         const Matrix< double>& Q = post->covariance().matrix();
         const Matrix< double>& P = prior->covariance().matrix();

         try {
            Matrix< double> tmpMatrix(P - Q);
            Vector< double> tmpVector(post->mean() - prior->mean());
            Vector< double> z(post->mean() - prior->mean());

            Cholesky< double> ch(tmpMatrix);
            ch.doForwardSubstitution(P, tmpMatrix);
            Matrix< double> cvar(tmpMatrix * tmpMatrix);
            Covariance::makeSymmetric(cvar);
            cvar -= P;

            ch.doForwardSubstitution(tmpVector, z);
            ch.doBackwardSubstitution(z, tmpVector);
            P.preMultiply(tmpVector, z);

            z += prior->mean();

            return new Gaussian(post->_frame, z, cvar);
         }
         catch (const ::std::exception& e) {
            // cholesky failed; that was to be expected for certain types of matrices
            return Pointer< Gaussian>();
         }

      }

      Reference< Gaussian> Gaussian::transform(const Reference< CoordinateFrame>& toFrame,
            const GaussianTransform& HF) const throws(::std::exception)
      {
         Vector< double> toX;
         Matrix< double> toP;
         if (HF.apply(*_frame, _mean, _covariance.matrix(), *toFrame, toX, &toP, 0)) {
            return new Gaussian(toFrame, toX, toP);
         }
         else {
            // identity transform, nothing was done
            return ::timber::Reference< Gaussian>(const_cast< Gaussian*>(this));
         }
      }

      Pointer< Gaussian> Gaussian::transform(const Reference< CoordinateFrame>& toFrame) const throws()
      {
         // there may be a lot of attempt to convert an inertial frame into an inertial frame,
         // which is an identity, so just return this estimate
         if (toFrame == _frame) {
            return ::timber::Pointer< Gaussian>(const_cast< Gaussian*>(this));
         }

         try {
            Reference< GaussianTransform> tx = GaussianTransform::find(*_frame, *toFrame);
            return transform(toFrame, *tx);
         }
         catch (const ::std::exception& e) {
        	 LogEntry entry(LOGGER);
        	 entry << "Failed to transform from " << typeid(*_frame).name() << " to "
                  << typeid(*toFrame).name() << " due to exception " << e.what() << doLog;
         }
         catch (...) {
            Log(LOGGER).caughtUnexpected();

         }
         return Pointer< Gaussian>();
      }

      Vector< double> Gaussian::transformMean(const Reference< CoordinateFrame>& toFrame,
            const GaussianTransform& HF) const throws(::std::exception)
      {
         Vector< double> toX;
         if (HF.apply(*_frame, _mean, _covariance.matrix(), *toFrame, toX, nullptr, 0)) {
            return ::std::move(toX);
         }
         else {
            return _mean;
         }
      }

      Vector< double> Gaussian::transformMean(const Reference< CoordinateFrame>& toFrame) const throws(::std::exception)
      {
         // there may be a lot of attempt to convert an inertial frame into an inertial frame,
         // which is an identity, so just return this estimate
         if (toFrame == _frame) {
            return _mean;
         }

         Reference< GaussianTransform> tx = GaussianTransform::find(*_frame, *toFrame);
         return ::std::move(transformMean(toFrame, *tx));
      }

      ::timber::Pointer< PreparedUpdate> Gaussian::prepareKalmanUpdate(const Reference< Gaussian>& z,
            const Reference< GaussianTransform>& H, double mhd) const throws()
      {
         unique_ptr< Innovation> innov = Innovation::create(*z, *H, *this, mhd);
         if (innov.get()) {
            return new PreparedKalmanUpdate(const_cast< Gaussian*>(this), z, move(innov));
         }

         return ::timber::Pointer< PreparedUpdate>();
      }

      Reference< PreparedUpdate> Gaussian::prepareKalmanUpdate(const Reference< Gaussian>& z,
            const Reference< GaussianTransform>& HF) const throws()
      {
         unique_ptr< Innovation> innov(new Innovation(*z, *HF, *this));
         return new PreparedKalmanUpdate(const_cast< Gaussian*>(this), z, move(innov));
      }

      Reference< PreparedUpdate> Gaussian::prepareKalmanUpdate(const Reference< Gaussian>& z) const throws()
      {
         Reference< GaussianTransform> HF = GaussianTransform::find(*_frame, *z->_frame);
         unique_ptr< Innovation> innov(new Innovation(*z, *HF, *this));
         return new PreparedKalmanUpdate(const_cast< Gaussian*>(this), z, move(innov));
      }

      ::timber::Reference< Gaussian> Gaussian::doKalmanUpdate(
            const Innovation& innovation) const throws(::std::exception)
      {
         const Matrix< double>& HP = innovation.HP();
         const Vector< double>& x = innovation.mean();

#if THALES_USE_ALLOCA == 1
         size_t allocaSize = HP.rowSize()*HP.colSize()*sizeof(double);
         double* dataBuffer = reinterpret_cast<double*>(alloca(allocaSize));
#endif

         // the gain K is computed as PHt * inv(HPHt + R)
         // let A = HPHt+R and
         // let L = Cholesky(A).matrix() be the lower triangulation matrix
         // thus: K = PHt * inv(A) = PHt * inv(L*Lt) = PHt*inv(Lt)*inv(L)
         //         = PHt * transpose(inv(L))*inv(L) =
         //         = transpose(inv(L)*H*P)*inv(L)
         // we can compute inv(L)*H*P by using the backward substitution algorithm
         // of the cholesky decomposition
         // invL_HP = P*Ht*inv(Lt)
         // this will be Cholesky(P).matrix().inverse()*HP
#if THALES_USE_ALLOCA == 1
         memset(dataBuffer,0,allocaSize);
         Matrix<double> invL_HP=Matrix<double>::createWithBuffer(HP.rowSize(),HP.colSize(),dataBuffer);
#else
         Matrix< double> invL_HP(HP.rowSize(), HP.colSize());
#endif
         innovation.cholesky().doForwardSubstitution(HP, invL_HP);
         invL_HP.transposeMatrix();

         // invL_HP.transpose()*invL_HP = invL_HP.multiplyTranspose(invL_HP) due to symmetry
         // of the resuling matrix
         Matrix< double> xCovar = invL_HP.multiplyTranspose(invL_HP);
         xCovar.negate();
         xCovar += _covariance.matrix();

         //
         //
         // the update of the means is:
         // newMean = _mean + K*x
         //           _mean + transpose(inv(L)*H*P)*inv(L)*x
         // we can compute the term  inv(L)*x using forward substitution
         // invL_x = inv(L)*x
         // therefore, the new mean is simply:
         // newMean = _mean + K*x = _mean + invL_HP*invL_x
#if THALES_USE_ALLOCA == 1
         memset(dataBuffer,0,x.dimension()*sizeof(double));
         Vector<double> invL_x=Vector<double>::createWithBuffer(x.dimension(),dataBuffer);
#else
         Vector< double> invL_x(x.dimension());
#endif
         innovation.cholesky().doForwardSubstitution(x, invL_x);
         Vector< double> xMean(invL_HP * invL_x);
         xMean += _mean;

         return new Gaussian(_frame, xMean, xCovar);
      }

      Reference< Gaussian> Gaussian::doKalmanUpdate(const Reference< Gaussian>& z,
            const GaussianTransform& HF) const throws(::std::exception)
      {
         Innovation innovation(*z, HF, *this);
         return doKalmanUpdate(innovation);
      }

      Reference< Gaussian> Gaussian::doKalmanUpdate(
            const Reference< Gaussian>& z) const throws(::std::exception,TransformNotFound)
      {
         Reference< GaussianTransform> tx = GaussianTransform::find(*_frame, *z->_frame);
         return doKalmanUpdate(z, *tx);
      }

      double Gaussian::mahalanobisDistance2(const ::timber::Reference< Gaussian>& z) const throws(TransformNotFound)
      {
         Reference< GaussianTransform> HF = GaussianTransform::find(*_frame, *z->frame());
         Innovation innovation(*z, *HF, *this);
         return innovation.mahalanobisDistance2();
      }

      double Gaussian::mahalanobisDistance2(const ::timber::Reference< Gaussian>& z,
            const GaussianTransform& HF) const throws()
      {
         Innovation innovation(*z, HF, *this);
         return innovation.mahalanobisDistance2();
      }

      Likelihood Gaussian::likelihood(const ::timber::Reference< Gaussian>& z,
            const GaussianTransform& HF) const throws()
      {
         Innovation innovation(*z, HF, *this);
         return innovation.likelihood();
      }

      Likelihood Gaussian::likelihood(const ::timber::Reference< Gaussian>& z) const throws(TransformNotFound)
      {
         Reference< GaussianTransform> HF = GaussianTransform::find(*_frame, *z->frame());
         Innovation innovation(*z, *HF, *this);
         return innovation.likelihood();
      }

      void Gaussian::print(ostream& out) const
      {
         out << "Gaussian { " << endl << "mean { " << _mean << " }" << endl << "covariance { " << _covariance << " }"
               << endl << "frame " << typeid(*_frame).name() << endl << "}" << endl;
      }

      Vector< double> Gaussian::randomSample() const throws ()
      {
         return randomSamples(_mean, _covariance.matrix(), 1).at(0);
      }

      Vector< double> Gaussian::randomSample(const Vector< double>& mean, const Matrix< double>& P) throws ()
      {
         return randomSamples(mean, P, 1).at(0);
      }

      ::std::vector< Vector< double> > Gaussian::randomSamples(const Vector< double>& mean, const Matrix< double>& P,
            size_t n) throws ()
      {
         ::std::vector< Vector< double> > res(n);
         const size_t dim = P.rowSize();

         assert(P.colSize() == dim);
         assert(mean.dimension() == dim);

         ::thales::probability::NormalDistribution N;
         Cholesky< double> ch(P);

         for (size_t k = 0; k < n; ++k) {
            Vector< double> x(dim);
            for (size_t j = 0, sz = dim; j < sz; ++j) {
               x(j) = N.drawRandom();
            }
            res[k] = ch.matrix() * x + mean;
         }
         return res;
      }

      ::timber::Pointer< Gaussian> Gaussian::inertialEstimate() const throws()
      {
         ::timber::Pointer< CoordinateFrame> iframe = _frame->getInertialFrame();
         if (iframe) {
            return transform(iframe);
         }
         else {
            return ::timber::Pointer< Gaussian>();
         }
      }
   }

}
