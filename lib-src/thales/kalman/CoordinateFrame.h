#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#define _THALES_KALMAN_COORDINATEFRAME_H

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

namespace thales {
  namespace kalman {
    class Gaussian;
    class CartesianInertial;

    /**
     * The primary purpose of CoordinateFrames is to provide the mapping 
     * function between different Gaussians.
     */
    class CoordinateFrame : public ::timber::Counted {
          CANOPY_BOILERPLATE_PREVENT_COPYING(CoordinateFrame);

      /** The default constructor */
    protected:
      CoordinateFrame() throws();
      
      /** The destructor */
    public:
      virtual ~CoordinateFrame() throws() = 0;

      /** 
       * Create a a coordinate frame in the specified dimension.
       * @param d the dimensionality of the space
       * @return a coordinate frame
       */
    public:
      static ::timber::Reference<CoordinateFrame> createGenericFrame(size_t d) throws();

      /**
       * Ensure that this coordinate frame is the same as the specified one. The default
       * is to check the typeid of both types and and if they match, returns true. Certain
       * types of frames, however, require, more than that and need to override this method.
       * @param f a frame
       * @return true if this frame and f are identical
       */
    public:
      virtual bool equals(const CoordinateFrame& f) const throws();

      /**
       * Compute the difference between two vectors in this space.
       * @param x a vector
       * @param y a vector
       * @return x-y in this space.
       */
    public:
      virtual ::newton::Vector<double> difference(const ::newton::Vector<double>& x, const ::newton::Vector<double>& y) const throws();

      /**
       * Get an cartesian inertial frame corresponding to this frame.
       * @return an inertial cartesian coordinate frame
       */
      virtual ::timber::Pointer<CartesianInertial> getInertialFrame() const throws();
      
       /**
       * Get the dimension of the space of this gaussian
       * @return the dimension of the space for this gaussian
       */
    public:
      virtual size_t dimension() const throws() = 0;
    };
      
  }
}
#endif
