#ifndef _THALES_KALMAN_GAUSSIAN_H
#define _THALES_KALMAN_GAUSSIAN_H

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _THALES_KALMAN_TRANSFORMNOTFOUND_H
#include <thales/kalman/TransformNotFound.h>
#endif

#ifndef _THALES_KALMAN_GAUSSIANTRANSFORM_H
#include <thales/kalman/GaussianTransform.h>
#endif

#ifndef _THALES_KALMAN_COVARIANCE_H
#include <thales/kalman/Covariance.h>
#endif

#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#include <thales/kalman/CoordinateFrame.h>
#endif

#ifndef _THALES_KALMAN_LIKELIHOOD_H
#include <thales/kalman/Likelihood.h>
#endif

#ifndef _THALES_KALMAN_PREPAREDUPDATE_H
#include <thales/kalman/PreparedUpdate.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

#include <iosfwd>
#include <vector>

namespace thales {
   namespace kalman {
      class Innovation;

      /**
       * This class represents a multi-variate Gaussian probability distribution. A description
       * of one can be found <a href="http://en.wikipedia.org/wiki/Multivariate_normal_distribution">here</a>.
       * @todo derive this class from a ProbabilityDistribution
       */
      class Gaussian : public ::timber::Counted
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Gaussian);

            /** A prepared update object */
         private:
            class PreparedKalmanUpdate;

            /**
             * Create a new gaussian. The covariance and mean will not be valid after construction.
             * @param frame the frame for this gaussian
             * @param mean the mean for this new gaussian distribution
             * @param covar the covariance for this gaussian distribution
             * @throws ::std::exception if the gaussian could not be created
             * @throws ::std::invalid_argument if mean.dimension()!=covar.dimension()
             */
         private:
            Gaussian(const ::timber::Reference< CoordinateFrame>& frame, ::newton::Vector< double> mean,
                Covariance covar) throws (::std::exception);

            /**
             * The default constructor
             * @param frame the coordinate frame for this gaussian
             */
         private:
            Gaussian(const ::timber::Reference< CoordinateFrame>& frame) throws();


            /** The destructor */
         public:
            virtual ~Gaussian() throws();

            /**
             * The default constructor
             * @param frame the coordinate frame for this gaussian
             */
         public:
            static ::timber::Reference< Gaussian> create(const ::timber::Reference< CoordinateFrame>& frame) throws();

            /**
             * Create a new gaussian. The covariance and mean will not be valid after construction.
             * @param frame the frame for this gaussian
             * @param mean the mean for this new gaussian distribution
             * @param covar the covariance for this gaussian distribution
             * @throws ::std::exception if the gaussian could not be created
             * @throws ::std::invalid_argument if mean.dimension()!=covar.dimension()
             * @deprecated Use the create function instead
             */
         public:
            static ::timber::Reference< Gaussian> create(const ::timber::Reference< CoordinateFrame>& frame,
                  ::newton::Vector< double> mean, Covariance covar) throws (::std::exception);

            /**
             * Transform this gaussian to its inertial coordinate frame.
             * @return this Gaussian in its inertial frame or null if it could not be converted
             */
         public:
            ::timber::Pointer< Gaussian> inertialEstimate() const throws();

            /**
             * Convert this gaussian into the specified coordinate frame using the given transform function.
             * @param toFrame a coordinate frame
             * @param tx a transform function
             * @return a gaussian in the specified coordinate frame
             * @throws exception if tx is an invalid conversion function
             */
         public:
            ::timber::Reference< Gaussian> transform(const ::timber::Reference< CoordinateFrame>& toFrame,
                  const GaussianTransform& tx) const throws (::std::exception);

            /**
             * Convert this gaussian into the specified coordinate frame.
             * @param toFrame a coordinate frame
             * @return a gaussian in the specified coordinate frame, or 0 if there is no conversion function
             */
         public:
            ::timber::Pointer< Gaussian> transform(const ::timber::Reference< CoordinateFrame>& toFrame) const throws();

            /**
             * Convert the mean of this gaussian into the specified coordinate frame using the given transform function.
             * @param toFrame a coordinate frame
             * @param tx a transform function
             * @return a vector in the specified coordinate frame
             * @throws exception if tx is an invalid conversion function
             */
         public:
            ::newton::Vector< double> transformMean(const ::timber::Reference< CoordinateFrame>& toFrame,
                  const GaussianTransform& tx) const throws (::std::exception);

            /**
             * Convert the mean of this gaussian into the specified coordinate frame.
             * @param toFrame a coordinate frame
             * @return a vector in the specified coordinate frame
             * @throws exception if tx is an invalid conversion function
             */
         public:
            ::newton::Vector< double> transformMean(
                  const ::timber::Reference< CoordinateFrame>& toFrame) const throws (::std::exception);

            /**
             * Get the dimension of the space of this gaussian
             * @return the dimension of the space for this gaussian
             */
         public:
            inline size_t dimension() const throws()
            {
               return _mean.dimension();
            }

            /**
             * Get the state vector.
             * @return mean for this distribution
             */
         public:
            const ::newton::Vector< double>& mean() const throws()
            {
               return _mean;
            }

            /**
             * Get the i'th value of the mean.
             * This is just syntactic sugar for mean()(i)
             * @param i an index
             * @return the i'th mean value
             */
         public:
            inline double get(size_t i) const throws()
            {
               return _mean(i);
            }

            /**
             * Get the i'th value of the mean.
             * This is just syntactic sugar for mean()(i)
             * @param i an index
             * @return the i'th mean value
             */
         public:
            inline double operator()(size_t i) const throws()
            {
               return _mean(i);
            }

            /**
             * Get the covariance for this gaussian.
             * @return the covariance matrix
             */
         public:
            inline const Covariance& covariance() const throws()
            {
               return _covariance;
            }

            /**
             * Get the i,j covariance entry.
             * This is just syntactic sugar for covariance()(i)
             * @param i an index
             * @param j an index
             * @return the covariance matrix entry i,j
             */
         public:
            inline double get(size_t i, size_t j) const throws()
            {
               return _covariance(i, j);
            }

            /**
             * Get the i,j covariance entry.
             * This is just syntactic sugar for covariance()(i)
             * @param i an index
             * @param j an index
             * @return the covariance matrix entry i,j
             */
         public:
            inline double operator()(size_t i, size_t j) const throws()
            {
               return _covariance(i, j);
            }

            /**
             * Get the coordinate frame.
             * @return the coordinate frame
             */
         public:
            inline const ::timber::Reference< CoordinateFrame>& frame() const throws()
            {
               return _frame;
            }

            /**
             * Prepare for an update, but only if the mahalanobis distance of this gaussian
             * is less than the specified value.
             * @param z a measurement gaussian
             * @param H the transform function
             * @param mhd a mahalanobis distance function
             * @return a prepared update or 0 if the measurement is too far away
             */
         public:
            ::timber::Pointer< PreparedUpdate> prepareKalmanUpdate(const ::timber::Reference< Gaussian>& z,
                  const ::timber::Reference< GaussianTransform>& H, double mhd) const throws ();

            /**
             * Prepare an update of this gaussian with the given measurement.
             * @param z a measurement gaussian
             * @param H the transform function
             * @return a prepared update
             */
         public:
            ::timber::Reference< PreparedUpdate> prepareKalmanUpdate(const ::timber::Reference< Gaussian>& z,
                  const ::timber::Reference< GaussianTransform>& H) const throws ();

            /**
             * Prepare an update of this gaussian with the given measurement.
             * @param z a measurement gaussian
             * @return a prepared update
             */
         public:
            ::timber::Reference< PreparedUpdate> prepareKalmanUpdate(
                  const ::timber::Reference< Gaussian>& z) const throws ();

            /**
             * Find the report that produced the post-state of kalman filter update given a predicted state.
             * The following invariant holds:
             * <code>
             *    post = prior->kalmanUpdate(z);
             *    z = findMeasurement(prior,post); // assuming H == I
             * </code>
             * @param prior the predicted state
             * @param post the post update state
             * @return a pointer to a gaussian or null if it was infeasible.
             */
         public:
            static ::timber::Pointer< Gaussian> findMeasurement(const ::timber::Reference< Gaussian>& prior,
                  const ::timber::Reference< Gaussian>& post) throws();

            /**
             * Update this Gaussian with the specified measurement via
             * the Kalman update equation.
             * @param z the gaussian with which this Gaussian will be updated
             * @param H a gaussian transform
             * @return a new Gaussian estimate
             * @throws ::std::exception if the update could not be performed.
             */
         public:
            ::timber::Reference< Gaussian> doKalmanUpdate(const ::timber::Reference< Gaussian>& z,
                  const GaussianTransform& H) const throws (::std::exception);

            /**
             * Update this Gaussian with the specified measurement via
             * the Kalman update equation.
             * This method may fail if the update would result in a non-positive-definite covariance.
             * @param z the gaussian with which this Gaussian will be updated
             * @return a new Gaussian estimate
             * @throws TransformNotFound if a transform cannot be found from this frame to z's frame
             * @throws ::std::exception if the update could not be performed.
             */
         public:
            ::timber::Reference< Gaussian> doKalmanUpdate(
                  const ::timber::Reference< Gaussian>& z) const throws (::std::exception,TransformNotFound);

            /**
             * Update this Gaussian with the specified measurement via the specified innovation.
             * This method may fail if the update would result in a non-positive-definite covariance.
             * @param innovation an innovation
             * @return a new Gaussian estimate
             * @throws ::std::exception if the update could not be performed.
             */
         private:
            ::timber::Reference< Gaussian> doKalmanUpdate(
                  const Innovation& innovation) const throws ( ::std::exception);

            /**
             * Compute the square of the Mahalanobis distance between this gaussian and the specified gaussian.
             * The mahalanobis distance is computed as follows:
             * <code>
             *   Matrix<double> S = (H*covariance()*Ht + z.covariance())
             *   Vector<double> dz = z.mean() - H*mean()
             *   double distanceSquared = dz*S.inverse()*dz
             * </code>
             * @param z a gaussian
             * @param H a gaussian transform
             * @return the square of the Mahalanobis distance between two gaussians
             */
         public:
            double mahalanobisDistance2(const ::timber::Reference< Gaussian>& z,
                  const GaussianTransform& H) const throws();

            /**
             * Compute the square of the Mahalanobis distance between this gaussian and the specified gaussian.
             * This method finds the appropriate transform from z to this coordinate frame.
             * @param z a gaussian
             * @return the square of the Mahalanobis distance between two gaussians
             * @throws TransformNotFound if a transform cannot be found from this frame to z's frame
             */
         public:
            double mahalanobisDistance2(const ::timber::Reference< Gaussian>& z) const throws (TransformNotFound);

            /**
             * Get the likelihood that measurements is associated with the original filter. The
             * likelihood is obtained by computing the following function:
             * <pre>
             * <code>
             *   Matrix<double> S = (H*covariance()*Ht + z.covariance())
             *   double lambda = -0.5*(mahalanobisDistance2(*this,z,H) + log_e((2*PI)^z->dimension * S.determinant())
             * </code>
             * </pre>
             * Basically, we're computing the negative log-likelihood function. Most of the time, the standard likelihood
             * calculation will result in negative numbers in log-space.
             * A description of a multi-variate Gaussian distribution can be found <a href="http://en.wikipedia.org/wiki/Multivariate_normal_distribution">here</a>.
             * @param z a gaussian
             * @param H a gaussian transform
             * @return the likelihood of associating this gaussian with the specified measurement
             * @note The resulting likelihood values are usually negative in the log-likelihood calculation!
             */
         public:
            Likelihood likelihood(const ::timber::Reference< Gaussian>& z, const GaussianTransform& H) const throws();

            /**
             * Get the likelihood that measurements is associated with the original filter. The
             * likelihood is obtained by computing the following function:
             * <pre>
             * <code>
             *   Matrix<double> S = (H*covariance()*Ht + z.covariance())
             *   double lambda = -0.5*(mahalanobisDistance2(*this,z,H) + log_e((2*PI)^z->dimension * S.determinant())
             * </code>
             * </pre>
             * Basically, we're computing the negative log-likelihood function. Most of the time, the standard likelihood
             * calculation will result in negative numbers in log-space.
             * A description of a multi-variate Gaussian distribution can be found <a href="http://en.wikipedia.org/wiki/Multivariate_normal_distribution">here</a>.
             * @param z a gaussian
             * @return the likelihood of associating this gaussian with the specified measurement
             * @note The resulting likelihood values are usually negative in the log-likelihood calculation!
             * @throws TransformNotFound if a transform cannot be found from this frame to z's frame
             */
         public:
            Likelihood likelihood(const ::timber::Reference< Gaussian>& z) const throws (TransformNotFound);

            /**
             * Print this gaussian to the specified stream.
             * @param out a stream
             */
         public:
            void print(::std::ostream& out) const;

            /**
             * Get a random sample of the distribution that this Gaussian represents.
             * @return a sample drawn from this distribution
             */
         public:
            ::newton::Vector< double> randomSample() const throws ();

            /**
             * Draw the specified number of random samples fro the specified distribution.
             * @param mean the mean of the distribution
             * @param covar the covariance of the distribution
             * @param n the number of samples to draw
             * @return a vector of sample points
             */
         public:
            static ::std::vector< ::newton::Vector< double> > randomSamples(const ::newton::Vector< double>& mean,
                  const ::newton::Matrix< double>& covar, size_t n) throws ();

            /**
             * Draw a single random sample from the specified distribution.
             * @param mean the mean of the distribution
             * @param covar the covariance of the distribution
             * @return a sample
             */
         public:
            static ::newton::Vector< double> randomSample(const ::newton::Vector< double>& mean,
                  const ::newton::Matrix< double>& covar) throws ();

            /** The mean vector */
         private:
            const ::newton::Vector< double> _mean;

            /** The covariance matrix */
         private:
            const Covariance _covariance;

            /** The coordinate frame */
         private:
            const ::timber::Reference< CoordinateFrame> _frame;
      };
   }
}

/**
 * Print a gaussian.
 * @param out a stream
 * @param ge a gaussian
 * @return out
 */
inline ::std::ostream& operator<<(::std::ostream& out, const ::thales::kalman::Gaussian& ge)
{
   ge.print(out);
   return out;
}

#endif
