#ifndef _THALES_KALMAN_COVARIANCE_H
#define _THALES_KALMAN_COVARIANCE_H

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

#include <iosfwd>

namespace thales {
   namespace kalman {

      /**
       * This class is used to represent the covariance matrix for a Gaussian.
       * It is essentially a wrapper around a matrix.
       */
      class Covariance
      {
            /**
             * Default constructor.
             */
         public:
            inline Covariance() throws()
            {
            }

            /**
             * Create a new covariance.
             * @param dim the dimension of the covariance.
             */
         public:
            Covariance(size_t dim) throws (::std::exception);

            /**
             * Create a new covariance.
             * @param matrix the covariance matrix
             * @throws ::std::exception if the covariance could not be created
             * @throws ::std::invalid_argument if the matrix is not square
             */
         public:
            Covariance(::newton::Matrix< double> matrix) throws (::std::exception);

             /**
             * Copy constructor.
             * @param src another covariance
             */
         public:
            Covariance(const Covariance& cvar) throws ()
                  : _matrix(cvar._matrix)
            {
            }

            /**
             * Move constructor.
             * @param src another covariance
             */
         public:
            Covariance(Covariance&& cvar) throws ()
                  : _matrix(::std::move(cvar._matrix))
            {
            }

            /**
             * Copy operator.
             * @param src another covariance
             * @return this
             */
         public:
            Covariance& operator=(const Covariance& cvar) throws ()
            {
               _matrix = cvar._matrix;
               return *this;
            }

            /**
             * Move constructor.
             * @param src another covariance
             */
         public:
            Covariance& operator=(Covariance&& cvar) throws ()
            {
               _matrix = ::std::move(cvar._matrix);
               return *this;
            }

            /** The destructor */
         public:
            ~Covariance() throws();

            /**
             * Create a 2D covariance.
             * @param xx the variance in x
             * @param yy the variance in y
             * @param xy the covariance between x and y
             * @return a covariance
             * @throws ::std::exception if xx*yy-xy*xy <= 0
             */
         public:
            static Covariance createCovariance2D(double xx, double yy, double xy) throws (::std::exception);

            /**
             * Create a covariance from a matrix. The contents of the matrix are undefined
             * after the covariance is created.
             * @param matrix the covariance matrix
             * @return a covariance
             * @throws ::std::exception if the covariance could not be created
             * @throws ::std::invalid_argument if the matrix is not square
             */
         public:
            static Covariance createCovariance2D(::newton::Matrix< double>&& matrix) throws (::std::exception);

            /**
             * Swap this covariance with another one
             */
         public:
            inline void swap(Covariance& c) throws()
            {
               _matrix.swap(c._matrix);
            }

            /**
             * Get the dimension of the space of this covariance
             * @return the dimension of the space for this covariance
             */
         public:
            inline size_t dimension() const throws()
            {
               return _matrix.rowSize();
            }

            /**
             * Get the covariance matrix.
             * @return the covariance matrix.
             */
         public:
            inline const ::newton::Matrix< double>& matrix() const throws()
            {
               return _matrix;
            }

            /**
             * Get the i,j value of the covariance.
             * @param i an index
             ( @param j an index
             * @return matrix()(i,j)
             */
         public:
            inline double operator()(size_t i, size_t j) const throws()
            {
               return _matrix(i, j);
            }

            /**
             * Determine the correlation coefficient of two variates determined
             * by their index. If i==j, then 1 is returned. The correlation
             * coefficient is a value between -1 (maximum negative correlation)
             * to 0 (no correlation) to 1 (maximum correlation).
             * @param i an index
             * @param j an index
             * @return this(i,j)/sqrt(this(i,i)*this(j,i))
             */
         public:
            inline double correlationCoefficient(size_t i, size_t j) const throws()
            {
               return _matrix(i, j) / ::std::sqrt(_matrix(i, i) * _matrix(j, j));
            }

            /**
             * Determine the square of the correlation coefficient of two variates determined
             * by their index. If i==j, then 1 is returned. The correlation
             * coefficient is a value between 0 (no correlation) to 1 (maximum correlation).
             * @param i an index
             * @param j an index
             * @return (this(i,j)*this(i,j))/(this(i,i)*this(j,i))
             */
         public:
            inline double correlationCoefficient2(size_t i, size_t j) const throws()
            {
               double t = _matrix(i, j);
               return (t * t) / (_matrix(i, i) * _matrix(j, j));
            }

            /**
             * Decompose a 2D covariance into its major and minor axes and the
             * angle between the major axis and the horizontal x-axis.
             * @param xx the (0,0) entry
             * @param yy the (1,1) entry
             * @param xy the (0,1) or (1,0) entry
             * @param maj the major axis length
             * @param min the minor axis length
             * @param theta the angle between the major axis and the horizontal (in radians)
             * @return true if the matrix is positive definite, false otherwise
             */
         public:
            static bool decompose(double xx, double yy, double xy, double& maj, double& min, double& theta) throws();

            /**
             * Create a 2D covariance from the 2 diagonal entries and 1 off-diagonal entry.
             * @param maj the major axis
             * @param min the minor axis
             * @param theta the angle between the major axis and the horizontal (in radians)
             * @param xx the (0,0) entry (output)
             * @param yy the (1,1) entry (output)
             * @param xy the (0,1) or (1,0) entry (output)
             */
         public:
            static void recompose(double maj, double min, double theta, double& xx, double& yy, double& xy) throws();

            /**
             * Create a 2D covariance from the 2 diagonal entries and 1 off-diagonal entry.
             * @param maj the major axis
             * @param min the minor axis
             * @param theta the angle between the major axis and the horizontal (in radians)
             */
         public:
            static Covariance recompose(double maj, double min, double theta) throws();

            /**
             * Make a matrix symmetric for use as a covariance. This will
             * set P = 0.5 (P + P.transpose()).
             * @param P a matrix
             * @return a reference to P
             */
         public:
            static ::newton::Matrix< double>& makeSymmetric(::newton::Matrix< double>& P) throws();

            /** The mean vector */
         private:
            ::newton::Matrix< double> _matrix;
      };

   }
}

/**
 * Print a covariance matrix.
 * @param out an output stream
 * @param c a covariance
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::thales::kalman::Covariance& c);
#endif
