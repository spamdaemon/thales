#ifndef _THALES_KALMAN_FILTERUPDATE_H
#define _THALES_KALMAN_FILTERUPDATE_H

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _THALES_KALMAN_GAUSSIAN_H
#include <thales/kalman/Gaussian.h>
#endif

#ifndef _THALES_KALMAN_LIKELIHOOD_H
#include <thales/kalman/Likelihood.h>
#endif

namespace thales {
  namespace kalman {
    class Filter;

    /**
     * The FilterUpdate is used to represents an update of a filter with a measurement.
     * The motivation for this class is to combine the functions for likelihood, distance,
     * and Kalman Gain in such a way as to minimize the number of calculations. 
     */
    class FilterUpdate : public ::timber::Counted {
          CANOPY_BOILERPLATE_PREVENT_COPYING(FilterUpdate);

      /** The default constructor */
    protected:
      inline FilterUpdate() throws() {}
      
      /** The destructor */
    public:
      virtual ~FilterUpdate() throws() = 0;

      /**
       * Get the original filter
       * @return the pre-update filter
       */
    public:
      virtual ::timber::Reference<Filter> preUpdateFilter() const throws() = 0;

      /**
       * Get the measurement.
       * @return the measurement with which the preUpdateFilter() is updated.
       */
    public:
      virtual  ::timber::Reference<Gaussian> measurement() const throws() = 0;

      /**
       * Get the post-update filter that results from this update.
       * @return the filter that results from an update
       */
    public:
      virtual ::timber::Reference<Filter> postUpdateFilter() const throws() = 0;

      /**
       * Compute the square of the Mahalanobis distance between this update and its measurement.
       * @return the square of the Mahalanobis distance between the measurement and original filter.
       */
    public:
      virtual double mahalanobisDistance2() const throws() = 0;

      /**
       * Get the likelihood that measurements is associated with the original filter.
       * @return the likelihood
       */
    public:
      virtual Likelihood likelihood() const throws() = 0;

    };
      
  }
}
#endif
