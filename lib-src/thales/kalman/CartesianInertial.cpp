#include <thales/kalman/CartesianInertial.h>

namespace thales {
  namespace kalman {
    CartesianInertial::CartesianInertial(size_t cd, size_t sd) throws()
    : _cartesianDimension(cd),_spaceDimension(sd)
    {
      assert(cd>0 && "Invalid spatial dimension");
      assert(sd>=cd && "Invalid frame dimension (less than spatial dimension)");
    }
    
    CartesianInertial::~CartesianInertial() throws()
    {}
    
    size_t CartesianInertial::dimension() const throws()
    { return _spaceDimension; }
    
    ::timber::Pointer<CartesianInertial> CartesianInertial::getInertialFrame() const throws()
    { return const_cast<CartesianInertial*>(this); }
  }
}
