#include <thales/kalman/unscented/DefaultPoints.h>

using namespace newton;

namespace thales {
  namespace kalman {
    namespace unscented {

      DefaultPoints::DefaultPoints() throws()
	: _k(0) {}

      DefaultPoints::DefaultPoints(double k) throws()
	: _k(k) 
      {
      }

      void  DefaultPoints::operator() (size_t n, Matrix<double>& M, Vector<double>& weights) const throws(::std::exception)
      {
	Matrix<double> X(2*n+1,n);
	Vector<double> W(X.rowSize());

	// need to compute the square root of P (we use cholesky for that)
	double kappa = _k;
	if (n+kappa<0) {
	  throw ::std::runtime_error("Invalid free parameter k");
	}
	
	double tmp = ::std::sqrt(n+kappa);
	// generate first the pairs of points
	// now, the last point
	W(0) = kappa/(n+kappa);

	for (size_t i=1;i<=n;++i) {
	  W(i) = W(i+n) = 1.0/(2*(n+kappa));
	  
	  // compute the pair of ypoints

	  X(i,i-1) = tmp;
	  X(i+n,i-1) = -tmp;
	}
	
	M.swap(X);
	W.swap(weights);
      }
    }
  }
}
