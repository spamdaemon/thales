#ifndef _THALES_KALMAN_UNSCENTED_POINTSET_H
#define _THALES_KALMAN_UNSCENTED_POINTSET_H

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

namespace thales {
  namespace kalman {
    namespace unscented {

      /**
       * This class computes the simplex points for a given dimension
       * needed by an UnscentedTransform.
       */
      class PointSet {

	/**
	 * Default constructor.
	 */
      public:
	PointSet() throws();


	/**
	 * Create a new point set.
	 * @param pts the points (1 per row)
	 * @param weights the weights (1 per point)
	 */
      public:
	PointSet (const ::newton::Matrix<double>& pts, const ::newton::Vector<double>& weights) throws();
	
	/**
	 * Create simplex points in the specified dimension. The point
	 * generator must have function of the following signature:
	 * @code void operator() (size_t n, ::newton::Matrix<double>&, ::newton::Vector<double>&) const
	 * @endcode
	 * The point generator is required to generate the mean of the distribution at i=0, which means the
	 * first row of the returned matrix must be 0.
	 * @param n the dimension
	 * @param gen the point generator
	 * @pre assert n>0
	 */
      public:
	template <class Generator>
	  inline PointSet (size_t n, Generator gen) throws()
	  {
	    gen(n,_points,_weights); 
#ifndef NDEBUG
	    for (size_t j=0;j<dimension();++j) {
	      assert(_points(0,j)==0);
	    }
#endif
	  }
	
	/**
	 * The destructor
	 */
      public:
	~PointSet () throws();

	/**
	 * Get the dimensionality 
	 * @return the dimensionality of the space
	 */
      public:
	inline size_t dimension() const throws() { return _points.colSize(); }

	/**
	 * Get the number of points
	 * @return the number of points
	 */
      public:
	inline size_t pointCount() const throws() { return _points.rowSize(); }

	/**
	 * Get the j'th coordinate of the i'th point.
	 * @param i a point index
	 * @param j the coordinate index
	 * @return the j'th coordinate of the i'th point
	 */
      public:
	inline double operator() (size_t i, size_t j) const throws()
	{ return  _points(i,j); }


	/**
	 * Get the weight corresponding to the i'th point.
	 * @param i a point index
	 * @return the weight corresponding to point i
	 */
      public:
	inline double operator() (size_t i) const throws()
	{ return  _weights(i); }
	
	/** The matrix of points */
      private:
	::newton::Matrix<double> _points;

	/** The weights */
      private:
	::newton::Vector<double> _weights;
      };
    }
  }
}
#endif
