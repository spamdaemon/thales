#ifndef _THALES_KALMAN_UNSCENTED_SIMPLEXPOINTS_H
#define _THALES_KALMAN_UNSCENTED_SIMPLEXPOINTS_H

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

namespace thales {
  namespace kalman {
    namespace unscented {

      /**
       * This class computes the simplex points for a given dimension
       * needed by an UnscentedTransform.
       */
      class SimplexPoints {
	
	/** The default constructor */
      public:
	SimplexPoints () throws();
	
	/** 
	 * constructor 
	 * @param W0 the free parameter
	 */
      public:
	SimplexPoints (double W0) throws();

	/**
	 * Generate simplex points.
	 * @param n the point dimension
	 * @param M the generated points
	 * @param W the generated point weights
	 */
      public:
	void operator() (size_t n, ::newton::Matrix<double>& M, ::newton::Vector<double>& W) const throws();

	/** The kappa value */
      private:
	double _W0;
      };
    }
  }
}
#endif
