#include <thales/kalman/unscented/PointSet.h>

namespace thales {
  namespace kalman {
    namespace unscented {

      PointSet::PointSet() throws()
      {}

      PointSet::PointSet (const ::newton::Matrix<double>& pts, const ::newton::Vector<double>& weights) throws()
      : _points(pts),_weights(weights)
      {	assert(_points.rowSize()==_weights.dimension()); }

      PointSet::~PointSet () throws()
      {}
    }
  }
}
