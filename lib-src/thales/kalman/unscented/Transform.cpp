#include <thales/kalman/unscented/Transform.h>
#include <thales/kalman/unscented/DefaultPoints.h>
#include <iostream>

using namespace newton;

namespace thales {
  namespace kalman {
    namespace unscented {
      namespace {
	
	// extract a column from a matrix
	static void extractRow (const Matrix<double>& m, size_t i, Vector<double>& v)
	{
	  for (size_t j=0;j<m.colSize();++j) {
	    v(j) = m(i,j);
	  }
	}

	static void setRow (const Vector<double>& v, size_t i, Matrix<double>& m)
	{
	  for (size_t j=0;j<m.colSize();++j) {
	    m(i,j) = v(j);
	  }
	}

	
	static PointSet createScaledPointSet (const PointSet& ps, double alpha)
	{
	  assert(alpha>0);
	  Matrix<double> M(ps.pointCount(),ps.dimension());
	  Vector<double> W(ps.pointCount());
	  
	  for (size_t i=0;i<M.rowSize();++i) {
	    for (size_t j=0;j<M.colSize();++j) {
	      M(i,j) = (1-alpha)*ps(0,j)  + alpha * ps(i,j);
	    }
	    W(i) = ps(i)/(alpha*alpha);
	  }
	  // special treatment for scaling W(0)
	  W(0) += 1.0 - 1.0/(alpha*alpha);
	  
	  return PointSet(M,W);
	}
      }

      Transform::Transform(size_t n) throws()
      : _points(n,DefaultPoints(1)),_alpha(1),_beta(0)
      {}

      Transform::Transform(const PointSet& ps) throws()
      : _points(ps),_alpha(1),_beta(0)
      {}

      Transform::Transform(const PointSet& ps, double alpha,double beta) throws()
      : _points(createScaledPointSet(ps,alpha)),_alpha(alpha),_beta(beta)
      {}

      Transform::Transform(size_t n, double alpha,double beta) throws()
      : _points(createScaledPointSet(PointSet(n,DefaultPoints(1)),alpha)),_alpha(alpha),_beta(beta)
      {}

      Transform::~Transform() throws()
      {}

      Matrix<double> Transform::doPreTransform (const Vector<double>& x, const Matrix<double>& P) const throws(::std::exception)
      {
	// adjust X for the true mean and covariance
	::newton::Cholesky<double> ch(P);
	
	// multiply X by the square root of P
	::newton::Matrix<double> X(_points.pointCount(),_points.dimension());
	
	// multiply X by the lower triangular matrix  and also add
	// the mean to each transformed point
	for (size_t k=0;k<_points.dimension();++k) {
	  for (size_t i=0;i<_points.pointCount();++i) {
	    double tmp=x(k); // add the mean
	    for (size_t j=k;j<_points.dimension();++j) {
	      tmp += _points(i,j)*ch.matrix()(j,k);
	    }
	    X(i,k) = tmp;
	  }
	}

	return X;
      }

      Vector<double> Transform::doPostTransform (const CoordinateFrame& from, Matrix<double>& X, 
						 const CoordinateFrame& to, Matrix<double>& Y, 
						 const Vector<double>& x, 
						 Matrix<double>* py,
						 Matrix<double>* HP) const throws()
      {
	// now, we compute the mean of Y
	::newton::Vector<double> y(Y.colSize());
	::newton::Vector<double> assumedMean(y.dimension());
	::newton::Vector<double> tmpX(x.dimension());
	::newton::Vector<double> tmpY(y.dimension());

	// we need to use the difference functions of the coordinate frame
	// because otherwise things don't really work:
	// consider this:
	//  polar coordinates may be  at az=350 and az=10; what's the average
	//  of these? we're assuming the average will be 0 and not 180, even though
	//  it's a valid number
	// we're relying on the fact that the first row in X is the transformed 
	// mean of the original distribution and assume it more or less maps into
	// to mean of the new distribution; if this assumption is not true,
	// then this function may not work correctly
	
	double wSum = _points(0);
	
	extractRow(Y,0,assumedMean);
	for (size_t i=1;i<Y.rowSize();++i) { 
	  double w = _points(i);
	  wSum += w;
	  extractRow(Y,i,tmpY);
	  // subtract the assumed mean
	  y += w*to.difference(tmpY,assumedMean);
	}

	// this difference is equivalent to adding the mean to y
	y = to.difference(y,-wSum * assumedMean);
	
	// subtract the means of x and y from their matrices
	for (size_t i=0;i<X.rowSize();++i) {
	  extractRow(X,i,tmpX);
	  setRow(from.difference(tmpX,x),i,X);
	  extractRow(Y,i,tmpY);
	  setRow(to.difference(tmpY,y),i,Y);
	}
	
	
 
	// now, we can compute covariance of Y
	if (py) {
	  ::newton::Matrix<double> PY(Y.colSize());
	  double offset = 1+_beta-_alpha*_alpha;
	  for (size_t i=0;i<Y.rowSize();++i) {
	    double w = _points(i) + offset;
	    offset = 0; // only apply the offset to the first point
	    // fill in only the lower-triangular
	    for (size_t k=0;k<Y.colSize();++k) {
	      double y_ik = w*Y(i,k);
	      for (size_t j=0;j<=k;++j) {
		PY(k,j) += y_ik*Y(i,j);
	      }
	    }
	  }
	  for (size_t i=0;i<PY.rowSize();++i) {
	    for (size_t j=0;j<i;++j) {
	      PY(j,i) = PY(i,j);
	    }
	  }
	  py->swap(PY);
	}
	if (HP) {
	  ::newton::Matrix<double> corr(Y.colSize(),X.colSize());
	  double offset = 1+_beta-_alpha*_alpha;
	  for (size_t i=0;i<Y.rowSize();++i) {
	    double w = _points(i) + offset;
	    offset = 0; // only apply the offset to the first point
	    for (size_t k=0;k<Y.colSize();++k) {
	      for (size_t j=0;j<X.colSize();++j) {
		corr(k,j) += w*Y(i,k)*X(i,j);
	      }
	    }
	  }
	  HP->swap(corr);
	}
	return y;
	
      }

    
    }
  }
}
