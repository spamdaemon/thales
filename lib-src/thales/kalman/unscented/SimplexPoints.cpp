#include <thales/kalman/unscented/SimplexPoints.h>

using namespace newton;

namespace thales {
  namespace kalman {
    namespace unscented {

      SimplexPoints::SimplexPoints() throws()
	: _W0(0.5) {}

      SimplexPoints::SimplexPoints(double w0) throws()
	: _W0(w0) 
      {
	assert(w0>=0 && w0<1);
      }

      void  SimplexPoints::operator() (size_t n, Matrix<double>& M, Vector<double>& weights) const throws()
      {
	Matrix<double> X(n+2,n);
	Vector<double> W(X.rowSize());
	
	// first, generate the weights
	W(0) = _W0; // can choose anything
	W(1) = (1-W(0))/(1<<n);
	W(2) = W(1);
	
	for (size_t i=3;i<=n+1;++i) {
	  W(i) =  (1<<(i-2))*W(1);
	}
	
	// generate the points for a distribution with mean=0 and covariance=I
	X(0,0) = 0;
	X(1,0) = -1.0/::std::sqrt(2*W(1));
	X(2,0) = -X(1,0);
	
	for (size_t j=1;j<n;++j) {
	  assert(X(0,j) ==0); // already set as needed
	  for (size_t i=1;i<=j+1;++i) {
	    X(i,j) = -1.0/::std::sqrt(2*W(j));
	  }
	  X(j+2,j) = 1.0/::std::sqrt(2*W(j));
	}

	M.swap(X);
	W.swap(weights);
      }
    }
  }
}
