#include <thales/kalman/unscented/SphericalSimplexPoints.h>

using namespace newton;

namespace thales {
  namespace kalman {
    namespace unscented {

      SphericalSimplexPoints::SphericalSimplexPoints() throws()
	: _W0(0.5) {}

      SphericalSimplexPoints::SphericalSimplexPoints(double w0) throws()
	: _W0(w0) 
      {
	assert(w0>=0 && w0<1);
      }

      void  SphericalSimplexPoints::operator() (size_t n, Matrix<double>& M, Vector<double>& weights) const throws()
      {
	Matrix<double> X(n+2,n);
	Vector<double> W(X.rowSize());
	
	// first, generate the weights
	W(0) = _W0; // can choose anything
	double w = (1-_W0)/(n+1);
	for (size_t i=1;i<=n+1;++i) {
	  W(i) = w;
	}
	
	// generate the points for a distribution with mean=0 and covariance=I
	X(0,0) = 0;
	X(1,0) = -1.0/::std::sqrt(2*w);
	X(2,0) = -X(1,0);
	
	for (size_t j=1;j<n;++j) {
	  double tmp = 1.0/::std::sqrt((j+1)*(j+2)*w);
	  assert(X(0,j) ==0); // already set as needed
	  for (size_t i=1;i<=j+1;++i) {
	    X(i,j) = -tmp;
	  }
	  X(j+2,j) = tmp;
	}

	M.swap(X);
	W.swap(weights);
      }
    }
  }
}
