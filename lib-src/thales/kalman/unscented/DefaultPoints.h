#ifndef _THALES_KALMAN_UNSCENTED_DEFAULTPOINTS_H
#define _THALES_KALMAN_UNSCENTED_DEFAULTPOINTS_H

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

namespace thales {
  namespace kalman {
    namespace unscented {

      /**
       * This class computes the default points for a given dimension
       * needed by an UnscentedTransform.
       */
      class DefaultPoints {
	
	/** The default constructor */
      public:
	DefaultPoints () throws();
	
	/** 
	 * constructor 
	 * @param k the free parameter
	 */
      public:
	DefaultPoints (double k) throws();

	/**
	 * Generate default points.
	 * @param n the point dimension
	 * @param M the generated points
	 * @param W the generated point weights
	 */
      public:
	void operator() (size_t n, ::newton::Matrix<double>& M, ::newton::Vector<double>& W) const throws (::std::exception);

	/** The kappa value */
      private:
	double _k;
      };
    }
  }
}
#endif
