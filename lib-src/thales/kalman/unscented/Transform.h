#ifndef _THALES_KALMAN_UNSCENTED_TRANSFORM_H
#define _THALES_KALMAN_UNSCENTED_TRANSFORM_H

#ifndef _THALES_KALMAN_COORDINATEFRAME_H
#include <thales/kalman/CoordinateFrame.h>
#endif

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

#ifndef _NEWTON_CHOLESKY_H
#include <newton/Cholesky.h>
#endif

#ifndef _THALES_KALMAN_UNSCENTED_POINTSET_H
#include <thales/kalman/unscented/PointSet.h>
#endif

namespace thales {
  namespace kalman {
    namespace unscented {
      
      /**
       * The Transform provides all the functionality to determine
       * the necessary components for a extended Kalman filter.
       * <p>
       * For this transform to work, the assumption is made that the mean of the distribution
       * to be transformed maps more or less into the mean of the desired distribution. It does not
       * have to map exactly, but it is needed to establish a temporary coordinate frame for computing
       * the mean and variance of the new distribution. The means of a polar coordinate frame, for example,
       * are not easy to compute without this assumption, due the ambiguous nature of the coordinate frame.
       */
      class Transform {
      
	/** 
	 * Creates an unscaled transform using DefaultPoints.
	 * @param n the dimension of the estimates to be transformed.
	 */
      public:
	Transform(size_t n) throws();
      
	/** 
	 * Creates an unscaled transform.
	 * @param ps the points to be used
	 */
      public:
	Transform(const PointSet& ps) throws();
      
	/** 
	 * Create a scaled transform using DefaultPoints. Normally beta should be
	 * set to 2 for gaussians.
	 * @param n the dimension of point to be transformed
	 * @param alpha a scale factor
	 * @param beta a value to control the transform.
	 */
      public:
	Transform(size_t n, double alpha, double beta) throws();

	/** 
	 * Create a scaled transform. Normally beta should be
	 * set to 2 for gaussians.
	 * @param ps the points to be used
	 * @param alpha a scale factor
	 * @param beta a value to control the transform.
	 */
      public:
	Transform(const PointSet& ps, double alpha, double beta) throws();

	/** Destroy this transform */
      public:
	~Transform() throws();


	/**
	 * Compute the  transform for an estimate. The default implementation implements
	 * the original algorithm described here:
	 * <pre>
	 *      A New Extension of the Kalman Filter to Nonlinear Systems
	 *               Simon J. Julier Jeffrey K. Uhlmann
	 * </pre>
	 *
	 * @param transformation a (nonlinear) transformation
	 * @param from the original frame
	 * @param x a the original mean
	 * @param P the original covariance
	 * @param to the target frame
	 * @param toX the new mean in the target frame (output)
	 * @param toP the covariance in the target frame (output, 0 if not desired)
	 * @param HP the product of H*P (output, 0 if not desired)
	 * @return always true
	 */
      public:
	template <class Transformation>
	  void transform (const Transformation& transformation,
			  const CoordinateFrame& from,
			  const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P,
			  const CoordinateFrame& to,
			  ::newton::Vector<double>& toX, 
			  ::newton::Matrix<double>* toP,
			  ::newton::Matrix<double>* HP) const  throws (::std::exception)
	{
	  ::newton::Matrix<double> X = doPreTransform(x,P);

	  // transform each point using a non-linear transformation
	  ::newton::Matrix<double> Y = transformation(from,X,to);
	  
	  assert(X.rowSize()==Y.rowSize());
	  assert(Y.colSize()==to.dimension());

	  ::newton::Vector<double> y = doPostTransform(from,X,to,Y,x,toP,HP);
	  toX.swap(y);
	}
	
	/**
	 * The pre-transform
	 * @param x the mean 
	 * @param P the covariance
	 * @return the set of sample points
	 */
      private:
	::newton::Matrix<double> doPreTransform (const ::newton::Vector<double>& x, const ::newton::Matrix<double>& P) const throws( ::std::exception);

	/**
	 * Do the post transform
	 * @param from the target coordinate frame
	 * @param X the sample points (will be modified)
	 * @param to the target coordinate frame
	 * @param Y the transformed sample points (will be modified)
	 * @param x the  mean of the sample points
	 * @param y the mean of the transformed points
	 * @param pY the covariance of the transformed points
	 * @param HP the correlation matrix of the transformed points with the sample points
	 */
      private:
	::newton::Vector<double> doPostTransform (const CoordinateFrame& from, ::newton::Matrix<double>& X, 
						  const CoordinateFrame& to,::newton::Matrix<double>& Y, 
						  const ::newton::Vector<double>& x, 
						  ::newton::Matrix<double>* py,
						  ::newton::Matrix<double>* HP) const throws ();

	
	/** The points of a standard normal distribution */
      private:
	PointSet _points;
	 
	/** A scale factor */
      private:
	double _alpha;
	 
	/** A scale factor */
      private:
	double _beta;
      };
      
      
    }
  }
}
#endif
