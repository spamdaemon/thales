#include <thales/kalman/UKFTransform.h>

using namespace ::std;
using namespace ::newton;

namespace thales {
  namespace kalman {
    namespace {
      struct TX {
	TX(const ::timber::Reference<GaussianTransform>& f)
	  : _transform(f) {}
	~TX() throws() {}

	Matrix<double> operator() (const CoordinateFrame& from,const ::newton::Matrix<double>& X,const CoordinateFrame& to) const throws()
	{ 
	  Matrix<double> P = Matrix<double>::identity(X.colSize());
	  Matrix<double> res(X.rowSize(),to.dimension());
	  Vector<double> x(from.dimension()),y;
	  for (size_t i=0;i<X.rowSize();++i) {
	    for (size_t j=0;j<x.dimension();++j) {
	      x(j) = X(i,j);
	    }
	    _transform->apply(from,x,P,to,y,0,0);
	    for (size_t j=0;j<y.dimension();++j) {
	      res(i,j) = y(j);
	    }
	  }
	  return res;
	}

      private:
	const ::timber::Reference<GaussianTransform> _transform;
      };

    }
    
    UKFTransform::UKFTransform() throws()
    {}

    UKFTransform::~UKFTransform() throws()
    {}

    ::timber::Reference<UKFTransform> UKFTransform::create (const ::thales::kalman::unscented::Transform& t, const ::timber::Reference<GaussianTransform>& func) throws()
    {
      TX tx(func);
      return createUKF<TX>(t,tx);
    }

    ::timber::Reference<UKFTransform> UKFTransform::create (const ::timber::Reference<CoordinateFrame>& from, const ::timber::Reference<GaussianTransform>& func) throws()
    {
      ::thales::kalman::unscented::Transform t(from->dimension());
      TX tx(func);
      return createUKF<TX>(t,tx);
    }

  }
}
