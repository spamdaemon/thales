#include <thales/kalman/model/ConstantCartesianVelocityModel.h>
#include <thales/kalman/frame/Cartesian_Inertial_PV.h>
#include <thales/kalman/Gaussian.h>

#include <cmath>

using namespace ::std;
using namespace ::newton;
using namespace ::timber;

namespace thales {
   namespace kalman {
      namespace model {

         ConstantCartesianVelocityModel::ConstantCartesianVelocityModel(const Vector< double>& a)
throws               ()
               : _a(a),
               _frame(::thales::kalman::frame::Cartesian_Inertial_PV::create(a.dimension()))
               {
                  for (size_t i=0;i<a.dimension();++i) {
                     _a(i) *= _a(i);
                  }
               }

         ConstantCartesianVelocityModel::~ConstantCartesianVelocityModel() throws()
         {
         }

         Reference< Gaussian> ConstantCartesianVelocityModel::predict(const Reference< Gaussian>& state,
               const Time& longDT) const throws (::std::exception)
         {
            assert(state->frame()->equals(*_frame) && "Incompatible coordinate frames");
            if (longDT == 0) {
               return state;
            }

            const double dt = ::std::abs(longDT);

            Vector< double> v = state->mean();
            const Matrix< double> P = state->covariance().matrix();

            const size_t m = P.rowSize();
            const size_t n = m / 2;
            Matrix< double> p(m, m);

            // initialize p with the noise component
            {
               double dt2 = dt * dt;
               double dt3 = dt * dt2 / 3.0;
               dt2 *= 0.5;

               for (size_t i = 0; i < n; ++i) {
                  p(i, i) = dt3 * _a(i);
                  p(i + n, i) = p(i, i + n) = dt2 * _a(i);
                  p(i + n, i + n) = dt * _a(i);
               }
            }

            // we're splitting the A and P matrices into 4 submatrices of equal size
            // and then work on the four blocks (quadrants) simulatenously
            // we're also updating the state vector v
            for (size_t i = 0; i < n; ++i) {
               v(i) += dt * v(n + i);
               for (size_t j = 0; j < n; ++j) {
                  const double qBR = P(n + i, n + j);
                  double qTR = P(i, n + j) + dt * qBR;
                  double qTL = P(i, j) + dt * (qTR + P(j, n + i));

                  p(i, j) += qTL;
                  p(i, n + j) += qTR;
                  p(n + j, i) = p(i, n + j); // due to symmetry
                  p(n + i, n + j) += qBR;
               }
            }

            return Gaussian::create(_frame, ::std::move(v),::std::move( p));
         }

         Reference< CoordinateFrame> ConstantCartesianVelocityModel::coordinateFrame() const throws()
         {
            return _frame;
         }
      }
   }
}
