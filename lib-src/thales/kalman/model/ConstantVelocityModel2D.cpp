#include <thales/kalman/model/ConstantVelocityModel2D.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/Gaussian.h>

#include <cmath>

using namespace ::std;
using namespace ::newton;
using namespace ::timber;

namespace thales {
   namespace kalman {
      namespace model {

         ConstantVelocityModel2D::ConstantVelocityModel2D(double ax, double ay)
throws               ()
               : _ax(ax*ax), _ay(ay*ay),_frame(::thales::kalman::frame::Cartesian_2D_Inertial_PV::create())
               {
               }

         ConstantVelocityModel2D::ConstantVelocityModel2D(double a)
throws               ()
               : _ax(a*a),_ay(a*a),_frame(::thales::kalman::frame::Cartesian_2D_Inertial_PV::create())
               {
               }

         ConstantVelocityModel2D::~ConstantVelocityModel2D() throws()
         {
         }

         Reference< Gaussian> ConstantVelocityModel2D::predict(const Reference< Gaussian>& state,
               const Time& longDT) const throws (::std::exception)
         {
            assert(state->frame()->equals(*_frame) && "Incompatible coordinate frames");
            if (longDT == 0) {
               return state;
            }

            const double dt = ::std::abs(longDT);

            Vector< double> v = state->mean();
            v(0) += v(2) * dt;
            v(1) += v(3) * dt;

#if 0	
            Matrix<double> A = Matrix<double>::identity(4);
            A(0,2) = A(1,3) = dt;

            Matrix<double> P = state->covariance().matrix().multiplyTranspose(A);
            P = A*P;
#else
            // we need to make a copy anyway
            const Matrix< double>& p = state->covariance().matrix();
            Matrix< double> P(4, 4);
            // new entries are written to allow a compiler to easily use fused-multiply-add if available
            {
               double t1 = p(0, 2);
               double t3 = p(2, 2);
               double t4 = t1 + dt * t3;
               double t2 = p(0, 0);
               P(2, 2) = t3;
               P(0, 2) = t4;
               P(0, 0) = t2 + dt * (t1 + t4);
            }

            {
               double t1 = p(1, 3);
               double t3 = p(3, 3);
               double t4 = t1 + dt * t3;
               double t2 = p(1, 1);

               P(3, 3) = t3;
               P(1, 3) = t4;
               P(1, 1) = t2 + dt * (t1 + t4);
            }

            {
               double t1 = p(2, 3);
               double t2 = dt * t1;

               double t3 = p(1, 2);
               double t4 = t3 + t2;
               double t5 = p(0, 3);

               P(2, 3) = t1;
               P(0, 3) = t5 + t2;
               P(1, 2) = t4;
               P(0, 1) = p(0, 1) + dt * (t4 + t5);
            }
#endif
            // not making it symmetric just yet!!

            // add some process noise to the mix
            // the process noise matrix is briefly described in
            // "Performance Modeling for Multisensor Data Fusion"
            // by  Chang, Song, and Liggins
            double dt2 = dt * dt;
            double dt3 = dt * dt2;
            dt2 /= 2.0;
            dt3 /= 3.0;

            // only use the upper triangular matrix of P
            P(0, 0) += dt3 * _ax;
            P(2, 0) = (P(0, 2) += dt2 * _ax);
            P(1, 1) += dt3 * _ay;
            P(3, 1) = (P(1, 3) += dt2 * _ay);
            P(2, 2) += dt * _ax;
            P(3, 3) += dt * _ay;

            // make completely symmetric
            P(1, 0) = P(0, 1);
            P(2, 1) = P(1, 2);
            P(3, 0) = P(0, 3);
            P(3, 2) = P(2, 3);
            return Gaussian::create(_frame, ::std::move(v), ::std::move(P));
         }

         Reference< CoordinateFrame> ConstantVelocityModel2D::coordinateFrame() const throws()
         {
            return _frame;
         }
      }
   }
}
