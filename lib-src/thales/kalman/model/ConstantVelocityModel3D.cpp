#include <thales/kalman/model/ConstantVelocityModel3D.h>

using namespace ::newton;

namespace thales {
  namespace kalman {
    namespace model {
      
      ConstantVelocityModel3D::ConstantVelocityModel3D(double ax, double ay, double az) throws()
      : ConstantCartesianVelocityModel(Vector<double>(ax,ay,az))
      {
      }
      
      ConstantVelocityModel3D::ConstantVelocityModel3D(double a) throws()
      : ConstantCartesianVelocityModel(Vector<double>(a,a,a))
      {
      }
      
      ConstantVelocityModel3D::~ConstantVelocityModel3D() throws()
      {}
    }
  }
}

