#ifndef _THALES_KALMAN_MODEL_CONSTANTVELOCITYMODEL_H
#define _THALES_KALMAN_MODEL_CONSTANTVELOCITYMODEL_H

#ifndef _THALES_KALMAN_PROCESSMODEL_H
#include <thales/kalman/ProcessModel.h>
#endif

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

namespace thales {
   namespace kalman {
      namespace model {

         /**
          * This process model represents an object moving
          * with constant velocity in a cartesian coordinate frame of arbitrary dimension.
          */
         class ConstantCartesianVelocityModel : public ProcessModel
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(ConstantCartesianVelocityModel);

               /**
                * Create a new constant velocity model.
                * @param a a vector containing acceleration values
                */
            public:
               ConstantCartesianVelocityModel(const ::newton::Vector< double>& a) throws();

               /** The destructor */
            public:
               ~ConstantCartesianVelocityModel() throws();

               ::timber::Reference< Gaussian> predict(const ::timber::Reference< Gaussian>& state,
                     const Time& dt) const throws (::std::exception);

               ::timber::Reference< CoordinateFrame> coordinateFrame() const throws();

               /** The acceleration components (actually, we'll be storing the squares of these) */
            private:
               ::newton::Vector< double> _a;

               /** The coordinate frame */
            private:
               ::timber::Reference< CoordinateFrame> _frame;
         };
      }
   }
}

#endif
