#ifndef _THALES_KALMAN_MODEL_CONSTANTVELOCITYMODEL2D_H
#define _THALES_KALMAN_MODEL_CONSTANTVELOCITYMODEL2D_H

#ifndef _THALES_KALMAN_PROCESSMODEL_H
#include <thales/kalman/ProcessModel.h>
#endif

#ifndef _THALES_H
#include <thales/thales.h>
#endif

namespace thales {
   namespace kalman {
      namespace model {

         /**
          * This process model represents an object moving
          * with constant velocity.
          */
         class ConstantVelocityModel2D : public ProcessModel
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(ConstantVelocityModel2D);

               /**
                * Create a new constant velocity model.
                * @param ax the acceleration in x
                * @param ay the acceleration in y
                */
            public:
               ConstantVelocityModel2D(double ax, double ay) throws();

               /**
                * Create a new constant velocity model.
                * @param a the acceleration in both directions
                */
            public:
               ConstantVelocityModel2D(double a) throws();

               /** The destructor */
            public:
               ~ConstantVelocityModel2D() throws();

               ::timber::Reference< Gaussian> predict(const ::timber::Reference< Gaussian>& state,
                     const Time& dt) const throws (::std::exception);
               ::timber::Reference< CoordinateFrame> coordinateFrame() const throws();

               /** The acceleration components (actually, we'll be storing the squares of these) */
            private:
               double _ax, _ay;

               /** The coordinate frame */
            private:
               ::timber::Reference< CoordinateFrame> _frame;
         };
      }
   }
}

#endif
