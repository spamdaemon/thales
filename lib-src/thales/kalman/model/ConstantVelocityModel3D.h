#ifndef _THALES_KALMAN_MODEL_CONSTANTVELOCITYMODEL3D_H
#define _THALES_KALMAN_MODEL_CONSTANTVELOCITYMODEL3D_H

#ifndef _THALES_KALMAN_MODEL_CONSTANTCARTESIANVELOCITYMODEL_H
#include <thales/kalman/model/ConstantCartesianVelocityModel.h>
#endif

namespace thales {
   namespace kalman {
      namespace model {

         /**
          * This process model represents an object moving
          * with constant velocity in a 3D cartesian coordinate frame.
          */
         class ConstantVelocityModel3D : public ConstantCartesianVelocityModel
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(ConstantVelocityModel3D);

               /**
                * Create a new constant velocity model.
                * @param ax the acceleration in x
                * @param ay the acceleration in y
                * @param az the acceleration in z
                */
            public:
               ConstantVelocityModel3D(double ax, double ay, double az) throws();

               /**
                * Create a new constant velocity model.
                * @param a the acceleration in all directions
                */
            public:
               ConstantVelocityModel3D(double a) throws();

               /** The destructor */
            public:
               ~ConstantVelocityModel3D() throws();
         };
      }
   }
}

#endif
