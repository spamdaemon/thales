#ifndef _THALES_KALMAN_MODEL_H
#define _THALES_KALMAN_MODEL_H


namespace thales {
  namespace kalman {

    /**
     * This namespace contains several process models that may be of use for most applications.
     */
    namespace model {
    }
  }
}

#endif
