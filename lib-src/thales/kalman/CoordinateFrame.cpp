#include <thales/kalman/CoordinateFrame.h>
#include <thales/kalman/CartesianInertial.h>
#include <thales/kalman/Gaussian.h>

namespace thales {
  namespace kalman {

    namespace {
      struct GenericFrame : public CoordinateFrame 
      {
	inline GenericFrame(size_t d) throws() 
	  : _dimension(d)
	{}
	
	~GenericFrame() throws()
	{}
	
	bool equals(const CoordinateFrame& f) const throws()
	{
	  const GenericFrame* g = dynamic_cast<const GenericFrame*>(&f);
	  return g!=0 && _dimension == g->_dimension;
	}

	size_t dimension() const throws()
	{ return _dimension; }

      private:
	const size_t _dimension;
      };
    }

    CoordinateFrame::CoordinateFrame() throws() 
    {}

    CoordinateFrame::~CoordinateFrame() throws()
    {}

    ::timber::Reference<CoordinateFrame> CoordinateFrame::createGenericFrame(size_t d) throws()
    {
      assert(d>0 && "Zero-dimensional coordinate frames are not valid");
      return new GenericFrame(d); 
    }

    ::timber::Pointer<CartesianInertial> CoordinateFrame::getInertialFrame() const throws()
    { return ::timber::Pointer<CartesianInertial>(); }

    ::newton::Vector<double> CoordinateFrame::difference(const ::newton::Vector<double>& x, const ::newton::Vector<double>& y) const throws()
    {
      assert(x.dimension()==dimension());
      return x - y; 
    }

    bool CoordinateFrame::equals(const CoordinateFrame& f) const throws()
    {
      return typeid(*this).name()==typeid(f).name();
    }

    
  }
}
