#ifndef _THALES_KALMAN_PREPAREDUPDATE_H
#define _THALES_KALMAN_PREPAREDUPDATE_H

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _THALES_KALMAN_LIKELIHOOD_H
#include <thales/kalman/Likelihood.h>
#endif

namespace thales {
  namespace kalman {
    class Gaussian;

    /**
     * This class captures an intended update of a Gaussian with another Gaussian. 
     */
    class PreparedUpdate : public ::timber::Counted {
          CANOPY_BOILERPLATE_PREVENT_COPYING(PreparedUpdate);

      /** The default constructor */
    protected:
      inline PreparedUpdate() throws() {}
      
      /** The destructor */
    public:
      virtual ~PreparedUpdate() throws() = 0;

      /**
       * Get the pre update Gaussian.
       * @return the pre-update Gaussian
       */
    public:
      virtual ::timber::Reference<Gaussian> preUpdate() const throws() = 0;

      /**
       * Get the measurement.
       * @return the measurement with which the preUpdatePrepared() is updated.
       */
    public:
      virtual  ::timber::Reference<Gaussian> measurement() const throws() = 0;

      /**
       * Get the post-update Gaussian that results from this update.
       * @return the prepared that results from an update
       * @throws ::std::exception if the update cannot be performed
       */
    public:
      virtual ::timber::Reference<Gaussian> postUpdate() const throws (::std::exception) = 0;

      /**
       * Compute the square of the Mahalanobis distance between this preUpdate() and its measurement.
       * @return the square of the Mahalanobis distance between the measurement and original prepared.
       */
    public:
      virtual double mahalanobisDistance2() const throws() = 0;

      /**
       * Get the likelihood that measurements is associated with the original prepared.
       * @return the likelihood
       */
    public:
      virtual Likelihood likelihood() const throws() = 0;

    };
      
  }
}
#endif
