#ifndef _THALES_KALMAN_MonteCarloTRANSFORM_H
#define _THALES_KALMAN_MonteCarloTRANSFORM_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
  namespace kalman {
    class GaussianTransform;
    class Gaussian;
    class CoordinateFrame;
    
    /**
     * The MonteCarlo transform is used to generate true distributions, based on
     * many (thousands) of sample points. It is not meant to be used in practice,
     * but exists primarily for testing and tuning purposes.
     */
    class MonteCarloTransform {
    private:
          CANOPY_BOILERPLATE_PREVENT_COPYING(MonteCarloTransform);

      MonteCarloTransform() = delete;
      ~MonteCarloTransform()  = delete;

      /**
       * Create a new MonteCarloTransform.
       * @param t the transform to be computed
       * @param nSamples the number of samples to use for this transform
       */
    public:
      static ::timber::Reference<GaussianTransform> create (const ::timber::Reference<GaussianTransform>& t,size_t nSamples=1024*16) throws();

      /**
       * Generate a distribution of the transformed estimate
       * @param t the transform to be simulated
       * @param ge a gaussian estimate
       * @param toFrame the frame into which to map the gaussian
       * @param nSamples the number of montecarlo sample points
       * @return a gaussian estimate transformed by this transform
       */
    public:
      static ::timber::Reference<Gaussian> transform (const ::timber::Reference<GaussianTransform>& t,const ::timber::Reference<Gaussian>& ge, const ::timber::Reference<CoordinateFrame>& toFrame, size_t nSamples=1024*16) throws();
    };

  }
}
#endif
