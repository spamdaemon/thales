#ifndef _THALES_KALMAN_PROCESSMODEL_H
#define _THALES_KALMAN_PROCESSMODEL_H

#ifndef _THALES_H
#include <thales/thales.h>
#endif

#ifndef _THALES_KALMAN_TIME_H
#include <thales/kalman/Time.h>
#endif

namespace thales {
  namespace kalman {
    class CoordinateFrame;
    class Gaussian;

    /**
     * This is the base class for all kinds of Kalman filters variations,
     * such as IMM and VSMM, etc. 
     */
    class ProcessModel : public ::timber::Counted {
      CANOPY_BOILERPLATE_PREVENT_COPYING(ProcessModel);

      /** The default constructor */
    protected:
      inline ProcessModel() throws() {}
      
      /** The destructor */
    public:
      virtual ~ProcessModel() throws() = 0;

      /**
       * Create a static process model. This model simply returns
       * the state when predicted.
       * @return a static process model.
       */
    public:
      static ::timber::Reference<ProcessModel> createStaticModel(const ::timber::Reference<CoordinateFrame>& frame) throws();

      /**
       * Get the coordinate frame for this process model.
       * @return the coordinate frame used by this model
       */
    public:
      virtual ::timber::Reference<CoordinateFrame> coordinateFrame() const throws() = 0;

      /**
       * Predict the specified gaussian forward in time by the given change in time.
       * @param state a gaussian
       * @param dt a difference 
       * @return a state predicted forward in time by the specified amount
       * @throws ::std::exception if some kind of error occurred.
       */
    public:
      virtual ::timber::Reference<Gaussian> predict (const ::timber::Reference<Gaussian>& state, const Time& dt) const throws (::std::exception) = 0;
      
    };
  }
}
#endif
