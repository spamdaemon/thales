#include <thales/kalman/ProcessModel.h>
#include <thales/kalman/CoordinateFrame.h>
#include <thales/kalman/Gaussian.h>

namespace thales {
  namespace kalman {
    namespace {
      
      struct StaticModel : public ProcessModel {
	
	inline StaticModel(const ::timber::Reference<CoordinateFrame>& frame) throws()
	  : _frame(frame) 
	{}

	~StaticModel() throws() {}

	::timber::Reference<CoordinateFrame> coordinateFrame() const throws()
	{
	  return _frame;
	}

	::timber::Reference<Gaussian> predict (const ::timber::Reference<Gaussian>& state, const Time&) const throws()
	{ 
	  assert(state->frame()->equals(*_frame));
	  return state;	
	}
	
      private:
	::timber::Reference<CoordinateFrame> _frame;
      };
    }

    ProcessModel::~ProcessModel() throws()
    {}

    ::timber::Reference<ProcessModel> ProcessModel::createStaticModel(const ::timber::Reference<CoordinateFrame>& frame) throws()
    { return new StaticModel(frame); }

  }
}
