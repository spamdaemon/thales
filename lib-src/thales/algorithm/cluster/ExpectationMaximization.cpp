#include <thales/algorithm/cluster/ExpectationMaximization.h>
#include <newton/Matrix.h>
#include <cmath>

using namespace ::std;
using namespace ::timber;
using namespace ::thales::kalman;
using namespace ::newton;

namespace thales {
   namespace algorithm {
      namespace cluster {
         namespace {
            struct MixtureImpl : public ExpectationMaximization::Mixture
            {
                  MixtureImpl() throws()
                  {
                  }
                  ~MixtureImpl() throws()
                  {
                  }

                  size_t size() const throws()
                  {
                     return _weights.size();
                  }

                  double probability(size_t i) const throws()
                  {
                     assert(i < _weights.size());
                     return _weights[i];
                  }

                  Reference< Gaussian> gaussian(size_t i) const throws()
                  {
                     assert(i < _gaussians.size());
                     return _gaussians[i];
                  }

                  /** The weights */
               public:
                  vector< double> _weights;

                  /** The gaussians */
               public:
                  vector< Reference< Gaussian> > _gaussians;
            };

         }

         ExpectationMaximization::Mixture::Mixture() throws()
         {
         }
         ExpectationMaximization::Mixture::~Mixture() throws()
         {
         }

         ExpectationMaximization::ExpectationMaximization() throws()
         {
         }

         ExpectationMaximization::~ExpectationMaximization() throws()
         {
         }

         unique_ptr< ExpectationMaximization::Mixture> ExpectationMaximization::apply(const vector<
               Reference< Gaussian> >& points, const vector< Reference< Gaussian> >& initial) const throws()
         {
            const double err = 1E-8;
            const double minDeltaAlpha = .01;
            const double minDeltaMean = .01;
            const double minDeltaVar = .01;

            // initialize the mixture and its weights
            const size_t k = initial.size();
            unique_ptr< MixtureImpl> res(new MixtureImpl());

            // initialize the categories (we'll use the first k gaussians to initialize the mixture
            // this initialize could be done by passing a function that generates
            // a good initial guess
            res->_gaussians = initial;
            res->_weights.insert(res->_weights.end(), k, 1.0 / k);

            bool changed = true;

            // this vector contains the unnormalized probabilites for each point
            unique_ptr< MixtureImpl> tmp(new MixtureImpl());
            tmp->_gaussians = res->_gaussians;
            tmp->_weights = res->_weights;

            while (changed) {
               changed = false;

               Matrix< double> h(k, points.size());
               Vector< double> hSum(points.size());

               // do the estimation and maximization step in one
               // as described in the Bilmes Paper

               for (size_t i = 0; i < k; ++i) {
                  const Gaussian& G = *(res->_gaussians[i]);
                  for (size_t j = 0; j < points.size(); ++j) {
                     double L = ::std::exp(G.likelihood(points[j]).value());
                     L *= res->_weights[i];
                     if (L < err) {
                        L = err;
                     }
                     h(i, j) = L;
                     hSum(j) += L;
                  }
               }

               // now, we normalize all values in the matrix
               for (size_t i = 0; i < k; ++i) {
                  for (size_t j = 0; j < points.size(); ++j) {
                     assert(hSum(j) == hSum(j));
                     assert(hSum(j) > 0);
                     h(i, j) /= hSum(j);
                  }
               }

               // now, let's do the update state

               for (size_t i = 0; i < k; ++i) {
                  double sumH = 0;

                  for (size_t j = 0; j < points.size(); ++j) {
                     sumH += h(i, j);
                  }
                  assert(sumH > 0);
                  Vector< double> sumM(points[0]->dimension());
                  for (size_t j = 0; j < points.size(); ++j) {
                     sumM += (h(i, j) / sumH) * points[j]->mean();
                  }
                  const Gaussian& ge = *res->_gaussians[i];

                  Matrix< double> sumE(sumM.dimension());

                  for (size_t j = 0; j < points.size(); ++j) {
                     // we use the difference function used by the coordinate frame; this way, we'll
                     // be able to even deal with azimuth like coordinate frames
                     Vector< double> delta = ge.frame()->difference(points[j]->mean(), ge.mean());
                     Matrix< double> m(delta, delta);
                     m += points[j]->covariance().matrix();
                     sumE += m * (h(i, j) / sumH);
                  }
                  for (size_t r = 0; r < sumE.rowSize(); ++r) {
                     for (size_t s = 1; s < r; ++s) {
                        double avg = 0.5 * (sumE(r, s) + sumE(s, r));
                        sumE(r, s) = sumE(s, r) = avg;
                     }
                  }

                  tmp->_weights[i] = sumH / points.size();
                  tmp->_gaussians[i] = Gaussian::create(res->_gaussians[i]->frame(),sumM, sumE);

                  changed = changed || (::std::abs(tmp->_weights[i] - res->_weights[i]) > minDeltaAlpha);
                  changed = changed || (ge.frame()->difference(sumM, ge.mean()).abs().max() > minDeltaMean);
                  changed = changed || ((sumE - res->_gaussians[i]->covariance().matrix()).abs().max() > minDeltaVar);
               }


               unique_ptr< MixtureImpl> tmpRes = move(res);
               res = move(tmp);
               tmp = move(tmpRes);

            }

            return move(res);
         }
      }
   }
}

