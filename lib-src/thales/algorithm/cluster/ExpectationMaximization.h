#ifndef _THALES_ALGORITHM_CLUSTER_EXPECTATIONMAXIMIZATION_H
#define _THALES_ALGORITHM_CLUSTER_EXPECTATIONMAXIMIZATION_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _THALES_KALMAN_GAUSSIAN_H
#include <thales/kalman/Gaussian.h>
#endif

#include <vector>

namespace thales {
  namespace algorithm {
    namespace cluster {

      /**
       * The Expectation Maximization algorithm.
       */
      class ExpectationMaximization {
	
	/** 
	 * A gaussian mixture.
	 */
      public:
	class Mixture {
	  /** No copying */
	private:
	  Mixture&operator=(const Mixture&);
	  Mixture(const Mixture&);
	  
	  /** Default constructor */
	protected:
	  Mixture() throws(); 
	  /**
	   * Destructor
	   */
	public:
	  virtual ~Mixture() throws() = 0;

	  /**
	   * Get the size of this mixture. There will at least be 1
	   * gaussian in this mixture.
	   * @return the number of gaussians in the mixture
	   */
	public:
	  virtual size_t size() const throws() = 0;

	  /**
	   * Get the probability of the specified gaussian. All the probabilities
	   * will sum up to 1.
	   * @param i a gaussian index
	   * @pre i < size()
	   * @return the the probability of the gaussian at the specified index
	   */
	public:
	  virtual double probability(size_t i) const throws() = 0;

	  /**
	   * Get a specific gaussian.
	   * @param i a gaussian index
	   * @pre i < size()
	   * @return the gaussian at index i
	   */
	public:
	  virtual ::timber::Reference<  ::thales::kalman::Gaussian> gaussian(size_t i) const throws() = 0;
	};

	/**
	 * Default constructor.
	 */
      public:
	ExpectationMaximization() throws();

	/**
	 * Destructor.
	 */
      public:
	virtual ~ExpectationMaximization() throws();

	/**
	 * Compute a set of clusters that approximate a dataset.
	 * @param points a vector of gaussians
	 * @param initial the initial mixture
	 * @return a vector of gaussians that model the points
	 */
      public:
	::std::unique_ptr<Mixture> apply (const ::std::vector< ::timber::Reference< ::thales::kalman::Gaussian> >& points,
					const ::std::vector< ::timber::Reference< ::thales::kalman::Gaussian> >& initial)  const throws();

      };

    }
  }
}
    
#endif
