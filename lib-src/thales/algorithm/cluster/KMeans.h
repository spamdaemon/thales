#ifndef _THALES_ALGORITHM_CLUSTER_KMEANS_H
#define _THALES_ALGORITHM_CLUSTER_KMEANS_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

namespace thales {
  namespace algorithm {
    namespace cluster {
      /**
       * This algorithm is an implementation of K-means clustering.
       */
      class KMeans {
	KMeans(const KMeans&);
	KMeans&operator=(const KMeans&);

	/** A data source */
      public:
	class DataSet {
	  /** A cluster */
	public:
	  typedef size_t Cluster;

	  /** A point */
	public:
	  typedef size_t Point;

	  /** Default contructor */
	protected:
	  inline DataSet () throws() {}

	  /** Destructor */
	public:
	  virtual ~DataSet() throws() = 0;
	  
	  /**
	   * Get the number of points in this data set
	   * @return the number of points
	   */
	public:
	  virtual size_t size() const throws() = 0;
	  
	  /**
	   * Create a new cluster.
	   * @return a index for the cluster
	   */
	public:
	  virtual Cluster createCluster() throws() = 0;
	  
	  /**
	   * Add the point at the given index to the specified cluster. If the
	   * point as already added to the cluster, then the result is undefined.
	   * @param point a point index
	   * @param cluster a cluster
	   */
	public:
	  virtual void assignPoint (Point point, Cluster cluster) throws() = 0;

	  /**
	   * Remove the specified point from a cluster. If the point was not previously
	   * added to the cluster, then the result of this function will be undefined.
	   * @param point a point
	   * @param cluster a cluster
	   */
	public:
	  virtual void unassignPoint (Point point, Cluster cluster) throws() = 0;

	  /**
	   * Determine the cluster that is closest to the specified point.
	   * @param point a point index
	   * @return the cluster that is closest to the point
	   */
	public:
	  virtual Cluster findClosestCluster(Point point) const throws() =0;
	};
      
	/**
	 * Create an KMeans implementation.
	 */
      public:
	KMeans() throws();

	/**
	 * Destructor
	 */
      public:
	~KMeans() throws();

	/**
	 * Apply the EM algorithm to the specified data set
	 * @param ds a data set
	 * @param k the number of clusters to locate
	 * @return an assignment of points in the data set to clusters
	 */
      public:
	void findClusters (DataSet& ds, size_t k) const throws();
      };

    }
  }
}
#endif
