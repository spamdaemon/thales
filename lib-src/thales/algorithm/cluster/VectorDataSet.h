#ifndef _THALES_ALGORITHM_CLUSTER_VECTORDATASET_H
#define _THALES_ALGORITHM_CLUSTER_VECTORDATASET_H

#ifndef _NEWTON_VECTOR_H
#include <newton/Vector.h>
#endif

#ifndef _THALES_ALGORITHM_CLUSTER_KMEANS_H
#include <thales/algorithm/cluster/KMeans.h>
#endif

#include <cmath>
#include <vector>


namespace thales {
  namespace algorithm {
    namespace cluster {
      
      template <class T=double> struct VectorDataSet : public KMeans::DataSet {
	struct ClusterImpl  {
	  ClusterImpl() throws() : _size(0) {}
	  ~ClusterImpl() throws() {}
	  
	  ::newton::Vector<T> _mean;
	  size_t _size;
	};
	
	VectorDataSet (const ::std::vector< ::newton::Vector<T> >& data)
	  : _data(data) {}

	~VectorDataSet() throws() {}
	  
	size_t size() const throws() { return _data.size(); }

	::std::vector< ::newton::Vector<T> > getMeans() const throws()
	{ 
	  ::std::vector< ::newton::Vector<T> > means;
	  means.reserve(_clusters.size());
	  for (size_t i=0;i<_clusters.size();++i) {
	    if (_clusters[i]._size!=0) {
	      means.push_back(_clusters[i]._mean/_clusters[i]._size);
	    }
	  }
	  return means;
	}


	Cluster createCluster() throws() 
	{
	  size_t i = _clusters.size();
	  _clusters.push_back(ClusterImpl());
	  return i;
	}

	void assignPoint (Point point, Cluster cl) throws()
	{
	  ClusterImpl& C = _clusters[cl];
	  if (C._size++ == 0) {
	    C._mean = _data[point];
	  }
	  else {
	    C._mean += _data[point];
	  }
	}
	
	void unassignPoint (Point point, Cluster cl) throws()
	{
	  ClusterImpl& C = _clusters[cl];
	  C._mean -= _data[point];
	  --C._size;

	}
	
	Cluster findClosestCluster(size_t point) const throws()
	{
	  assert (!_clusters.empty());
	  const ::newton::Vector<T>& pt = _data[point];
	  const ClusterImpl* C = 0;
	  T dist;
	  const ClusterImpl* start = &_clusters[0];
	  const ClusterImpl* end = start+_clusters.size();
	  while(start != end) {
	    const ClusterImpl* c = start;
	    if (c->_size>0) { 
	      T d = (c->_mean/c->_size).sqrDistance(pt);
	      if (C==0 || d<dist) {
		C = c;
		dist = d;
	      }
	    }
	    ++start;
	  }
	  return C - &_clusters[0];
	}

      private:
	const ::std::vector< ::newton::Vector<T> > _data;
	::std::vector<ClusterImpl> _clusters;
      };
      
    }
  }
}
#endif
