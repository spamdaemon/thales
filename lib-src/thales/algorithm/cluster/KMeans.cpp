#include <thales/algorithm/cluster/KMeans.h>

#include <vector>

using namespace ::std;

namespace thales {
  namespace algorithm {
    namespace cluster {
      namespace {
	typedef KMeans::DataSet DataSet;

	static void doLloyd (DataSet& ds, size_t k)
	{
	  typedef DataSet::Cluster Cluster;
	  typedef DataSet::Point Point;

	  // this vector will hold the assigned cluster for each point in the dataset
	  vector<Cluster> assignedCluster(ds.size());
	  vector<Cluster> tmpAssignment(ds.size());
	  
	  // this vector is just the set of clusters we'll use
	  vector<Cluster> clusters;
	  clusters.reserve(k);
	
	  // initial step, assign all points to a cluster and compute
	  // the initial cluster means
	  for (size_t i=0;i<k;++i) {
	    if (i==ds.size()) {
	      break;
	    }
	    
	    Cluster cluster = ds.createCluster();
	    size_t n =0;
	    for (size_t j=i;j<ds.size(); j+= k) {
	      assignedCluster[j] = cluster;
	      ds.assignPoint(j,cluster);
	      ++n;
	    }
	    if (n!=0) {
	      clusters.push_back(cluster);
	    }
	  }
	  
	  // now, loop until the assignment no longer changes
	  bool changed = true;
	  while(changed) {
	    // for each point, compute the closest cluster
	    for (size_t i=0;i<assignedCluster.size();++i) {
	      tmpAssignment[i] = ds.findClosestCluster(i);
	    }
	    changed = false;
	    for (size_t i=0;i<assignedCluster.size();++i) {
	      if (tmpAssignment[i]!=assignedCluster[i]) {
		changed = true;
		ds.unassignPoint(i,assignedCluster[i]);
		ds.assignPoint(i,tmpAssignment[i]);
	      }
	    }
	    ::std::swap(assignedCluster,tmpAssignment);
	  }
	  
	}
      }
      
      KMeans::DataSet::~DataSet() throws() {}

      KMeans::KMeans() throws()
      {}

      KMeans::~KMeans() throws()
      {}

      void KMeans::findClusters (DataSet& ds, size_t k) const throws()
      {
	doLloyd(ds,k);
      }
      
    }
  }
}
