#ifndef _THALES_ALGORITHM_UNIONFIND_H
#define _THALES_ALGORITHM_UNIONFIND_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <cassert>
#include <vector>

namespace thales {
   namespace algorithm {

      /**
       * The type of structure required as the UnionFind template.
       */
      struct UnionFindDataStruct
      {
            /** Default constructor */
            UnionFindDataStruct()
            {
            }

            /**
             * Reinitialize this struct with the index of the time that
             * is in a set by itself.
             * @param i the set name
             */
            void reset(size_t i)
            {
            }

            /** The merge operator */
            UnionFindDataStruct& operator+=(const UnionFindDataStruct&)
            {
               return *this;
            }
      };

      /**
       * This class provides an implementation of a union-algorithm that is fairly efficient and fast. This
       * type of union find structure requires that all possible items to be merged are known
       * ahead of time. The union find set can be reused across various union-find searches by using the reset() method, which
       * <em>never</em> releases any memory unless new memory must be obtained.
       */
      template<class T = UnionFindDataStruct> class UnionFind
      {
            /** This both an item in a set and a set by itself */
            class SetItem
            {
                  friend class UnionFind;

                  /** Create a new item */
               public:
                  inline SetItem()throws() : _set(this),_rank(0) {}

                  /** Destructor */
               public:
                  ~SetItem()throws() {}

                  /** The struct that contains other information supplied from outside */
               private:
                  T _data;

                  /** A pointer to another item in the same set or NULL if this is a root (or set) itself */
               private:
                  SetItem* _set;

                  /** The number of times this set was merged (only applicable if _parent==null) */
               private:
                  size_t _rank;
            };

            /** A set item; the items is also a set */
         public:
            typedef SetItem* Set;

            /**
             * Constructor for a normal UnionFind function that requires no extra information to be maintained.
             * @param nItems the number of items to be considered.
             */
         public:
            UnionFind()throws() : _nItems(0),_nSets(0)
            {
            }

            /**
             * Constructor for a normal UnionFind function that requires no extra information to be maintained.
             * @param nItems the number of items to be considered.
             */
         public:
            UnionFind(size_t nItems)throws() : _nItems(0),_nSets(0)
            {
               reset(nItems);
            }

            /** The destructor */
         public:
            ~UnionFind()throws() {}

            /**
             * Get the number of items.
             * @return the number of items
             */
         public:
            inline size_t itemCount() const throws() {return _nItems;}

            /**
             * Get the number of sets.
             * @return the number of set
             */
         public:
            inline size_t setCount() const throws() {return _nSets;}

            /**
             * Check if the specified set object is indeed a valid set. This is primarily
             * a helper function used to validate input to some of the other functions.
             * @param set a set
             * @return true if the set is a valid set, false otherwise
             */
         public:
            bool isSet(Set set) const throws()
            {  return set->_set==set;}

            /**
             * Reset this  union find set.
             * @param nItems the new number of items
             */
         public:
            void reset(size_t nItems)throws()
            {
               if (_items.size() < nItems) {
                  _items.resize(nItems);
               }
               _nItems = nItems;
               _nSets = nItems;
               for (size_t i = 0; i < nItems; ++i) {
                  _items[i]._set = &_items[i];
                  _items[i]._rank = 1;
                  _items[i]._data.reset(i);
               }
            }

            /**
             * Find the set of the specified item.
             * @param i an item
             * @return the set for the item
             */
         public:
            Set findSet(size_t i)throws()
            {
               assert (i< _nItems);
               SetItem& leaf = _items[i];
               SetItem* cur = &leaf;
               while(cur->_set!=cur) {
                  cur=cur->_set;
               }
               leaf._set = cur;
               return leaf._set;
            }

            /**
             * Get the data associated with the specified set.
             * @param set a set
             * @return the data for the set
             */
         public:
            const T& data(Set set) const throws()
            { return set->_data; }

            /**
             * Get the data associated with the specified set.
             * @param set a set
             * @return the data for the set
             */
         public:
            T& data(Set set) throws()
            { return set->_data; }

            /**
             * Merge two sets into a single set. The first set in the
             * returned pair is the set into which the other set was merged,
             * while the second set is the other set.
             * @param setA a set
             * @param setB a set
             * @return the sets in the order in which they were merged
             * @pre setA!=setB
             * @pre isSet(setA)
             * @pre isSet(setB)
             */
         public:
            Set mergeSets(Set setA, Set setB)throws()
            {
               assert (isSet(setA));
               assert (isSet(setB));
               assert(setA!=setB);
               assert(setA >= &_items[0] && setA< &_items[_nItems]);
               assert(setB >= &_items[0] && setB< &_items[_nItems]);

               --_nSets;

               if (setA->_rank < setB->_rank) {
                  setA->_set = setB;
                  setB->_data += setA->_data;
                  return setB;
               }
               else {
                  if (setB->_rank == setA->_rank) {
                     ++setA->_rank;
                  }
                  setB->_set = setA;
                  setA->_data += setB->_data;
                  return setA;
               }
            }

            /** The items */
         private:
            ::std::vector< SetItem> _items;

            /** The number of items actually valid */
         private:
            size_t _nItems;

            /** The number of sets */
         private:
            size_t _nSets;
      };

   }
}
#endif
