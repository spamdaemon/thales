#ifndef _THALES_ALGORITHM_MUNKRES_H
#define _THALES_ALGORITHM_MUNKRES_H

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif

#include <vector>
#include <algorithm>
#include <stdexcept>

namespace thales {
  namespace algorithm {

    /**
     * The munkres 2D assignment algorithm.
     * @param m a cost matrix
     * @return a vector pairs of row index, column index representing the selected entries
     * @throws ::std::exception if an error occurred
     */
    ::std::vector< ::std::pair<size_t,size_t> > solveMunkres (const ::newton::Matrix<double>& m) throws (::std::exception);

  }
}
  
#endif
