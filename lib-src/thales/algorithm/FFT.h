#ifndef _THALES_ALGORITHM_FFT_H
#define _THALES_ALGORITHM_FFT_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#ifndef _CANOPY_MATH_COMPLEXNUMBER_H
#include <canopy/math/ComplexNumber.h>
#endif

#include <vector>
#include <memory>

namespace thales {
  namespace algorithm {

    /**
     * This class implements the fast-fourier transform. All arrays must be
     * padded to a size that is a power of 2.
     */
    class FFT {
      FFT&operator=(const FFT&);
      FFT(const FFT&);

      /** Constructor */
    protected:
      FFT() throws();

      /** Destructor */
    protected:
      virtual ~FFT() throws();

      /**
       * Get an FFT algorithm implementation. Clients should use this instead
       * of allocating a new FFT instance. In the future, the implementation
       * will be configurable and thus support for better FFT implementations
       * will be available at runtime.
       * @return an instance of this algorith
       */
    public:
      static const FFT& instance() throws();

      /**
       * Perform an FFT on the specified array of complex numbers. 
       * The size of the array must be a power of 2.
       * @param n the size of the input array
       * @param input the input and output array
       * @param output an array of size n of the resulting complex numbers
       */
    public:
      virtual void doFFT(size_t n, const double* input, ::canopy::math::ComplexNumber* output) const throws();

      /**
       * Perform an FFT on the specified array of complex numbers. 
       * The size of the array must be a power of 2.
       * @param n the size of the input array
       * @param stride the distance between adjacent elements in the data array
       * @param data the input and output array
       */
    public:
      virtual void doFFT(size_t n, size_t stride, ::canopy::math::ComplexNumber* data) const throws();

      /**
       * Perform an FFT on the specified array of complex numbers. 
       * The size of the array must be a power of 2.
       * @param n the size of the input array
       * @param data the input and output array
       */
    public:
      virtual void doFFT(size_t n, ::canopy::math::ComplexNumber* data) const throws();

      /**
       * Perform an inverse FFT on the specified array of complex numbers. 
       * The size of the array must be a power of 2.
       * @param n the size of the input array
       * @param data the input and output array
       */
    public:
      virtual void doInverseFFT(size_t n, ::canopy::math::ComplexNumber* data) const throws();

      /**
       * Perform an inverse FFT on the specified array of complex numbers. 
       * The size of the array must be a power of 2.
       * @param n the size of the input array
       * @param stride the distance to the next element in the fft
       * @param data the input and output array
       */
    public:
      virtual void doInverseFFT(size_t n, size_t stride, ::canopy::math::ComplexNumber* data) const throws();
    };
  }
}

#endif
