#include <thales/algorithm/FFT.h>
#include <cmath>
#include <cstdlib>
#include <cassert>

using namespace ::std;
using namespace ::canopy::math;

namespace thales {
  namespace algorithm {
    namespace {
      static const double PI = M_PI;

      // count the 1 bits in the specified value
      static size_t countBits (size_t n)
      {
	size_t nBits=0;
	size_t mask = 1;
	while(mask!=0) {
	  if ((mask & n) != 0) {
	    ++nBits;
	  }
	  mask <<= 1;
	}
	return nBits;
      }

      
      static size_t bitReverse (size_t n, size_t N)
      {
	size_t res = 0;
	while(N!=1) {
	  res <<= 1;
	  res |= (n & 1);
	  N >>= 1;
	  n >>= 1;
	}
	return res;
      }

      static ComplexNumber nthRootOfUnity (size_t k, size_t N)
      {
	return ComplexNumber::fromPolar(1,(-2.0*PI*k)/N);
      }
      
      static ComplexNumber nthRootOfUnityInverse (size_t k, size_t N)
      {
	return ComplexNumber::fromPolar(1,(2.0*PI*k)/N);
      }
      
      static void performFFT(size_t n, size_t stride, ::canopy::math::ComplexNumber* data) throws()
      {
	if (countBits(n)!=1) {
	  assert("Not a power of two"==0);
	  ::std::abort();
	}
	
	// sort the array as needed by the Cookley-Turkey algorithm
	for (size_t i=0;i<n;++i) {
	  size_t j = bitReverse(i,n);
	  if (j<i) {
	    // this if statement ensures that we
	    // only swap i,j once during the scan
	    ::std::swap(data[i*stride],data[j*stride]);
	  }
	}
	
	size_t m =1;
	size_t N =2;
	while(m!=n) {        // executed log(n) times!
	  assert(m==N/2);
	  // the two nested loops are executed m + n/(2*m) = (2*m^2 + n)/(2*m)
	  for (size_t j=0;j<m;++j) {
	    ComplexNumber root = nthRootOfUnity(j,N);
	    for (size_t k=j;k<n;k+=N) {
	      ComplexNumber even = data[k*stride];
	      ComplexNumber odd = root*data[(k+m)*stride];
	      
	      data[k*stride] = even + odd;
	      data[(k+m)*stride] = even - odd;
	    }
	  }
	  m = N;
	  N *= 2;
	}
      }
      static void performFFT(size_t n, ::canopy::math::ComplexNumber* data) throws()
      {
	if (countBits(n)!=1) {
	  assert("Not a power of two"==0);
	  ::std::abort();
	}
	
	// sort the array as needed by the Cookley-Turkey algorithm
	for (size_t i=0;i<n;++i) {
	  size_t j = bitReverse(i,n);
	  if (j<i) {
	    // this if statement ensures that we
	    // only swap i,j once during the scan
	    ::std::swap(data[i],data[j]);
	  }
	}
	
	size_t m =1;
	size_t N =2;
	while(m!=n) {        // executed log(n) times!
	  assert(m==N/2);
	  // the two nested loops are executed m + n/(2*m) = (2*m^2 + n)/(2*m)
	  for (size_t j=0;j<m;++j) {
	    ComplexNumber root = nthRootOfUnity(j,N);
	    for (size_t k=j;k<n;k+=N) {
	      ComplexNumber even = data[k];
	      ComplexNumber odd = root*data[k+m];
	      
	      data[k] = even + odd;
	      data[k+m] = even - odd;
	    }
	  }
	  m = N;
	  N *= 2;
	}
      }

      static void performInverseFFT(size_t n, size_t stride, ::canopy::math::ComplexNumber* data) throws()
      {
	if (countBits(n)!=1) {
	  assert("Not a power of two"==0);
	  ::std::abort();
	}
	
	// sort the array as needed by the Cookley-Turkey algorithm
	for (size_t i=0;i<n;++i) {
	  size_t j = bitReverse(i,n);
	  if (j<i) {
	    // this if statement ensures that we
	    // only swap i,j once during the scan
	    ::std::swap(data[i*stride],data[j*stride]);
	  }
	}
	
	size_t m =1;
	size_t N =2;
	while(m!=n) {        // executed log(n) times!
	  assert(m==N/2);
	  // the two nested loops are executed m + n/(2*m) = (2*m^2 + n)/(2*m)
	  for (size_t j=0;j<m;++j) {
	    ComplexNumber root = nthRootOfUnityInverse(j,N);
	    for (size_t k=j;k<n;k+=N) {
	      ComplexNumber even = data[k*stride];
	      ComplexNumber odd = root*data[(k+m)*stride];
	      
	      data[k*stride] = even + odd;
	      data[(k+m)*stride] = even - odd;
	    }
	  }
	  m = N;
	  N *= 2;
	}
	for (size_t i=0;i<n;++i) {
	  data[stride*i] /= ComplexNumber(n);
	}
      }
      static void performInverseFFT(size_t n, ::canopy::math::ComplexNumber* data) throws()
      {
	if (countBits(n)!=1) {
	  assert("Not a power of two"==0);
	  ::std::abort();
	}
	// sort the array as needed by the Cookley-Turkey algorithm
	for (size_t i=0;i<n;++i) {
	  size_t j = bitReverse(i,n);
	  if (j<i) {
	    // this if statement ensures that we
	    // only swap i,j once during the scan
	    ::std::swap(data[i],data[j]);
	  }
	}
	
	size_t m =1;
	size_t N =2;
	while(m!=n) {        // executed log(n) times!
	  assert(m==N/2);
	  // the two nested loops are executed m + n/(2*m) = (2*m^2 + n)/(2*m)
	  for (size_t j=0;j<m;++j) {
	    ComplexNumber root = nthRootOfUnityInverse(j,N);
	    for (size_t k=j;k<n;k+=N) {
	      ComplexNumber even = data[k];
	      ComplexNumber odd = root*data[(k+m)];
	      
	      data[k] = even + odd;
	      data[(k+m)] = even - odd;
	    }
	  }
	  m = N;
	  N *= 2;
	}
	for (size_t i=0;i<n;++i) {
	  data[i] /= ComplexNumber(n);
	}
      }
    }
    
    FFT::FFT() throws()
    {}
    FFT::~FFT() throws()
    {}
    
    const FFT& FFT::instance() throws()
    {
      static const FFT INSTANCE;
      return INSTANCE;
    }
      
    void FFT::doFFT(size_t n, size_t stride, ::canopy::math::ComplexNumber* data) const throws()
    {
      performFFT(n,stride,data);
    }
      
    void FFT::doFFT(size_t n, ::canopy::math::ComplexNumber* data) const throws()
    {
      performFFT(n,data);
    }
      
    void FFT::doInverseFFT(size_t n, size_t stride, ::canopy::math::ComplexNumber* data) const throws()
    {
      performInverseFFT(n,stride,data);
    }
      
    void FFT::doInverseFFT(size_t n, ::canopy::math::ComplexNumber* data) const throws()
    {
      performInverseFFT(n,data);
    }
    
    void FFT::doFFT(size_t n, const double*input, ::canopy::math::ComplexNumber* output) const throws ()
    {
      for (size_t i=0;i<n;++i) {
	output[i] = input[i];
      }
      doFFT(n,1,output);
    }
      
  }
}
