#include <thales/algorithm/Munkres.h>

using namespace ::std;

namespace thales {
  namespace algorithm {
    /**
     * The munkres 2D assignment algorithm.
     * @param m a cost matrix
     * @return a vector pairs of row index, column index representing the selected entries
     */
      vector<pair<size_t,size_t> > solveMunkres (const ::newton::Matrix<double>& m) throws (exception)
    {
      vector<pair<size_t,size_t> > result;
      result.reserve(m.colSize());

      ::newton::Matrix<double> M;
      
      if (m.rowSize() < m.colSize()) {
	::newton::Matrix<double> xm(m.colSize(),m.rowSize());
	for (size_t i=0,ni=m.rowSize();i<ni;++i) {
	  for (size_t j=0,nj=m.colSize();j<nj;++j) {
	    xm(j,i) = m(i,j);
	  }
	}
	xm.swap(M);
      }
      else {
	M = m;
      }


      const size_t nr = M.rowSize();
      const size_t nc = M.colSize();
      
      vector<int> starredCols(nc,-1),starredRows(nr,-1);
      vector<int> primedCols(nc,-1),primedRows(nr,-1);
      vector<bool> coveredCols(nc,false),coveredRows(nr,false);
      vector<pair<int,int> > pairs;

      int primedZi,primedZj;
      
    STEP1:
      {
	for (size_t j=0;j<nc;++j) {
	  double min = M(0,j);
	  for (size_t i=1;i<nr;++i) {
	    double t = M(i,j);
	    if (t < min) { 
	      min = t;
	    }
	  }
	  for (size_t i=0;i<nr;++i) {
	    M(i,j) -= min;
	  }
	}

	for (size_t i=0;i<nr;++i) {
	  if (starredRows[i] < 0) { 
	    for (size_t j=0;j<nc;++j) {
	      if (M(i,j)==0 && starredCols[j]<0) {
		starredRows[i] =j;
		starredCols[j] =i;
		break;
	      }
	    }
	  }
	}
      }
    STEP2:
      {
	result.clear();
	// count the number of colums
	for (size_t j=0;j<nc;++j) {
	  int i=starredCols[j];
	  if (i<0) {
	    coveredCols[j] = false;
	  }
	  else {
	    result.push_back(make_pair(size_t(i),size_t(j)));
	    coveredCols[j] = true;
	  }
	}
	if (result.size()==nc) {
	  goto DONE;
	}
      }
    STEP3:
      {
	// find a non-covered zer0
	for (size_t i=0;i<nr;++i) {
	  if (!coveredRows[i]) {
	    for (size_t j=0;j<nc;++j) {
	      if (!coveredCols[j]) {
		if (M(i,j)==0) {
		  primedRows[i] = j;
		  primedCols[j] = i;
		  if (starredRows[i]<0) {
		    primedZi = i;
		    primedZj = j;
		    goto STEP4;
		  }
		  else {
		    coveredRows[i] = true;
		    coveredCols[starredRows[i]] = false;
		    goto STEP3;
		  }
		}
	      }
	    }
	  }
	}
	goto  STEP5;
      }
    STEP4:
      {
	pairs.clear();
	int i = primedZi;
	int j = primedZj;
	do {
	  pairs.push_back(make_pair(i,j));
	  i = starredCols[j];
	  if (i < 0) {
	    break;
	  }
	  pairs.push_back(make_pair(i,j));
	  j = primedRows[i];
	  assert(j>=0);
	} while(true);
	
	for (size_t k=1;k<pairs.size();k+=2) {
	  i = pairs[k].first;
	  j = pairs[k].second;
	  starredRows[i] = -1;
	  starredCols[j] = -1;
	}
	for (size_t k=0;k<pairs.size();k+=2) {
	  i = pairs[k].first;
	  j = pairs[k].second;
	  starredRows[i] = j;
	  starredCols[j] = i;
	}
	primedRows.assign(nr,-1);
	primedCols.assign(nc,-1);
	coveredRows.assign(nr,false);
	coveredCols.assign(nr,false);
	goto STEP2;
      }
     STEP5:
      {
	bool found=false;
	double min = 0;
	for (size_t i=0;i<nr;++i) {
	  if (!coveredRows[i]) {
	    for (size_t j=0;j<nc;++j) {
	      if (!coveredCols[j]) {
		if (!found || M(i,j) < min) {
		  min = M(i,j);
		  found = true;
		}
	      }
	    }
	  }
	}
	for (size_t i=0;i<nr;++i) {
	  if (coveredRows[i]) {
	    for (size_t j=0;j<nc;++j) {
	      M(i,j) += min;
	    }
	  }
	}
	for (size_t j=0;j<nc;++j) {
	  if (!coveredCols[j]) {
	    for (size_t i=0;i<nr;++i) {
	      M(i,j) -= min;
	    }
	  }
	}
	goto STEP3;
      }
      
    DONE:
      if (nr != m.rowSize()) {
	// swap the result entries as the initial matrix was swapped
	for (vector<pair<size_t,size_t> >::iterator k=result.begin();k!=result.end();++k) {
	  ::std::swap(k->first,k->second);
	}
      }
      return result;
    }

  }
}
  
