#ifndef _THALES_IMAGE_GRADIENTFUNCTION_H
#define _THALES_IMAGE_GRADIENTFUNCTION_H

#ifndef _TIMBER_MEDIA_INTENSITYIMAGE_H
#include <timber/media/IntensityImage.h>
#endif

#ifndef _THALES_IMAGE_IMAGEGRADIENT_H
#include <thales/image/ImageGradient.h>
#endif

#include <memory>

namespace thales {
  namespace image {

    /**
     * This class defines an interface to compute 2D image gradients.
     */
    class GradientFunction {

      /**
       * Constructor.
       */
    protected:
      GradientFunction() throws();

      /**
       * Destructor
       */
    public:
      virtual ~GradientFunction() throws() = 0;

      /**
       * Compute the gradient of the specified image.
       * @param img an image
       * @return the gradient
       */
    public:
      virtual ::timber::Pointer<ImageGradient> computeGradient (const ::timber::SharedRef< ::timber::media::ImageBuffer>& img) const throws() = 0;

      /**
       * Compute the gradient of the specified image using  
       * <em>central difference</em> partial derivatives. The gradient will be computed in the
       * on a gray-scale image.
       */
    public:
      static ::std::unique_ptr<GradientFunction> createCentralDifferenceGradient() throws();


    };

  }
}

#endif
