#include <thales/image/TemporalFilter.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::media;

namespace thales {
   namespace image {
      namespace {

         template<class T> struct Queue
         {
            private:
               Queue(const Queue&);
               Queue&operator=(const Queue&);
            public:
               Queue(size_t sz) :
                     _queue(new T[sz]), _capacity(sz), _pos(0), _size(0)
               {
               }
               ~Queue()throws()
               {  delete[] _queue;}

               void enqueue(const T& t)throws()
               {
                  if (_size==_capacity) {
                     _queue[_pos] = t;
                     _pos = (_pos+1)%_capacity;
                  }
                  else {
                     _queue[(_pos+_size) % _capacity]=t;
                     ++_size;
                  }
               }
               void clear()throws() {_pos = 0; _size = 0;}
               size_t size() const throws() {return _size;}
               size_t capacity() const throws() {return _capacity;}
               bool isFull() const throws() {return _capacity==_size;}
               bool isEmpty() const throws() {return _size==0;}

               const T& operator[](size_t i) const throws()
               {  return _queue[(_pos+i)%_capacity];}

            private:
               T* _queue;
               size_t _capacity;
               size_t _pos;
               size_t _size;
         };

         void blendPixels(size_t n, const ImageBuffer::PixelColor* a, const double* alpha, ImageBuffer::PixelColor* p)
         {
            double r = 0;
            double g = 0;
            double b = 0;
            for (size_t i = 0; i < n; ++i) {
               r += a[i]._red * alpha[i];
               g += a[i]._green * alpha[i];
               b += a[i]._blue * alpha[i];
            }
            p->_red = static_cast< UShort> (r + .5);
            p->_green = static_cast< UShort> (g + .5);
            p->_blue = static_cast< UShort> (b + .5);
            p->_opacity = ImageBuffer::MAX_OPACITY;
         }

      }

      TemporalFilter::TemporalFilter()
      throws()
      {}

      TemporalFilter::~TemporalFilter()
      throws()
      {}

      unique_ptr< TemporalFilter> TemporalFilter::createNoFilter()
      throws()
      {
         struct Impl : public TemporalFilter {
            Impl() throws() {}
            ~Impl() throws() {}

            Frame enqueue (const Frame& f) throws()
            {  return f;}
         };

         TemporalFilter* tf = new Impl();
         return unique_ptr<TemporalFilter>(tf);
      }

      unique_ptr< TemporalFilter> TemporalFilter::createExponentialFilter(double alpha)
      throws()
      {
         struct Impl : public TemporalFilter {
            Impl(double xalpha) throws()
            {
               _alpha[0] = 1-xalpha;
               _alpha[1] = xalpha;
            }

            ~Impl() throws() {}

            Frame enqueue (const Frame& f) throws()
            {
              auto img = f.image;
               size_t w = img->width();
               size_t h = img->height();

               if (!_history || _history->width()!=w || _history->height()!=h) {
                  // process the first image or if the dimensions have changed
                  _history = f.image;
                  return f;
               }

               ::std::unique_ptr<ImageBuffer::PixelColor[]> cols( new ImageBuffer::PixelColor[w*h]);
               ImageBuffer::PixelColor pixels[2];
               for (size_t y=0;y<h;++y) {
                  for (size_t x=0;x<w;++x) {
                     _history->pixel(x,y,pixels[0]);
                     img->pixel(x,y,pixels[1]);
                     blendPixels(2,pixels,_alpha,&cols[y*w+x]);
                  }
               }
               auto buf = ImageBuffer::createImage(w,h,move(cols));
               _history = buf;
               return Frame(f.frame,buf);
            }

            private:
            double _alpha[2];
            ::std::shared_ptr<ImageBuffer> _history;
         };

         assert(alpha > 0 && alpha <= 1);
         if (alpha==1) {
            return createNoFilter();
         }

         TemporalFilter* tf = new Impl(alpha);
         return unique_ptr<TemporalFilter>(tf);
      }

      unique_ptr< TemporalFilter> TemporalFilter::createWeightedAverageFilter(size_t nAlpha, const double* alpha)
throws   ()
   {
      struct Impl : public TemporalFilter {
         Impl(size_t xn, const double* xalpha) throws()
         : _weights(new double[xn]),
         _history(xn),
         _pixels(new ImageBuffer::PixelColor[xn])
         {
            for (size_t i=0;i<xn;++i) {
               _weights[i] = xalpha[i];
            }
         }

         ~Impl() throws()
         {
            delete[] _weights;
            delete[] _pixels;
         }

         Frame enqueue (const Frame& f) throws()
         {
            if (!f.image) {
               return Frame();
            }

            if (_history.isEmpty()) {
               // process the first firame
               _history.enqueue(f);
               return Frame();
            }
            const Frame& g = _history[0];
            size_t w = g.image->width();
            size_t h = g.image->height();

            if (g.image->width()!=w || g.image->height()!=h) {
               _history.clear();
               _history.enqueue(f);
               return Frame();
            }
            _history.enqueue(f);

            // make sure we have enough frames
            if (!_history.isFull()) {
               return Frame();
            }

            ::std::unique_ptr<ImageBuffer::PixelColor[]> cols ( new ImageBuffer::PixelColor[w*h]);
            const size_t n = _history.size();
            for (size_t y=0;y<h;++y) {
               for (size_t x=0;x<w;++x) {
                  for (size_t t=0;t<n;++t) {
                     _history[t].image->pixel(x,y,_pixels[t]);
                  }
                  blendPixels(n,_pixels,_weights,&cols[y*w+x]);
               }
            }
            auto buf = ImageBuffer::createImage(w,h,move(cols));
            return Frame(_history[n/2].frame,buf);
         }

         private:
         double* _weights;
         Queue<Frame> _history;
         ImageBuffer::PixelColor* _pixels;
      };

      assert((nAlpha % 2)==1 && "Number of weights must be odd");

      TemporalFilter* tf = new Impl(nAlpha,alpha);
      return unique_ptr<TemporalFilter>(tf);
   }

}
}

