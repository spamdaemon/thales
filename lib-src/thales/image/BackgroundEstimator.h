#ifndef _THALES_IMAGE_BACKGROUNDESTIMATOR_H
#define _THALES_IMAGE_BACKGROUNDESTIMATOR_H

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

namespace thales {
  namespace image {
    
    /**
     * This class provides an interface for estimating background and foreground in images. The input
     * to this algorithm is a series of images looking at the same scene (always same field of view).
     * The output of the estimator is a single black-white image where black is the background and
     * white represents forground pixels.
     * <p>
     * The set of images add to this estimator, should all have the same size, but the what happens if 
     * images of different sizes are passed to the estimator is dependent on the class instance. However, 
     * the implementation of this estimator must not fail if different sized images are added.
     */
    class BackgroundEstimator {
      CANOPY_BOILERPLATE_PREVENT_COPYING(BackgroundEstimator);

      /** The constructor */
    protected:
      BackgroundEstimator() throws();

      /** The destructor */
    public:
      virtual ~BackgroundEstimator() throws() = 0;

      /**
       * Incorporate a new image and return an estimation of the background and current foreground.
       * @param image an image
       * @return a binary image where black indicates background and white indicates a foreground object.
       */
    public:
      virtual ::timber::SharedRef< ::timber::media::ImageBuffer> addImage (const ::timber::SharedRef< ::timber::media::ImageBuffer>& image) throws() = 0;

    };
  }
}

#endif
