#ifndef _THALES_IMAGE_LUCASKANADEFLOWESTIMATOR_H
#define _THALES_IMAGE_LUCASKANADEFLOWESTIMATOR_H

#ifndef _THALES_IMAGE_OPTICALFLOWESTIMATOR_H
#include <thales/image/OpticalFlowEstimator.h>
#endif

#include <memory>

namespace thales {
  namespace image {
    
    /**
     * This class implements the Lucas-Kanade optical flow estimation algorithm. This algorithm
     * is described in 
     * <code>
     * </code>
     * <p>
     */
    class LucasKanadeFlowEstimator : public OpticalFlowEstimator {
          CANOPY_BOILERPLATE_PREVENT_COPYING(LucasKanadeFlowEstimator);

      /** The implementation */
    private:
      class Impl;

      /** The constructor */
    private:
      LucasKanadeFlowEstimator() throws();

      /** The destructor */
    public:
      ~LucasKanadeFlowEstimator() throws();

      /**
       * Create a new estimator. The number of levels used will be maxLevel+1.
       *
       * @param maxLevel the maximum level to use in a multiresoluition scheme.
       * @param patchSize the size of the region around a pixel for sampling
       * @return an flowEstimator
       */
    public:
      static ::std::unique_ptr<LucasKanadeFlowEstimator> createEstimator (size_t maxLevel, size_t patchSize) throws();

    };
  }
}

#endif
