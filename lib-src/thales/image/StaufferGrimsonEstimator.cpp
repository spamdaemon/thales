#include <thales/image/StaufferGrimsonEstimator.h>
#include <cmath>
#include <cstring>
#include <iostream>
#include <fstream>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::media;

#define ENABLE_LOGFILE 0

namespace thales {
  namespace image {

    namespace {
      
      static const float GATE_SIZE[] = { 0,2.5f*2.5f,2*2.5f*2.5f,3*2.5f*2.5f };
      
      struct RGBPixel{
	
	static const size_t N = 3;

	
	RGBPixel (const ImageBuffer::PixelColor& col)
	{
	  _value[0] = static_cast<float>(col._red/257);
	  _value[1] = static_cast<float>(col._green/257);
	  _value[2] = static_cast<float>(col._blue/257);
	}
	
	const float* value() const throws() { return _value; }
	
      private:
	float _value[N];
      };

      struct GrayPixel {
	static const size_t N = 1;

	GrayPixel (const ImageBuffer::PixelColor& col)
	{
	  UInt imean = col._red;
	  imean += col._green;
	  imean += col._blue;
	  imean /= (3*257);
	  _value[0] = static_cast<float>(imean);
	}

	const float* value() const throws() { return _value; }

      private:
	float _value[N];
      };

      // a gaussian
      template <class T> class VectorFilter {

	/** The pixel type */
      public:
	typedef T PixelColor;

	/** The size of the vector */
      public:
	static const size_t N = PixelColor::N;

      public:
	VectorFilter() throws()
	: _weight(1) 
	{
	  for (size_t i=0;i<N;++i) {
	    _mean[i]=0;
	    _variance[i] = 1;
	  }
	}
      public:
	~VectorFilter() throws() {}

	/** 
	 * Swap this filter with another one.
	 * @param f another filter
	 */
      public:
	void swap (VectorFilter& f) throws()
	{
	  for (size_t i=0;i<N;++i) {
	    ::std::swap(_mean[i],f._mean[i]);
	    ::std::swap(_variance[i],f._variance[i]);
	  }
	  ::std::swap(_weight,f._weight);
	}

	/**
	 * Reinitialize this filter.
	 * @param m the mean
	 * @param v the variance
	 * @param w the initial weight
	 */
      public:
	inline void reinitialize(const PixelColor& m, float v, float w) throws()
	{ 
	  for (size_t i=0;i<N;++i) {
	    _mean[i] = m.value()[i];
	    _variance[i] = v;
	  }
	  _weight = w;
	}

	/**
	 * Get the mean
	 * @return the mean
	 */
      public:
	inline float mean(size_t i) const throws() { return _mean[i]; }

	/**
	 * Get the variance
	 * @return the variance
	 */
      public:
	inline float variance(size_t i) const throws() { return _variance[i]; }

	/**
	 * Get the weight
	 * @return the weight
	 */
      public:
	inline float weight() const throws() { return _weight; }

	/**
	 * Scale the weight by the specified value.
	 * @param ws the weight-scale factor
	 */
      public:
	inline void scaleWeight (float ws) throws()
	{ _weight *= ws; }

	/**
	 * Get the quality of this filter.
	 * @return weight/_variance
	 */
      public:
	inline double quality () const throws()
	{
	  double var = _variance[0];
	  for (size_t i=1;i<N;++i) {
	    var *= _variance[i];
	  }
	  return (_weight*_weight)/var; 
	}
 

	/**
	 * Update the weight
	 * @param alpha the alpha value
	 */
      public:
	inline void updateWeight (float alpha) throws()
	{
	  _weight *= (1-alpha);
	}

	/**
	 * Update this filter with the specified measurements. If m does not match
	 * then only the weight of this filter is adjusted.
	 * @param m a measurement
	 * @param dist2 the maximum allowed distance for the update (gate size)^2
	 * @param alpha the learning rate
	 * @return true if this filter was updated, false otherwise.
	 */
      public:
	bool update (const PixelColor& Z, float dist2, float alpha, double Zvariance) throws()
	{
	  double innovation[N];
	  
	  double d2 = 0;
	  for (size_t i=0;i<N;++i) {
	    innovation[i] = Z.value()[i] - _mean[i];
	    d2 += innovation[i]*innovation[i] / (_variance[i]+Zvariance);
	  }
	  assert(d2==d2);
	  if (d2 > dist2) {
	    return false;
	  }
	  // ok, do the update
	  
	  _weight +=alpha; // update the weight first
	  double k = alpha / _weight;
	  
	  assert(k<=1);

	  // we're assuming that the color components are independent, and so we don't 
	  // have to account for the covariance between the color components
	  for (size_t i=0;i<N;++i) {
	    double mu = _mean[i] + k * (innovation[i]);
	    double dz = Z.value()[i]-mu;
	    double var  = _variance[i] + (k*(Zvariance + dz*dz-_variance[i]));
	    assert(var>0);
	    
	    _mean[i] = static_cast<float>(mu);
	    _variance[i] = static_cast<float>(var);
	  }
	  
	  return true;
	}

	/** The gaussian's weight */
      private:
	float _weight;
	float _mean[N];
	float _variance[N];
      };
      
    }


    template <class T> class StaufferGrimsonEstimator::Impl : public StaufferGrimsonEstimator {
      
      /** The filter type we want to use */
    private:
      typedef VectorFilter<T> Filter;
      typedef typename Filter::PixelColor PixelColor;

    public:
      Impl (size_t nGaussians, double learningRate, double backgroundThreshold, UShort error) throws()
      :
#if ENABLE_LOGFILE == 1
      _logfile(new ::std::ofstream("stauffer.log")),
#endif
	_frame(0),
	_learningRate(static_cast<float>(learningRate)),
	_backgroundThreshold(backgroundThreshold),
	_defaultVariance(static_cast<double>(error)*error),
	_pixelVariance(static_cast<double>(error)*error),
	_nFiltersPerPixel(nGaussians),_width(0),_height(0),_filters(0)
      {
	assert(nGaussians>0);
	assert(learningRate>=0);
	assert(learningRate<=1);
	assert(backgroundThreshold>=0);
	assert(backgroundThreshold<=1);

      }

      ~Impl() throws() 
      {
	delete[] _filters;
      } 

      void setLearningRate (double learningRate) throws()
      {
	assert(learningRate>=0);
	assert(learningRate<=1);
	_learningRate = static_cast<float>(learningRate);
      }

      ::timber::SharedRef<ImageBuffer> addImage (const ::timber::SharedRef<ImageBuffer>& image) throws()
      {
#if ENABLE_LOGFILE == 1
	if (_logfile.get()!=0) {
	  *_logfile << "Frame " << _frame << ::std::endl;
	}
#endif
	const size_t wx = ::std::min(_width,image->width());
	const size_t hx = ::std::min(_height,image->height());
	resize(*image);
	
	size_t byteWidth = (_width+7)/8;
	::std::unique_ptr<char[]> pixels (new char[byteWidth*_height]);
	::std::memset(pixels.get(),0,byteWidth*_height);

	ImageBuffer::PixelColor color;
	for (size_t i=0;i<hx;++i) {
	  for (size_t j=0;j<wx;++j) {
	    Filter* first = &_filters[(i*_width + j)*_nFiltersPerPixel];
	    Filter* last = first+_nFiltersPerPixel;
	    
	    image->pixel(j,i,color);
	    // the mean will be value between 0 and 1
	    const PixelColor col(color);
	    
	    // first, update the weights of all filters

	    for (Filter* f=  first;f!=last;++f) {
	      f->updateWeight(_learningRate);
	    }
	    Filter* fUpdated=0;
	    for (Filter* f=  first;f!=last;++f) {
	      if (f->update(col,GATE_SIZE[Filter::N],_learningRate,_pixelVariance)) {
		// re sort 
		double qk =f->quality();
		
		while (f!=first && (f-1)->quality() < qk) {
		  f->swap(*(f-1));
		  --f;
		}
		while((f+1)!=last && (f+1)->quality() > qk) {
		  f->swap(*(f+1));
		  ++f;
		}
		
		fUpdated = f;
		break;
	      }
	    }

	    // if no filter was updated, then we reinitialize the worst filter
	    // and consider it updated
	    if (fUpdated==0) {
	      Filter* f = last-1;
	      f->reinitialize(col,_defaultVariance,_learningRate);
	      double qk =f->quality();
	      while (f!=first && (f-1)->quality() < qk) {
		f->swap(*(f-1));
		--f;
	      }
	      //	      fUpdated = f;
	    }
	    
	    // renormalize the weights
	    double weightSum=0;
	    for (Filter* f=first;f!=last;++f) {
	      weightSum += f->weight();
	    }
	    assert (weightSum > 0.0);
	    for (Filter* f=first;f!=last;++f) {
	      f->scaleWeight(static_cast<float>(1.0/weightSum));
	    }

	    weightSum=0;
	    Filter* B=first;
	    while(B!=last && weightSum<_backgroundThreshold) {
	      weightSum += B->weight();
	      ++B;
	    }
	    
	    // set the image pixel
	    {
	      char& pixel = pixels[i*byteWidth+j/8];
	      char bit = static_cast<char>(1<<(j&7));
	      if (fUpdated>=B) {
		pixel = static_cast<char>(pixel | bit);
	      }
	      else {
		pixel = static_cast<char>(pixel & ~bit);
	      }
	    }

#if ENABLE_LOGFILE == 1
	    if (Filter::N == 1 && _logfile.get()!=0 && (_width<=25 && _height<=25)) {
	      *_logfile << _frame << " ("<<j<<","<<i<<") : " << col.value()[0] << " : " << (B-first) << " - " << (fUpdated-first) << " : ";
	      for (Filter* f=first;f!=last;++f) {
		*_logfile << "  [" << f->mean(0) << " / " << f->weight() << "/" << sqrt(f->variance(0)) << ']';
	      }
	      *_logfile << ::std::endl;
	    }
#endif    
	  }
	}

	++_frame;
	return ImageBuffer::createBinaryImage(_width,_height,move(pixels));
      }

      /**
       * Resize the filters
       */
    private:
      void resize (const ImageBuffer& image) throws()
      {
	const size_t w = image.width();
	const size_t h = image.height();
	if (w==_width && h==_height) {
	  return;
	}
	
	// create the new filters
	Filter* filters = new Filter[h*w*_nFiltersPerPixel];

	size_t minW = ::std::min(w,_width);
	size_t minH = ::std::min(h,_height);

	// copy over the existing filters, and initialize the 
	// new ones
	for (size_t i=0;i<minH;++i) {
	  for (size_t j=0;j<minW;++j) {
	    for (size_t k=0;k<_nFiltersPerPixel;++k) {
	      filters[(i*w + j)*_nFiltersPerPixel + k] = _filters[(i*_width + j)*_nFiltersPerPixel + k];
	    }
	  }
	}

	// add new filters for each row 
	ImageBuffer::PixelColor color;
	for (size_t i=minH;i<h;++i) {
	  for (size_t j=0;j<w;++j) {
	    image.pixel(j,i,color);
	    // the mean will be value between 0 and 1
	    const PixelColor col(color);
	    for (size_t k=0;k<_nFiltersPerPixel;++k) {
	      Filter& f = filters[(i*w + j)*_nFiltersPerPixel + k];
	      if (k==0) {
		f.reinitialize(col,_defaultVariance,_learningRate);
	      }
	      else {
		f.reinitialize(col,_defaultVariance,_learningRate);
	      }
	    }
	  }
	}
	
	delete[] _filters;
	_filters = filters;

	// add new filter for each column
	for (size_t i=0;i<minH;++i) {
	  for (size_t j=minW;j<w;++j) {
	    image.pixel(j,i,color);
	    // the mean will be value between 0 and 1
	    const PixelColor col(color);
	    for (size_t k=0;k<_nFiltersPerPixel;++k) {
	      Filter& f = filters[(i*w + j)*_nFiltersPerPixel + k];
	      if (k==0) {
		f.reinitialize(col,_defaultVariance,_learningRate);
	      }
	      else {
		f.reinitialize(col,_defaultVariance,_learningRate);
	      }
	    }
	  }
	}
 	_width = w;
	_height = h;
      }

      /** A log file */
    private:
#if ENABLE_LOGFILE == 1
      ::std::unique_ptr< ::std::ofstream> _logfile;
#endif

      /** The number of frames processed */
    private:
      size_t _frame;

      /** The learning rate */
    private:
      float _learningRate;

      /** The background threshold */
    private:
      const double _backgroundThreshold;
      
      /** The default variance for new filter */
    private:
      const double _defaultVariance;

      /** The pixel error */
    private:
      const double _pixelVariance;

      /** The number of filters per pixel */
    private:
      const size_t _nFiltersPerPixel;

      /** The current width and height */
    private:
      size_t _width, _height;

      /** The filters */
    private:
      Filter* _filters;
    };
    
    StaufferGrimsonEstimator::StaufferGrimsonEstimator() throws()
    {}

    StaufferGrimsonEstimator::~StaufferGrimsonEstimator() throws()
    {}

    unique_ptr<StaufferGrimsonEstimator> StaufferGrimsonEstimator::createRGBEstimator (size_t n, double learningRate, double backgroundThreshold, UShort error) throws()
    { return unique_ptr<StaufferGrimsonEstimator>(new Impl<RGBPixel>(n,learningRate,backgroundThreshold,error)); }

    unique_ptr<StaufferGrimsonEstimator> StaufferGrimsonEstimator::createGrayEstimator (size_t n, double learningRate, double backgroundThreshold, UShort error) throws()
    { return unique_ptr<StaufferGrimsonEstimator>(new Impl<GrayPixel>(n,learningRate,backgroundThreshold,error)); }

  }
}
