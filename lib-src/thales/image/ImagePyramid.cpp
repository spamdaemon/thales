#include <thales/image/ImagePyramid.h>
#include <timber/media/ImageBuffer.h>
#include <timber/media/image/ScaleOp.h>

using namespace ::timber;
using namespace ::timber::media;
using namespace ::timber::media::image;

namespace thales {
   namespace image {

      ImagePyramid::ImagePyramid() throws()
      {
      }

      ImagePyramid::~ImagePyramid() throws()
      {
      }

      Reference< ImagePyramid> ImagePyramid::create() throws()
      {
         struct SimplePyramid : public ImagePyramid
         {
               ~SimplePyramid() throws()
               {
               }
               size_t size() const throws()
               {
                  return 0;
               }
               SharedRef< ImageBuffer> level(size_t) const throws()
               {
                  ::std::abort();
               }
         };
         return Reference< ImagePyramid>(new SimplePyramid());
      }

      Reference< ImagePyramid> ImagePyramid::create(const SharedRef< ImageBuffer>& image, size_t nLevels) throws ()
      {
         struct SimplePyramid : public ImagePyramid
         {
               SimplePyramid(const SharedRef< ImageBuffer>& image)
throws                  () : _image(image) {}
                  ~SimplePyramid() throws() {}

                  size_t size() const throws() {return 1;}
                  SharedRef< ImageBuffer> level(size_t levelIndex) const throws() {assert(levelIndex==0); return _image;}

                  private:
                  SharedRef<ImageBuffer> _image;
               };

               struct MultiLevelPyramid : public ImagePyramid {
                  MultiLevelPyramid (const ::timber::SharedRef<ImageBuffer>& image, size_t nLevels)
                  : _nLevels(1),
                  _pyramid(new ::std::shared_ptr<ImageBuffer>[nLevels])
                  {
                     ::std::unique_ptr< ScaleOp> iop(ScaleOp::createHalfResolution());

                     _pyramid[0] = image;
                     while( _nLevels < nLevels) {
                        std::shared_ptr<ImageBuffer>& last = _pyramid[_nLevels-1];
                        if (last->width() <2 || last->height()<2 ) {
                           break;
                        }

                        _pyramid[_nLevels] = iop->apply(last);
                     }
                  }
                  ~MultiLevelPyramid() throws() {}

                  size_t size() const throws() {return _nLevels;}

                  ::timber::SharedRef< ImageBuffer> level(size_t levelIndex) const throws()
                  {
                     assert(levelIndex<_nLevels);
                     return _pyramid[levelIndex];
                  }

                  private:
                  size_t _nLevels;
                  ::std::unique_ptr<::std::shared_ptr<ImageBuffer>[]> _pyramid;

               };

               assert (nLevels>0 && "Need at least 1 level");

               if (nLevels==1) {
                  return Reference< ImagePyramid>(new SimplePyramid(image));
               }
               else {
                  return Reference< ImagePyramid>(new MultiLevelPyramid(image,nLevels));
               }
            }

         }
      }
