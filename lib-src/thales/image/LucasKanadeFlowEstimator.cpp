#include <thales/image/LucasKanadeFlowEstimator.h>
#include <thales/image/GradientFunction.h>
#include <thales/image/ImageGradient.h>

#include <timber/media/image/ScaleOp.h>

#include <cmath>
#include <cstring>
#include <iostream>

#define TRACE 0

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::timber::media;

namespace thales {
   namespace image {

      namespace {

         static double interpolatePixel(const IntensityImage& i, double px, double py)
         {
            Int32 ix = static_cast< Int32>(::std::floor(px));
            Int32 iy = static_cast< Int32>(::std::floor(py));

            if (ix < 0 || iy < 0) {
               return -1;
            }
            if (ix >= static_cast< Int32>(i.width() - 1) || iy >= static_cast< Int32>(i.height() - 1)) {
               return -1;
            }

            double dx = px - ix;
            double dy = py - iy;

            double pix = (1 - dx) * (1 - dy) * i.pixel(ix, iy)._intensity;
            pix += dx * (1 - dy) * i.pixel(ix + 1, iy)._intensity;
            pix += dx * dy * i.pixel(ix + 1, iy + 1)._intensity;
            pix += (1 - dx) * (dy) * i.pixel(ix, iy + 1)._intensity;

            return pix;
         }
      }

      struct LucasKanadeFlowEstimator::Impl : public LucasKanadeFlowEstimator
      {
            Impl(size_t maxLevel, size_t patchSize)throws() :
            _maxLevel(maxLevel), _patchSize(patchSize), _allImages(0), _newPyramid(0), _referencePyramid(0), _gradient(
                  GradientFunction::createCentralDifferenceGradient())
            {
               _allImages = new ::std::shared_ptr< IntensityImage> [2 * (_maxLevel + 1)];
               _newPyramid = _allImages;
               _referencePyramid = _allImages + _maxLevel + 1;
            }

            ~Impl()throws()
            {
               delete[] _allImages;
            }

            static OpticalFlow::Vector updateFlowNew(int x, int y, size_t level, ::timber::SharedRef< IntensityImage> I,
                  Reference< ImageGradient> imageGradient, ::timber::SharedRef< IntensityImage> J, size_t patchSize,
                  const OpticalFlow::Vector& g)
            {
               const int w = imageGradient->width();
               const int h = imageGradient->height();

               assert(w > 0);
               assert(h > 0);

               int patchHalfWidth = patchSize;
               int patchHalfHeight = patchSize;

               // compute the coordinates of the patch
               int ymin = ::std::max(0, y - patchHalfHeight);
               int ymax = ::std::min(h - 1, y + patchHalfHeight);
               int xmin = ::std::max(0, x - patchHalfWidth);
               int xmax = ::std::min(w - 1, x + patchHalfWidth);

               double maxGradient = 0;
               double minGradient = 100000000000.0;
               double maxIntensity = 0;
               double minIntensity = 10000000000.0;

               double magGrad = imageGradient->at(x, y).magnitude();
               maxGradient = ::std::max(maxGradient, magGrad);
               minGradient = ::std::min(minGradient, magGrad);

               // do at most N iterations
               size_t N = 20;
               double iterationThreshold = .03;

               // compute a small gradient matrix
               double G[2][2] { { 0, 0 }, { 0, 0 } };
               // compute the gradient matrix in the small neighbor hood
               for (int yy = ymin; yy <= ymax; ++yy) {
                  for (int xx = xmin; xx <= xmax; ++xx) {
                     const ImageGradient::Vector& gradient = imageGradient->at(xx, yy);
                     G[0][0] += gradient.x * gradient.x;
                     G[0][1] += gradient.x * gradient.y;
                     G[1][0] = G[0][1];
                     G[1][1] += gradient.y * gradient.y;
                  }
               }
               double detG = G[0][0] * G[1][1] - G[0][1] * G[1][0];
               if (::std::abs(detG) < 1E-3){
                  return OpticalFlow::Vector();
               }
               //FIXME: invert G here; do via the determinant
               double Ginv[2][2] = { { G[1][1] / detG, -G[1][0] / detG }, { -G[0][1] / detG, G[0][0] / detG } };

               // iterate a few times
               OpticalFlow::Vector v;
               for (size_t i = 0; i < N; ++i) {
                  // compute the image difference
                  double b[2] = { 0, 0 };
                  for (int yy = ymin; yy <= ymax; ++yy) {
                     for (int xx = xmin; xx <= xmax; ++xx) {

                        // compute the image difference of the neighborhood pixel xx,yy
                        const double pi = I->pixel(xx, yy)._intensity;
                        const double pj = interpolatePixel(*J, xx + g.x + v.x, yy + g.y + v.y);
                        if (pj < 0) {
                           // returns -1 if we've hit the edge of the image
                           break;
                        }
                        const double deltaIJ = pi - pj;
                        const ImageGradient::Vector& gradient = imageGradient->at(xx, yy);
                        b[0] += deltaIJ * gradient.x;
                        b[1] += deltaIJ * gradient.y;
                     }
                  }

                  // multiply by the inverse of G
                  double deltaV[2] = { Ginv[0][0] * b[0] + Ginv[0][1] * b[1], Ginv[1][0] * b[0] + Ginv[1][1] * b[1] };

                  OpticalFlow::Vector vOld = v;

                  v.x += deltaV[0];
                  v.y += deltaV[1];

                  if (::std::abs(vOld.x-v.x) < iterationThreshold && std::abs(vOld.y-v.y) < iterationThreshold) {
                     break;
                  }
               }
               return v;
            }

            Pointer< OpticalFlow> computeFlow(size_t level, Reference< IntensityImage> I, Reference< ImageGradient> G,
                  Reference< IntensityImage> J, size_t patchSize, Pointer< OpticalFlow> oflow)
            {

               const int w = G->width();
               const int h = G->height();

               assert(w > 0);
               assert(h > 0);

               OpticalFlow::Vector* flow = new OpticalFlow::Vector[w * h];
               int patchHalfWidth = patchSize;
               int patchHalfHeight = patchSize;

               double maxGradient = 0;
               double minGradient = 100000000000.0;
               double maxIntensity = 0;
               double minIntensity = 10000000000.0;
               size_t nFlowFound = 0;
               double avgIterationCount = 0;
               for (int y = 0; y < h; y+=20) {
                  for (int x = 0; x < w; x+=20) {
                     double magGrad = G->at(x, y).magnitude();
                     maxGradient = ::std::max(maxGradient, magGrad);
                     minGradient = ::std::min(minGradient, magGrad);
                     OpticalFlow::Vector delta;
                     delta.x = delta.y = 0;
                     if (oflow) {
                        delta = oflow->vectorAt(x / 2, y / 2);
                        delta.x *= 2;
                        delta.y *= 2;

                        if (delta.x != delta.x) {
                           delta.x = delta.y = 0;
                        }
                     }
#if TRACE
                     if (x!=10 || y!=10) {
                        //	      continue;
                     }
#endif
                     // compute the coordinates of the patch
                     int ymin = ::std::max(0, y - patchHalfHeight);
                     int ymax = ::std::min(h - 1, y + patchHalfHeight);
                     int xmin = ::std::max(0, x - patchHalfWidth);
                     int xmax = ::std::min(w - 1, x + patchHalfWidth);

                     // do at most N iterations
                     size_t N = 10;

                     bool flowFound = false;
                     size_t iteration = 0;
                     while (iteration++ < N) {
                        double sum[5] = { 0, 0, 0, 0, 0 };
                        for (int yy = ymin; yy <= ymax; ++yy) {
                           for (int xx = xmin; xx <= xmax; ++xx) {
                              const double f = interpolatePixel(*J, xx + delta.x, yy + delta.y);
                              if (f < 0) {
                                 xx = xmax + 1;
                                 yy = ymax + 1;
                                 iteration = N;
                                 sum[2] = sum[4] = 0;
                                 break;
                              }
                              else {
                                 const double g = I->pixel(xx, yy)._intensity;
                                 const ImageGradient::Vector grad = G->at(xx, yy);

#if TRACE
                                 ::std::cerr << "G("<<xx+delta.x<<","<<yy+delta.y<<") = <" << grad.x << ", " << grad.y << "> f: " << f << "  g: " << g
                                 << ::std::endl;
#endif
                                 maxIntensity = ::std::max(maxIntensity, ::std::abs(g - f));
                                 minIntensity = ::std::min(minIntensity, ::std::abs(g - f));
                                 sum[0] += grad.x * (g - f);
                                 sum[1] += grad.y * (g - f);
                                 sum[2] += grad.x * grad.x;
                                 sum[3] += grad.y * grad.y;
                                 sum[4] += grad.x * grad.y;
                              }
                           }
                        }

                        double denom = sum[2] * sum[3] - sum[4] * sum[4];
                        // if the determinant is too small, then we need to ignore this point
                        if (::std::abs(denom) < 1E-1) {
                           ++nFlowFound;
                           break;
                        }

                        double vx = static_cast< float>((sum[0] * sum[3] - sum[1] * sum[4]) / denom);
                        double vy = static_cast< float>((-sum[0] * sum[4] + sum[1] * sum[2]) / denom);

                        delta.x += vx;
                        delta.y += vy;
#if TRACE
                        ::std::cerr << "("<<x<<","<<y<<") : ";
                        ::std::cerr << "Delta: " << delta.x << ", " << delta.y
                        << ::std::endl;
#endif
                        if (::std::abs(vx) < .7 && ::std::abs(vy) < .7) {
                           flowFound = true;
                           break;
                        }
                     }
                     if (!flowFound) {
                        delta.x = delta.y = 0.0 / 0.0;
                        assert(delta.x != delta.x);
                        assert(::std::isnan(delta.x));
                     }
                     else {
                        ++nFlowFound;
                        avgIterationCount += iteration;
                     }
                     flow[y * w + x] = delta;
#if TRACE
                     if (sqrt(delta.x*delta.x + delta.y*delta.y) > 100) {
                        ::std::cerr << "("<<x<<","<<y<<") V: " << delta.x << ", " << delta.y << " : " << sqrt(delta.x*delta.x + delta.y*delta.y) << ::std::endl;
                     }
#endif
                  }
               }
               //	::std::cerr << "Flow at level " << level  << ::std::endl;
               ::std::cerr << "MinMax Intensity : " << minIntensity << ", " << maxIntensity << ::std::endl;
               ::std::cerr << "MinMax Gradient : " << minGradient << ", " << maxGradient << ::std::endl;
               ::std::cerr << "nFlow / Total : " << nFlowFound << ",  " << ((double) nFlowFound) / (w * h) << ", avg "
                     << avgIterationCount / nFlowFound << ::std::endl;
               return OpticalFlow::create(flow, static_cast< size_t>(w), static_cast< size_t>(h));
            }

            Pointer< OpticalFlow> addImage(const ::timber::SharedRef< ImageBuffer>& image)throws()
            {
                return addImage2(image);

#if 0
               unique_ptr< ::timber::media::image::ScaleOp>
               iop(::timber::media::image::ScaleOp::createHalfResolution());

               _newPyramid[0] = IntensityImage::createImage(image);
               for (size_t i = 1; i <= _maxLevel; ++i) {
                  _newPyramid[i] = IntensityImage::createImage(iop->apply(_newPyramid[i - 1]));
               }

               Pointer< OpticalFlow> flow;
               if (_referencePyramid[0]) {
                  for (size_t i = 1 + _maxLevel; i-- > 0;) {
                     if (_newPyramid[i]) {
                        Reference<ImageGradient> G = _gradient->computeGradient(_referencePyramid[i]);
                        flow = computeFlow(i, _referencePyramid[i], G,_newPyramid[i], _patchSize, flow);
                     }
                  }
               }

               ::std::swap(_referencePyramid, _newPyramid);
               return flow;
#endif
            }

            Pointer< OpticalFlow> addImage2(const ::timber::SharedRef< ImageBuffer>& image)throws()
            {
               // build the image pyramid for the new image; this is done by halfing the size
               // of the image for user-specified number of levels
               {
                  unique_ptr< ::timber::media::image::ScaleOp>
                  iop(::timber::media::image::ScaleOp::createHalfResolution());

                  // create the pyramid for the new image; we already have the pyramid for the previous image
                  // in the reference pyramid
                  _newPyramid[0] = IntensityImage::createImage(image);
                  for (size_t i = 1; i <= _maxLevel; ++i) {
                     _newPyramid[i] = IntensityImage::createImage(iop->apply(_newPyramid[i - 1]));
                  }
               }

               OpticalFlow::Vector* flow = new OpticalFlow::Vector[image->width()*image->height()];

               if (_referencePyramid[0]) {
                  // compute the gradients for the reference image
                  unique_ptr< Pointer< ImageGradient>[]> gradients(new Pointer< ImageGradient>[1+_maxLevel]);
                  {
                     for (size_t i = 0; i <= _maxLevel; ++i) {
                        gradients[i] = _gradient->computeGradient(_referencePyramid[i]);
                     }
                  }

                  for (size_t x0=0;x0<image->width();x0 += 20) {
                     for (size_t y0=0;y0<image->height();y0 += 20) {

                        // g is the global displacement
                        OpticalFlow::Vector g;

                        // the current displacement vector at each level
                        OpticalFlow::Vector d;
                        size_t x  = x0;
                        size_t y = y0;
                        for (size_t i = 1 + _maxLevel; i-- > 0;) {
                           x = x0 / (1<<i);
                           y = y0 / (1<<i);

                           // update the estimate for the next iteration
                           g.x = 2*(g.x + d.x);
                           g.y = 2*(g.y + d.y);

                           if (_newPyramid[i]) {
                              // compute the new displacement vector
                              d = updateFlowNew(x,y,i, _referencePyramid[i],gradients[i] , _newPyramid[i], _patchSize, g);
                           }
                        }

                        d.x += g.x;
                        d.y += g.y;

                        flow[y0*image->width()+x0] = d;
                        //::std::cerr << "Flow at : " << x0 << "," << y0 << ": " << d.x << "," << d.y << ::std::endl;
                     }
                  }
               }

               // make the pyramid of the new image the reference pyramid for the next iteration
               ::std::swap(_referencePyramid, _newPyramid);

               return OpticalFlow::create(flow,image->width(),image->height());
            }

            /** The number of levels */
         private:
            const size_t _maxLevel;

            /** The size of a patch */
         private:
            const size_t _patchSize;

            /** All images */
         private:
            ::std::shared_ptr< IntensityImage>* _allImages;

            /** The image pyramid for the new image (referred to as image J) */
         private:
            ::std::shared_ptr< IntensityImage>* _newPyramid;

            /** The image pyramid of the last image  (referred to as image I) */
         private:
            ::std::shared_ptr< IntensityImage>* _referencePyramid;

            /** The gradient function */
         private:
            ::std::unique_ptr< GradientFunction> _gradient;
      };

      LucasKanadeFlowEstimator::LucasKanadeFlowEstimator()
      throws()
      {
      }

      LucasKanadeFlowEstimator::~LucasKanadeFlowEstimator()
      throws()
      {
      }

      unique_ptr< LucasKanadeFlowEstimator> LucasKanadeFlowEstimator::createEstimator(size_t maxLevel, size_t patchSize)
throws   ()
   {
      return unique_ptr< LucasKanadeFlowEstimator> (new Impl(maxLevel, patchSize));
   }

}}
