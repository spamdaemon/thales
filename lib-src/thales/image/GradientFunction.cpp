#include <thales/image/GradientFunction.h>

namespace thales {
  namespace image {

    GradientFunction::GradientFunction() throws()
    {}

    GradientFunction::~GradientFunction() throws()
    {}

    ::std::unique_ptr<GradientFunction> GradientFunction::createCentralDifferenceGradient() throws()
    {
      struct Func : public GradientFunction {
	~Func() throws() {}
	
	::timber::Pointer<ImageGradient> computeGradient (const ::timber::SharedRef< ::timber::media::ImageBuffer>& img) const throws()
	{
	   ::timber::SharedRef< ::timber::media::IntensityImage> xmg = ::timber::media::IntensityImage::createImage(img);
	  return ImageGradient::createCentralDifference(*xmg);
	}
      };

      GradientFunction* f = new Func();
      return ::std::unique_ptr<GradientFunction>(f);
    }


  }
}
