#ifndef _THALES_IMAGE_OPTICALFLOWESTIMATOR_H
#define _THALES_IMAGE_OPTICALFLOWESTIMATOR_H

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

#ifndef _THALES_IMAGE_OPTICALFLOW_H
#include <thales/image/OpticalFlow.h>
#endif

namespace thales {
  namespace image {

    /**
     * The Optical Flow estimator provides an alternative approach to the Background Subtraction approach
     * for motion detection. 
     */
    class OpticalFlowEstimator {
          CANOPY_BOILERPLATE_PREVENT_COPYING(OpticalFlowEstimator);

      /** The constructor */
    protected:
      OpticalFlowEstimator() throws();

      /** The destructor */
    public:
      virtual ~OpticalFlowEstimator() throws() = 0;

      /**
       * Incorporate a new image and return a black-and-white image where each white pixel represents movement as defined
       * by the implementation.
       * @param image an image
       * @return a binary image where black indicates no-movement and white indicates movement.
       */
    public:
      virtual ::timber::Pointer<OpticalFlow> addImage (const ::timber::SharedRef< ::timber::media::ImageBuffer>& image) throws() = 0;

    };
  }
}

#endif
