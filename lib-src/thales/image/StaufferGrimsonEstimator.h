#ifndef _THALES_IMAGE_STAUFFERGRIMSONESTIMATOR_H
#define _THALES_IMAGE_STAUFFERGRIMSONESTIMATOR_H

#ifndef _THALES_IMAGE_BACKGROUNDESTIMATOR_H
#include <thales/image/BackgroundEstimator.h>
#endif

#include <memory>

namespace thales {
  namespace image {
    
    /**
     * This class implements the Stauffer-Grimson background estimation algorithm. This algorithm
     * is described in 
     * <code>
     *    Learning Patters of Activity Using Real-Time Tracking
     *       IEEE Trans. on Pattern Analysis and Machine Intelligence, Vol. 22, No. 8, Aug 2000
     * </code>
     * Modifications have been made to the original algorithm according to:
     * <code>
     *    An Improved Adaptive Background Mixture Model for Real-time Tracking with Shadow Detection
     *      KaewTraKulPong  and Bowden
     *    Proc. 2nd European Workshop on Advanced Video Based Surveillance Systems, ABS01 Sept. 2001
     * </code>
     * and also 
     * <code>
     *   Understanding Background Mixture Models for Foreground Segmentation
     *     P. Wayne Power   Johann A. Schoonees
     *    Proc. Image and Vision Computing New Zealand 2002
     * </code>
     * <p>
     * The estimator works reasonably well at this time, but it noisy image data causes
     * the filter to create false alarms as the covariance of the most likely filter 
     * shrinks to its minimum. It might actually perform better in semi-dynamic settings.
     */
    class StaufferGrimsonEstimator : public BackgroundEstimator {
          CANOPY_BOILERPLATE_PREVENT_COPYING(StaufferGrimsonEstimator);

      /** The implementation */
    private:
      template <class T> class Impl;

      /** The constructor */
    private:
      StaufferGrimsonEstimator() throws();

      /** The destructor */
    public:
      ~StaufferGrimsonEstimator() throws();

      /**
       * Set the learning rate. Modifying the learning rate allows the algorithm
       * to adapt to changing conditions, such as lighting changes, or wind.
       * @param learningRate the new learningRate
       * @pre learningRate >= 0 && learningRate <= 1
       */
    public:
      virtual void setLearningRate (double learningRate) throws() = 0;

      /**
       * Create a new estimator that works on the RGB values of an image. 
       * The learning rate is the <pre>alpha</pre> value from the original paper, 
       * cited in the class description.
       *
       * @param n the number of gaussians to use
       * @param learningRate the rate at which new information is incorporated
       * @param backgroundThreshold the min probability that a pixel is a background
       * @pre n > 0
       * @param learningRate >= 0 && learningRate<= 1
       * @param backgroundThreshold >= 0 && backgroundThreshold<= 1
       * @param error the per color error
       * @return an estimator
       */
    public:
      static ::std::unique_ptr<StaufferGrimsonEstimator> createRGBEstimator (size_t n, 
									   double learningRate,
									   double backgroundThreshold,
									   ::timber::UShort error) throws();

      /**
       * Create a new  estimator using only grayscale images. The learning rate is 
       * the <pre>alpha</pre> value from the original paper, cited in the class description.
       *
       * @param n the number of gaussians to use
       * @param learningRate the rate at which new information is incorporated
       * @param backgroundThreshold the min probability that a pixel is a background
       * @pre n > 0
       * @param learningRate >= 0 && learningRate<= 1
       * @param backgroundThreshold >= 0 && backgroundThreshold<= 1
       * @param error the gray pixel error
       * @return an estimator
       */
    public:
      static ::std::unique_ptr<StaufferGrimsonEstimator> createGrayEstimator (size_t n, 
									    double learningRate,
									    double backgroundThreshold,
									    ::timber::UShort error) throws();

    };
  }
}

#endif
