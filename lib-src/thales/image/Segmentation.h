#ifndef THALES_IMAGE_SEGMENTATION_H_
#define THALES_IMAGE_SEGMENTATION_H_

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

#ifndef _TIMBER_MEDIA_IMAGE_IMAGEOP_H
#include <timber/media/image/ImageOp.h>
#endif

namespace thales {
   namespace image {

      /**
       * A segmentation is a partitioning of an image into regions or components of the same property. The maximum
       * number of regions is simply the number of pixels in an image. Image segmentations can be
       * based on a variety of algorithms.
       */
      class Segmentation : public ::timber::media::image::ImageOp
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(Segmentation);

            /** Default constructor */
         public:
            Segmentation()throws();

            /** Destructor */
         public:
            virtual ~Segmentation()throws();

            /**
             * Segment the specified image. No smoothing will be applied by this algorithm.
             * @param image an image
             * @return a new image (never null)
             */
         public:
            ::std::shared_ptr< ::timber::media::ImageBuffer> apply(::timber::SharedRef< ::timber::media::ImageBuffer> src) throws () = 0;

            /**
             * Create a default segmentation algorithm that is based on
             * <pre>
             *     Efficient Graph-Based Image Segmentation
             * <pre>
             * by Felzenszwalb and Huttenlocher.
             *
             * @param k the parameter
             * @param minSize the minimum component size (or 0 to disable)
             * @return a segmentation algorithm
             * @throws invalid_argument if k<0
             */
         public:
            static ::std::unique_ptr< Segmentation> create(double k, size_t minSize)
            throws( ::std::invalid_argument);
      };
   }
}
#endif
