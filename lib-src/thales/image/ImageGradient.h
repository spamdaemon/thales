#ifndef _THALES_IMAGE_IMAGEGRADIENT_H
#define _THALES_IMAGE_IMAGEGRADIENT_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_MEDIA_INTENSITYIMAGE_H
#include <timber/media/IntensityImage.h>
#endif

#include <cmath>

namespace thales {
  namespace image {

    /**
     * This class represents a 2D image gradient.
     */
    class ImageGradient : public ::timber::Counted {

      /** The gradient vector */
    public:
      struct Vector {

	inline double magnitude() const throws()
	{ return ::std::sqrt(x*x + y*y); }

	double x,y;
      };

      /**
       * Constructor.
       */
    protected:
      ImageGradient() throws();

      /**
       * Destructor
       */
    public:
      virtual ~ImageGradient() throws() = 0;

      /**
       * Create the gradient of an intensity image. The gradient 
       * is computed as a central difference.
       * @param img an intensity only image
       * @return a gradient or null if none could be created
       */
    public:
      static ::timber::Pointer<ImageGradient> createCentralDifference(const ::timber::media::IntensityImage& img) throws();

       
      /**
       * Get the number of vectors in the horizontal.
       * @return the width of the source image
       */
    public:
      virtual size_t width() const throws() = 0;

      /**
       * Get the number of vectors in the vertical.
       * @return the height of the source image
       */
    public:
      virtual size_t height() const throws() = 0;

      /**
       * Get the gradient at the specified pixel position. For pixels outside
       * the image, the vector (0,0) is returned.
       * @param x the x-coordinate of a pixel
       * @param y the y-coordinate of a pixel
       * @return the gradient vector 
       */
    public:
      virtual Vector at(size_t x, size_t y) const throws() = 0;
      
      /**
       * Interpolate the gradient vector at the specified non-integral pixel location.
       * The default function is to perform bi-linear interpolation.  For pixels outside
       * the image, the vector (0,0) is returned.
       * @param x the x-coordinate of a pixel
       * @param y the y-coordinate of a pixel
       * @return the interpolated gradient vector
       */
    public:
      virtual Vector interpolateAt(double x, double y) const throws();
    };

  }
}

#endif
