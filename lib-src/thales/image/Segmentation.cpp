#include <thales/image/Segmentation.h>
#include <thales/algorithm/UnionFind.h>
#include <timber/logging.h>

#include <vector>
#include <algorithm>
#include <cmath>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::media;
using namespace ::timber::logging;

namespace thales {
   namespace image {

      namespace {

         static Log logger()
         {
            return "thales.image.Segmentation";
         }

         struct FelzenszwalbSegmentation : public Segmentation
         {
               struct Component
               {
                     void reset(size_t i)
                     {
                        size = 1;
                        threshold = 0;
                        pixel[0] = pixel[1] = pixel[2] = 0;
                     }
                     Component& operator+=(const Component& c)
                     {
                        size += c.size;
                        pixel[0] += c.pixel[0];
                        pixel[1] += c.pixel[1];
                        pixel[2] += c.pixel[2];
                        return *this;
                     }

                     size_t size;
                     double threshold;
                     size_t pixel[3];
               };

               /** An edge between two pixels */
               struct Edge
               {
                     size_t start, end;
                     double weight;

                     bool operator<(const Edge& e) const throws() {return weight < e.weight;}
               };

               FelzenszwalbSegmentation(double k, size_t minSize)throws() : _k(k),_minSize(minSize) {}
               ~FelzenszwalbSegmentation()throws() {}
               ::std::shared_ptr< ImageBuffer> apply(::timber::SharedRef< ImageBuffer> image) throws ()
               {
                  _unionFind.reset(image->width()*image->height());
                  for (size_t i=0;i<_unionFind.itemCount();++i) {
                     Component& c = _unionFind.data(_unionFind.findSet(i));
                     ImageBuffer::PixelColor col;
                     size_t y = i / image->width();
                     size_t x = i % image->width();
                     image->pixel(x,y,col);
                     c.pixel[0] = col._red;
                     c.pixel[1] = col._green;
                     c.pixel[2] = col._blue;
                     c.threshold = _k;
                  }

                  createGraph(*image);
                  findSegmentation();
                  applyMinimumSize();

                  return makeOutputImage(image->width(),image->height());
               }

               /**
                * Create the graph.
                * @param image an image
                */
            private:
               void createGraph(const ImageBuffer& buf)throws()
               {
                  _edges.clear();
                  _edges.reserve(4*_unionFind.itemCount());
                  for (size_t y=0,h=buf.height()-1;y<h;++y) {
                     for (size_t x=0,w=buf.width()-1;x<w;++x) {
                        if (y>0) {
                           _edges.push_back(makeEdge(buf,x,y,x+1,y-1));
                        }
                        _edges.push_back(makeEdge(buf,x,y,x+1,y));
                        _edges.push_back(makeEdge(buf,x,y,x+1,y+1));
                        _edges.push_back(makeEdge(buf,x,y,x,y+1));
                     }
                  }
               }

               /**
                * Compute the weight between two pixels.
                * @param buf an image
                * @param x0 x-coordinate of the first pixel
                * @param y0 y-coordinate of the first pixel
                * @param x1 x-coordinate of the second pixel
                * @param y1 y-coordinate of the second pixel
                * @return the weight
                */
            public:
               Edge makeEdge(const ImageBuffer& buf, size_t x0, size_t y0, size_t x1, size_t y1) const throws()
               {
                  Edge e;
                  size_t w = buf.width();
                  e.start = y0*w+x0;
                  e.end = y1*w+x1;

                  ImageBuffer::PixelColor col[2];
                  buf.pixel(x0,y0,col[0]);
                  buf.pixel(x1,y1,col[1]);

                  double dr = (col[0]._red - col[1]._red)/65535.0;
                  double dg = (col[0]._green - col[1]._green)/65535.0;
                  double db = (col[0]._blue - col[1]._blue)/65535.0;

                  e.weight = ::std::sqrt(dr*dr+dg*dg+db*db);

                  return e;
               }

               /**
                * Find the segmentation.
                */
            private:
               void findSegmentation()
               {
                  sort(_edges.begin(), _edges.end());
                  for (const Edge& e : _edges) {
                     auto setA = _unionFind.findSet(e.start);
                     auto setB = _unionFind.findSet(e.end);
                     if (setA != setB) {
                        double threshold = ::std::min(_unionFind.data(setA).threshold, _unionFind.data(setB).threshold);
                        if (e.weight < threshold) {
                           auto setC = _unionFind.mergeSets(setA, setB);
                           Component& c = _unionFind.data(setC);
                           c.threshold = e.weight + _k / c.size;
                        }
                     }
                  }
                  LogEntry(logger()).info() << "Found " << _unionFind.setCount() << " initial components" << doLog;
               }

               /**
                * Create components of minimum size.
                */
            private:
               void applyMinimumSize()
               {
                  if (_minSize == 0) {
                     return;
                  }
                  for (const Edge& e : _edges) {
                     auto setA = _unionFind.findSet(e.start);
                     auto setB = _unionFind.findSet(e.end);
                     if (setA != setB
                           && (_unionFind.data(setA).size < _minSize || _unionFind.data(setB).size < _minSize)) {
                        _unionFind.mergeSets(setA, setB);
                        // we no longer need to update the threshold, so we don't care
                     }
                  }
                  LogEntry(logger()).info() << "Found " << _unionFind.setCount()
                        << " components after applying minimum size" << doLog;
               }

               /**
                * Create the result image.
                * @return an image
                */
            private:
               ::timber::SharedRef< ImageBuffer> makeOutputImage(size_t w, size_t h)
               {
                  assert(_unionFind.itemCount() == w * h);
                  ::std::unique_ptr< ImageBuffer::PixelColor[]> pixels(
                        new ImageBuffer::PixelColor[_unionFind.itemCount()]);

for(                  size_t i = 0; i < _unionFind.itemCount(); ++i) {
                     Component& c = _unionFind.data(_unionFind.findSet(i));
                     pixels[i]._red = (UShort)(c.pixel[0] / c.size);
                     pixels[i]._green = (UShort)(c.pixel[1] / c.size);
                     pixels[i]._blue = (UShort)(c.pixel[2] / c.size);
                     pixels[i]._opacity = ImageBuffer::MAX_OPACITY;
                  }
                  return ImageBuffer::createImage(w, h, move(pixels));
               }

               /** The k value */
            private:
               const double _k;

               /** The minimium component size */
            private:
               const size_t _minSize;

               /** The union find structure */
            private:
               ::thales::algorithm::UnionFind< Component> _unionFind;

               /** A vector of edges */
            private:
               ::std::vector< Edge> _edges;

         };

      }

      Segmentation::Segmentation()
      throws()
      {
      }

      Segmentation::~Segmentation()
      throws()
      {
      }

      unique_ptr< Segmentation> Segmentation::create(double k, size_t minSize)
throws   (invalid_argument)
   {
      if (k<0) {
         throw invalid_argument("Invalid k");
      }
      return unique_ptr<Segmentation>( new FelzenszwalbSegmentation(k,minSize));
   }

}
}
