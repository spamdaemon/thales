#ifndef _THALES_IMAGE_TEMPORALFILTER_H
#define _THALES_IMAGE_TEMPORALFILTER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

#include <memory>

namespace thales {
  namespace image {

    /**
     * This class represents a 2D image gradient.
     */
    class TemporalFilter {
          CANOPY_BOILERPLATE_PREVENT_COPYING(TemporalFilter);

      /**
       * A frame is just a pair of frame number and image id.
       */
    public:
      struct Frame {
	/** 
	 * Create a frame without an image and frame number 0.
	 */
      public:
	inline Frame() : frame(0) {}
	
	/**
	 * Create an image with the specified frame number and an image
	 * @param no the frame number
	 * @param img the image 
	 */
      public:
	inline Frame (size_t no, ::std::shared_ptr< ::timber::media::ImageBuffer> img)
	  : frame(no),image(::std::move(img)) {}
	
	/**
	 * The current frame number.
	 * @return the frame number
	 */
      public:
	size_t frame;

	/**
	 * The image corresponding to this frame.
	 */
      public:
	::std::shared_ptr< ::timber::media::ImageBuffer> image;
      };
      
      /**
       * Constructor.
       */
    protected:
      TemporalFilter() throws();

      /**
       * Destructor
       */
    public:
      virtual ~TemporalFilter() throws() = 0;

      /**
       * Create a temporal filter that actually does not do any filtering.
       * The frame passed to the filter will be the same frame returned from it.
       * @return a temporal filter
       */
    public:
      static ::std::unique_ptr<TemporalFilter> createNoFilter() throws();

      /**
       * Create an exponentially weighted temporal filter. This filter
       * multiplies the current image by alpha, and the previous image by 1-alpha.
       * The resulting image will become the previous image during the next call to
       * enqueue. The advantage of this filter is that it will have no lag.
       * @return a temporal filter
       */
    public:
      static ::std::unique_ptr<TemporalFilter> createExponentialFilter(double alpha) throws();

      /**
       * Create a weighted average filter. The number of weights must be odd. The delay
       * for this filter is n/2. If the number of weights is 5, then the delay is 2.
       * @param n the number of weights
       * @param alpha the weights
       * @return a temporal filter
       */
    public:
      static ::std::unique_ptr<TemporalFilter> createWeightedAverageFilter(size_t n, const double* alpha) throws();

      /**
       * Enqueue a new frame and return the frame that is for use. It is possible
       * for this filter to return no image in the frame indicating, that the filter
       * was could not be applied yet. Also, the returned frame may be a frame from
       * a previous time. 
       * @param f a frame
       * @return a filtered frame.
       */
    public:
      virtual Frame enqueue (const Frame& f) throws() = 0;

    };

  }
}

#endif
