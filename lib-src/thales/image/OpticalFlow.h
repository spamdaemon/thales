#ifndef _THALES_IMAGE_OPTICALFLOW_H
#define _THALES_IMAGE_OPTICALFLOW_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

namespace thales {
   namespace image {

      /**
       * The Optical Flow estimator provides an alternative approach to the Background Subtraction approach
       * for motion detection.
       */
      class OpticalFlow : public ::timber::Counted
      {
            CANOPY_BOILERPLATE_PREVENT_COPYING(OpticalFlow);

            /**
             * A flow vector.
             */
         public:
            struct Vector
            {
                  double x, y;

                  /** Default constructor that initializes the vector with (0,0) */
                  inline Vector()
                        : x(0), y(0)
                  {
                  }
            };

            /**
             * Create a new optical flow field. The vector will be owned by this flow, once done.
             * @param w the number of vectors in the horizontal
             * @param h the number of vectors in the vertical
             * @param flow the flow vectors
             **/
         protected:
            OpticalFlow(size_t w, size_t h, const Vector*& flow)throws();

            /** The destructor */
         public:
            virtual ~OpticalFlow()throws();

            /**
             * Create a new optical flow field
             * @param w the number of vectors in the horizontal
             * @param h the number of vectors in the vertical
             * @param flow a flow field
             * @return an optical flow field
             */
         public:
            static ::timber::Reference< OpticalFlow> create(size_t w, size_t h, const Vector* flow)throws();

            /**
             * Create a new optical flow field. The flow data will not be copied, but the pointer
             * will be owned by this flow object.
             * @param flow a reference to a pointer.
             * @param w the number of vectors in the horizontal
             * @param h the number of vectors in the vertical
             * @return an optical flow field
             */
         public:
            static ::timber::Reference< OpticalFlow> create(Vector*& flow, size_t w, size_t h)throws();

            /**
             * Create a magnitude image for this flow field. If a vector's magnitude
             * is below the specified lo threshold, then its intensity is set to black. If
             * the vector's magnitude is above the hi threshold, then the intensity is set to white.
             * @param hiThreshold the higher threshold
             * @param loThreshold the lower threshold
             * @pre assert(loThreshold<=hiThreshold)
             * @return a gray scale image
             */
         public:
            ::std::shared_ptr< ::timber::media::ImageBuffer> createMagnitudeImage(double hiThreshold,
                  double loThreshold = 0.0) const throws();

            /**
             * Get the width of this flow field.
             * @return the width of this flow field
             */
         public:
            inline size_t width() const throws() {return _width;}

            /**
             * Get the height of this flow field.
             * @return the height of this flow field
             */
         public:
            inline size_t height() const throws() {return _height;}

            /**
             * Get the flow for the specified pixel.
             * @param x horizontal pixel
             * @param y vertical pixel index
             * @return the flow vector
             */
         public:
            inline Vector vectorAt(size_t x, size_t y) const throws()
            {  return _flow[y*_width + x];}

         private:
            const size_t _width, _height;

            /** The float vectors */
         private:
            const Vector* _flow;
      };
   }
}

#endif
