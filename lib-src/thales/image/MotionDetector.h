#ifndef _THALES_IMAGE_MOTIONDETECTOR_H
#define _THALES_IMAGE_MOTIONDETECTOR_H

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

namespace thales {
  namespace image {
    
    /**
     * 
     */
    class MotionDetector {
          CANOPY_BOILERPLATE_PREVENT_COPYING(MotionDetector);

      /** The constructor */
    protected:
      MotionDetector() throws();

      /** The destructor */
    public:
      virtual ~MotionDetector() throws() = 0;
    };
  }
}

#endif
