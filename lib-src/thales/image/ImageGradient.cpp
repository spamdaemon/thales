#include <thales/image/ImageGradient.h>
#include <canopy/canopy.h>
#include <cmath>

using namespace ::canopy;
using namespace ::timber;

namespace thales {
  namespace image {
    namespace {

      /** The default gradient implementation */
      class GradientImpl : public ImageGradient {
      public:
	GradientImpl(size_t w, size_t h)
	  : _width(w),_height(h),_gradient(new Vector[w*h])
	{
	  
	}

	~GradientImpl() throws() { delete [] _gradient; }

	size_t width() const throws() { return _width; }
	size_t height() const throws() { return _height; }

	Vector at(size_t x, size_t y) const throws()
	{
	  Vector res;
	  if (x<_width && y<_height) {
	    return _gradient[y*_width+x];
	  }
	  res.x=res.y = 0;
	  return res;
	}

	Vector* getVector(size_t x, size_t y) throws()
	{
	  assert(x<_width);
	  assert(y<_height);
	  return _gradient+(y*_width+x);
	}
	
      private:
	const size_t _width,_height;
	Vector* _gradient;
      };

    };

    ImageGradient::ImageGradient() throws()
    {}

    ImageGradient::~ImageGradient() throws()
    {}

    Pointer<ImageGradient> ImageGradient::createCentralDifference(const ::timber::media::IntensityImage& img) throws()
    {
      const size_t w = img.width();
      const size_t h = img.height();
      Reference<GradientImpl> G(new GradientImpl(w,h));
      
      if (w>1) {
	for (size_t y=0;y<h;++y) {
	  Vector* g = G->getVector(0,y);
	  
	  float i = img.pixel(0,y)._intensity;
	  float j = img.pixel(1,y)._intensity;
	  
	  (g++)->x = 0;
	  
	  for (size_t x=2;x<w;++x,++g) {
	    float k = img.pixel(x,y)._intensity;
	    g->x = (k-i)/2;
	    i  = j;
	    j  = k;
	  }
	  g->x = j-i;
	}
      }
      else {
	for (size_t y=0;y<h;++y) {
	  G->getVector(0,y)->x=0;
	}
      }

      if (h>1) {
	for (size_t x=0;x<w;++x) {
	  Vector* g = G->getVector(x,0);
	  
	  float i = img.pixel(x,0)._intensity;
	  float j = img.pixel(x,1)._intensity;
	  
	  g->y = 0;
	  g+=w;
	  for (size_t y=2;y<h;++y,g+=w) {
	    float k = img.pixel(x,y)._intensity;
	    g->y = (k-i)/2;
	    i  = j;
	    j  = k;
	  }
	  g->y = j-i;
	}
      }
      else {
	for (size_t x=0;x<w;++x) {
	  G->getVector(x,0)->y=0;
	}
      }

      return G;
    }


    ImageGradient::Vector ImageGradient::interpolateAt(double x, double y) const throws()
    {
      Int32 ix = static_cast<Int32>(::std::floor(x));
      Int32 iy = static_cast<Int32>(::std::floor(y));
      
      double dx = x-ix;
      double dy = y-iy;

      Vector v = at(ix,iy);
      double gx = v.x * (1-dx)*(1-dy);
      double gy = v.y * (1-dx)*(1-dy);

      v = at(ix+1,iy);
      gx += dx*(1-dy)*v.x;
      gy += dx*(1-dy)*v.y;

      v = at(ix+1,iy+1);
      gx += dx*dy*v.x;
      gy += dx*dy*v.y;
      
      v = at(ix,iy+1);
      gx += (1-dx)*dy*v.x;
      gy += (1-dx)*dy*v.y;

      Vector res;
      res.x = gx;
      res.y = gy;
      return res;
    }


  }
}

