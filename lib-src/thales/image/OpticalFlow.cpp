#include <thales/image/OpticalFlow.h>
#include <canopy/arraycopy.h>

#include <cmath>
#include <iostream>

using namespace ::timber;
using namespace ::timber::media;

namespace thales {
   namespace image {

      OpticalFlow::OpticalFlow(size_t w, size_t h, const Vector*& flow)
      throws()
      : _width(w),_height(h),_flow(flow)
      {
         assert(w>0);
         assert(h>0);
         flow = 0;
      }

      OpticalFlow::~OpticalFlow()
      throws()
      {  delete[] _flow;}

      Reference< OpticalFlow> OpticalFlow::create(size_t w, size_t h, const Vector* flow)
      throws()
      {
         Vector* field = new Vector[w*h];
         ::canopy::arraycopy(flow,field,w*h);
         return new OpticalFlow(w,h,flow);
      }

      Reference< OpticalFlow> OpticalFlow::create(Vector*& flow, size_t w, size_t h)
      throws()
      {
         const Vector* v = flow;
         flow =0;
         return new OpticalFlow(w,h,v);
      }

      ::std::shared_ptr< ImageBuffer> OpticalFlow::createMagnitudeImage(double hiThreshold, double loThreshold) const
throws   ()
   {
      assert(loThreshold <= hiThreshold);

      ::std::unique_ptr<char[]> buf(new char[_width*_height]);
      const char* bufEnd = buf.get() + _width*_height;

      const Vector* v=_flow;
      double maxVelocity = 0;
      double minVelocity = 1000000000;
      for (char* pixel = buf.get();pixel!=bufEnd;++pixel) {
         double x = v->x;
         double y = v->y;
         double m = 0;
         if (!::std::isnan(x) && !::std::isnan(y)) {
            m = ::std::sqrt(x*x + y*y);
            maxVelocity = ::std::max(maxVelocity,m);
            minVelocity = ::std::min(minVelocity,m);

            if (m>=hiThreshold) {
               m = 1;
            }
            else if (m<=loThreshold) {
               m = 0;
            }
            else {
               m = ::std::min(m,hiThreshold);
               m = ::std::max(m,loThreshold);
               assert(hiThreshold!=loThreshold);
               m = (m-loThreshold)/(hiThreshold-loThreshold);
            }
         }
         *pixel = static_cast<char>(255 * m);

         ++v;
      }
      ::std::cerr << "MinMax velocity : " << minVelocity << ", " << maxVelocity << ::std::endl;

      return ImageBuffer::createGrayImage(_width,_height,move(buf));
   }

}
}

