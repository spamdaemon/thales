#ifndef _THALES_IMAGE_IMAGEPYRAMID_H
#define _THALES_IMAGE_IMAGEPYRAMID_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace timber {
   namespace media {
      class ImageBuffer;
   }
}
namespace thales {
   namespace image {

      /**
       * An image pyramid is a structure used in various image processing algorithms.
       */
      class ImagePyramid : public ::timber::Counted
      {
            /**
             * Default constructor.
             */
         protected:
            ImagePyramid()throws();

            /** Destructor */
         public:
            ~ImagePyramid()throws();

            /**
             * Create an empty pyramid.
             * @return a pyramid with 0 levels.
             */
         public:
            static ::timber::Reference< ImagePyramid> create()throws();

            /**
             * Create a simple image pyramid with the specified number of levels.
             * <p>
             * Callers must test the resulting pyramid for the actual number of images.
             * @param image an image
             * @param nLevels the number of levels to create
             * @return an image pyramid
             * @pre nLevels > 0
             */
         public:
            static ::timber::Reference< ImagePyramid> create(
                  const ::timber::SharedRef< ::timber::media::ImageBuffer>& image, size_t nLevels)throws();

            /**
             * Get the number of levels in this pyramid.
             * @return the number of levels.
             */
         public:
            virtual size_t size() const throws() = 0;

            /**
             * Get the pyramid at the specified level. The image at level 0 is the original image.
             * @param level a level
             * @return an image reference
             * @pre level<size()
             */
         public:
            virtual ::timber::SharedRef< ::timber::media::ImageBuffer> level(size_t level) const throws() = 0;
      };
   }
}
#endif
