#ifndef _THALES_H
#define _THALES_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace thales {
  
  /** The types from timber that are used */
  using ::timber::UInt;
  using ::timber::Int;
  
  using ::timber::ULong;
  using ::timber::Long;

  using ::timber::UShort;
  using ::timber::Short;

  using ::timber::UByte;
  using ::timber::Byte;

  using ::timber::Double;
  using ::timber::Float;

}

#endif
