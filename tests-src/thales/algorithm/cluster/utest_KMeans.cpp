#include <thales/algorithm/cluster/KMeans.h>
#include <thales/algorithm/cluster/VectorDataSet.h>
#include <thales/kalman/Gaussian.h>

using namespace ::std;
using namespace ::thales::algorithm::cluster;
using namespace ::thales::kalman;
using namespace ::newton;

void utest_1()
{
  vector<Vector<> > data;
  size_t nClusters = 10;
  for (size_t i=0;i<10;++i) {
    Vector<double> mean(10.0*i,10.0*i);
    Matrix<double> cvar(2,2);
    cvar(0,0) = cvar(1,1) = 3;

    ::std::cerr << "Input Mean " << mean << ::std::endl;

    vector<Vector<double > > samples = Gaussian::randomSamples(mean,cvar,100);
    data.insert(data.end(),samples.begin(),samples.end());
  }

  VectorDataSet<> ds(data);
  
  KMeans algorithm;
  algorithm.findClusters(ds,nClusters);
  
  vector<Vector<double> > means = ds.getMeans();
  for (size_t i =0;i<means.size();++i) {
    ::std::cerr << "Output Mean " << means[i] << ::std::endl;
  }

}

