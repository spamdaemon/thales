#include <thales/algorithm/cluster/ExpectationMaximization.h>
#include <thales/kalman/Gaussian.h>

using namespace ::std;
using namespace ::timber;
using namespace ::thales::algorithm::cluster;
using namespace ::thales::kalman;
using namespace ::newton;

void utest_1()
{
  vector<Vector<> > data,initialData;
  size_t nClusters = 5;
  size_t nSamples = 100;
  Matrix<double> cvar(2,2);

  Vector<> dataMean(100.0,100.0);
  Matrix<> dataVar(2);
  dataVar(0,0) = dataVar(1,1) = 900;
  
  initialData = Gaussian::randomSamples(dataMean,dataVar,nClusters);
  
  cvar(0,0) = cvar(1,1) = 100;
  for (size_t i=0;i<nClusters;++i) {
    // select a point around we generate random samples, at random
    Vector<double> mean = Gaussian::randomSample(dataMean,dataVar);
    data.push_back(mean);
    vector<Vector<double > > samples = Gaussian::randomSamples(mean,cvar,nSamples);
    //data.insert(data.end(),samples.begin(),samples.end());
  }

  vector<Reference<Gaussian> > initial,ds;
  for (size_t i=0;i<data.size();++i) {
    ds.push_back(Gaussian::create(CoordinateFrame::createGenericFrame(data[i].dimension()),data[i],cvar));
  }
  
  cvar(0,0) = cvar(1,1) = 10;
  for (size_t i=0;i<initialData.size();++i) {
    ::std::cerr << "Initial " << initialData[i] << ::std::endl;
    initial.push_back(Gaussian::create(CoordinateFrame::createGenericFrame(data[i].dimension()),initialData[i],cvar));
  }
  
  ExpectationMaximization algorithm;
  unique_ptr<ExpectationMaximization::Mixture> M  = algorithm.apply(ds,initial);
  
  for (size_t i =0;i<M->size();++i) {
    const Gaussian& ge = *M->gaussian(i);
    ::std::cerr << "cluster " << i << " : " << ::std::endl
		<< "  weight : " << M->probability(i) << ::std::endl
		<< "  mean " << ge.mean() << ::std::endl;
    double mx,mn, theta;
    Covariance::decompose(ge(0,0),ge(1,1),ge(0,1),mx,mn,theta);
    ::std::cerr << "  cov  " << mx << " ; " << mn << " ; " << theta << ::std::endl;
  }

}

