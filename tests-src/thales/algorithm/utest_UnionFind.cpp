#include <thales/algorithm/UnionFind.h>
#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <newton/newton.h>

#include <set>
#include <memory>

#include <stdlib.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales;

struct SetSize {
      size_t _size;

      void reset (size_t)
      { _size = 1; }

      SetSize& operator+=(const SetSize& a)
      {
         _size += a._size;
         return *this;
      }
};

typedef ::thales::algorithm::UnionFind< SetSize> UnionFind;

void utest_UnionFind()
{
   UnionFind uf(5);

   assert(uf.setCount() == 5);
   assert(uf.isSet(uf.findSet(0)));
   assert(uf.isSet(uf.findSet(1)));

   UnionFind::Set a, b, c;
   a = uf.findSet(0);
   b = uf.findSet(1);
   c = uf.findSet(2);

   UnionFind::Set A = uf.mergeSets(a, b);
   assert(uf.isSet(A));
   UnionFind::Set B = uf.mergeSets(A, c);
   assert(uf.isSet(B));

   assert(uf.findSet(0) == B);
   assert(uf.findSet(1) == B);
   assert(uf.findSet(2) == B);
   assert(uf.findSet(3) != B);
   assert(uf.findSet(4) != B);
   assert(uf.setCount() == 3);

   assert(uf.data(B)._size == 3);
}

