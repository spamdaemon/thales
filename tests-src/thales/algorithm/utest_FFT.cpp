#include <iostream>
#include <cmath>

#include <timber/timber.h>
#include <canopy/canopy.h>
#include <canopy/time/Time.h>
#include <canopy/math/ComplexNumber.h>
#include <thales/thales.h>
#include <thales/algorithm/FFT.h>
#include <newton/newton.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::math;
using namespace ::canopy::time;
using namespace ::timber;
using namespace ::thales;
using namespace ::thales::algorithm;
using namespace ::newton;

static void dft(size_t n, ComplexNumber* data)
{
  vector<ComplexNumber> res;
  for (size_t k=0;k<n;++k) {
    ComplexNumber c;
    for (size_t i=0;i<n;++i) {
      c += data[i]*ComplexNumber::fromPolar(1,(-2.0*M_PI/n)*i*k);
    }
    res.push_back(c);
  }
  for (size_t i=0;i<n;++i) {
    data[i] = res[i];
  }
}

static void dft(size_t n, const double* input, vector<ComplexNumber>& res)
{
  for (size_t k=0;k<n;++k) {
    ComplexNumber c;
    for (size_t i=0;i<n;++i) {
      c += ComplexNumber(input[i])*ComplexNumber::fromPolar(1,(-2.0*M_PI/n)*i*k);
    }
    res.push_back(c);
  }
}

static void inverseDFT(const vector<ComplexNumber>& input, vector<ComplexNumber>& res)
{
  size_t n = input.size();
  for (size_t k=0;k<n;++k) {
    ComplexNumber c;
    for (size_t i=0;i<n;++i) {
      c += input[i]*ComplexNumber::fromPolar(1,(2.0*M_PI/n)*i*k);
    }
    c /= ComplexNumber(n);
    res.push_back(c);
  }
}

void utest_checkDFT()
{
  double reals[] = { 0,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0 };
  vector<ComplexNumber> c;

  dft(16,reals,c);
  
  for (size_t i=0;i<c.size();++i) {
    double r,theta;
    c[i].toPolar(r,theta);
    ::std::cerr << c[i] << ' ';
  }
  ::std::cerr << ::std::endl;
  ::std::cerr << ::std::flush;
  vector<ComplexNumber> invC;
  inverseDFT(c,invC);
  for (size_t i=0;i<c.size();++i) {
    double r,theta;
    invC[i].toPolar(r,theta);
    ::std::cerr << r << ' ';
  }  
  ::std::cerr << ::std::endl;
}

void utest_doFFT()
{
  size_t N = 32;
  ComplexNumber dftInput[N];
  ComplexNumber fftInput[N];

  for (int i=0;i<N;++i) {
    dftInput[i] = i;
    fftInput[i] = i;
  }

  
  dft(N,dftInput);
  FFT::instance().doFFT(N,fftInput);
  
  for (size_t i=0;i<N;++i) {
    double r,theta;
    dftInput[i].toPolar(r,theta);
    ::std::cerr << r << ':' << theta << "  ";
  }  
  ::std::cerr << ::std::endl;
  for (size_t i=0;i<N;++i) {
    double r,theta;
    fftInput[i].toPolar(r,theta);
    ::std::cerr << r << ':' << theta << "  ";
  }  
  ::std::cerr << ::std::endl;

  for (size_t i=0;i<N;++i) {
    assert(::std::abs(fftInput[i].r()-dftInput[i].r()) < 0.000001);
    assert(::std::abs(fftInput[i].i()-dftInput[i].i()) < 0.000001);
  }

  FFT::instance().doInverseFFT(N,fftInput);
  for (size_t i=0;i<N;++i) {
    assert(::std::abs(fftInput[i].r()-i) < 0.000001);
    assert(::std::abs(fftInput[i].i()) < 0.000001);
  }
  
}

void utest_doFFTTimed()
{
  size_t N = 2048;
  ComplexNumber dftInput[N];
  ComplexNumber fftInput[N];

  for (int i=0;i<N;++i) {
    dftInput[i] = i;
    fftInput[i] = i;
  }

  Time startDFT = Time::now();
  dft(N,dftInput);
  Time endDFT = Time::now();

  Time startFFT = Time::now();
  FFT::instance().doFFT(N,fftInput);
  Time endFFT = Time::now();

  ::std::cerr << "DFT in " << ((endDFT.time()-startDFT.time()).count()/1000000000.0) << "  seconds" << ::std::endl;
  ::std::cerr << "FFT in " << ((endFFT.time()-startFFT.time()).count()/1000000000.0) << "  seconds" << ::std::endl;
  for (size_t i=0;i<N;++i) {
    assert(::std::abs(fftInput[i].r()-dftInput[i].r()) < 0.000001);
    assert(::std::abs(fftInput[i].i()-dftInput[i].i()) < 0.000001);
  }

  FFT::instance().doInverseFFT(N,fftInput);
  for (size_t i=0;i<N;++i) {
    assert(::std::abs(fftInput[i].r()-i) < 0.000001);
    assert(::std::abs(fftInput[i].i()) < 0.000001);
  }
  
}

void utest_doFFTWithStrides()
{
  const size_t N = 2048;
  const size_t stride = 3;
  ComplexNumber dftInput[N];
  ComplexNumber fftInput[stride*N];

  for (size_t i=0;i<N;++i) {
    dftInput[i] = i;
    fftInput[stride*i] = i;
  }

  
  dft(N,dftInput);
  FFT::instance().doFFT(N,stride,fftInput);

  for (size_t i=0;i<N;++i) {
    assert(::std::abs(fftInput[stride*i].r()-dftInput[i].r()) < 0.000001);
    assert(::std::abs(fftInput[stride*i].i()-dftInput[i].i()) < 0.000001);
  }

  FFT::instance().doInverseFFT(N,stride,fftInput);
  for (size_t i=0;i<N;++i) {
    assert(::std::abs(fftInput[stride*i].r()-i) < 0.000001);
    assert(::std::abs(fftInput[stride*i].i()) < 0.000001);
  }
  
}
