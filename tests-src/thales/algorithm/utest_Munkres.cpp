#include <thales/algorithm/Munkres.h>
#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <newton/newton.h>

#include <set>
#include <memory>

#include <stdlib.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales;
using namespace ::thales::algorithm;

typedef ::newton::Matrix<double> Matrix;
typedef vector<pair<size_t,size_t> > Assignment;

static double getCost(const Matrix& m, const Assignment& a)
{
  double c = 0;
  for (Assignment::const_iterator k=a.begin();k!=a.end();++k) {
    c += m(k->first,k->second);
  }
  return c;
}

static void printAssignment(const Assignment& A)
{
  cerr << "Assignment :";
  for (Assignment::const_iterator k=A.begin();k!=A.end();++k) {
    cerr << " (" << k->first << ", " << k->second << ")";
  } 
  cerr << endl << flush;
}

namespace {
  struct Node {
    Node() : cost(0) {}
    Assignment possible;
    Assignment fixed;
    double cost;
    void swap(Node& n)
    {
      possible.swap(n.possible);
      fixed.swap(n.fixed);
      ::std::swap(cost,n.cost);
    }
  };
}
static Assignment findAssignment (const Matrix& m)
{
  
  unique_ptr<Node> bestNode;

  vector<Node> nodes;
  {
    Node n;
    for (size_t i=0;i<m.rowSize();++i) {
      for (size_t j=0;j<m.colSize();++j) {
	n.possible.push_back(make_pair(i,j));
      }
    }
    nodes.push_back(n);
  }
  while (!nodes.empty()) {
    Node left = nodes.back();
    nodes.pop_back();
    if (left.possible.empty()) {
      if (left.fixed.size()==m.colSize()) {
	if (bestNode.get()==0 || bestNode->cost > left.cost) {
	  bestNode.reset(new Node(left));
	}
      }
    }
    else {
      

      size_t i=left.possible.back().first;
      size_t j=left.possible.back().second;
      left.possible.pop_back();

      Node right = left;
      
      left.fixed.push_back(make_pair(i,j));
      left.cost += m(i,j);
      
      for (size_t k=0;k<left.possible.size();) {
	if (left.possible[k].first==i || left.possible[k].second==j) {
	  left.possible[k] = *left.possible.rbegin();
	  left.possible.pop_back();
	}
	else {
	  ++k;
	}
      }
      if (left.possible.size()+left.fixed.size() >= m.colSize()) {      
	nodes.push_back(left);
      }
      if (right.possible.size()+right.fixed.size() >= m.colSize()) {      
	nodes.push_back(right);
      }
    }
  }
  return bestNode->fixed;
}


static void verifyAssignment(const Assignment& A)
{
  set<size_t> rows,cols;
  for (Assignment::const_iterator k=A.begin();k!=A.end();++k) {
    assert(rows.insert(k->first).second && "duplicate row");
    assert(cols.insert(k->second).second && "duplicate column");
  }
}
static void verifyAssignment(const Assignment& A, size_t i, size_t j)
{
  for (Assignment::const_iterator k=A.begin();k!=A.end();++k) {
    if (k->first == i && k->second==j) {
      return;
    }
  }
  assert("Assignment not found"==0);
}

static void verifyAssignments(const Matrix& m, const Assignment& a, const Assignment& b)
{
  ::std::cerr << "A: " << getCost(m,a) << " :";printAssignment(a);
  ::std::cerr << "B: "<< getCost(m,b) << " :";printAssignment(b);
  assert(a.size()==b.size());
  assert(getCost(m,a)==getCost(m,b) && "Costs are different");
}

void utest_Matrix_1x1()
{
  {
    Matrix m(1,1);
    m(0,0) = 1;
    Assignment res = solveMunkres(m);  
    printAssignment(res);
    verifyAssignment(res);
    verifyAssignment(res,0,0);
    assert(res.size()==1);
  }
  {
    Matrix m(1,1);
    m(0,0) = 0;
    Assignment res = solveMunkres(m);  
    printAssignment(res);
    verifyAssignment(res);
    verifyAssignment(res,0,0);
    assert(res.size()==1);
  }
  
}

void utest_Matrix_2x2()
{
  {
    Matrix m(2,2);
    m(0,0) = 1;
    m(0,1) = 2;
    m(1,0) = 2;
    m(1,1) = 1;

    Assignment res = solveMunkres(m);
    printAssignment(res);
    verifyAssignment(res);
    verifyAssignment(res,0,0);
    verifyAssignment(res,1,1);
    assert(res.size()==2);
  }

  {
    Matrix m(2,2);
    m(0,0) = 2;
    m(0,1) = 1;
    m(1,0) = 1;
    m(1,1) = 2;

    Assignment res = solveMunkres(m);
    printAssignment(res);
    verifyAssignment(res);
    verifyAssignment(res,0,1);
    verifyAssignment(res,1,0);
    assert(res.size()==2);
  }
}

void utest_Matrix_3x3()
{
  {
    Matrix m(3,3);
    m(0,0) = 1;
    m(0,1) = 2;
    m(0,2) = 3;
    m(1,0) = 2;
    m(1,1) = 1;
    m(1,2) = 3;
    m(2,0) = 3;
    m(2,1) = 2;
    m(2,2) = 1;

    Assignment res = solveMunkres(m);
    printAssignment(res);
    verifyAssignment(res);
    verifyAssignment(res,0,0);
    verifyAssignment(res,1,1);
    verifyAssignment(res,2,2);
    assert(res.size()==3);
  }
}

void utest_Matrix_Random1()
{
  {
    Matrix m(37,33); // a random matrix
    Matrix n(33,37); // the transpose of m
    Matrix r(37,33); // the reverse of m
    
    // we need unique values to ensure that a solution is unique
    set<double> entries; 
    
    // fill the matrix with unique random values
    for (size_t i=0;i<m.rowSize();++i) {
      for (size_t j=0;j<m.colSize();++j) {
	double rnd = (size_t)(drand48()*10000000);
	while (!entries.insert(rnd).second) {
	  rnd = (size_t)(drand48()*10000000);
	}
	n(j,i) = m(i,j) = rnd;
	r(m.rowSize()-1-i,m.colSize()-1-j) = rnd;
      }
    }
    
    Assignment res1 = solveMunkres(m);
    Assignment res2 = solveMunkres(n);
    Assignment res3 = solveMunkres(r);
    printAssignment(res1);
    assert(res1.size()==m.colSize());
    verifyAssignment(res1);
    verifyAssignment(res2);
    verifyAssignment(res3);
    
    for (size_t i=0;i<res1.size();++i) {
      verifyAssignment(res2,res1[i].second,res1[i].first);
      verifyAssignment(res3,m.rowSize()-1-res1[i].first,m.colSize()-1-res1[i].second);
    }
  }
}

void utest_Matrix_Random2()
{
  for (size_t k=0;k<20;++k) {
    Matrix m(9,5); // a random matrix
    
    // we need unique values to ensure that a solution is unique
    set<double> entries; 
    
    // fill the matrix with unique random values
    for (size_t i=0;i<m.rowSize();++i) {
      for (size_t j=0;j<m.colSize();++j) {
	double rnd = (size_t)(drand48()*1000);
	while (!entries.insert(rnd).second) {
	  rnd = (size_t)(drand48()*1000);
	}
	m(i,j) = rnd;
      }
    }
    Assignment res = solveMunkres(m);
    verifyAssignments(m,res,findAssignment(m));
    verifyAssignment(res);
  }
}

