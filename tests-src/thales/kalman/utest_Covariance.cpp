#include <iostream>
#include <cassert>
#include <cmath>

#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <newton/newton.h>

#include <thales/kalman/Covariance.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales;
using namespace ::thales::kalman;
using namespace ::newton;

static const double PI = 3.14159265358979323846;
void utest_1()
{
  double a00 = 3;
  double a11 = 2;
  double theta = PI/4.0;
  
  Covariance cvar = Covariance::recompose(a00,a11,theta);

  double mj,mn,angle;
  Covariance::decompose(cvar(0,0),cvar(1,1),cvar(0,1),mj,mn,angle);
  
  cerr << "Covariance " << cvar << endl;
  cerr << "Major " << mj << ", minor " << mn << ", theta " << 180.0*angle/PI << endl;

  assert(abs(mj-a00) < 0.001);
  assert(abs(mn-a11) < 0.001);
  assert(abs(theta-angle)<0.001);
}

