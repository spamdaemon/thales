#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/GaussianTransform.h>
#include <thales/kalman/MonteCarloTransform.h>
#include <thales/kalman/UKFTransform.h>
#include <thales/kalman/CoordinateFrame.h>

#include <thales/kalman/unscented/Transform.h>
#include <thales/kalman/unscented/PointSet.h>
#include <thales/kalman/unscented/SimplexPoints.h>
#include <thales/kalman/unscented/SphericalSimplexPoints.h>
#include <thales/kalman/unscented/DefaultPoints.h>

#include <thales/kalman/frame/ProjectivePlane3D_P.h>
#include <thales/kalman/frame/Polar_2D_PV.h>
#include <thales/kalman/frame/Polar_2D_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_1D_Inertial_P.h>

#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Polar_2D_PV.h>
#include <thales/kalman/transform/Polar_2D_P__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_P__ProjectivePlane3D_P.h>
#include <thales/kalman/transform/ProjectivePlane3D_P__Cartesian_2D_Inertial_P.h>

#include <newton/newton.h>
#include <cmath>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::unscented;
using namespace ::thales::kalman::transform;
using namespace ::newton;

static Reference<Gaussian> newCartesian_P(double x, double y, double err=1)
{
  Vector<double> mean(x,y);
  Matrix<double> cvar(mean.dimension());
  cvar(0,0) = err;
  cvar(1,1) = err;
  
  return Gaussian::create(Cartesian_2D_Inertial_P::create(),mean,cvar);
}

static Reference<Gaussian> newCartesian_PV(double x, double y, double vx, double vy)
{
  Vector<double> mean(x,y,vx,vy);
  Matrix<double> cvar(mean.dimension());
  cvar(0,0) = 10;
  cvar(1,1) = 100;
  cvar(2,2) = 2;
  cvar(3,3) = 2;
  
  return Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean,cvar);
}

static Reference<Gaussian> newPolar_PV(double az, double r, double rr)
{
  Vector<double> mean(az,r,rr);
  Matrix<double> cvar(mean.dimension());
  cvar(0,0) = 1;
  cvar(1,1) = 1;
  cvar(2,2) = 1;
  
  return Gaussian::create(Polar_2D_PV::create(0,0),mean,cvar);
}
static Reference<Gaussian> newPolar_P(double cx, double cy, double az, double r, double daz, double dr)
{
  Vector<double> mean(az,r);
  Matrix<double> cvar(mean.dimension());
  cvar(0,0) = daz*daz;
  cvar(1,1) = dr*dr;
  
  return Gaussian::create(Polar_2D_P::create(cx,cy),mean,cvar);
}
struct PolarTransform {
  ::newton::Matrix<double> operator()  (const CoordinateFrame& from,
					const ::newton::Matrix<double>& X, 
					const CoordinateFrame& to) const throws ()
  {
    Vector<double> y(to.dimension());
    Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Polar_2D_PV::create());
    Vector<double> x(from.dimension());

    size_t n = X.rowSize();
    ::newton::Matrix<double> tmp(n,y.dimension());
    ::newton::Matrix<double> P = ::newton::Matrix<double>::identity(x.dimension());

    for (size_t i=0;i<n;++i) {
      for (size_t j=0;j<x.dimension();++j) {
	x(j) = X(i,j);
      }
      tx->apply(from,x,P,to,y,0,0);
      for (size_t j=0;j<y.dimension();++j) {
	tmp(i,j) = y(j);
      }
    }
    return tmp;
  }
};

struct InvPolarTransform {
  ::newton::Matrix<double> operator()  (const CoordinateFrame& from,
					const ::newton::Matrix<double>& X, 
					const CoordinateFrame& to) const throws ()
  {
    Vector<double> y(to.dimension());
    Reference<GaussianTransform> tx(Polar_2D_P__Cartesian_2D_Inertial_P::create());
    Vector<double> x(from.dimension());

    size_t n = X.rowSize();
    ::newton::Matrix<double> tmp(n,y.dimension());
    ::newton::Matrix<double> P = ::newton::Matrix<double>::identity(x.dimension());

    for (size_t i=0;i<n;++i) {
      for (size_t j=0;j<x.dimension();++j) {
	x(j) = X(i,j);
      }
      tx->apply(from,x,P,to,y,0,0);
      for (size_t j=0;j<y.dimension();++j) {
	tmp(i,j) = y(j);
      }
    }
    return tmp;
  }
};


struct MyTransform {
  MyTransform(double speed, double t) throws() 
  : _delta(t),_speed(speed) {}
  ~MyTransform() throws () {}
  
  ::newton::Matrix<double> operator()  (const CoordinateFrame& from,
					const ::newton::Matrix<double>& x, 
					const CoordinateFrame& to) const throws ()
  {
    size_t n = x.rowSize();
    ::newton::Matrix<double> tmp(n,1);
    for (size_t i=0;i<n;++i) {
      tmp(i,0) =  ::std::abs(x(i,0) + _delta*_speed);
    }
    return tmp;
  }
  
private:
  double _delta;
  double _speed;
};

struct MyLinearTransform {
  MyLinearTransform(double speed, double t) throws() 
  : _delta(t),_speed(speed) {}
  ~MyLinearTransform() throws () {}
  
  ::newton::Matrix<double> operator()  (const CoordinateFrame& from,
					const ::newton::Matrix<double>& x, 
					const CoordinateFrame& to) const throws ()
  {
    size_t n = x.rowSize();
    ::newton::Matrix<double> tmp(n,1);
    for (size_t i=0;i<n;++i) {
      tmp(i,0) =  x(i,0) + _delta*_speed;
    }
    return tmp;
  }
  
private:
  double _delta;
  double _speed;
};


struct IdentityTransform {
  IdentityTransform() throws()  {}
  ~IdentityTransform() throws () {}
  
  ::newton::Matrix<double> operator()  (const CoordinateFrame& from,
					const ::newton::Matrix<double>& x, 
					const CoordinateFrame& to) const throws ()
  { return x; }
  
};



void utest_simplexNonLinearConversion1D()
{
  ::std::cerr << ::std::endl;
  Reference<CoordinateFrame> frame = Cartesian_1D_Inertial_P::create();
  for (double t = -1;t<1;t = t+.25) {
    Matrix<double> HP,yP,xP(1,1);
    Vector<double> x(1),y;
    xP(0,0) = 1;
    x(0) = 0;
    Transform simplex(PointSet(1,SimplexPoints(.5)));
    simplex.transform(MyTransform(-1,t),*frame,x,xP,*frame,y,&yP,&HP);
    ::std::cerr << "Simplex : " << t << ": " << y(0) << " " << sqrt(yP(0,0)) << " " << HP(0,0) << ::std::endl;
  }


}

void utest_defaultNonLinearConversion1D()
{
  ::std::cerr << ::std::endl;
  Reference<CoordinateFrame> frame = Cartesian_1D_Inertial_P::create();
  for (double t = -1;t<1;t = t+.25) {
    Matrix<double> HP,yP,xP(1,1);

    Vector<double> x(1),y;
    xP(0,0) = 1;
    x(0) = 0;
    Transform defTransform(PointSet(1,DefaultPoints(1)));
    
    defTransform.transform(MyTransform(-1,t),*frame,x,xP,*frame,y,&yP,&HP);
    ::std::cerr << "Default : " << t << ": " << y(0) << " " << sqrt(yP(0,0)) << " " << HP(0,0) << ::std::endl;
  }


}

void utest_linearConversion1D()
{
  ::std::cerr << ::std::endl;
  Reference<CoordinateFrame> frame = Cartesian_1D_Inertial_P::create();
  for (double t = -2;t<2;t = t+.1) {
    Matrix<double> HP,yP,xP(1,1);
    Vector<double> x(1),y;
    xP(0,0) = 1;
    x(0) = 0;
    Transform simplex(PointSet(1,SimplexPoints(.5)));
    Transform defTransform(PointSet(1,DefaultPoints(1)));
    
    simplex.transform(MyLinearTransform(-1,t),*frame,x,xP,*frame,y,&yP,&HP);
    ::std::cerr << "Simplex : " << t << ": " << y(0) << " " << sqrt(yP(0,0)) << " " << HP(0,0) << ::std::endl;
    defTransform.transform(MyLinearTransform(-1,t),*frame,x,xP,*frame,y,&yP,&HP);
    ::std::cerr << "Default : " << t << ": " << y(0) << " " << sqrt(yP(0,0)) << " " << HP(0,0) << ::std::endl;
  }


}

void utest_identity()
{
  Reference<CoordinateFrame> frame = Cartesian_1D_Inertial_P::create();
  ::std::cerr << ::std::endl;
  for (double sigma = .0001; sigma < 10; sigma= sigma + 0.5) {
    Matrix<double> HP,yP,xP(1,1);
    Vector<double> x(1),y;
    xP(0,0) = sigma*sigma;
    x(0) = 5;
    Transform simplex(PointSet(1,SimplexPoints(0.5)));
    Transform defTransform(PointSet(1,DefaultPoints(1)));
    Transform sphTransform(PointSet(1,SphericalSimplexPoints(.5)));
    simplex.transform(IdentityTransform(),*frame,x,xP,*frame,y,&yP,&HP);
    ::std::cerr << "Simplex   : " << sigma << ": " << y(0) << " " << sqrt(yP(0,0)) << ::std::endl;
    sphTransform.transform(IdentityTransform(),*frame,x,xP,*frame,y,&yP,&HP);
    ::std::cerr << "Spherical : " << sigma << ": " << y(0) << " " << sqrt(yP(0,0)) << ::std::endl;
    defTransform.transform(IdentityTransform(),*frame,x,xP,*frame,y,&yP,&HP);
    ::std::cerr << "Default   : " << sigma << ": " << y(0) << " " << sqrt(yP(0,0)) << ::std::endl;
  }


}

void utest_scaledIdentity()
{
  Reference<CoordinateFrame> frame = Cartesian_1D_Inertial_P::create();
  ::std::cerr << ::std::endl;
  for (double sigma = .0001; sigma < 10; sigma= sigma + 0.5) {
    Matrix<double> HP,yP,xP(1,1);
    Vector<double> x(1),y;
    xP(0,0) = sigma*sigma;
    x(0) = 5;
    Transform simplex(PointSet(1,SimplexPoints(0.5)));
    Transform scaled(PointSet(1,SimplexPoints(0.5)),.5,2);
    simplex.transform(IdentityTransform(),*frame,x,xP,*frame,y,&yP,&HP);
    ::std::cerr << "Simplex : " << sigma << ": " << y(0) << " " << sqrt(yP(0,0)) << ::std::endl;
    scaled.transform(IdentityTransform(),*frame,x,xP,*frame,y,&yP,&HP);
    ::std::cerr << "Scaled  : " << sigma << ": " << y(0) << " " << sqrt(yP(0,0)) << ::std::endl;
  }


}

void utest_polar()
{
  ::std::cerr <<  ::std::endl; 
  Reference<Polar_2D_PV> polar(Polar_2D_PV::create(-1,1));
  Reference<Gaussian> est = newCartesian_PV(99,101,2,2);


  {
    Vector<double> mean;
    Matrix<double> HPHt,HP;
    
    Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Polar_2D_PV::create());
    tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	      *polar,mean,&HPHt,&HP);
    
    ::std::cerr << "Default EKF : " << mean << ' ' << HPHt << ::std::endl;
  }

 {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform tx(PointSet(est->dimension(),SimplexPoints(0.95)));
    tx.transform(PolarTransform(),*est->frame(),est->mean(),est->covariance().matrix(),
		 *polar,mean,&HPHt,&HP);
    ::std::cerr << "Simplex UKF : " << mean << ' ' << HPHt << ::std::endl;
  }
 {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform tx(PointSet(est->dimension(),SphericalSimplexPoints(0.95)));
    tx.transform(PolarTransform(),*est->frame(),est->mean(),est->covariance().matrix(),
		 *polar,mean,&HPHt,&HP);
    ::std::cerr << "Sphere UKF :  " << mean << ' ' << HPHt << ::std::endl;
  }

  {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform tx(PointSet(est->dimension(),DefaultPoints(1)));
    tx.transform(PolarTransform(),*est->frame(),est->mean(),est->covariance().matrix(),
		 *polar,mean,&HPHt,&HP);
    ::std::cerr << "Default UKF : " << mean << ' ' << HPHt << ::std::endl;
  }

 {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform tx(PointSet(est->dimension(),SimplexPoints(0)),.001,0);
    tx.transform(PolarTransform(),*est->frame(),est->mean(),est->covariance().matrix(),
		 *polar,mean,&HPHt,&HP);
    ::std::cerr << "Simplex SUKF: " << mean << ' ' << HPHt << ::std::endl;
  }


  {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform tx(PointSet(est->dimension(),DefaultPoints(1)),.001,0);
    tx.transform(PolarTransform(),*est->frame(),est->mean(),est->covariance().matrix(),
		 *polar,mean,&HPHt,&HP);
    ::std::cerr << "Default SUKF: " << mean << ' ' << HPHt << ::std::endl;
  }

  {
    Vector<double> mean;
    Matrix<double> HPHt,HP;
    
    Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Polar_2D_PV::create());
    Reference<Gaussian> ge = MonteCarloTransform::transform(tx,est,polar);
    ::std::cerr << "Monte Carlo : " << ge->mean() << ' ' << ge->covariance() << ::std::endl;
  }

}


void utest_invpolar()
{
  ::std::cerr <<  ::std::endl; 
  Reference<Gaussian> est = newPolar_P(0,0,0,1,15*M_PI/180,.02);
  Reference<CoordinateFrame> cart = Cartesian_2D_Inertial_P::create();

  {
    Vector<double> mean;
    Matrix<double> HPHt,HP;
    
    Reference<GaussianTransform> tx(Polar_2D_P__Cartesian_2D_Inertial_P::create());
    tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	      *cart,mean,&HPHt,&HP);
    
    ::std::cerr << "Default EKF : " << mean << ' ' << HPHt << ::std::endl;
  }

 {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform tx(PointSet(est->dimension(),SimplexPoints(0)));
    tx.transform(InvPolarTransform(),*est->frame(),est->mean(),est->covariance().matrix(),
		 *cart,mean,&HPHt,&HP);
    ::std::cerr << "Simplex UKF : " << mean << ' ' << HPHt << ::std::endl;
  }
 {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform tx(PointSet(est->dimension(),SphericalSimplexPoints(0)));
    tx.transform(InvPolarTransform(),*est->frame(),est->mean(),est->covariance().matrix(),
		 *cart,mean,&HPHt,&HP);
    ::std::cerr << "Sphere UKF :  " << mean << ' ' << HPHt << ::std::endl;
  }

  {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform tx(PointSet(est->dimension(),DefaultPoints(0)));
    tx.transform(InvPolarTransform(),*est->frame(),est->mean(),est->covariance().matrix(),
		 *cart,mean,&HPHt,&HP);
    ::std::cerr << "Default UKF : " << mean << ' ' << HPHt << ::std::endl;
  }

 {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform tx(PointSet(est->dimension(),SimplexPoints(0)),.95,0);
    tx.transform(InvPolarTransform(),*est->frame(),est->mean(),est->covariance().matrix(),
		 *cart,mean,&HPHt,&HP);
    ::std::cerr << "Simplex SUKF: " << mean << ' ' << HPHt << ::std::endl;
  }


  {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform tx(PointSet(est->dimension(),DefaultPoints(0)),.95,0);
    tx.transform(InvPolarTransform(),*est->frame(),est->mean(),est->covariance().matrix(),
		 *cart,mean,&HPHt,&HP);
    ::std::cerr << "Default SUKF: " << mean << ' ' << HPHt << ::std::endl;
  }

  {
    Reference<GaussianTransform> tx(Polar_2D_P__Cartesian_2D_Inertial_P::create());
    
    Reference<Gaussian> ge = MonteCarloTransform::transform(tx,est,cart);
    ::std::cerr << "Monte Carlo : " << ge->mean() << ' ' << ge->covariance() << ::std::endl;
  }
}


void utest_projective()
{
  ::std::cerr << ::std::endl; 
  Reference<ProjectivePlane3D_P> plane(new ProjectivePlane3D_P(Vector<>(0,-10,10),10,Vector<>(0,1,0),Vector<>(1,0,0)));
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_P__ProjectivePlane3D_P::create(false));
  Reference<Gaussian> est = newCartesian_P(0,-5,10);

  {
    Vector<double> mean;
    Matrix<double> HPHt,HP;
    
    tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	      *plane,mean,&HPHt,&HP);
    ::std::cerr << "Default EKF : " << mean << ' ' << HPHt << ::std::endl;
  }

  {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform t(PointSet(est->dimension(),DefaultPoints(1)));
    tx = UKFTransform::create(t,tx);
    tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	      *plane,mean,&HPHt,&HP);
    ::std::cerr << "Default UKF : " << mean << ' ' << HPHt << ::std::endl;
  }

  {
    Reference<Gaussian> ge = MonteCarloTransform::transform(tx,est,plane,32000);
    ::std::cerr << "Monte Carlo : " << ge->mean() << ' ' << ge->covariance() << ::std::endl;
  }
}

void utest_invProjective()
{
  ::std::cerr << ::std::endl; 
  Reference<CoordinateFrame> cart = Cartesian_2D_Inertial_P::create();
  Reference<ProjectivePlane3D_P> plane(new ProjectivePlane3D_P(Vector<>(0,-10,10),10,Vector<>(0,1,0),Vector<>(1,0,0)));
  Reference<GaussianTransform> tx(ProjectivePlane3D_P__Cartesian_2D_Inertial_P::create());
  Reference<Gaussian> est = newCartesian_P(0,-5,1);
  est = Gaussian::create(plane,est->mean(),est->covariance());
  
  {
    Reference<Gaussian> ge = MonteCarloTransform::transform(tx,est,cart,32000);
    ::std::cerr << "Monte Carlo : " << ge->mean() << ' ' << ge->covariance() << ::std::endl;
  }
  {
   Vector<double> mean;
    Matrix<double> HPHt,HP;
    Transform t(PointSet(est->dimension(),DefaultPoints(1)));
    tx = UKFTransform::create(t,tx);
    ::std::cerr << "Transform " << *est << ::std::endl;
    tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	      *cart,mean,&HPHt,&HP);
    ::std::cerr << "Default UKF : " << mean << ' ' << HPHt << ::std::endl;
  }
  {
    Vector<double> mean;
    Matrix<double> HPHt,HP;
    
    tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	      *cart,mean,&HPHt,&HP);
    ::std::cerr << "Default EKF : " << mean << ' ' << HPHt << ::std::endl;
  }


}

