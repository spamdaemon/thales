#include <thales/kalman/Gaussian.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Polar_2D_PV.h>
#include <thales/kalman/transform/Polar_2D_PV__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Polar_2D_PV.h>

using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;
using namespace ::newton;
using namespace ::timber;

static Reference<Gaussian> newCartesian_PV(double x, double y, double vx, double vy)
{
  Vector<Double> mean(x,y,vx,vy);
  Matrix<Double> cvar(mean.dimension());
  cvar(0,0) = 10;
  cvar(1,1) = 100;
  cvar(2,2) = 2;
  cvar(3,3) = 2;
  
  return Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean,cvar);
}

static Reference<Gaussian> newPolar_PV(double az, double r, double rr)
{
  Vector<Double> mean(az,r,rr);
  Matrix<Double> cvar(mean.dimension());
  cvar(0,0) = .01;
  cvar(1,1) = .01;
  cvar(2,2) = 1;
  
  return Gaussian::create(Polar_2D_PV::create(0,0),mean,cvar);
}

void utest_1()
{
  Reference<Polar_2D_PV> polar(Polar_2D_PV::create(-1,1));
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Polar_2D_PV::create());
  Reference<Gaussian> est = newCartesian_PV(99,101,2,2);
  Vector<Double> mean;
  Matrix<Double> HPHt,HP;

  tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	    *polar,mean,&HPHt,&HP);

  ::std::cerr << "MEAN : " << mean << ::std::endl;
}

void utest_2()
{
  Double maxSpeed = 12;

  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Polar_2D_PV::create());
  Reference<GaussianTransform> itx(Polar_2D_PV__Cartesian_2D_Inertial_PV::create(maxSpeed));

  Reference<Gaussian> polar = newPolar_PV(M_PI/4,10,maxSpeed/3);
  Reference<Gaussian> cartesian = polar->transform(polar->frame()->getInertialFrame(),*itx);
  
  ::std::cerr << "GAUSSIAN : " << cartesian->mean() << ::std::endl << cartesian->covariance() << ::std::endl;
  Double mx,mn,theta;
  
  Covariance::decompose((*cartesian)(0,0),(*cartesian)(1,1), (*cartesian)(1,0), mx,mn,theta);
  ::std::cerr << "Ellipse Pos: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
  assert(::std::abs(180*theta/M_PI - (-45)) < 0.0001);

  Covariance::decompose((*cartesian)(2,2),(*cartesian)(3,3), (*cartesian)(2,3), mx,mn,theta);
  ::std::cerr << "Ellipse Vel: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
  assert(::std::abs(180*theta/M_PI - (-45)) < 0.0001);
  assert(::std::abs(mn-1) < 0.1);
  assert(::std::abs(mx-(12-maxSpeed/3)) < 0.1);
  assert(::std::abs(Vector<Double>((*cartesian)(2),(*cartesian)(3)).length() - maxSpeed/3) < 0.0001);
}

void utest_3()
{

  {
    Double maxSpeed = 12;
    Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Polar_2D_PV::create());
    Reference<GaussianTransform> itx(Polar_2D_PV__Cartesian_2D_Inertial_PV::create(maxSpeed));

    Reference<Gaussian> polar = newPolar_PV(M_PI/4,10,maxSpeed);
    ::std::cerr<< "POLAR: " << polar->mean() << ::std::endl << polar->covariance() << ::std::endl;
    Reference<Gaussian> cartesian = polar->transform(polar->frame()->getInertialFrame(),*itx);
  
    ::std::cerr << "GAUSSIAN : " << cartesian->mean() << ::std::endl << cartesian->covariance() << ::std::endl;
    Double mx,mn,theta;
    Covariance::decompose((*cartesian)(2,2),(*cartesian)(3,3), (*cartesian)(2,3), mx,mn,theta);
    ::std::cerr << "Ellipse Vel: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
    assert(::std::abs(mn-1) < 0.1);
    assert(::std::abs(mx-1.3) < 0.1);
  }

  {
    Double maxSpeed = 12;
    Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Polar_2D_PV::create());
    Reference<GaussianTransform> itx(Polar_2D_PV__Cartesian_2D_Inertial_PV::create(maxSpeed));
    
    Reference<Gaussian> polar = newPolar_PV(M_PI/4,10,2*maxSpeed);
    Reference<Gaussian> cartesian = polar->transform(polar->frame()->getInertialFrame(),*itx);
    
    ::std::cerr << "GAUSSIAN : " << cartesian->mean() << ::std::endl << cartesian->covariance() << ::std::endl;
    Double mx,mn,theta;
    Covariance::decompose((*cartesian)(2,2),(*cartesian)(3,3), (*cartesian)(2,3), mx,mn,theta);
    ::std::cerr << "Ellipse Vel: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
    assert(::std::abs(mn-1) < 0.1);
    assert(::std::abs(mx-2.4) < 0.1);
  }
}

