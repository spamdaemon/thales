#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Polar_2D_P.h>
#include <thales/kalman/transform/Polar_2D_P__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Polar_2D_P.h>
#include <newton/newton.h>

using namespace ::std;
using namespace ::timber;
using namespace ::thales;
using namespace ::newton;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;

static Reference<Gaussian> newCartesian_PV(double x, double y, double vx, double vy)
{
  Vector<Double> mean(x,y,vx,vy);
  Matrix<Double> cvar(mean.dimension());
  cvar(0,0) = 10;
  cvar(1,1) = 100;
  cvar(2,2) = 2;
  cvar(3,3) = 2;
  
  return Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean,cvar);
}

static Reference<Gaussian> newPolar_P(double az, double r)
{
  Vector<Double> mean(az,r);
  Matrix<Double> cvar(mean.dimension());
  cvar(0,0) = 1;
  cvar(1,1) = 1;
  
  return Gaussian::create(Polar_2D_P::create(0,0),mean,cvar);
}

void utest_1() throws()
{
  Reference<Polar_2D_P> polar(Polar_2D_P::create(-1,1));
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Polar_2D_P::create());
  Reference<Gaussian> est = newCartesian_PV(99,101,2,2);
  Vector<Double> mean;
  Matrix<Double> HPHt,HP;
  
  tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
            *polar,mean,&HPHt,&HP);
  ::std::cerr << "MEAN : " << mean << ::std::endl;
}

void utest_2()
{
  Double maxSpeed = 12;
  
  Reference<CoordinateFrame> ix = Cartesian_2D_Inertial_PV::create();
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Polar_2D_P::create());
  Reference<GaussianTransform> itx(Polar_2D_P__Cartesian_2D_Inertial_PV::create(maxSpeed));

  Reference<Gaussian> polar = newPolar_P(M_PI/4,10);

  Reference<Gaussian> cartesian = polar->transform(ix,*itx);
  

  ::std::cerr << "GAUSSIAN : " << cartesian->mean() << ::std::endl << cartesian->covariance() << ::std::endl;
  assert((*cartesian)(2)==0.0);
  assert((*cartesian)(3)==0.0);
  Double mx,mn,theta;
  
  Covariance::decompose((*cartesian)(0,0),(*cartesian)(1,1), (*cartesian)(1,0), mx,mn,theta);
  ::std::cerr << "Ellipse Pos: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
  assert(::std::abs(180*theta/M_PI - (-45)) < 0.0001);

  Covariance::decompose((*cartesian)(2,2),(*cartesian)(3,3), (*cartesian)(2,3), mx,mn,theta);
  ::std::cerr << "Ellipse Vel: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
  assert(::std::abs(mn-maxSpeed) < 0.0001);
  assert(::std::abs(mx-maxSpeed) < 0.0001);
}
