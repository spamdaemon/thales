#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <newton/newton.h>
#include <thales/thales.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/ProjectivePlane3D_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__ProjectivePlane3D_P.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::newton;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;

static Reference<Gaussian> newCartesian_P(double x, double y, double vx, double vy, double err=1)
{
  Vector<double> mean(x,y,vx,vy);
  Matrix<double> cvar(mean.dimension());
  cvar(0,0) = err;
  cvar(1,1) = err;
  cvar(2,2) = err/2;
  cvar(3,3) = err/2;
  
  return Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean,cvar);
}


void utest_simpleProjection()
{
  Reference<ProjectivePlane3D_P> ix(new ProjectivePlane3D_P(Vector<>(0,-10,10),10,Vector<>(0,1,0),Vector<>(1,0,0)));
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__ProjectivePlane3D_P::create());
  Reference<Gaussian> est = newCartesian_P(0,5,5,-1,1);
  Vector<double> mean;
  Matrix<double> HPHt,HP;
  
  tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	    *ix,mean,&HPHt,&HP);
  
  ::std::cerr << "Forward : " << ::std::endl;
  ::std::cerr << "MEAN : " << mean << ::std::endl;
  ::std::cerr << "HPHt : " << HPHt << ::std::endl;
  
  Vector<double> mean2;
  Matrix<double> HPHt2;

  Reference<Gaussian> est2 = Gaussian::create(ix,mean,HPHt);
  Pointer<Gaussian> inv = est2->transform(est->frame());
  ::std::cerr << "Inverse : " << *inv << ::std::endl;
}

