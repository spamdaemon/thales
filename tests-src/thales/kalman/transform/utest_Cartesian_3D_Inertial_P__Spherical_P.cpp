#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/transform/Cartesian_3D_Inertial_PV__Spherical_P.h>
#include <thales/kalman/transform/Spherical_P__Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/frame/Spherical_P.h>
#include <newton/newton.h>

using namespace ::std;
using namespace ::timber;
using namespace ::thales;
using namespace ::newton;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;

static Reference< Gaussian> newCartesian_PV(double x, double y, double z, double vx, double vy, double vz)
{
   Vector< Double> mean(x, y, z, vx, vy, vz);
   Matrix< Double> cvar(mean.dimension());
   cvar(0, 0) = 10;
   cvar(1, 1) = 100;
   cvar(2, 2) = 100;
   cvar(3, 3) = 2;
   cvar(4, 4) = 2;
   cvar(5, 5) = 2;

   return Gaussian::create( Cartesian_3D_Inertial_PV::create(),mean, cvar);
}

static Reference< Gaussian> newPolar_P(double az, double el, double r)
{
   Vector< Double> mean(az, el, r);
   Matrix< Double> cvar(mean.dimension());
   cvar(0, 0) = 1;
   cvar(1, 1) = 1;
   cvar(2, 2) = 1;
      return Gaussian::create(Spherical_P::create(0, 0, 0),mean, cvar);
}

void utest_1() throws()
{
   Reference< Spherical_P> polar(Spherical_P::create(-1, 0, 1));
   Reference< GaussianTransform> tx(Cartesian_3D_Inertial_PV__Spherical_P::create());
   Reference< Gaussian> est = newCartesian_PV(99, 101, 0, 2, 2, 0);
   Vector< Double> mean;
   Matrix< Double> HPHt, HP;

   tx->apply(*est->frame(), est->mean(), est->covariance().matrix(), *polar, mean, &HPHt, &HP);
   ::std::cerr << "MEAN : " << mean << ::std::endl;
}

void utest_2()
{
   Double maxSpeed = 12;

   Reference< CoordinateFrame> ix = Cartesian_3D_Inertial_PV::create();
   Reference< GaussianTransform> tx(Cartesian_3D_Inertial_PV__Spherical_P::create());
   Reference< GaussianTransform> itx(Spherical_P__Cartesian_3D_Inertial_PV::create(maxSpeed));
   Reference< Gaussian> polar = newPolar_P(M_PI / 4, 0, 10);
   Reference< Gaussian> cartesian = polar->transform(ix, *itx);

   ::std::cerr << "GAUSSIAN : " << cartesian->mean() << ::std::endl << cartesian->covariance() << ::std::endl;
   assert((*cartesian)(3) == 0.0);
   assert((*cartesian)(4) == 0.0);
   assert((*cartesian)(5) == 0.0);
   Double mx, mn, theta;

#if 0
   Covariance::decompose((*cartesian)(0, 0), (*cartesian)(1, 1), (*cartesian)(1, 0), mx, mn, theta);
   ::std::cerr << "Ellipse Pos: " << mx << ", " << mn << ", " << 180 * theta / M_PI << ::std::endl;
   assert(::std::abs(180 * theta / M_PI - (-45)) < 0.0001);

   Covariance::decompose((*cartesian)(2, 2), (*cartesian)(3, 3), (*cartesian)(2, 3), mx, mn, theta);
   ::std::cerr << "Ellipse Vel: " << mx << ", " << mn << ", " << 180 * theta / M_PI << ::std::endl;
   assert(::std::abs(mn - maxSpeed) < 0.0001);
   assert(::std::abs(mx - maxSpeed) < 0.0001);
#endif
}
