#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/transform/Cartesian_3D_Inertial_P__Spherical_P.h>
#include <thales/kalman/transform/Spherical_P__Cartesian_3D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_P.h>
#include <thales/kalman/frame/Spherical_P.h>
#include <newton/newton.h>

using namespace ::std;
using namespace ::timber;
using namespace ::thales;
using namespace ::newton;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;

static double toRadians(double x)
{
   return x * M_PI / 180.0;
}

static Reference< Gaussian> newPolar_P(double az, double el, double r, double varAz = .1, double varEl = .1,
      double varR = .1)
{
   Vector< Double> mean(az, el, r);
   Matrix< Double> cvar(mean.dimension());
   cvar(0, 0) = varAz;
   cvar(1, 1) = varEl;
   cvar(2, 2) = varR;
   return Gaussian::create( Spherical_P::create(0, 0, 0),mean, cvar);
}

void utest_1() throws()
{
   Reference< Cartesian_3D_Inertial_P> tgt(Cartesian_3D_Inertial_P::create());
   Reference< GaussianTransform> tx(Spherical_P__Cartesian_3D_Inertial_P::create());
   Reference< GaussianTransform> txBack(Cartesian_3D_Inertial_P__Spherical_P::create());
   Reference< Gaussian> est = newPolar_P(M_PI / 4, M_PI / 4, 1, 1, 1, 1);
   Vector< Double> mean;
   Matrix< Double> HPHt, HP;
   Matrix< Double> notused(3, 3);
   tx->apply(*est->frame(), est->mean(), est->covariance().matrix(), *tgt, mean, &HPHt, &HP);
   ::std::cerr << "MEAN : " << mean << ::std::endl;
   assert(abs(mean(0) - mean(1)) < .0001);
   assert(mean(0) > 0);
   assert(abs(mean.length() - 1) < .0001);

   Vector< Double> meanBack;
   txBack->apply(*tgt, mean, notused, *est->frame(), meanBack, nullptr, nullptr);

   ::std::cerr << "INITIAL   " << *est << ::std::endl;
   auto F = est->transform(tgt, *tx);
   ::std::cerr << "TEMPORARY " << *F  << ::std::endl;
   ::std::cerr << "RETURN    " << *F->transform(est->frame(), *txBack) << ::std::endl;

   assert((est->mean() - meanBack).length() < 0.0001);
}

void utest_PrimaryAxes() throws()
{
   ::std::cerr << "Test 3 primary axes" << ::std::endl;

   Reference< Cartesian_3D_Inertial_P> tgt(Cartesian_3D_Inertial_P::create());
   Reference< GaussianTransform> tx(Spherical_P__Cartesian_3D_Inertial_P::create());
   {
      ::std::cerr << "Test X-axis" << ::std::endl;
      Reference< Gaussian> est = newPolar_P(0, 0, 1);
      Vector< Double> mean;
      Matrix< Double> HPHt, HP;
      tx->apply(*est->frame(), est->mean(), est->covariance().matrix(), *tgt, mean, &HPHt, &HP);
      ::std::cerr << "MEAN : " << mean << ::std::endl;
      assert(abs(mean(0) - 0) < .00001);
      assert(abs(mean(1) - 1) < .00001);
      assert(abs(mean(2) - 0) < .00001);
   }

   {
      ::std::cerr << "Test Y-axis" << ::std::endl;
      Reference< Gaussian> est = newPolar_P(M_PI / 2, 0, 1);
      Vector< Double> mean;
      Matrix< Double> HPHt, HP;
      tx->apply(*est->frame(), est->mean(), est->covariance().matrix(), *tgt, mean, &HPHt, &HP);
      ::std::cerr << "MEAN : " << mean << ::std::endl;
      assert(abs(mean(0) - 1) < .00001);
      assert(abs(mean(1) - 0) < .00001);
      assert(abs(mean(2) - 0) < .00001);

   }

   {
      ::std::cerr << "Test Z-axis" << ::std::endl;
      Reference< Gaussian> est = newPolar_P(M_PI / 2, M_PI / 2, 1);
      Vector< Double> mean;
      Matrix< Double> HPHt, HP;
      tx->apply(*est->frame(), est->mean(), est->covariance().matrix(), *tgt, mean, &HPHt, &HP);
      ::std::cerr << "MEAN : " << mean << ::std::endl;
      assert(abs(mean(0) - 0) < .00001);
      assert(abs(mean(1) - 0) < .00001);
      assert(abs(mean(2) - 1) < .00001);
   }
}

void utest_Covariance()
{
   ::std::cerr << "utest_Covariance" << ::std::endl;

   Reference< Cartesian_3D_Inertial_P> tgt(Cartesian_3D_Inertial_P::create());
   Reference< GaussianTransform> tx(Spherical_P__Cartesian_3D_Inertial_P::create());

   {
      Reference< Gaussian> est = newPolar_P(M_PI / 4, 0, 1, toRadians(1), toRadians(.002), .0025);
      Vector< Double> mean;
      Matrix< Double> cvar, HP;
      tx->apply(*est->frame(), est->mean(), est->covariance().matrix(), *tgt, mean, &cvar, &HP);
      ::std::cerr << "MEAN : " << mean << ::std::endl;
      ::std::cerr << "CVAR : " << cvar << ::std::endl;

      // decompose the covariance
      double M, m, az;
      Covariance::decompose(cvar(0, 0), cvar(1, 1), cvar(0, 1), M, m, az);
      ::std::cerr << "Decomposed : M=" << M << ", m=" << m << ", az=" << az << ::std::endl;
      double azErr = (-M_PI / 4);
      ::std::cerr << "Expecting azErr =" << azErr << ::std::endl;
      assert(abs(az - azErr) < .01);
      assert(abs(m - .05) < .0001);
   }

}
