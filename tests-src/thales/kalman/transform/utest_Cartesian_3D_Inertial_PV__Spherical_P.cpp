#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/transform/Cartesian_3D_Inertial_P__Spherical_P.h>
#include <thales/kalman/transform/Spherical_P__Cartesian_3D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_P.h>
#include <thales/kalman/frame/Spherical_P.h>
#include <newton/newton.h>

using namespace ::std;
using namespace ::timber;
using namespace ::thales;
using namespace ::newton;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;

static Reference< Gaussian> newCartesian_P(double x, double y, double z, double varX = 10, double varY = 100,
      double varZ = 100)
{
   Vector< Double> mean(x, y, z);
   Matrix< Double> cvar(mean.dimension());
   cvar(0, 0) = varX;
   cvar(1, 1) = varY;
   cvar(2, 2) = varZ;

   return Gaussian::create(Cartesian_3D_Inertial_P::create(),mean, cvar);
}

void utest_1() throws()
{
   Reference< Spherical_P> polar(Spherical_P::create(-1, 0, 1));
   Reference< GaussianTransform> tx(Cartesian_3D_Inertial_P__Spherical_P::create());
   Reference< Gaussian> est = newCartesian_P(99, 101, 0);
   Vector< Double> mean;
   Matrix< Double> HPHt, HP;

   tx->apply(*est->frame(), est->mean(), est->covariance().matrix(), *polar, mean, &HPHt, &HP);
   ::std::cerr << "MEAN : " << mean << ::std::endl;
}

void utest_Covariance()
{
   ::std::cerr << "utest_Covariance" << ::std::endl;

   Reference< Spherical_P> tgt(Spherical_P::create(0, 0, 0));
   Reference< GaussianTransform> tx(Cartesian_3D_Inertial_P__Spherical_P::create());
   {
      Reference< Gaussian> est = newCartesian_P(1, 1, 0, 1, 1, 1);
      Vector< Double> mean;
      Matrix< Double> cvar, HP;
      tx->apply(*est->frame(), est->mean(), est->covariance().matrix(), *tgt, mean, &cvar, &HP);
      ::std::cerr << "MEAN : " << mean << ::std::endl;
      ::std::cerr << "CVAR : " << cvar << ::std::endl;
      ::std::cerr << "HP   : " << HP << ::std::endl;

      assert(abs(cvar(2, 2) - 1) < .001);
      assert(abs(cvar(0, 0) - cvar(1,1)) < .00001);
      assert(abs(mean(0)-M_PI/4) < .001);
      assert(abs(mean(2)-::std::sqrt(2)) < .001);
   }

}
