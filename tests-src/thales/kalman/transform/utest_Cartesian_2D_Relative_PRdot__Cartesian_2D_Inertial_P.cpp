#include <cmath>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/transform/Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Relative_PRdot.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>

using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;
using namespace ::newton;
using namespace ::timber;

void utest_1()
{
  // setup a filter
  Vector<double> mean(100.0,0,10.0);
  Matrix<double> cov = Matrix<double>::identity(mean.dimension());
  cov(0,2) = cov(2,0) = .1;
  cov(1,2) = cov(2,1) = .2;

  Reference<Gaussian> p = Gaussian::create(Cartesian_2D_Relative_PRdot::create(0,0),mean,cov);

  Reference<GaussianTransform> tx(Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_P::create());
  Reference<Cartesian_2D_Inertial_P> ix(Cartesian_2D_Inertial_P::create());
  
  Matrix<double> HPHt,HP;
  
  tx->apply(*p->frame(),p->mean(),p->covariance().matrix(),
	    *ix,mean,&HPHt,&HP);

  ::std::cerr << "P    : " << *p << ::std::endl;

  ::std::cerr << "MEAN : " << mean << ::std::endl;
  
  assert(mean(0)==p->get(0));
  assert(mean(1)==p->get(1));
  assert(HPHt(0,0)==p->get(0,0));
  assert(HPHt(1,1)==p->get(1,1));
  assert(HPHt(0,1)==p->get(0,1));
  assert(HPHt(1,0)==p->get(1,0));
}

