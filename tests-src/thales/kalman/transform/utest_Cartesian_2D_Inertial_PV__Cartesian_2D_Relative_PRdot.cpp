#include <thales/kalman/Gaussian.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Cartesian_2D_Relative_PRdot.h>
#include <thales/kalman/transform/Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Relative_PRdot.h>

using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;
using namespace ::newton;
using namespace ::timber;

static Reference<Gaussian> newCartesian_PV(double x, double y, double vx, double vy)
{
  Vector<Double> mean(x,y,vx,vy);
  Matrix<Double> cvar(mean.dimension());
  cvar(0,0) = 10;
  cvar(1,1) = 100;
  cvar(2,2) = 2;
  cvar(3,3) = 2;
  
  return Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean,cvar);
}

static Reference<Gaussian> newRelative_PRdot(double az, double r, double rr)
{
  Vector<Double> mean(r* ::std::sin(az),r* ::std::cos(az),rr);
  Matrix<Double> cvar(mean.dimension());
  cvar(0,0) = 1;
  cvar(1,1) = 1;
  cvar(2,2) = 1;
  
  return Gaussian::create(Cartesian_2D_Relative_PRdot::create(0,0),mean,cvar);
}

void utest_1()
{
  Reference<Cartesian_2D_Relative_PRdot> polar(Cartesian_2D_Relative_PRdot::create(-1,1));
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Cartesian_2D_Relative_PRdot::create());
  Reference<Gaussian> est = newCartesian_PV(99,101,2,2);
  
  Vector<double> mean;
  Matrix<double> HPHt,HP;
  tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	    *polar,mean,&HPHt,&HP);

  ::std::cerr << "MEAN : " << mean << ::std::endl;
}

void utest_2()
{
  Double maxSpeed = 12;

  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Cartesian_2D_Relative_PRdot::create());
  Reference<GaussianTransform> itx(Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV::create(maxSpeed));

  Reference<Gaussian> relative = newRelative_PRdot(M_PI/4,10,maxSpeed/3);
  ::std::cerr << "RELATIVE " << relative->mean() << ::std::endl << relative->covariance() << ::std::endl;
  Reference<Gaussian> cartesian = relative->transform(relative->frame()->getInertialFrame(),*itx);
  
  ::std::cerr << "GAUSSIAN : " << cartesian->mean() << ::std::endl << cartesian->covariance() << ::std::endl;
  Double mx,mn,theta;
  

  Covariance::decompose((*cartesian)(2,2),(*cartesian)(3,3), (*cartesian)(2,3), mx,mn,theta);
  ::std::cerr << "Ellipse Vel: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
  assert(::std::abs(180*theta/M_PI - (-45)) < 0.0001);
  assert(::std::abs(mn-1) < 0.1);
  assert(::std::abs(mx-(12-maxSpeed/3)) < 0.1);
  assert(::std::abs(Vector<Double>((*cartesian)(2),(*cartesian)(3)).length() - maxSpeed/3) < 0.01);
}

void utest_3()
{

  {
    Double maxSpeed = 12;
    Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Cartesian_2D_Relative_PRdot::create());
    Reference<GaussianTransform> itx(Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV::create(maxSpeed));
    
    Reference<Gaussian> relative = newRelative_PRdot(M_PI/4,10,maxSpeed);
    Reference<Gaussian> cartesian = relative->transform(relative->frame()->getInertialFrame(),*itx);
    
    ::std::cerr << "GAUSSIAN : " << cartesian->mean() << ::std::endl << cartesian->covariance() << ::std::endl;
    Double mx,mn,theta;
    Covariance::decompose((*cartesian)(2,2),(*cartesian)(3,3), (*cartesian)(2,3), mx,mn,theta);
    ::std::cerr << "Ellipse Vel: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
    assert(::std::abs(mn-1) < 0.1);
    assert(::std::abs(mx-1.5) < 0.1);
  }

  {
    Double maxSpeed = 12;
    Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Cartesian_2D_Relative_PRdot::create());
    Reference<GaussianTransform> itx(Cartesian_2D_Relative_PRdot__Cartesian_2D_Inertial_PV::create(maxSpeed));
    
    Reference<Gaussian> relative = newRelative_PRdot(M_PI/4,10,2*maxSpeed);
    Reference<Gaussian> cartesian = relative->transform(relative->frame()->getInertialFrame(),*itx);
    
    ::std::cerr << "GAUSSIAN : " << cartesian->mean() << ::std::endl << cartesian->covariance() << ::std::endl;
    Double mx,mn,theta;
    Covariance::decompose((*cartesian)(2,2),(*cartesian)(3,3), (*cartesian)(2,3), mx,mn,theta);
    ::std::cerr << "Ellipse Vel: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
    assert(::std::abs(mn-1) < 0.1);
    assert(::std::abs(mx-2.6) < 0.1);
  }
}

