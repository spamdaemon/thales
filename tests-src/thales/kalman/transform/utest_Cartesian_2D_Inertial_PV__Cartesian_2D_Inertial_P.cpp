#include <cmath>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>

using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;
using namespace ::newton;
using namespace ::timber;

static Reference<Gaussian> newCartesian_PV(double x, double y, double vx, double vy)
{
  Vector<Double> mean(x,y,vx,vy);
  Matrix<Double> cvar(mean.dimension());
  cvar(0,0) = 10;
  cvar(1,1) = 100;
  cvar(2,2) = 2;
  cvar(3,3) = 2;
  
  return Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean,cvar);
}

void utest_1()
{
  Reference<Cartesian_2D_Inertial_P> ix(Cartesian_2D_Inertial_P::create());
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Cartesian_2D_Inertial_P::create());
  Reference<Gaussian> est = newCartesian_PV(99,101,2,2);
  
  Vector<double> mean;
  Matrix<double> HPHt,HP;
  
  tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	    *ix,mean,&HPHt,&HP);

  ::std::cerr << "MEAN : " << mean << ::std::endl;
  
  assert(mean(0)==est->get(0));
  assert(mean(1)==est->get(1));
  assert(HPHt(0,0)==est->get(0,0));
  assert(HPHt(1,1)==est->get(1,1));
  assert(HPHt(0,1)==est->get(0,1));
  assert(HPHt(1,0)==est->get(1,0));
}

void utest_2()
{
  // setup a filter
  Vector<double> mean(0.0,0.0,0.0,0.0);
  Matrix<double> cov = Matrix<double>::identity(mean.dimension());
  cov(0,2) = cov(2,0) = .5;
  cov(1,3) = cov(3,1) = .5;
  
  Reference<Gaussian> p = Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean,Covariance(cov));
  
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Cartesian_2D_Inertial_P::create());
  Reference<Cartesian_2D_Inertial_P> ix(Cartesian_2D_Inertial_P::create());
  
  Matrix<double> HPHt,HP;
  
  tx->apply(*p->frame(),p->mean(),p->covariance().matrix(),
	    *ix,mean,&HPHt,&HP);

  ::std::cerr << "P    : " << *p << ::std::endl;

  ::std::cerr << "MEAN : " << mean << ::std::endl;
  
  assert(mean(0)==p->get(0));
  assert(mean(1)==p->get(1));
  assert(HPHt(0,0)==p->get(0,0));
  assert(HPHt(1,1)==p->get(1,1));
  assert(HPHt(0,1)==p->get(0,1));
  assert(HPHt(1,0)==p->get(1,0));

}

