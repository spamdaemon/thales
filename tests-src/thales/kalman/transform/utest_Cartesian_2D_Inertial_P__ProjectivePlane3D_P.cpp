#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <newton/newton.h>
#include <thales/thales.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/ProjectivePlane3D_P.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_P__ProjectivePlane3D_P.h>
#include <thales/kalman/transform/ProjectivePlane3D_P__Cartesian_2D_Inertial_P.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::newton;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;

static Reference<Gaussian> newCartesian_P(double x, double y, double err=1)
{
  Vector<double> mean(x,y);
  Matrix<double> cvar(mean.dimension());
  cvar(0,0) = err;
  cvar(1,1) = err;
  
  return Gaussian::create(Cartesian_2D_Inertial_P::create(),mean,cvar);
}


void utest_simpleProjection()
{
  Reference<ProjectivePlane3D_P> ix(new ProjectivePlane3D_P(Vector<>(0,-10,10),10,Vector<>(0,1,0),Vector<>(1,0,0)));
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_P__ProjectivePlane3D_P::create());
  Reference<GaussianTransform> itx(ProjectivePlane3D_P__Cartesian_2D_Inertial_P::create());
  Reference<Gaussian> est = newCartesian_P(0,5,1);
  Vector<double> mean;
  Matrix<double> HPHt,HP;
  
  tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	    *ix,mean,&HPHt,&HP);
  
  ::std::cerr << "Forward : " << ::std::endl;
  ::std::cerr << "MEAN : " << mean << ::std::endl;
  ::std::cerr << "HPHt : " << HPHt << ::std::endl;
  
  Vector<double> mean2;
  Matrix<double> HPHt2;

  Reference<Gaussian> est2 = Gaussian::create(ix,mean,HPHt);
  Pointer<Gaussian> inv = est2->transform(est->frame(),*itx);
  ::std::cerr << "Inverse : " << *inv << ::std::endl;
}

void utest_behindProjectionPlane()
{
  Reference<ProjectivePlane3D_P> ix(new ProjectivePlane3D_P(Vector<>(0,-10,10),10,Vector<>(0,1,0),Vector<>(1,0,0)));
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_P__ProjectivePlane3D_P::create());
  Reference<Gaussian> est = newCartesian_P(0,-5,10);
  Vector<double> mean;
  Matrix<double> HPHt,HP;

  try {
    tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	      *ix,mean,&HPHt,&HP);
    assert(false);
  }
  catch (...) {
  }
}

void utest_crossingProjectionPlane()
{
  Reference<ProjectivePlane3D_P> ix(new ProjectivePlane3D_P(Vector<>(0,-10,10),10,Vector<>(0,1,0),Vector<>(1,0,0)));
  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_P__ProjectivePlane3D_P::create());
  Reference<GaussianTransform> itx(ProjectivePlane3D_P__Cartesian_2D_Inertial_P::create());
  Reference<Gaussian> est = newCartesian_P(0,5,10);
  Vector<double> mean;
  Matrix<double> HPHt,HP;

  tx->apply(*est->frame(),est->mean(),est->covariance().matrix(),
	    *ix,mean,&HPHt,&HP);
  
  ::std::cerr << "Forward : " << ::std::endl;
  ::std::cerr << "MEAN : " << mean << ::std::endl;
  ::std::cerr << "HPHt : " << HPHt << ::std::endl;
  
  Vector<double> mean2;
  Matrix<double> HPHt2;

  Reference<Gaussian> est2 = Gaussian::create(ix,mean,HPHt);
  Pointer<Gaussian> inv = est2->transform(est->frame(),*itx);
  ::std::cerr << "Inverse : " << *inv << ::std::endl;
}
