#include <thales/kalman/Gaussian.h>
#include <thales/kalman/MonteCarloTransform.h>
#include <thales/kalman/transform/Cartesian_2D_Inertial_PV__Polar_2D_PV.h>
#include <thales/kalman/transform/Polar_2D_PV__Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Polar_2D_PV.h>
#include <thales/kalman/frame/Rdot_2D.h>

using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::transform;
using namespace ::newton;
using namespace ::timber;

static Reference<Gaussian> newCartesian_PV(double x, double y, double vx, double vy)
{
  Vector<double> mean(x,y,vx,vy);
  Matrix<double> cvar(mean.dimension());
  cvar(0,0) = 10;
  cvar(1,1) = 100;
  cvar(2,2) = 2;
  cvar(3,3) = 2;
  
  return Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean,cvar);
}

static Reference<Gaussian> newPolar_PV(double az, double r, double rr)
{
  Vector<double> mean(az,r,rr);
  Matrix<double> cvar(mean.dimension());
  cvar(0,0) = .001;
  cvar(1,1) = .001;
  cvar(2,2) = 1;
  
  return Gaussian::create(Polar_2D_PV::create(0,0),mean,cvar);
}

void utest_1()
{
  double maxSpeed = 12;

  Reference<GaussianTransform> tx(Cartesian_2D_Inertial_PV__Polar_2D_PV::create());
  Reference<GaussianTransform> itx(Polar_2D_PV__Cartesian_2D_Inertial_PV::create(maxSpeed));

  Reference<Gaussian> polar = newPolar_PV(M_PI/4,10,maxSpeed/3);
  Reference<Gaussian> cartesian = MonteCarloTransform::transform(itx,polar,polar->frame()->getInertialFrame());
  ::std::cerr << "Montecarlo: " << cartesian->mean() << ::std::endl << cartesian->covariance() << ::std::endl;
  double mx,mn,theta;
  Covariance::decompose((*cartesian)(2,2),(*cartesian)(3,3), (*cartesian)(2,3), mx,mn,theta);
  ::std::cerr << "Ellipse Vel: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
  cartesian = polar->transform(polar->frame()->getInertialFrame(),*itx);

  ::std::cerr << "GAUSSIAN : " << cartesian->mean() << ::std::endl << cartesian->covariance() << ::std::endl;
  
  Covariance::decompose((*cartesian)(0,0),(*cartesian)(1,1), (*cartesian)(1,0), mx,mn,theta);
  ::std::cerr << "Ellipse Pos: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
  assert(::std::abs(180*theta/M_PI - (-45)) < 0.0001);

  Covariance::decompose((*cartesian)(2,2),(*cartesian)(3,3), (*cartesian)(2,3), mx,mn,theta);
  ::std::cerr << "Ellipse Vel: " << mx << ", " << mn << ", " << 180*theta/M_PI << ::std::endl;
  assert(::std::abs(180*theta/M_PI - (-45)) < 0.0001);
  assert(::std::abs(mn-1) < 0.1);
  assert(::std::abs(mx-(12-maxSpeed/3)) < 0.1);
  assert(::std::abs(Vector<double>((*cartesian)(2),(*cartesian)(3)).length() - maxSpeed/3) < 0.0001);

  Reference<Rdot_2D> rdot(Rdot_2D::create(0,0));
  Reference<Gaussian> speed = cartesian->transform(rdot);

  ::std::cerr << "SPEED " << *speed << ::std::endl;
  assert(::std::abs(speed->get(0)-polar->get(2)) < 1E-8);
  assert(::std::abs(speed->get(0,0)-polar->get(2,2)) < 1E-8);

}

