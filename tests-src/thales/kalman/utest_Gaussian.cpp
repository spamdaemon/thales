#include <iostream>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/GaussianTransform.h>
#include <thales/kalman/CoordinateFrame.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/model/ConstantVelocityModel2D.h>


using namespace timber;
using namespace newton;
using namespace thales;
using namespace thales::kalman;
using namespace thales::kalman::frame;
using namespace thales::kalman::model;

static const Double PI = 3.14159265358979323846;
static const Double TWO_PI = 2*3.14159265358979323846;

struct GTx : public GaussianTransform {
  ~GTx() throws()
  {}

  bool apply(const CoordinateFrame&, const Vector<Double>& mean, const Matrix<double>& P, 
	     const CoordinateFrame&, Vector<Double>& xmean, Matrix<double>* HPHt, Matrix<double>* HP) const throws()
  {
    xmean = mean;
    if (HPHt) {
      *HPHt = P;
    }
    if (HP) {
      *HP = P;
    }
    return true;
  }
};

static Reference<Gaussian> newGaussian2D(double x, double y, double xx, double yy, double xy=0.0)
{
  Vector<Double> mean(x,y);
  Matrix<Double> cvar(mean.dimension());
  cvar(0,0) = xx;
  cvar(1,1) = yy;
  cvar(0,1) = cvar(1,0) = xy;

  return Gaussian::create(CoordinateFrame::createGenericFrame(2),mean,cvar);
}

static Reference<Gaussian> newGaussian3D(double x, double y, double z, double xx, double yy, double zz, double xy=0.0, double xz=0.0, double yz=0.0)
{
  Vector<Double> mean(x,y,z);
  Matrix<Double> cvar(mean.dimension());
  cvar(0,0) = xx;
  cvar(1,1) = yy;
  cvar(2,2) = zz;
  cvar(0,1) = cvar(1,0) = xy;
  cvar(0,2) = cvar(2,0) = xz;
  cvar(1,2) = cvar(2,1) = yz;

  return Gaussian::create(CoordinateFrame::createGenericFrame(3),mean,cvar);
}

static void verifyEstimates (const Gaussian& a, const Gaussian& b, double eps=0.001)
{
  if (a.frame()->equals(*b.frame())) {
    assert((a.covariance().matrix()-b.covariance().matrix()).abs().max() < eps);
    assert((a.mean()-b.mean()).abs().max() < eps);
  }
  else {
    ::std::cerr << "Verify estimates: different coordinate frames; cannot compare" << ::std::endl;
  }
}

static void verifyMHD(Reference<Gaussian> z0, Reference<Gaussian> z1, double expectedDist, double eps=0.0001)
{
  GTx transform;
  double expected2 = (expectedDist*expectedDist);
  double dist2 = z0->mahalanobisDistance2(z1,transform);
  ::std::cerr << "Expected: " << expected2 << ", got Mahalanobis distance : " << dist2 << ::std::endl;
  assert(::std::abs(dist2- expected2) < eps);
}

static void verifyLambda(Reference<Gaussian> z0, Reference<Gaussian> z1, Likelihood expectedLambda, double eps=0.0001)
{
  GTx transform;
  Likelihood lambda = z0->likelihood(z1,transform);
    ::std::cerr << "Expected: " << expectedLambda.value() << ", got lambda : " << lambda.value() << ::std::endl;
    assert(::std::abs(lambda.value()-expectedLambda.value()) < eps);
}

void utest_MahalanobisDistance()
{
  GTx transform;
  {
    Reference<Gaussian> z0(newGaussian2D(0,0,1,1,0));
    Reference<Gaussian> z1(newGaussian2D(1,1,1,1,0));
    verifyMHD(z0,z1,1);
  }

  {
    Reference<Gaussian> z0(newGaussian2D(0,0,1,1,0));
    Reference<Gaussian> z1(newGaussian2D(1,0,1,1,0));
    verifyMHD(z0,z1,sqrt(0.5));
  }

  {
    Reference<Gaussian> z0(newGaussian2D(0,0,1,1,0));
    Reference<Gaussian> z1(newGaussian2D(0,1,1,1,0));
    verifyMHD(z0,z1,::sqrt(.5));
  }

  {
    Reference<Gaussian> z0(newGaussian3D(0,0,0,1,1,1));
    Reference<Gaussian> z1(newGaussian3D(1,1,1,1,1,1));
    verifyMHD(z0,z1,::sqrt(1.5));
  }

  {
    Reference<Gaussian> z0(newGaussian3D(0,0,0,1,1,1));
    Reference<Gaussian> z1(newGaussian3D(0,1,1,1,1,1));
    verifyMHD(z0,z1,::sqrt(1));
  }
  {
    Reference<Gaussian> z0(newGaussian3D(0,0,0,1,1,1));
    Reference<Gaussian> z1(newGaussian3D(1,0,1,1,1,1));
    verifyMHD(z0,z1,::sqrt(1));
  }
  {
    Reference<Gaussian> z0(newGaussian3D(0,0,0,1,1,1));
    Reference<Gaussian> z1(newGaussian3D(1,1,0,1,1,1));
    verifyMHD(z0,z1,::sqrt(1));
  }

  {
    Reference<Gaussian> z0(newGaussian3D(0,0,0,1,1,1));
    Reference<Gaussian> z1(newGaussian3D(0,0,1,1,1,1));
    verifyMHD(z0,z1,::sqrt(0.5));
  }
  {
    Reference<Gaussian> z0(newGaussian3D(0,0,0,1,1,1));
    Reference<Gaussian> z1(newGaussian3D(1,0,0,1,1,1));
    verifyMHD(z0,z1,::sqrt(0.5));
  }
  {
    Reference<Gaussian> z0(newGaussian3D(0,0,0,1,1,1));
    Reference<Gaussian> z1(newGaussian3D(0,1,0,1,1,1));
    verifyMHD(z0,z1,::sqrt(0.5));
  }
  
}

void utest_prepareUpdate()
{
  Reference<ProcessModel> model = new ConstantVelocityModel2D(1);
  Vector<Double> mean(0.0,0.0,1,0);
  Matrix<Double> cvar = Matrix<Double>::identity(mean.dimension());
  Reference<Gaussian> g0(Gaussian::create(model->coordinateFrame(),mean,cvar));
  
  Reference<Gaussian> p1 = model->predict(g0,1.0);
  
  Vector<Double> zMean(2.0,0.0);
  Matrix<Double> zVar = Matrix<Double>::identity(2);
  Reference<Gaussian> z(Gaussian::create(Cartesian_2D_Inertial_P::create(),zMean,zVar));

  Reference<PreparedUpdate> preparedUpdate = p1->prepareKalmanUpdate(z);

  ::std::cerr << "Likelihood : " << preparedUpdate->likelihood().value() << ::std::endl
	      << "Distance   : " << ::std::sqrt(preparedUpdate->mahalanobisDistance2()) << ::std::endl;
  
  double d1 = ::std::sqrt(preparedUpdate->mahalanobisDistance2());
  assert(d1 == d1 && "Mahalanobis distance is NaN");

  Reference<Gaussian> g1 = preparedUpdate->postUpdate();
  
  ::std::cerr << "Measurement: " << z->mean() << ::std::endl
	      << "Estimate   : " << g1->mean() << ::std::endl;
  // check for a NaN in the mahalanobis distance
}

void utest_doKalmanUpdate()
{
  Vector<Double> mean(2.0,2.0);
  Matrix<Double> cvar(2);
  cvar(0,0) = cvar(1,1) = 2;
  
  Reference<Gaussian> initial(Gaussian::create(Cartesian_2D_Inertial_P::create(),mean,cvar));
  Reference<Gaussian> g0 = initial;
  
  mean = Vector<Double>(3.0,3.0);
  cvar = Matrix<Double>(2);
  cvar(0,0) = cvar(1,1) = 2;
  Reference<Gaussian> z(Gaussian::create(Cartesian_2D_Inertial_P::create(),mean,cvar));

  Reference<Gaussian> g1 = g0->doKalmanUpdate(z);

  ::std::cerr << "State : " << g1->mean() << ::std::endl;

  assert((g1->mean() - Vector<Double>(2.5,2.5)).abs().max()<0.0001);
  verifyEstimates(*z,*Gaussian::findMeasurement(g0,g1));

}

void utest_Likelihood()
{
  GTx transform;
  {
    Reference<Gaussian> z0(newGaussian2D(0,0,1,1,0));
    Reference<Gaussian> z1(newGaussian2D(1,1,1,1,0));
    verifyLambda(z0,z1,Likelihood((-0.5 - ::std::log(4*PI))));
  }

  {
    Reference<Gaussian> z0(newGaussian2D(0,0,1,1,0));
    Reference<Gaussian> z1(newGaussian2D(0,0,1,1,0));
    verifyLambda(z0,z1,Likelihood(::std::log(1.0/(4*PI))));
  }

  {
    Reference<Gaussian> z0(newGaussian2D(0,0,1,1,0));
    Reference<Gaussian> z1(newGaussian2D(10,10,1,1,0));
    verifyLambda(z0,z1,Likelihood((-50 - ::std::log(4*PI))));
  }

  {
    Reference<Gaussian> z0(newGaussian2D(0,0,1.0/1024,1,0));
    Reference<Gaussian> z1(newGaussian2D(0,1,1.0/1024,1,0));

    // this one will actually result in a positive value (before negating) !!!
    verifyLambda(z0,z1,Likelihood((-0.25 - 0.5*::std::log((2*PI)*(2*PI) * 1/256.0))));
    assert(z0->likelihood(z1,transform).value()>0);
  }

  {
    Reference<Gaussian> z0(newGaussian2D(0,0,1.0/1024,1,0));
    Reference<Gaussian> z1(newGaussian2D(0,0,1.0/1024,1,0));

    // this one will actually result in a positive value !!!
    verifyLambda(z0,z1,Likelihood(( - 0.5*::std::log((2*PI)*(2*PI) * 1/256.0))));
    assert(z0->likelihood(z1,transform).value()>0);
  }
}


void utest_findMeasurement()
{
  Pointer<Gaussian> prior,post,z,checkPost;
  {
    prior = newGaussian3D(0,0,0,2,2,2);
    post = newGaussian3D(10,10,10,1,1,1);
    z = Gaussian::findMeasurement(prior,post);
    
    checkPost = prior->doKalmanUpdate(z);

    ::std::cout << "z="; z->print(::std::cout);
    ::std::cout << "post="; post->print(::std::cout);
    ::std::cout << "check="; checkPost->print(::std::cout);

    verifyEstimates(*post,*checkPost);
  }

  {
    prior = newGaussian3D(0,0,0,1,1,1);
    post = newGaussian3D(1,1,1,1,1,1);
    z = Gaussian::findMeasurement(prior,post);
    assert(!z);
  }

  {
    prior = newGaussian3D(0,0,0,1,1,1);
    post = newGaussian3D(1,1,1,2,2,2);
    z = Gaussian::findMeasurement(prior,post);
    assert(!z);
  }
}

