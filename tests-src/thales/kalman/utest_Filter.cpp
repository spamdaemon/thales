#include <iostream>
#include <thales/kalman/FilterUpdate.h>
#include <thales/kalman/Filter.h>
#include <thales/kalman/Prediction.h>
#include <thales/kalman/ProcessModel.h>
#include <thales/kalman/CoordinateFrame.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/model/ConstantVelocityModel2D.h>

using namespace timber;
using namespace newton;
using namespace thales;
using namespace thales::kalman;
using namespace thales::kalman::frame;
using namespace thales::kalman::model;

void utest_test1()
{
  Reference<ProcessModel> model = ProcessModel::createStaticModel(Cartesian_2D_Inertial_P::create());
  Vector<Double> mean(2.0,2.0);
  Matrix<Double> cvar(2);
  cvar(0,0) = cvar(1,1) = 2;
  
  Reference<Gaussian> initial(Gaussian::create(Cartesian_2D_Inertial_P::create(),mean,cvar));
  Reference<Gaussian> z0 = initial;

  Reference<Filter> filter0 = Filter::createFilter(0,initial,model,z0);
  Reference<Prediction> prediction = filter0->predict(1);

  mean = Vector<Double>(3.0,3.0);
  cvar = Matrix<Double>(2);
  cvar(0,0) = cvar(1,1) = 2;
  Reference<Gaussian> z1(Gaussian::create(Cartesian_2D_Inertial_P::create(),mean,cvar));

  Reference<Filter> filter1 = prediction->update(z1);
  assert(filter1->time()==1);
  assert(filter0->time()==0);

  ::std::cerr << "State : " << filter1->state()->mean() << ::std::endl;

  assert((filter1->state()->mean() - Vector<Double>(2.5,2.5)).abs().max()<0.0001);
}

void utest_predictConstantVelocity()
{
  Reference<ProcessModel> model = new ConstantVelocityModel2D(1);
  Vector<Double> mean(0.0,0.0,1,0);
  
  Matrix<Double> cvar = Matrix<Double>::identity(mean.dimension());
  Reference<Gaussian> initial(Gaussian::create(model->coordinateFrame(),mean,cvar));
  Reference<Filter> f0 = Filter::createFilter(0,initial,model);
  
  Reference<Filter> p1 = f0->predict(1);
  
  Vector<Double> zMean(2.0,0.0);
  Matrix<Double> zVar = Matrix<Double>::identity(2);
  Reference<Gaussian> z(Gaussian::create(Cartesian_2D_Inertial_P::create(),zMean,zVar));

  Reference<FilterUpdate> preparedUpdate = p1->prepareUpdate(z);

  ::std::cerr << "Likelihood : " << preparedUpdate->likelihood().value() << ::std::endl
	      << "Distance   : " << ::std::sqrt(preparedUpdate->mahalanobisDistance2()) << ::std::endl;
  double d1 = ::std::sqrt(preparedUpdate->mahalanobisDistance2());
			  
  assert(d1==d1 && "Mahalanobis distance is NaN");
  
  Reference<Filter> f1 = preparedUpdate->postUpdateFilter();
  
  ::std::cerr << "Measurement: " << z->mean() << ::std::endl
	      << "Estimate   : " << f1->state()->mean() << ::std::endl;
}
