#include <iostream>
#include <iomanip>
#include <vector>
#include <cstdlib>

#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <newton/newton.h>
#include <newton/Matrix.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Polar_2D_P.h>
#include <thales/kalman/frame/Polar_2D_PV.h>

#include <thales/kalman/UKFTransform.h>

#include <canopy/time/Time.h>


using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales;
using namespace ::newton;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::unscented;

#ifdef NDEBUG 
static const size_t MAX_ITERATIONS = 50000;
#else
static const size_t MAX_ITERATIONS = 10000;
#endif

static void checkDelta (const Gaussian& x1, const Gaussian& x2, double dMean=1.0/(1<<24), double dCov=1.0/(1<<24))
{
  double deltaMean =  x1.frame()->difference(x1.mean(),x2.mean()).abs().max();
  double deltaCov  =  (x1.covariance().matrix() - x2.covariance().matrix()).abs().max();

  if (deltaMean>dMean) {
    cerr << "Mean Delta " << deltaMean << ", expected " << dMean << endl;
    assert(deltaMean < dMean);
    abort();
  }

  if (deltaCov>dCov) {
    cerr << "Covariance Delta " << deltaCov  << ", expected " << dCov << endl;
    assert(deltaCov < dCov);
    abort();
  }
  cerr << "delta : " << deltaMean << ", " << deltaCov << endl;
}

static Reference<Gaussian> doKalmanUpdate(const Gaussian& z, const Gaussian& ge, Reference<GaussianTransform> tx)
{
  // do a standard kalman update, un-optimized
  const Matrix<double> I = Matrix<double>::identity(ge.dimension());
  Matrix<double> P_,HP_Ht,HP_;
  Vector<double> x_;

  P_ = ge.covariance().matrix();
  if (!tx->apply(*ge.frame(),ge.mean(),P_,
		 *z.frame(),x_,&HP_Ht,&HP_)) {
    
    x_= ge.mean();
    HP_ = P_;
    HP_Ht = P_;
  }

  // the gain computation
  Matrix<double> Innov = HP_Ht  + z.covariance().matrix();
  Covariance::makeSymmetric(Innov);
  const Matrix<double> K = HP_.transpose()*Innov.inverse();

  {
    assert(Innov.isPositiveDefinite());
    ::newton::Cholesky<double> ch(Innov);
  }
  Matrix<double> KHP = K*HP_;
  Matrix<double> P = P_ - KHP;
  Covariance::makeSymmetric(P);
  ::newton::Cholesky<double> ch(P);

  Vector<double> x = ge.mean() + K* z.frame()->difference(z.mean(),x_);
  
  return Gaussian::create(ge.frame(),x,P);
}

static Reference<Gaussian> doKalmanUpdate(const Gaussian& z, const Gaussian& ge)
{
  return doKalmanUpdate(z,ge,GaussianTransform::find(*ge.frame(),*z.frame()));
}

static Reference<Gaussian> newGaussian2D(double x, double y, double xx, double yy, double xy=0.0)
{
  Vector<double> mean(x,y);
  Matrix<double> cvar(mean.dimension());
  cvar(0,0) = xx;
  cvar(1,1) = yy;
  cvar(0,1) = cvar(1,0) = xy;

  Reference<Gaussian> res(Gaussian::create(Cartesian_2D_Inertial_P::create(),mean,cvar));
  mean = res->randomSample();
  return Gaussian::create(res->frame(),mean,res->covariance());
}

static Reference<Gaussian> newGaussian4D(double x, double y, double vx, double vy, double xx, double yy, double vxvx, double vyvy, double xy=0.0, double vxvy=0)
{
  Vector<double> mean(x,y,vx,vy);
  Matrix<double> cvar(mean.dimension());
  cvar(0,0) = xx;
  cvar(1,1) = yy;
  cvar(2,2) = vxvx;
  cvar(3,3) = vyvy;
  cvar(0,1) = cvar(1,0) = xy;
  cvar(2,3) = cvar(3,2) = vxvy;

  Reference<Gaussian> res(Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean,cvar));
  mean = res->randomSample();
  return Gaussian::create(res->frame(),mean,res->covariance());
}

void utest_1a()
{
  ::std::cerr << "Measure the update with the innovation (4D -> 2D)" << endl;
  vector<Reference<PreparedUpdate> > U;
  vector<Reference<Gaussian> > CHECK;
  
  for (size_t i=0;i<MAX_ITERATIONS;++i) {
    Reference<Gaussian> z (newGaussian2D(10*i,10*i,1,1));
    Reference<Gaussian> p (newGaussian4D(10*i+1,10*i+1,-1,1,		 1,1,.125,.125));
    U.push_back(p->prepareKalmanUpdate(z));
    CHECK.push_back(doKalmanUpdate(*z,*p));
  }
  
  ::canopy::time::Time start = ::canopy::time::Time::now();
  for (size_t i=0;i<U.size();++i) {
    Reference<Gaussian> ge=U[i]->postUpdate();
    checkDelta(*ge,*CHECK[i]);
  }
  ::canopy::time::Time end =::canopy::time::Time::now();
  UInt64 elapsedTimeUS = (end.time()-start.time()).count() / 1000;
  UInt64 elapsedTimeMS = elapsedTimeUS / 1000;
  ::std::cerr << "Total time           " << elapsedTimeMS << " ms" << endl
	      << "Time per calculation " << elapsedTimeUS/U.size() << " us/calculation" << endl;
  if (elapsedTimeMS>0) {
    ::std::cerr  << "Calculation Rate     " << U.size() / elapsedTimeMS << " calculation/ms" << endl;
  }
}

void utest_1b()
{
  ::std::cerr << "Measure the update with the innovation (4D -> 4D)" << endl;
  vector<Reference<PreparedUpdate> > U;
  vector<Reference<Gaussian> > CHECK;
  
  for (size_t i=0;i<MAX_ITERATIONS;++i) {
    Reference<Gaussian> z (newGaussian4D(10*i,10*i,-1,1,		 1,1,.125,.125));
    Reference<Gaussian> p (newGaussian4D(10*i+1,10*i+1,-1,1,		 1,1,.125,.125));
    U.push_back(p->prepareKalmanUpdate(z));
    CHECK.push_back(doKalmanUpdate(*z,*p));
  }
  
  ::canopy::time::Time start= ::canopy::time::Time::now();
  for (size_t i=0;i<U.size();++i) {
    Reference<Gaussian> ge=U[i]->postUpdate();
    checkDelta(*ge,*CHECK[i]);
  }
  ::canopy::time::Time end= ::canopy::time::Time::now();
  UInt64 elapsedTimeUS = (end.time()-start.time()).count() / 1000;
  UInt64 elapsedTimeMS = elapsedTimeUS / 1000;
  ::std::cerr << "Total time           " << elapsedTimeMS << " ms" << endl
	      << "Time per calculation " << elapsedTimeUS/U.size() << " us/calculation" << endl;
  if (elapsedTimeMS>0) {
    ::std::cerr  << "Calculation Rate     " << U.size() / elapsedTimeMS << " calculation/ms" << endl;
  }
}

void utest_2a()
{
  ::std::cerr << "Measure the Mahalanobis distance calculation (4D -> 2D)" << endl;
  vector<Reference<Gaussian> > Z;
  vector<Reference<Gaussian> > P;

  Pointer<GaussianTransform> H;
  
  for (size_t i=0;i<MAX_ITERATIONS;++i) {
    Reference<Gaussian> z (newGaussian2D(10*i,10*i,1,1));
    Reference<Gaussian> p (newGaussian4D(10*i+1,10*i+1,-1,1,		 1,1,.125,.125));
    Z.push_back(z);
    P.push_back(p);
    if (!H) {
      H = GaussianTransform::find(*P.back()->frame(),*Z.back()->frame());
    }
  }
  
  ::canopy::time::Time start= ::canopy::time::Time::now();
  double avgMHD = 0;
  for (size_t i =0;i<Z.size();++i) {
    avgMHD += P[i]->prepareKalmanUpdate(Z[i],H) ->mahalanobisDistance2();
  }
  
  ::canopy::time::Time end =::canopy::time::Time::now();
  UInt64 elapsedTimeUS = (end.time()-start.time()).count() / 1000;
  UInt64 elapsedTimeMS = elapsedTimeUS / 1000;
  ::std::cerr << "Total time           " << elapsedTimeMS << " ms" << endl
	      << "Time per calculation " << elapsedTimeUS/Z.size() << " us/calculation" << endl;
  if (elapsedTimeMS>0) {
    ::std::cerr  << "Calculation Rate     " << Z.size() / elapsedTimeMS << " calculation/ms" << endl;
  }
  ::std::cerr << "Average MHD2         " << avgMHD/Z.size() << endl
      ;
  
}

void utest_2b()
{
  ::std::cerr << "Measure the Mahalanobis distance calculation (4D -> 4D)" << endl;
  vector<Reference<Gaussian> > Z;
  vector<Reference<Gaussian> > P;

  Pointer<GaussianTransform> H;
  
  for (size_t i=0;i<MAX_ITERATIONS;++i) {
    Reference<Gaussian> z (newGaussian4D(10*i,10*i,-1,1,		 1,1,.125,.125));
    Reference<Gaussian> p (newGaussian4D(10*i+1,10*i+1,-1,1,		 1,1,.125,.125));
    Z.push_back(z);
    P.push_back(p);
    if (!H) {
      H = GaussianTransform::find(*P.back()->frame(),*Z.back()->frame());
    }
  }
  
  ::canopy::time::Time start= ::canopy::time::Time::now();
  double avgMHD = 0;
  for (size_t i =0;i<Z.size();++i) {
    avgMHD += P[i]->prepareKalmanUpdate(Z[i],H) ->mahalanobisDistance2();
  }
   
  ::canopy::time::Time end =::canopy::time::Time::now();
  UInt64 elapsedTimeUS = (end.time()-start.time()).count() / 1000;
  UInt64 elapsedTimeMS = elapsedTimeUS / 1000;
  ::std::cerr << "Total time           " << elapsedTimeMS << " ms" << endl
	      << "Time per calculation " << elapsedTimeUS/Z.size() << " us/calculation" << endl;
  if (elapsedTimeMS>0) {
    ::std::cerr  << "Calculation Rate     " << Z.size() / elapsedTimeMS << " calculation/ms" << endl;
  }
  ::std::cerr << "Average MHD2         " << avgMHD/Z.size() << endl;
  
}

void utest_3()
{
  Reference<CoordinateFrame> kFrame(Cartesian_2D_Inertial_PV::create());
  Reference<CoordinateFrame> zFrame(Polar_2D_P::create(0,0));

  Vector<> k(4),z(2);
  Matrix<> K(4),Z(2);

  k(0) = -709.518 ;
  k(1) = 5194.81 ;
  k(2) = 20.154 ;
  k(3) =  -7.2801;

  z(0) = 5.6985 ;
  z(1) = 8029.8;

  K(0,0)  = 1.3364e+07;
  K(0,1)  =       405457  ;
  K(0,2)  = 93888.3 ;
  K(0,3)  = 1883.15;
  K(1,1)  = 1.32076e+07 ;
  K(1,2)  =     1884.51 ;
  K(1,3)  = 93184.3;
  K(2,2)  = 871.886;
  K(2,3)  =  8.9411;
  K(3,3)  = 868.642;

  for (int i=0;i<4;++i) {
    for (int j=i+1;j<4;++j) {
      K(j,i) = K(i,j);
    }
  }

  Z(0,0) = 0.000304618;
  Z(1,1) = 100;

  Reference<Gaussian> gK(Gaussian::create(kFrame,::std::move(k),::std::move(K)));
  Reference<Gaussian> gZ(Gaussian::create(zFrame,::std::move(z),::std::move(Z)));

  // do the same, but  use the unscented filter
  Reference<UKFTransform> ukf = UKFTransform::create(gK->frame(),GaussianTransform::find(*kFrame,*zFrame));
  try {
    Reference<Gaussian> post = doKalmanUpdate(*gZ,*gK,ukf);
    ::std::cerr << "UNSCENTED (UNOPTIMIZED) :" << std::endl
		<< "PRE:  " << *gK << ::std::endl
		<< " <>   " << *(gK->transform(zFrame,*ukf)) << ::std::endl
		<< "Z  :  " << *gZ << ::std::endl
		<< "POST: " << *post << ::std::endl;
    ::std::cerr 
		<< " <> " << *(post->transform(zFrame,*ukf)) << ::std::endl
	;
    
    assert(!"Expected to succeed");
  }
  catch (...) {
    // this will fail at the moment.
  }

  try {
    Reference<Gaussian> post = gK->doKalmanUpdate(gZ,*ukf);
    ::std::cerr << "UNSCENTED :" << std::endl
		<< "PRE:  " << *gK << ::std::endl
		<< " <>   " << *(gK->transform(zFrame,*ukf)) << ::std::endl
		<< "Z  :  " << *gZ << ::std::endl
		<< "POST: " << *post << ::std::endl;
    ::std::cerr 
		<< " <> " << *(post->transform(zFrame,*ukf)) << ::std::endl
	;
    
    
    assert(!"Expected to succeed");
  }
  catch (...) {
    // this will fail at the moment
  }

  {
    Reference<Gaussian> post = gK->doKalmanUpdate(gZ);
    ::std::cerr << "EKF" << ::std::endl
		<< "PRE:  " << *gK << ::std::endl
		<< " <>   " << *(gK->transform(zFrame,*ukf)) << ::std::endl
		<< "Z  :  " << *gZ << ::std::endl
		<< "POST: " << *post << ::std::endl;
    ::std::cerr 
		<< " <> " << *(post->transform(zFrame)) << ::std::endl
	;
  }

}
