#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <thales/kalman/CoordinateFrame.h>
#include <thales/kalman/GaussianTransform.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Polar_2D_P.h>
#include <thales/kalman/frame/Polar_2D_PV.h>
#include <thales/kalman/frame/Rdot_2D.h>
#include <thales/kalman/frame/Cartesian_2D_Relative_PRdot.h>


#include <newton/newton.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::newton;

void utest_supportedConversions()
{
  Pointer<CoordinateFrame> F1[] = { 
    Cartesian_2D_Inertial_P::create(),
    Cartesian_2D_Inertial_PV::create(),
    Polar_2D_P::create(0,0),
    Polar_2D_PV::create(0,0),
    Rdot_2D::create(0,0),
    Cartesian_2D_Relative_PRdot::create(0,0),
    Pointer<CoordinateFrame>()
  };
  
  Pointer<CoordinateFrame> F2[] = { 
    Cartesian_2D_Inertial_P::create(),
    Cartesian_2D_Inertial_PV::create(),
    Polar_2D_P::create(0,0),
    Polar_2D_PV::create(0,0),
    Rdot_2D::create(0,0),
    Cartesian_2D_Relative_PRdot::create(0,0),
     Pointer<CoordinateFrame>()
 };
  
  for(size_t i=0;F1[i];++i) {
    assert(Pointer<GaussianTransform>(GaussianTransform::find(*F1[i],*F2[i])));
  }
  
  // Cartesian_2D_Inertial_PV can be converted to any of the existing frames
  for (size_t i=0;F1[i];++i) {
    assert(Pointer<GaussianTransform>(GaussianTransform::find(*Cartesian_2D_Inertial_PV::create(),*F1[i])));
  }
}

void utest_invalidConversions()
{
  Pointer<CoordinateFrame> F1[] = { 
    Polar_2D_P::create(0,0),
    Polar_2D_PV::create(0,0),
    Rdot_2D::create(0,0),
    Cartesian_2D_Relative_PRdot::create(0,0),
    Pointer<CoordinateFrame>()
  };
  
  Pointer<CoordinateFrame> F2[] = { 
    Polar_2D_P::create(10,10),
    Polar_2D_PV::create(10,10),
    Rdot_2D::create(10,10),
    Cartesian_2D_Relative_PRdot::create(10,10),
     Pointer<CoordinateFrame>()
  };
  
  for(size_t i=0;F1[i];++i) {
    for(size_t j=0;F2[j];++j) {
      try {
	GaussianTransform::find(*F1[i],*F2[j]);
	assert(false);
      }
      catch (...) {
      }
    }
  }
  
}


