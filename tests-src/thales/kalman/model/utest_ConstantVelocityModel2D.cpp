#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/model/ConstantVelocityModel2D.h>
#include <thales/kalman/model/ConstantCartesianVelocityModel.h>
#include <newton/newton.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::model;
using namespace ::newton;

static double getDelta (const Gaussian& g, const Gaussian& h)
{
  double d1 = (g.mean()-h.mean()).abs().max();
  double d2 = (g.covariance().matrix()-h.covariance().matrix()).abs().max();

  return ::std::max(d1,d2);
}

static Reference<Gaussian> predict (const Reference<Gaussian>& state, const Time& longDT, double ax = 1, double ay=1)
{
  ax = ax*ax;
  ay = ay*ay;
  if (longDT==0) {
    return state;
  }
  
  const double dt = ::std::abs(longDT);
  
  Vector<double> v = state->mean();
  v(0) += v(2)*dt;
  v(1) += v(3)*dt;
  
  Matrix<double> A = Matrix<double>::identity(4);
  A(0,2) = A(1,3) = dt;
  
  Matrix<double> P = state->covariance().matrix().multiplyTranspose(A);
  P = A*P;
  
  // add some process noise to the mix
  // the process noise matrix is briefly described in 
  // "Performance Modeling for Multisensor Data Fusion"
  // by  Chang, Song, and Liggins
  double dt2 = dt*dt;
  double dt3 = dt*dt2;
  dt2 /= 2.0;
  dt3 /= 3.0;
  
  P(0,0) += dt3*ax;
  P(2,0) = (P(0,2) += dt2*ax);
  P(1,1) += dt3*ay;
  P(3,1) = (P(1,3) += dt2*ay);
  P(2,2) += dt*ax;
  P(3,3) += dt*ay;
  
  return Gaussian::create(state->frame(),v,P);
}


static Reference<Gaussian> newGaussian2D(double x, double y, double vx, double vy, double xx, double yy, double xy, double vxx, double vyy, double vyx)
{
  Vector<> mean(x,y,vx,vy);
  Matrix<> cvar(mean.dimension());
  cvar(0,0) = xx;
  cvar(1,1) = yy;
  cvar(0,1) = cvar(1,0) = xy;
  cvar(2,2) = vxx;
  cvar(3,3) = vyy;
  cvar(3,2) = cvar(2,3) = vyx;

  return Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean,cvar);
}

void utest_1()
{
  const ::newton::Vector<double> a(1.0,1.0);

  Reference<ConstantVelocityModel2D> m = new ConstantVelocityModel2D(1);
  Reference<ConstantCartesianVelocityModel> mc = new ConstantCartesianVelocityModel(a);
  Reference<Gaussian> z = newGaussian2D(0,0,1,0,1,1,0,1,1,0);

  Reference<Gaussian> z1 = z;
  Reference<Gaussian> z2 = z;
  for (int i=1;i<=10;++i) {

    Reference<Gaussian> check1 = mc->predict(z1,1);
    Reference<Gaussian> check2 = mc->predict(z,i);
    
    z1 = m->predict(z1,1);
    z2 = m->predict(z,i);
    ::std::cerr << "Incremental : " << *z1 << ::std::endl;
    ::std::cerr << "1-stop      : " << *z2 << ::std::endl;

    assert(getDelta(*z1,*check1) < .0001);
    assert(getDelta(*z2,*check2) < .0001);
    // doing incremental predictions results in underestimating the location error
    assert(getDelta(*z1,*z2) < .001);
  }
  assert(z1->get(0)==10);
  assert(z1->get(1)==0);
  assert(z1->get(2)==1);
  assert(z1->get(3)==0);



}

void utest_2()
{
  Reference<ConstantVelocityModel2D> m = new ConstantVelocityModel2D(1);
  Reference<Gaussian> z = newGaussian2D(0,0,1,.1,1,1,.1,1,1,.2);
  Matrix<> P = z->covariance().matrix();
  for (int i=1;i<=10;++i) {
    Reference<Gaussian> z1 = predict(z,i,1,1);
    Reference<Gaussian> z2 = m->predict(z,i);
    ::std::cerr << "Unoptimized : " << *z1 << ::std::endl;
    ::std::cerr << "Optimized   : " << *z2 << ::std::endl;
    
    assert(getDelta(*z1,*z2) < .001);
  }
}
