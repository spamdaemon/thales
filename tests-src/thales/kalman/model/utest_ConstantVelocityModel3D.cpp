#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/frame/Cartesian_3D_Inertial_PV.h>
#include <thales/kalman/model/ConstantVelocityModel3D.h>
#include <newton/newton.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;
using namespace ::thales::kalman::model;
using namespace ::newton;

static Reference<Gaussian> newGaussian3D(double x, double y, double z, 
					 double vx, double vy, double vz, 
					 double xx, double yy, double zz, 
					 double vxx,double vyy,double vzz,
					 double xy=0, double xz=0, double yz=0,
					 double vxy=0,double vxz=0, double vyz=0)
{
  Vector<> mean(6);
  mean(0) = x;
  mean(1) = y;
  mean(2) = z;
  mean(3) = vx;
  mean(4) = vy;
  mean(5) = vz;

  Matrix<> cvar(mean.dimension());
  cvar(0,0) = xx;
  cvar(1,1) = yy;
  cvar(2,2) = zz;
  cvar(3,3) = vxx;
  cvar(4,4) = vyy;
  cvar(5,5) = vzz;

  cvar(0,1) = xy;
  cvar(0,2) = xz;
  cvar(1,2) = yz;
  cvar(3,4) = vxy;
  cvar(3,5) = vxz;
  cvar(4,5) = vyz;

  Covariance::makeSymmetric(cvar);
  
  return Gaussian::create(Cartesian_3D_Inertial_PV::create(),::std::move(mean),::std::move(cvar));
}

void utest_1()
{
  Reference<ConstantVelocityModel3D> m = new ConstantVelocityModel3D(1);
  Reference<Gaussian> z = newGaussian3D(0,0,0,1,1,1,
					10,20,30,40,50,60,
					1,2,3,4,5,6);

  Reference<Gaussian> ge = m->predict(z,1);

  ge->print(::std::cerr);
}

