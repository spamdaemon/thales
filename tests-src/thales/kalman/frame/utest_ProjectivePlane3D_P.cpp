#include <thales/kalman/frame/ProjectivePlane3D_P.h>
#include <iostream>

using namespace ::std;
using namespace ::thales::kalman::frame;
using namespace ::newton;

void utest_simple1()
{
  const Vector<> fp(1,1,0);
  const Vector<> n(0,0,2);
  const Vector<> x(2,0,0);

  ProjectivePlane3D_P plane(fp,1,n,x);
  ::std::cerr << "y-axis " << plane.yAxis() << ::std::endl;
  {
    Vector<> p(1,1,5);
    Vector<> q;
    plane.projectPoint(p,q);
    ::std::cerr << "Projected point " << p << " into " << q << ::std::endl;
    assert((q-Vector<>(0.,0)).abs().max() < .0001);
  }

  {
    Vector<> p(-2,-2,3);
    Vector<> q;
    plane.projectPoint(p,q);
    ::std::cerr << "Projected point " << p << " into " << q << ::std::endl;
    assert((q-Vector<>(-1.,1)).abs().max() < .0001);
  }
  

}

void utest_simple2()
{
  // these are hand-computed values
  const Vector<> fp(5,-5,0);
  const Vector<> n(0,1,0);
  const Vector<> x(1,0,0);

  ProjectivePlane3D_P plane(fp,5,n,x);
  ::std::cerr << "y-axis " << plane.yAxis() << ::std::endl;
  {
    Vector<> p(2,2,0);
    Vector<> q;
    plane.projectPoint(p,q);
    ::std::cerr << "Projected point " << p << " into " << q << ::std::endl;
    assert((q-Vector<>(-2.1428571,0.0)).abs().max() < .0001);
  }


}

void utest_simple3()
{
  // these are hand-computed values
  const Vector<> fp(5,-5,5);
  const Vector<> n(0,1,0);
  const Vector<> x(1,0,0);

  ProjectivePlane3D_P plane(fp,5,n,x);
  ::std::cerr << "y-axis " << plane.yAxis() << ::std::endl;
  {
    Vector<> p(2,2,0);
    Vector<> q;
    plane.projectPoint(p,q);
    ::std::cerr << "Projected point " << p << " into " << q << ::std::endl;
    assert((q-Vector<>(-2.1428571,-3.5714286)).abs().max() < .0001);
  }


}

void utest_complex()
{
  const Vector<> fp(-1,-1,3);
  const Vector<> n(1,1,-1);
  const Vector<> x(1,-1,0);

  ProjectivePlane3D_P plane(fp,1,n,x);
  {
    Vector<> p(0,0,0);
    Vector<> q;
    plane.projectPoint(p,q);
    ::std::cerr << "Projected point " << p << " into " << q << ::std::endl;
  }

  {
    Vector<> p(1,1,1);
    Vector<> q;
    plane.projectPoint(p,q);
    ::std::cerr << "Projected point " << p << " into " << q << ::std::endl;
  }
  

}

void utest_Camera()
{
  const Vector<> fp(-10,0,10);
  const Vector<> center(10,0,0);
  const Vector<> east(10,-10,0);
  const Vector<> west(10,10,0);
  const Vector<> up(20,0,0);
  const Vector<> down(5,0,0);
  
  ::timber::Reference<ProjectivePlane3D_P> plane = ProjectivePlane3D_P::create(10,fp,center,east);
  ::std::cerr << "x-axis " << plane->xAxis() << endl
	      << "y-axis " << plane->yAxis() << endl;
  {
    Vector<> q;
    plane->projectPoint(center,q);
    ::std::cerr << "Projected point " << center << " into " << q << ::std::endl;
    assert((q-Vector<>(0.0,0.0)).abs().max() < .0001);
  }

  {
    // projecting east, must result in a point (+,0)
    Vector<> q;
    plane->projectPoint(east,q);
    ::std::cerr << "Projected point " << east << " into " << q << ::std::endl;
    assert(q(0) > 0);
    assert(::std::abs(q(1)) < .000001);
  }
  
  {
    // projecting east, must result in a point (+,0)
    Vector<> q;
    plane->projectPoint(west,q);
    ::std::cerr << "Projected point " << west<< " into " << q << ::std::endl;
    assert(q(0) < 0);
    assert(::std::abs(q(1)) < .000001);
  }
  
  {
    // projecting up, must result in a point (0,+)
    Vector<> q;
    plane->projectPoint(up,q);
    ::std::cerr << "Projected point " << up << " into " << q << ::std::endl;
    assert(::std::abs(q(0)) < .000001);
    assert(q(1) > 0);
  }
  
  {
    // projecting up, must result in a point (0,+)
    Vector<> q;
    plane->projectPoint(down,q);
    ::std::cerr << "Projected point " << down << " into " << q << ::std::endl;
    assert(::std::abs(q(0)) < .000001);
    assert(q(1) < 0);
  }
  

}
