#include <iostream>
#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <thales/kalman/CoordinateFrame.h>
#include <thales/kalman/GaussianTransform.h>
#include <thales/kalman/UKFTransform.h>
#include <thales/kalman/MonteCarloTransform.h>
#include <thales/kalman/unscented/Transform.h>
#include <thales/kalman/frame/Cartesian_1D_Inertial_P.h>

#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <thales/kalman/frame/Polar_2D_P.h>
#include <thales/kalman/frame/Polar_2D_PV.h>
#include <thales/kalman/Gaussian.h>

#include <newton/newton.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales;
using namespace ::thales::kalman;
using namespace ::thales::kalman::unscented;
using namespace ::thales::kalman::frame;
using namespace ::newton;

struct MyTransform  {
  MyTransform(double speed, double t) throws() 
  : _delta(t),_speed(speed) {}
  ~MyTransform() throws () {}

  ::newton::Matrix<double> operator() (const CoordinateFrame& from,
				      const ::newton::Matrix<double>& x, 
				      const CoordinateFrame& to) const throws (::std::exception)
  {
    size_t n = x.rowSize();
    ::newton::Matrix<double> tmp(n,1);
    for (size_t i=0;i<n;++i) {
      tmp(i,0) =  ::std::abs(x(i,0) + _delta*_speed);
    }
    return tmp;
  }
  
private:
  double _delta;
  double _speed;
};

void utest_nonLinearConversion1D()
{
  Transform tx(1);
  Reference<CoordinateFrame> frame = Cartesian_1D_Inertial_P::create();
  for (double t = -2;t<2;t = t+.1) {
    Matrix<double> HP,yP,xP(1,1);
    Vector<double> x(1),y;
    xP(0,0) = 1;
    x(0) = 0;
    Reference<UKFTransform> ukf = UKFTransform::createUKF(tx,MyTransform(-1,t));
    
    ukf->apply(*frame,x,xP,*frame,y,&yP,&HP);
    ::std::cerr << t << ": " << y(0) << " " << sqrt(yP(0,0)) << " " << HP(0,0) << ::std::endl;
  }


}

void utest_Polar_1()
{
  Reference<CoordinateFrame> kFrame(Cartesian_2D_Inertial_P::create());
  Reference<CoordinateFrame> zFrame(Polar_2D_P::create(0,0));
  Reference<UKFTransform> txn = UKFTransform::create(kFrame,GaussianTransform::find(*kFrame,*zFrame));

  Vector<> k(2),z(2);
  Matrix<> K(2),Z(2);

  k(0) = 0;
  k(1) = -100;

  K(0,0)  = 100;
  K(0,1)  = 0;
  K(1,0)  = K(0,1);
  K(1,1)  = 100;

  Reference<Gaussian> gK(Gaussian::create(kFrame,::std::move(k),::std::move(K)));
  Reference<Gaussian> ekf = gK->transform(zFrame);
  Reference<Gaussian> ukf = gK->transform(zFrame,*txn);
  
  ::std::cerr << "EKF       : " << *ekf << ::std::endl;
  ::std::cerr << "UNSCENTED : " << *ukf << ::std::endl;

  assert(::std::abs(ekf->get(0)-ukf->get(0)) < .001);
}

void utest_Polar_2()
{
  Reference<CoordinateFrame> kFrame(Cartesian_2D_Inertial_P::create());
  Reference<CoordinateFrame> zFrame(Polar_2D_P::create(0,0));
  Reference<GaussianTransform> txn = UKFTransform::create(kFrame,GaussianTransform::find(*kFrame,*zFrame));
  Reference<GaussianTransform> tmc = MonteCarloTransform::create(GaussianTransform::find(*kFrame,*zFrame));

  
  Vector<> k(2),z(2);
  Matrix<> K(2),Z(2);

  k(0) = 0;
  k(1) = -100;

  K(0,0)  = k(1)*k(1);
  K(0,1)  = 0;
  K(1,0)  = K(0,1);
  K(1,1)  = k(1)*k(1);

  Reference<Gaussian> gK(Gaussian::create(kFrame,::std::move(k),::std::move(K)));
  Reference<Gaussian> ekf = gK->transform(zFrame);
  ::std::cerr << "EKF       : " << *ekf << ::std::endl;
  Reference<Gaussian> ukf = gK->transform(zFrame,*txn);
  ::std::cerr << "UNSCENTED : " << *ukf << ::std::endl;

  try {
    Reference<Gaussian> mc = gK->transform(zFrame,*tmc);
    ::std::cerr << "MC        : " << *mc << ::std::endl;
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception : " << e.what() << ::std::endl;
    assert(false);
  }
  //FIXME: what's the correct value???
  //assert(::std::abs(ekf->get(0)-ukf->get(0)) < .001);
}

