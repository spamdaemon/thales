#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>

#include <timber/timber.h>
#include <thales/probability/ExponentialAverage.h>

using namespace ::std;
using namespace ::timber;
using namespace ::thales;
using namespace ::thales::probability;

static void assertEquals(double a, double b, double eps = 0.001)
{
   double diff = a - b;
   assert(-eps <= diff && diff <= eps);
}

void utest_1()
{
   ExponentialAverage< double> avg(1);
   avg.update(1);
   assertEquals(avg.average(), 1);
   avg.update(3);
   assertEquals(avg.average(), 3);
}

void utest_2()
{
   ExponentialAverage< double> avg(.5);
   avg.update(3);
   assertEquals(avg.average(), 3);
   avg.update(1);
   assertEquals(avg.average(), 2);
}

void utest_3()
{
   try {
      ExponentialAverage< double> avg(0);
      assert(!"Expected exception");
   }
   catch (...) {

   }

}
