#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>

#include <timber/timber.h>
#include <thales/probability/MovingAverage.h>

using namespace ::std;
using namespace ::timber;
using namespace ::thales;
using namespace ::thales::probability;

static void assertEquals(double a, double b, double eps=0.001)
{
   double diff = a-b;
   assert(-eps <= diff && diff <=eps);
}

void utest_1()
{
   MovingAverage< double> avg(1);
   avg.update(1);
   avg.update(3);
   assert(avg.average()==3);
}
void utest_2()
{
   MovingAverage< double> avg(2);
   avg.update(100);
   avg.update(1);
   avg.update(3);
   assert(avg.average()==2);
}

