#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>

#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <newton/newton.h>
#include <thales/kalman/Covariance.h>
#include <thales/probability/NormalDistribution.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales;
using namespace ::thales::kalman;
using namespace ::thales::probability;
using namespace ::newton;

// compute the probability mass that that lies inside a rectangle
static double montecarlo_2D (double x, double y, double xx, double yy, double xy, 
			     double minx, double miny, double maxx,double maxy, size_t N=1000)
{
  double majX,majY,minX,minY,theta;
  double mx,mn;
  Covariance::decompose(xx,yy,xy,mx,mn,theta);
  
  size_t nTested =0;
  size_t nInside =0;

  for (size_t i=0;i<N;++i) {
    double r1 = NormalDistribution().drawRandom();
    double r2 = NormalDistribution().drawRandom();

    majX = ::std::cos(theta)*mx*r1;
    majY = ::std::sin(theta)*mx*r1;
    minX = ::std::sin(theta)*mn*r2;
    minY = ::std::cos(theta)*mn*r2;

    double rx = x+majX + minX;
    double ry = y+majY + minY;

    if (minx <= rx && rx <= maxx) {
      if (miny <= ry && ry <= maxy) {
	++nInside;
      }
    }
    ++nTested;
  }

  double pInside = ((double)nInside)/nTested;
  return pInside;
}

void utest_1()
{
  double p = NormalDistribution().cdf(0);
  assert (::std::abs(p-.5) < 0.00001);
}

void utest_2()
{
  double p = NormalDistribution(0,1).cdf(-1,1);
  ::std::cerr << "P=" << setw(20) << setprecision(10) << p << ::std::endl;
  assert (::std::abs(p- .6826894921371) < 0.5E-4);
  p = NormalDistribution(0,1).cdf(-2,2);
  ::std::cerr << "P=" << setw(20) << setprecision(10) << p << ::std::endl;
  assert (::std::abs(p- .9544997361036) < 0.5E-4);
  p = NormalDistribution(0,1).cdf(-3,3);
  ::std::cerr << "P=" << setw(20) << setprecision(10) << p << ::std::endl;
  assert (::std::abs(p- .9973002039367) < 0.5E-4);
  p = NormalDistribution(0,1).cdf(-4,4);
  ::std::cerr << "P=" << setw(20) << setprecision(10) << p << ::std::endl;
  assert (::std::abs(p- .9999366575163) < 0.5E-5);
  p = NormalDistribution(0,1).cdf(-5,5);
  ::std::cerr << "P=" << setw(20) << setprecision(10) << p << ::std::endl;
  assert (::std::abs(p- .9999994266969) < 0.5E-7);
  p = NormalDistribution(0,1).cdf(-6,6);
  ::std::cerr << "P=" << setw(20) << setprecision(10) << p << ::std::endl;
  assert (::std::abs(p- .9999999980268) < 0.5E-9);
  p = NormalDistribution(0,1).cdf(-7,7);
  ::std::cerr << "P=" << setw(20) << setprecision(10) << p << ::std::endl;
  assert (::std::abs(p- .9999999999974) < 0.5E-12);
}

void utest_3()
{
  // a simple distribution examples
  for (double len=1;len<5;++len) {
    double minx = -len;
    double miny = -len;
    double maxx = len;
    double maxy = len;

    double p;
    ::std::cerr << "side length " << len << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,10);
    ::std::cerr << "Probability (10) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,50);
    ::std::cerr << "Probability (50) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,100);
    ::std::cerr << "Probability (100) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,1000);
    ::std::cerr << "Probability (1000) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,10000);
    ::std::cerr << "Probability (10000) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,100000);
    ::std::cerr << "Probability (100000) " << setw(10) << setprecision(8) << p << ::std::endl;
  }
}

void utest_4()
{
  // a simple distribution examples
  for (double len=1;len<5;++len) {
    double minx = 0;
    double miny = -100*len;
    double maxx = len;
    double maxy = 100*len;

    double p;
    ::std::cerr << "side length " << len << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,10);
    ::std::cerr << "Probability (10) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,50);
    ::std::cerr << "Probability (50) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,100);
    ::std::cerr << "Probability (100) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,1000);
    ::std::cerr << "Probability (1000) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,10000);
    ::std::cerr << "Probability (10000) " << setw(10) << setprecision(8) << p << ::std::endl;
  }

  // a simple distribution examples
  for (double len=100;len<101;++len) {
    double minx = 0;
    double miny = 0;
    double maxx = len;
    double maxy = len;

    double p;
    ::std::cerr << "side length " << len << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,10);
    ::std::cerr << "Probability (10) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,50);
    ::std::cerr << "Probability (50) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,100);
    ::std::cerr << "Probability (100) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,1000);
    ::std::cerr << "Probability (1000) " << setw(10) << setprecision(8) << p << ::std::endl;
    p = montecarlo_2D(0,0,1,1,0,minx,miny,maxx,maxy,10000);
    ::std::cerr << "Probability (10000) " << setw(10) << setprecision(8) << p << ::std::endl;
  }

}


