#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>

#include <timber/timber.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <newton/newton.h>
#include <thales/kalman/Covariance.h>
#include <thales/probability/NormalDistribution.h>
#include <thales/probability/BivariateNormal.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::thales;
using namespace ::thales::kalman;
using namespace ::thales::probability;
using namespace ::newton;

void utest_covariance()
{
  {
    BivariateNormal bvn = BivariateNormal::decomposeCovariance(4,16,0);
    assert(abs(bvn.varianceX()-4) < .00001);
    assert(abs(bvn.varianceY()-16) < .00001);
    assert(abs(bvn.correlation()) < .0000001);
  }

  {
    double xx = 4;
    double yy = 25;
    double xy = 5;
    const double rho = 0.5;
    BivariateNormal bvn = BivariateNormal::decomposeCovariance(xx,yy,xy);
    assert(bvn.correlation()==rho);
    
    double mx,mn,theta;
    Covariance::decompose(xx,yy,xy,mx,mn,theta);
    
    bvn = BivariateNormal(0,0,mx,mn,rho);

    Covariance cv = Covariance::createCovariance2D(xx,yy,xy);
    assert (cv.correlationCoefficient(0,1)==cv.correlationCoefficient(1,0));

    // make sure BivariateNormal and Covariance agree in how they compute the correlation
    assert (cv.correlationCoefficient(0,1)==rho);

    double cxx,cxy,cyy;
    bvn.createCovariance(cxx,cyy,cxy);
    double theta2;
    Covariance::decompose(cxx,cyy,cxy,mx,mn,theta2);
    
    // at the moment, I don't know of a good relationship between these
    // theta and theta2
  }
    
}

void utest_bivariateCDF()
{
  {
    double P;
    P = BivariateNormal(-1).cdf(0,0);
    ::std::cerr << "Probability " << P << ::std::endl;
    P = BivariateNormal(1).cdf(0,0);
    ::std::cerr << "Probability " << P << ::std::endl;
    P = BivariateNormal(0).cdf(0,0);
    ::std::cerr << "Probability " << P << ::std::endl;
    P = BivariateNormal(-.1).cdf(-1,0);
    ::std::cerr << "Probability " << P << ::std::endl;
    P = BivariateNormal(.7).cdf(.2,1);
    ::std::cerr << "Probability " << P << ::std::endl;
    P = BivariateNormal(-.7071).cdf(-sqrt(2),1);
    ::std::cerr << "Probability " << P << ::std::endl << ::std::endl;
  }

  {
    double P[4];
    P[0]= BivariateNormal(-.76).cdf(-1.31,0);
    ::std::cerr << "Probability " << P[0] << ::std::endl;
    P[1] = BivariateNormal(-.76).cdf(0,0);
    ::std::cerr << "Probability " << P[1] << ::std::endl;
    P[2] = BivariateNormal(-.14).cdf(-1.31,0);
    ::std::cerr << "Probability " << P[2] << ::std::endl;
    P[3] = BivariateNormal(-.14).cdf(0,0);
    ::std::cerr << "Probability " << P[3] << ::std::endl;
    ::std::cerr << "SUM : " << P[0] - P[1] - P[2] + P[3] 
		<< ::std::endl << ::std::endl;
  }

  {
    double P[4];
    P[0] = BivariateNormal(-.99).cdf(-.14,0);
    ::std::cerr << "Probability " << P[0] << ::std::endl;
    P[1] = BivariateNormal(-.99).cdf(0,0);
    ::std::cerr << "Probability " << P[1] << ::std::endl;
    P[2] = BivariateNormal(-1).cdf(-.14,0);
    ::std::cerr << "Probability " << P[2] << ::std::endl;
    P[3] = BivariateNormal(-1).cdf(0,0);
    ::std::cerr << "Probability " << P[3] << ::std::endl;
    ::std::cerr << "SUM : " << P[0] - P[1] + P[2] - P[3] 
		<< ::std::endl << ::std::endl;
  }

  {
    double P[4];
    P[0] = BivariateNormal(-.97).cdf(-.48,0);
    ::std::cerr << "Probability " << P[0] << ::std::endl;
    P[1] = BivariateNormal(-.97).cdf(0,0);
    ::std::cerr << "Probability " << P[1] << ::std::endl;
    P[2] = BivariateNormal(.96).cdf(-.48,0);
    ::std::cerr << "Probability " << P[2] << ::std::endl;
    P[3] = BivariateNormal(.96).cdf(0,0);
    ::std::cerr << "Probability " << P[3] << ::std::endl;
    ::std::cerr << "SUM : " << P[0] - P[1] + P[2] - P[3] 
		<< ::std::endl << ::std::endl;
  }

}

void utest_bivariateCDFOverUnitSquare()
{
  double V[] = { 1,1 ,-1,1 ,-1,-1 ,1,-1 };
  double P = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(),4,V);
  ::std::cerr << "Unit Square Probability : " << P << ::std::endl;
  double cdf_x = NormalDistribution().cdf(1) - NormalDistribution().cdf(-1);
  double cdf_y = NormalDistribution().cdf(1) - NormalDistribution().cdf(-1);
  
  double Pexpected = cdf_x * cdf_y;

  ::std::cerr << "Expected " << Pexpected << ::std::endl;
  ::std::cerr << "Error " << abs(Pexpected - P) << ::std::endl;
  assert(abs(Pexpected - P) < .001);
}

void utest_bivariateCDFOverOffsetSquare()
{
  double V[] = { 3,3 ,1,3 ,1,1 ,3,1 };
  double P = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(),4,V);
  ::std::cerr << "Offset Square Probability : " << P << ::std::endl;
  double cdf_x = NormalDistribution().cdf(3) - NormalDistribution().cdf(1);
  double cdf_y = NormalDistribution().cdf(3) - NormalDistribution().cdf(1);
  
  double Pexpected = cdf_x * cdf_y;
  ::std::cerr << "Expected " << Pexpected << ::std::endl;
  ::std::cerr << "Error " << abs(Pexpected - P) << ::std::endl;
  assert(abs(Pexpected - P) < .001);
}

void utest_signedTriangleCDF()
{
  double p = signedTriangleCDF(3,0,3,1,BivariateNormal::CDF());
  double pExpected = ::std::atan(1.0/3.0)/(2*M_PI);
  
  ::std::cerr << "Actual " << p << ::std::endl;
  ::std::cerr << "Expected " << pExpected << ::std::endl;
  ::std::cerr << "Error " << abs(pExpected - p) << ::std::endl;
  
  assert(abs(pExpected - p) < .001);
  
}

void utest_bivariateCDFTriangle()
{
  {
    double right = 3;
    double top = 1;
    double V[] = { right,top,0,0,right,0 };
    double P  = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(),3,V);
    ::std::cerr << "Triangle Probability : " << P << ::std::endl;
    // according to Abramovitz/Stegun
    double a = top/right;
    double Pexpected = ::std::atan(a)/(2*M_PI);
    ::std::cerr << "Expected " << Pexpected << ::std::endl;
    ::std::cerr << "Error " << abs(Pexpected - P) << ::std::endl;
    
    assert(abs(Pexpected - P) < .001);
  }

  {
    double V[] = { 0,0,1,-1,1,1 };
    double P  = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(),3,V);
    ::std::cerr << "Triangle Probability : " << P << ::std::endl;
    double cdf = NormalDistribution().cdf(1);
    double Pexpected = 0.25*(2*cdf - 1)*(2*cdf-1);
    ::std::cerr << "Error " << abs(Pexpected - P) << ::std::endl;
    
    assert(abs(Pexpected - P) < .001);
  }

  {
    double P1,P2,P3;
    {
      double V[] = { 7,8,9,13,0,0 };
      P1 = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(5,9,2,4,.5),3,V);
      ::std::cerr << "Triangle Probability : " << P1 << ::std::endl;
    }
    {
      double V[] = { 7,8,0,0,2,9 };
      P2 = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(5,9,2,4,.5),3,V);
      ::std::cerr << "Triangle Probability : " << P2 << ::std::endl;
    }
    {
      double V[] = { 0,0,9,13,2,9 };
      P3 = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(5,9,2,4,.5),3,V);
      ::std::cerr << "Triangle Probability : " << P3 << ::std::endl;
    }
  
    {
      double V[] = { 7,8,9,13,2,9 };
      double P = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(5,9,2,4,.5),3,V);
      ::std::cerr << "Triangle Probability : " << P << ::std::endl;
      double Papprox = signedPolygonCDF<BivariateNormal::ApproximateCDF>(BivariateNormal(5,9,2,4,.5),3,V);
      ::std::cerr << "Approx. Triangle Probability : " << P << ::std::endl;
      double Pexpected = .211;
      assert(abs(Pexpected-P) < .001);
      assert(abs(Pexpected - (P1+P2+P3)) < 0.001);
      ::std::cerr << "Error of approximation " << (P-Papprox) << ::std::endl;
    }
  }
  {
    double V1[] = { 0,0, 10,0, 0,5 };
    double V2[] = { 0,0, 0,5,-10,10 };
    double V[] = { 0,0,10,0,-10,10 };

    double P = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(),3,V);
    double P1 = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(),3,V1);
    double P2 = signedPolygonCDF<BivariateNormal::CDF>(BivariateNormal(),3,V2);
    ::std::cerr << "Triangle Probability P : " << P << ::std::endl;
    ::std::cerr << "Triangle Probability P1 : " << P1 << ::std::endl;
    ::std::cerr << "Triangle Probability P2 : " << P2 << ::std::endl;

    assert(P>0);
    assert(abs(P-(P1+P2)) < .0001);
  }

}

void utest_1()
{
  //BivariateNormal pdf = BivariateNormal::decomposeGaussian(-13136.6, -24474.2,3600, 2500,0);
  BivariateNormal pdf = BivariateNormal::decomposeGaussian(-13000, -24000,1,1,0);

  double V1[] = { -14000,-23000, -14000,-25000,  -7000,-25000  };
  double P1 = signedPolygonCDF<BivariateNormal::CDF>(pdf,3,V1);
  assert(P1==P1);
  ::std::cerr << "P1: " << P1 << ::std::endl;
  
  //BivariateNormal pdf = BivariateNormal::decomposeGaussian(-13136.6, -24474.2,3600, 2500,0);
  double V2[] = { -14000,-23000, -14000,-25000,-7000,-23000,  };
  double P2 = signedPolygonCDF<BivariateNormal::CDF>(pdf,4,V2);
  assert(P2==P2);
  ::std::cerr << "P2: " << P2 << ::std::endl;
  
  //BivariateNormal pdf = BivariateNormal::decomposeGaussian(-13136.6, -24474.2,3600, 2500,0);
  double V3[] = { -14000,-23000,   -7000,-25000,-7000,-23000,  };
  double P3 = signedPolygonCDF<BivariateNormal::CDF>(pdf,4,V3);
  assert(P3==P3);
  ::std::cerr << "P3: " << P3 << ::std::endl;
  
  //BivariateNormal pdf = BivariateNormal::decomposeGaussian(-13136.6, -24474.2,3600, 2500,0);
  double V4[] = { -14000,-25000,  -7000,-25000,-7000,-23000,  };
  double P4 = signedPolygonCDF<BivariateNormal::CDF>(pdf,4,V4);
  assert(P4==P4);
 ::std::cerr << "P4: " << P4 << ::std::endl;
  
  //BivariateNormal pdf = BivariateNormal::decomposeGaussian(-13136.6, -24474.2,3600, 2500,0);
  double V[] = { -14000,-23000, -14000,-25000,  -7000,-25000,-7000,-23000,  };
  double P = signedPolygonCDF<BivariateNormal::CDF>(pdf,4,V);
  assert(P==P);
 ::std::cerr << "P: " << P << ::std::endl;
  
  assert(abs(P) > 0.9);
}

void utest_2()
{
  //BivariateNormal pdf = BivariateNormal::decomposeGaussian(-13136.6, -24474.2,3600, 2500,0);
  BivariateNormal pdf = BivariateNormal::decomposeGaussian(-18856.1, -9510.52, 154.723, 21921.6, 1147.19);

  double V[] = { -30468.1,-22127.1,-31326.5,-21285.6,23620.7,34404.8,24114.9,33920.4 };
  double P = signedPolygonCDF<BivariateNormal::CDF>(pdf,4,V);
  ::std::cerr << "P: " << P << ::std::endl;
  assert(P==P);
  assert(abs(P) > 0.9);
}

void utest_3()
{
  BivariateNormal::CDF cdf;
  double p = cdf(-1000,0,0.5*sqrt(2));
  assert(p==p);
  ::std::cerr << "P: " << p << ::std::endl;

  BivariateNormal::ApproximateCDF acdf;
  double pa = acdf(-1000,0,0.5*sqrt(2));
  assert(pa==pa);
  ::std::cerr << "P: " << pa << ::std::endl;

  assert(abs(pa-p) < 1E-5);
}

