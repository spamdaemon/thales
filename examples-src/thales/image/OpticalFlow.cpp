#include <timber/media/IntensityImage.h>
#include <timber/media/ImageFactory.h>
#include <timber/media/image/Filter.h>
#include <timber/media/image/ScaleOp.h>

#include <thales/image/LucasKanadeFlowEstimator.h>
#include <thales/image/TemporalFilter.h>

#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <map>
#include <memory>

using namespace std;
using namespace canopy;
using namespace timber;
using namespace timber::media;
using namespace timber::media::image;
using namespace thales::image;

static void writeOpticalFlow(const OpticalFlow& flow, ::std::ostream& out)
{
   out << flow.width() << ' ' << flow.height();
   for (size_t y = 0; y < flow.height(); ++y) {
      for (size_t x = 0; x < flow.width(); ++x) {
         OpticalFlow::Vector v = flow.vectorAt(x, y);

         if (v.x == v.x) {
            out << ' ' << v.x << ' ' << v.y << ' ' << '1';
         }
         else {
            out << ' ' << 0 << ' ' << 0 << ' ' << '1';

         }
      }
   }
   out << ::std::endl << ::std::flush;
}

struct ::std::shared_ptr< ImageBuffer> crop(ImageBuffer& image, size_t x, size_t y, size_t width, size_t height)
{
   if (x + width > image.width() || y + height > image.height()) {
      return ::std::shared_ptr< ImageBuffer>();
   }

   ::std::unique_ptr< ImageBuffer::PixelColor[]> pixels(new ImageBuffer::PixelColor[width * height]);for
(   size_t i=0;i<height;++i) {
      for (size_t j=0;j<width;++j) {
         image.pixel(x+j,y+i,pixels[i*width+j]);
      }
   }
   return ImageFactory::getBuiltinFactory()->createImage(width, height, move(pixels));
}

int main(int argc, char** argv)
{
   size_t sz = 0;
   int arg = 1;
   for (; arg < argc; ++arg) {
      if (string(argv[arg]) == "-sz") {
         if (++arg == argc) {
            cerr << "Missing size" << endl;
            exit(1);
         }
         if (::std::sscanf(argv[arg], "%lu", &sz) != 1) {
            cerr << "Could not parse " << argv[arg] << " as an unsigned integer" << endl;
            exit(1);
         }
      }
      else {
         break;
      }
   }

   if (arg >= argc - 1) {
      ::std::cerr << "Need at least 2 arguments" << ::std::endl;
      return 1;
   }

   const char* sourceA = argv[arg++];
   const char* sourceB = argv[arg++];

   unique_ptr< LucasKanadeFlowEstimator> flowEstimator;
   flowEstimator = LucasKanadeFlowEstimator::createEstimator(3,19);

   unique_ptr< TemporalFilter> temporalFilter = TemporalFilter::createNoFilter();

   ::std::shared_ptr < ImageBuffer > imageA;
   {
      ::std::ifstream ain(sourceA);
      imageA = ImageBuffer::readSunRasterImage(ain);
      ain.close();
   }

   if (!imageA) {
      ::std::ifstream ain(sourceA);
      imageA = ::std::dynamic_pointer_cast<ImageBuffer>( ImageFactory::getBuiltinFactory()->read(ain));
      if (!imageA) {
         ::std::cerr << "Could not read image " << sourceA << " as a SunRaster" << ::std::endl;
         return 1;
      }
   }

   ::std::shared_ptr < ImageBuffer > imageB;
   {
      ::std::ifstream bin(sourceB);
      imageB = ImageBuffer::readSunRasterImage(bin);
      bin.close();
   }
   if (!imageB) {
      ::std::ifstream bin(sourceB);
      imageB =  ::std::dynamic_pointer_cast<ImageBuffer>(ImageFactory::getBuiltinFactory()->read(bin));
      if (!imageB) {
         ::std::cerr << "Could not read image " << sourceB << " as a SunRaster" << ::std::endl;
         return 1;
      }
   }

   if (sz > 0) {
      imageA = crop(*imageA, 0, 0, sz, sz);
      imageB = crop(*imageB, 0, 0, sz, sz);
   }
   unique_ptr < Filter > blur = Filter::createBlurFilter();
   blur = Filter::createGaussianFilter(1.5, 5);

#if 0
   for (size_t i = 0; i < 0; ++i) { // currently not wanted
      imageA = blur->apply(imageA);
      imageB = blur->apply(imageB);
   }
#endif

   unique_ptr < ::timber::media::image::ScaleOp > halfRes(::timber::media::image::ScaleOp::createHalfResolution());

#if 0
   for (size_t i = 0; i < 0; ++i) {
      imageA = halfRes->apply(IntensityImage::createImage(imageA));
      imageB = halfRes->apply(IntensityImage::createImage(imageB));
   }
#endif

   flowEstimator->addImage(imageA);
   Pointer< OpticalFlow> flow = flowEstimator->addImage(imageB);

   if (flow) {
      writeOpticalFlow(*flow, ::std::cout);
   }
   return 0;
}
