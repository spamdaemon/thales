#include <timber/media/ImageBuffer.h>

#define START_SERVER 1

#include <canopy/time/Time.h>
#include <canopy/net/Socket.h>
#include <canopy/net/Endpoint.h>
#include <canopy/mt/Thread.h>
#include <canopy/mt/Condition.h>

#include <timber/media/IntensityImage.h>
#include <timber/media/format/Jpeg.h>
#include <timber/media/format/Png.h>
#include <timber/media/image/ScaleOp.h>

#include <timber/net/http/server/port/HttpPort.h>
#include <timber/net/http/server/Service.h>
#include <timber/net/http/server/RequestContext.h>
#include <timber/net/http/server/RequestError.h>
#include <timber/net/http/server/AbstractHttpResponse.h>

#include <timber/net/server/Server.h> 

#include <sstream>
#include <cstring>
#include <string>
#include <cstdlib>
#include <map>
#include <vector>

#include <iostream>
#include <fstream>
#include <memory>

using namespace std;
using namespace canopy;

using namespace timber;
using namespace timber::media;

using namespace canopy::time;

using namespace timber::media::format;
using namespace timber::media::image;

using namespace canopy::net;
using namespace canopy::mt;
using namespace timber::net::http;
using namespace timber::net::http::server;
using namespace timber::net::http::server::port;
using namespace timber::net::server;

static ::canopy::mt::Condition* cmutex;
static ::std::shared_ptr< ImageBuffer>* sourceImage;

struct MJPEGResponse : public AbstractHttpResponse
{
      MJPEGResponse(double sz)
            : _size(sz)
      {
         AbstractHttpResponse::setHeader("Content-type", "multipart/x-mixed-replace;boundary=\"FOOBAR\"");
         AbstractHttpResponse::setHeader("connection", "close");
      }

      ~MJPEGResponse() throws()
      {
      }

      OutputState writeContent(::std::ostream &out) throws (::std::exception)
      {
         ::std::shared_ptr< ImageBuffer> img(_lastImage);
         cmutex->lock();
         img = *const_cast< ::std::shared_ptr< ImageBuffer>*>(sourceImage);
         if (!img) {
            cmutex->waitFor(::canopy::time::Time::Duration(INT64_C(50000000)));
            img = *const_cast< ::std::shared_ptr< ImageBuffer>*>(sourceImage);
         }
         cmutex->unlock();
         if (!img || img == _lastImage) {
            Thread::suspend(INT64_C(50000000));
            return OutputState::DATA_AVAILABLE;
         }
         _lastImage = img;
         if (_size > 0) {
            unique_ptr< ScaleOp> scaler = ScaleOp::createFastScale(size_t(_size * img->width()),
                  size_t(_size * img->height()));
            img = scaler->apply(img);
         }
         // now, write the image
         out << "--FOOBAR" << CRLF;
         out << "Content-type : image/jpeg" << CRLF;
         out << CRLF;

         Jpeg::write(*img, out);

         out << CRLF;
         out.flush();
         return OutputState::DATA_AVAILABLE;
      }

   private:
      ::std::shared_ptr< ImageBuffer> _lastImage;
      double _size;
};

struct MJPEGService : public Service
{

      MJPEGService(double sz)
            : _size(sz)
      {
      }

      ~MJPEGService() throws()
      {
      }

      unique_ptr< HttpResponse> doService(unique_ptr< RequestContext>, istream&)
      {
         return unique_ptr < HttpResponse > (new MJPEGResponse(_size));
      }

   private:
      double _size;
};

int main(int argc, char** argv)
{
   const char* url = nullptr;
   const char* source = nullptr;
   int port = 8080;
   double sz = 0;
   bool toCout = false;
   bool writeJPEG = false;

   for (int arg = 1; arg < argc; ++arg) {
      if (string(argv[arg]) == "-file") {
         if (++arg == argc) {
            cerr << "Missing file name" << endl;
            exit(1);
         }
         source = argv[arg];
      }
      else if (string(argv[arg]) == "-url") {
         if (++arg == argc) {
            cerr << "Missing url" << endl;
            exit(1);
         }
         url = argv[arg];
      }
      else if (string(argv[arg]) == "-port") {
         if (++arg == argc) {
            cerr << "Missing port value" << endl;
            exit(1);
         }
         unsigned int portNo;
         if (::std::sscanf(argv[arg], "%u", &portNo) != 1) {
            cerr << "Could not parse " << argv[arg] << " as a short" << endl;
            exit(1);
         }
         if (portNo > 0xffff) {
            cerr << "Port out of range" << endl;
            exit(1);
         }
         port = portNo;
      }
      else if (string(argv[arg]) == "-sz") {
         if (++arg == argc) {
            cerr << "Missing size" << endl;
            exit(1);
         }
         if (::std::sscanf(argv[arg], "%lf", &sz) != 1) {
            cerr << "Could not parse " << argv[arg] << " as a double" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-stdout") {
         toCout = true;
      }
      else if (string(argv[arg]) == "-jpeg") {
         writeJPEG = true;
      }
   }

#if START_SERVER == 1
   // create the service that we want
   Reference< HttpPort> httpPort(new HttpPort());

   // define the context that we want to use for the MJPEG service
   httpPort->addService("/mjpeg", new MJPEGService(sz));

   // create a new server
   unique_ptr < Server > server = Server::create(10);

   // before creating a socket, we need to create an endpoint
   Endpoint ep = Endpoint::createAnyEndpoint(port, STREAM);

   // now, create a socket; first, create an endpoint
   Socket socket = Socket::createBoundSocket(ep);
   socket.listen(5);

   // start the service
   server->addSessionPort(socket, httpPort);
#endif

   Condition xcmutex;
   cmutex = &xcmutex;

   ::std::shared_ptr< ImageBuffer> xbuf;

   sourceImage = &xbuf;
   do {
      unique_ptr < ifstream > fin;
      if (source != 0) {
         fin.reset(new ifstream(source, ::std::ios::binary));
      }
      istream& xin = fin.get() ? *fin : ::std::cin;
      size_t frame = 0;
      for (::std::shared_ptr< ImageBuffer> buf = ImageBuffer::readSunRasterImage(xin); buf;
            buf = ImageBuffer::readSunRasterImage(xin)) {
         if (toCout) {
            if (writeJPEG) {
               Jpeg::write(*buf, ::std::cout);
            }
            else {
               buf->writeSunRasterImage(::std::cout);
            }
         }
         cmutex->lock();
         *sourceImage = buf;
         cmutex->notifyAll();
         cmutex->unlock();
         if (source) {
            Thread::suspend(100000000);
         }
         ++frame;
      }
   } while (source != 0);

#if START_SERVER == 1
   server->shutdown();
   server->wait();
#endif

   return 0;
}
