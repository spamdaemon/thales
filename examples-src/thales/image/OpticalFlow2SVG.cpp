#include <thales/image/OpticalFlow.h>

#include <indigo/Color.h>
#include <indigo/StrokeNode.h>
#include <indigo/Points.h>
#include <indigo/PolyLine.h>
#include <indigo/PolyLines.h>
#include <indigo/Group.h>
#include <indigo/svg/svg.h>

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
using namespace timber;
using namespace thales::image;
using namespace indigo;

static size_t spacing = 1;

static Pointer< Group> createGraphic(Reference< OpticalFlow> buf)
{
   const size_t w = buf->width();
   const size_t h = buf->height();

   double strokeWidth = 5.0/::std::max(w,h);

   Reference< Group> group = new Group();
   Reference< PolyLines> polys = new PolyLines();
   Reference< Points> pts = new Points();
   group->add(new StrokeNode(::indigo::Color::BLACK, strokeWidth));
   group->add(polys);
   group->add(new StrokeNode(::indigo::Color::RED, strokeWidth));
   group->add(pts);

   for (size_t y = 0; y < h; y += spacing) {
      for (size_t x = 0; x < w; x += spacing) {
         OpticalFlow::Vector v = buf->vectorAt(x, y);
         if (::std::abs(v.x) < .1 && ::std::abs(v.y) < .1) {
            continue;
         }
         double px = static_cast< double>(x) / w;
         double py = static_cast< double>(y) / h;
         indigo::Point a(px, py);
         indigo::Point b(px + v.x / w, py + v.y / h);
         polys->add(::indigo::PolyLine(a, b));
         pts->add(a);
      }
   }
   if (pts->count() == 0) {
      return Pointer< Group>();
   }
   return group;
}

struct Pointer< OpticalFlow> readOpticalFlow(istream& in)
{
   size_t w = 0;
   size_t h = 0;

   in >> w >> h;
   if (w * h == 0) {
      return Pointer< OpticalFlow>();
   }

   OpticalFlow::Vector* vectors = new OpticalFlow::Vector[w * h];
   for (size_t y = 0; y < h; ++y) {
      for (size_t x = 0; x < w; ++x) {
         double intensity;
         in >> vectors[y * w + x].x >> vectors[y * w + x].y >> intensity;
      }
   }
   return OpticalFlow::create(vectors, w, h);
}

int main(int argc, const char** argv)
{
   const char* source = nullptr;
   if (argc == 2) {
      source = argv[1];
   }
   Pointer< OpticalFlow> flow;
   Pointer< Group> graphic;

   for (size_t i = 0; graphic == nullptr; ++i) {
      flow = readOpticalFlow(::std::cin);
      if (flow == nullptr) {
         break;
      }
      graphic = createGraphic(flow);
      if (graphic != nullptr) {
         if (source == nullptr) {
            ::indigo::svg::write(graphic, ::std::cout);
            break;
         }
         else {
            ::std::ostringstream ssout;
            ssout << source << "_" << i << ".svg";
            ::std::ofstream fout(ssout.str());
            ::indigo::svg::write(graphic, fout);
         }
         graphic = nullptr;
         flow = nullptr;
      }
   }
   return 0;
}
