#include <canopy/time/Time.h>
#include <timber/media/ImageFactory.h>
#include <timber/media/ImageBuffer.h>
#include <timber/media/IntensityImage.h>
#include <timber/media/image/Filter.h>
#include <timber/media/image/DifferenceFilter.h>
#include <timber/media/image/ScaleOp.h>
#include <timber/media/format/Png.h>
#include <timber/media/format/Jpeg.h>

#include <thales/image/StaufferGrimsonEstimator.h>
#include <thales/image/LucasKanadeFlowEstimator.h>
#include <thales/image/TemporalFilter.h>

#include <thales/probability/NormalDistribution.h>

#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <map>
#include <vector>
#include <memory>
#include <functional>

using namespace std;
using namespace canopy::time;
using namespace timber;
using namespace timber::media::image;
using namespace timber::media;
using namespace thales::image;

using canopy::UInt32;
using canopy::UInt16;

static double sqr(double a)
{
   return a * a;
}

static void writeOpticalFlow(const OpticalFlow& flow, ::std::ostream& out)
{
   out << flow.width() << ' ' << flow.height();
   for (size_t y = 0; y < flow.height(); ++y) {
      for (size_t x = 0; x < flow.width(); ++x) {
         OpticalFlow::Vector v = flow.vectorAt(x, y);

         if (v.x == v.x) {
            out << ' ' << v.x << ' ' << v.y << ' ' << '1';
         }
         else {
            out << ' ' << 0 << ' ' << 0 << ' ' << '1';

         }
      }
   }
   out << ::std::endl << ::std::flush;
}

// implementation of a union-find algorithm 
struct UnionFind
{

   public:
      typedef ::std::pair< UInt32, UInt32> Set;

      struct Entry
      {
            Entry()
                  : _link(0), _height(0)
            {
            }

            Entry* _link;
            UInt32 _height;
      };

   private:
      Entry* getEntry(Set p) const
      {
         return _sets + p.second * _width + p.first;
      }

      Set getSet(Entry* s) const
      {
         ptrdiff_t i = s - _sets;
         return Set(i % _width, i / _width);
      }

   public:
      UnionFind(UInt16 w, UInt16 h)
            : _nSets(0), _width(w), _height(h)

      {
         _sets = new Entry[_width * _height];
         for (Entry* e = _sets; e != _sets + _width * _height; ++e) {
            *e = Entry();
         }
      }

      ~UnionFind() throws()
      {
         delete[] _sets;
      }

      UInt32 width() const
      {
         return _width;
      }
      UInt32 height() const
      {
         return _height;
      }

   public:
      void clear()
      {
         _nSets = 0;
         for (Entry* e = _sets; e != _sets + _width * _height; ++e) {
            *e = Entry();
         }
      }

   public:
      inline size_t nSets() const throws()
      {
         return _nSets;
      }

   public:
      inline bool isSet(Set p) const
      {
         return getEntry(p)->_link != 0;
      }

      inline Set makeSet(Set p)
      {
         Entry* e = getEntry(p);
         if (e->_height == 0) {
            e->_link = e;
            e->_height = 1;
            ++_nSets;
         }
         return getSet(e->_link);
      }

      Set find(Set p) const
      {
         // initially root will actually be a leaf, but at the
         // end of the while-loop it will point to the root node
         Entry* root = getEntry(p);
         if (root->_height == 0) {
            return p;
         }
         while (true) {
            Entry* parent = root->_link;
            if (root == parent) {
               break;
            }
            _path.push_back(root);
            root = parent;
         }

         // now, we should path-compress all of them along the path, but we'll just do
         while (!_path.empty()) {
            Entry* e = _path.back();
            _path.pop_back();
            e->_link = root;
         }
         return getSet(root);
      }

      bool merge(Set s1, Set s2)
      {
         s1 = find(s1);
         s2 = find(s2);
         if (s1 == s2) {
            return false;
         }
         Entry* e1 = getEntry(s1);
         Entry* e2 = getEntry(s2);
         if (e1->_link == 0 || e2->_link == 0) {
            return false;
         }

         if (e1->_height > e2->_height) {
            e2->_link = e1;
            return true;
         }
         else if (e1->_height == e2->_height) {
            e1->_link = e2;
            ++e2->_height;
            return true;
         }
         else {
            e1->_link = e2;
            return true;
         }
      }

   private:
      UInt32 _nSets;
      UInt32 _width, _height;
      Entry* _sets;
      mutable ::std::vector< Entry*> _path;
};
typedef UnionFind::Set UPixel;

void clusterPixels(UInt32 dX, UInt32 dY, UnionFind& uf)
{
   UInt32 w = uf.width();
   UInt32 h = uf.height();

   for (UInt y = 0; y < h; ++y) {
      UInt maxI = ::std::min(y, dY);
      for (UInt x = 0; x < w; ++x) {
         if (uf.isSet(UPixel(x, y))) {
            UInt maxJ = ::std::min(x, dX);
            for (UInt j = 0; j <= maxJ; ++j) {
               for (UInt i = 0; i <= maxI; ++i) {
                  uf.merge(UPixel(x, y), UPixel(x - j, y - i));
               }
            }
         }
      }
   }
}

struct Rect
{
      Rect()
            : minx(0xffffffff), miny(0xffffffff), maxx(0), maxy(0)
      {
      }
      size_t size() const
      {
         return (maxx - minx) * (maxy - miny);
      }
      UInt32 minx, miny, maxx, maxy;
};

vector< Rect> findRectangles(const UnionFind& uf, size_t& maxRect)
{
   UInt32 w = uf.width();
   UInt32 h = uf.height();
   map< UPixel, Rect> tmap;
   for (UInt y = 0; y < h; ++y) {
      for (UInt x = 0; x < w; ++x) {
         if (uf.isSet(UPixel(x, y))) {
            Rect& rect = tmap[uf.find(UPixel(x, y))];
            rect.minx = ::std::min(rect.minx, x);
            rect.maxx = ::std::max(rect.maxx, x);
            rect.miny = ::std::min(rect.miny, y);
            rect.maxy = ::std::max(rect.maxy, y);
         }
      }
   }
   vector< Rect> rects;
   rects.reserve(tmap.size());
   int maxSize = 0;
   for (map< UPixel, Rect>::const_iterator xi = tmap.begin(); xi != tmap.end(); ++xi) {
      const Rect& r = xi->second;
      int xw = r.maxx - r.minx;
      int xh = r.maxy - r.miny;
      if (xw * xh > 5 && xw > 1 && xh > 1) {
         if (xw * xh > maxSize) {
            maxSize = xw * xh;
            maxRect = rects.size();
         }
         rects.push_back(r);
      }
   }

   for (size_t r = 0; r < rects.size(); ++r) {
      Rect& R = rects[r];
      for (size_t s = 0; s < rects.size();) {
         Rect& S = rects[s];
         if (s == r || S.maxx < R.minx || S.minx > R.maxx || S.miny > R.maxy || S.maxy < R.miny) {
            ++s;
         }
         else {
            R.minx = ::std::min(R.minx, S.minx);
            R.maxx = ::std::max(R.maxx, S.maxx);
            R.miny = ::std::min(R.miny, S.miny);
            R.maxy = ::std::max(R.maxy, S.maxy);
            S = rects.back();
            rects.pop_back();
            s = 0;
         }
      }
   }

   return rects;
}

struct ::std::shared_ptr< ImageBuffer> crop(ImageBuffer& image, size_t x, size_t y, size_t width, size_t height)
{
   if (x + width > image.width() || y + height > image.height()) {
      return ::std::shared_ptr< ImageBuffer>();
   }

   ::std::unique_ptr< ImageBuffer::PixelColor[]> pixels(new ImageBuffer::PixelColor[width * height]);

   for (size_t i = 0; i < height; ++i) {
      for (size_t j = 0; j < width; ++j) {
         image.pixel(x + j, y + i, pixels[i * width + j]);
      }
   }
   return ImageFactory::getBuiltinFactory()->createImage(width, height, move(pixels));
}

SharedRef< ImageBuffer> drawRectangles(const ImageBuffer& img, const vector< Rect>& rects)
{
   size_t w = img.width();
   size_t h = img.height();

   // the new image
   ::std::unique_ptr< ImageBuffer::PixelColor[]> col(new ImageBuffer::PixelColor[w * h]);
   // copy the old image into the new image
   for (size_t i = 0; i < h; ++i) {
      for (size_t j = 0; j < w; ++j) {
         img.pixel(j, i, col[i * w + j]);
      }
   }

   // render each rectangle into the image buffer, setting the red channel to 0x0ffff.
   for (size_t i = 0; i < rects.size(); ++i) {
      const Rect& R = rects[i];
      for (size_t x = R.minx; x <= R.maxx; ++x) {
         col[R.maxy * w + x]._red = col[R.miny * w + x]._red = 0xffff;
      }
      for (size_t y = R.miny; y <= R.maxy; ++y) {
         col[y * w + R.maxx]._red = col[y * w + R.minx]._red = 0xffff;
      }
   }

   return ImageFactory::getBuiltinFactory()->createImage(w, h, move(col));
}

SharedRef< ImageBuffer> mergeImages(const ImageBuffer& bottom, const ImageBuffer& top)
{
   size_t w = bottom.width();
   size_t h = bottom.height();
   assert(w == top.width());
   assert(h == top.height());

   ImageBuffer::PixelColor colbottom, coltop;
   ::std::unique_ptr< ImageBuffer::PixelColor[]> col(new ImageBuffer::PixelColor[w * h]);

   for (size_t i = 0; i < h; ++i) {
      for (size_t j = 0; j < w; ++j) {
         bottom.pixel(j, i, colbottom);
         top.pixel(j, i, coltop);
         size_t r = colbottom._red;
         size_t g = colbottom._green;
         size_t b = colbottom._blue;
         size_t a = colbottom._opacity;
         r += coltop._red;

         //g+= coltop._green;
         //b+= coltop._blue;

         r = ::std::min(r, size_t(0x0ffffU));
         g = ::std::min(g, size_t(0x0ffffU));
         b = ::std::min(b, size_t(0x0ffffU));

         col[i * w + j]._red = static_cast< UInt16>(r);
         col[i * w + j]._green = static_cast< UInt16>(g);
         col[i * w + j]._blue = static_cast< UInt16>(b);
         col[i * w + j]._opacity = ImageBuffer::MAX_OPACITY;
      }
   }
   return ImageFactory::getBuiltinFactory()->createImage(w, h, move(col));
}

vector< Rect> findClusters(SharedRef< ImageBuffer> image, bool& tooMuchChange)
{
   tooMuchChange = false;
   vector< Rect> rects;
   size_t w = image->width();
   size_t h = image->height();
   size_t resX = 5;
   size_t resY = 5;
   size_t filterWidth = w / resX;
   size_t filterHeight = h / resY;

   UnionFind unionFind(static_cast< UInt16>(filterWidth), static_cast< UInt16>(filterHeight));

   // now, loop over the image and find clusters
   for (size_t i = 0; i < h; ++i) {
      for (size_t j = 0; j < w; ++j) {
         ImageBuffer::PixelColor col;
         image->pixel(j, i, col);
         if (col._red != 0) {
            unionFind.makeSet(UnionFind::Set(j / resX, i / resY));
         }
      }
   }

   size_t maxRect;

   clusterPixels(1, 1, unionFind);
   rects = findRectangles(unionFind, maxRect);
   if (rects.size() > 0) {
      //      ::std::cerr << "Found " << rects.size() << " rectangles" << ::std::endl;
   }
   size_t nPixels = 0;
   for (size_t r = 0; r < rects.size(); ++r) {
      Rect& R = rects[r];
      R.minx *= resX;
      R.maxx = ::std::min(w, (1 + R.maxx) * resX - 1);
      R.miny *= resY;
      R.maxy = ::std::min(h, (1 + R.maxy) * resY - 1);
      nPixels += (R.maxx - R.minx) * (R.maxy - R.miny);
   }
   tooMuchChange = 1.0 * unionFind.nSets() > ((0.35 * w / resX) * h / resY);
   tooMuchChange = tooMuchChange || (nPixels > ((0.35 * w / resX) * h / resY));

   return rects;
}

static ::std::shared_ptr< ImageBuffer> readSunRasterImage(::std::istream& stream, const SharedRef< ImageFactory>&)
{
   return ImageBuffer::readSunRasterImage(stream);
}

static ::std::shared_ptr< ImageBuffer> readPNGImage(::std::istream& stream, const SharedRef< ImageFactory>& f)
{
   return ::timber::media::format::Png::read(stream, f);
}

typedef ::std::function< ::std::shared_ptr< ImageBuffer>(::std::istream&, const SharedRef< ImageFactory>&)> ImageReader;

int main(int argc, char** argv)
{
   ImageReader reader = readSunRasterImage;
   const char* source = 0;
   const char* saveAs = 0;
   const char* reportFile = 0;

   double learningRate = 0.15;
   double threshold = .51;
   unsigned int nFilters = 5;
   unsigned int sz = 0;
   bool useRGB = true;
   bool useOpticalFlow = false;
   bool outputFlow = false;
   bool outputDiff = false;
   UShort pixelError = 0;

   for (int arg = 1; arg < argc; ++arg) {
      if (string(argv[arg]) == "-file") {
         if (++arg == argc) {
            cerr << "Missing file name" << endl;
            exit(1);
         }
         source = argv[arg];
      }
      else if (string(argv[arg]) == "-o") {
         if (++arg == argc) {
            cerr << "Missing outfile name" << endl;
            exit(1);
         }
         saveAs = argv[arg];
      }
      else if (string(argv[arg]) == "-reports") {
         if (++arg == argc) {
            cerr << "Missing report filename" << endl;
            exit(1);
         }
         reportFile = argv[arg];
      }
      else if (string(argv[arg]) == "-png") {
         reader = readPNGImage;
      }
      else if (string(argv[arg]) == "-oflow") {
         outputFlow = true;
      }
      else if (string(argv[arg]) == "-odiff") {
         outputDiff = true;
      }
      else if (string(argv[arg]) == "-a") {
         if (++arg == argc) {
            cerr << "Missing alpha value" << endl;
            exit(1);
         }
         if (::std::sscanf(argv[arg], "%lf", &learningRate) != 1) {
            cerr << "Could not parse " << argv[arg] << " as a double" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-pe") {
         if (++arg == argc) {
            cerr << "Missing pixel error value" << endl;
            exit(1);
         }
         unsigned int pixerr;
         if (::std::sscanf(argv[arg], "%u", &pixerr) != 1) {
            cerr << "Could not parse " << argv[arg] << " as a short" << endl;
            exit(1);
         }
         pixelError = static_cast< UShort>(pixerr);
      }
      else if (string(argv[arg]) == "-T") {
         if (++arg == argc) {
            cerr << "Missing threshold value" << endl;
            exit(1);
         }
         if (::std::sscanf(argv[arg], "%lf", &threshold) != 1) {
            cerr << "Could not parse " << argv[arg] << " as a double" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-n") {
         if (++arg == argc) {
            cerr << "Missing filter count" << endl;
            exit(1);
         }
         if (::std::sscanf(argv[arg], "%u", &nFilters) != 1) {
            cerr << "Could not parse " << argv[arg] << " as an unsigned integer" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-sz") {
         if (++arg == argc) {
            cerr << "Missing size" << endl;
            exit(1);
         }
         if (::std::sscanf(argv[arg], "%u", &sz) != 1) {
            cerr << "Could not parse " << argv[arg] << " as an unsigned integer" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-gray") {
         useRGB = false;
      }
      else if (string(argv[arg]) == "-flow") {
         useOpticalFlow = true;
      }
   }

   if (pixelError == 0) {
      if (useRGB) {
         pixelError = 20;
      }
      else {
         pixelError = 10;
      }
   }
   unique_ptr< ScaleOp> scale = ScaleOp::createHalfResolution();
   unique_ptr < ifstream > fin;
   if (source != 0) {
      fin.reset(new ifstream(source, ::std::ios::binary));
   }
   unique_ptr < ofstream > fout;
   if (saveAs != 0) {
      fout.reset(new ofstream(saveAs, ::std::ios::binary));
   }
   istream& xin = fin.get() ? *fin : ::std::cin;
   ostream& xout = fout.get() ? *fout : ::std::cout;

   const size_t nWeights = 15;
   double weights[nWeights];
   ::thales::probability::NormalDistribution nd(nWeights / 2, 1.0 * 1.0);
   double sumWeight = 0;
   for (size_t i = 0; i < nWeights; ++i) {
      weights[i] = nd.pdf(i);
      sumWeight += weights[i];
   }
   for (size_t i = 0; i < nWeights; ++i) {
      weights[i] /= sumWeight;
   }
   if (nWeights < 5) {
      for (size_t i = 0; i < nWeights; ++i) {
         weights[i] = 1.0 / nWeights;
      }
   }

   unique_ptr < ostream > reports;
   if (reportFile) {
      reports.reset(new ofstream(reportFile, ::std::ios::binary));
   }

   unique_ptr< LucasKanadeFlowEstimator> flowEstimator;
   unique_ptr< StaufferGrimsonEstimator> estimator;
   if (useOpticalFlow) {
      flowEstimator = LucasKanadeFlowEstimator::createEstimator(2, 21);
   }
   else {
      if (useRGB) {
         estimator = StaufferGrimsonEstimator::createRGBEstimator(nFilters, learningRate, threshold, pixelError / 3);
      }
      else {
         estimator = StaufferGrimsonEstimator::createGrayEstimator(nFilters, learningRate, threshold, pixelError / 3);
      }
   }

   unique_ptr< TemporalFilter> temporalFilter = TemporalFilter::createWeightedAverageFilter(nWeights, weights);
   //temporalFilter= TemporalFilter::createNoFilter();
   if (estimator.get()) {
      temporalFilter = TemporalFilter::createExponentialFilter(.75);
   }

   unique_ptr< Filter> blur;
   //blur = Filter::createBlurFilter(15);
   blur = Filter::createGaussianFilter(6.25, 5);
   TemporalFilter::Frame frame;
   ::std::shared_ptr< ImageBuffer> lastImage;
   unique_ptr< DifferenceFilter> diff;

   size_t frameNumber = 0;
   size_t nRects = 0;
   Time lastTime(0);

   SharedRef< ImageFactory> factory = ImageFactory::getBuiltinFactory();

   for (::std::shared_ptr< ImageBuffer> buf; (buf = reader(xin, factory));) {
      Time start = Time::now();

      for (size_t i = 0; i < sz; ++i) {
         buf = scale->apply(buf);
      }
      SharedRef< ImageBuffer> origImage = buf;
      if (flowEstimator.get()) {
         buf = blur->apply(buf);
      }
      if (!buf) {
         continue;
      }

      if (temporalFilter.get()) {
         frame = TemporalFilter::Frame(frame.frame + 1, buf);

         // smooth the image over time (only a small time frame, though)
         // we need this so the we can filter out digitization issues or scan conversion issues
         TemporalFilter::Frame fframe = temporalFilter->enqueue(frame);
         // here's the actual image we should be processing
         buf = fframe.image;

         // check if we've got a filter frame (there might be some lag though)
         if (!buf) {
            continue;
         }
      }

      Pointer< OpticalFlow> flow;
      ::std::shared_ptr< ImageBuffer> motion;
      if (outputDiff) {
         if (diff.get()) {
            // do a difference image
            motion = diff->apply(buf);
            diff->setSource(buf);
         }
         else {
            diff.reset(new DifferenceFilter(buf, static_cast< UShort>(pixelError * 257)));
            continue;
         }
      }

      if (!motion) {
         if (flowEstimator.get()) {
            flow = flowEstimator->addImage(buf);
            if (flow) {
               motion = flow->createMagnitudeImage(.5, .5);
            }
         }
         else if (estimator.get()) {
            motion = estimator->addImage(buf);
         }
      }

      if (motion) {

         if (flowEstimator.get()) {
            if (outputFlow) {
               if (flow) {
                  writeOpticalFlow(*flow, xout);
                  if (frame.frame < 5) {
                     ::std::ostringstream sout;
                     sout << "frame" << frame.frame << ".ras";
                     ::std::ofstream iout(sout.str().c_str());
                     origImage->writeSunRasterImage(iout);
                  }
               }
               continue;
            }
         }

         {
            bool tooMuchChange;
            vector< Rect> rects = findClusters(motion, tooMuchChange);
            if (estimator.get()) {
               if (tooMuchChange) {
                  estimator->setLearningRate(::std::min(0.5, learningRate * 10));
               }
               else {
                  estimator->setLearningRate(learningRate);
               }
            }
            Time now = Time::now();
            bool emitNow = (now.time() - lastTime.time()) > ::canopy::time::Time::Duration(60 * INT64_C(1000000000));
            if (!rects.empty() || nRects > 0 || emitNow) {
               lastTime = now;
               ::std::cerr << "Writing image " << rects.size() << ", " << nRects << ", " << now.timeElapsed().count()
                     << ::std::endl;
	       //	       buf = mergeImages(*buf,*motion);
	       buf = drawRectangles(*origImage, rects);
	       ::timber::media::format::Png::write(*buf,xout);
	       //::timber::media::format::Jpeg::write(*buf,xout);
	       //               buf->writeSunRasterImage(xout);
               xout << flush;
               if (xout.bad()) {
                  break;
               }
            }
            nRects = rects.size();
            if (reports.get()) {
               // first line is time (nanoseconds), #reports, report dimension, pd
               (*reports) << now.timeElapsed().count() << ' ' << rects.size() << " 2 .8" << ::std::endl;
               for (size_t i = 0; i < rects.size(); ++i) {
                  // we're going to output the rectangle as gaussian which is
                  // axis aligned:
                  // x y xx xy yy      (xy == 0 due to axis alignment)
                  (*reports) << (rects[i].minx + rects[i].maxx) / 2 << ' ' << (rects[i].miny + rects[i].maxy) / 2 << ' '
                        << sqr((rects[i].maxx - rects[i].minx) / 2) << ' ' << "0 "
                        << sqr((rects[i].maxy - rects[i].miny) / 2) << endl;
               }
            }
         }
      }
      Time end = Time::now();
      size_t tm = ::canopy:: checked_cast< size_t>((end.time() - start.time()).count() / 1000000);
      if (tm > 100) {
         ::std::cerr << "Frame [ " << frameNumber << " ] : processing time " << tm << "ms" << ::std::endl;
      }
      ++frameNumber;
   }
   return 0;
}
