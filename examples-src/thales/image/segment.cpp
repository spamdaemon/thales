#include <timber/media/ImageBuffer.h>
#include <timber/media/ImageFactory.h>
#include <timber/media/format/Jpeg.h>
#include <timber/media/format/Png.h>
#include <timber/media/format/SunRaster.h>
#include <timber/media/image/Filter.h>
#include <thales/image/Segmentation.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace ::std;
using namespace ::thales::image;
using namespace ::timber;
using namespace ::timber::media;
using namespace ::timber::media::image;
using namespace ::timber::media::format;

int main(int argc, char** argv)
{
   string fname;
   double k = -1;
   int minSz = -1;
   double sigma = -1;

   if (argc < 5) {
      cerr << "Usage: " << argv[0] << " <image> <sigma> <k> <minSize>" << ::std::endl;
      return 1;
   }

   auto ifactory = ImageFactory::getBuiltinFactory();


   ifstream in(argv[1]);
   istringstream(argv[2]) >> sigma;
   istringstream(argv[3]) >> k;
   istringstream(argv[4]) >> minSz;

   if (k < 0 || minSz < 0 || sigma < 0) {
      cerr << "Usage: " << argv[0] << " <image> <sigma> <k> <minSize>" << ::std::endl;
      return 2;
   }

   auto img = ::std::dynamic_pointer_cast<ImageBuffer>(ifactory->read(in));

   if (sigma > 0) {
      unique_ptr < Filter > F = Filter::createGaussianFilter(sigma * sigma, 5);
      img = F->apply(img);
   }

   unique_ptr< Segmentation> S = Segmentation::create(k, minSz);
   img = S->apply(img);

   if (false) {
      ofstream out("seg.jpg");
      Jpeg::write(*img, out);
      out.flush();
   }

   {
      ofstream out("seg.ras");
      SunRaster::write(*img, out);
      out.flush();
   }

   if (false) {
      ofstream out("seg.png");
      Png::write(*img, out);
      out.flush();
   }

   return 0;
}
