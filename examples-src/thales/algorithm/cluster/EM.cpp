#include <thales/algorithm/cluster/ExpectationMaximization.h>
#include <thales/kalman/Gaussian.h>

#include <indigo/svg/svg.h>
#include <indigo/Color.h>
#include <indigo/StrokeNode.h>
#include <indigo/SolidFillNode.h>
#include <indigo/Polygon.h>
#include <indigo/Ellipses.h>
#include <indigo/Points.h>
#include <indigo/Group.h>

#include <iostream>

using namespace std;
using namespace timber;
using namespace indigo;

using namespace ::thales::algorithm::cluster;
using namespace ::thales::kalman;
using namespace ::newton;

static void generateClusters(vector< Reference< Gaussian> >& clusters, vector< Reference< Gaussian> >& rsamples)
{
   vector< Vector< > > data, initialData;
   size_t nClusters = 22;
   size_t nSamples = 30;
   Matrix< double> cvar(2, 2);

   Vector< > dataMean(0.0, 0.0);
   Matrix< > dataVar(2);
   dataVar(0, 0) = dataVar(1, 1) = 900;

   initialData = Gaussian::randomSamples(dataMean, dataVar, nClusters);

   cvar(0, 0) = cvar(1, 1) = 100;
   for (size_t i = 0; i < nClusters; ++i) {
      // select a point around we generate random samples, at random
      Vector< double> mean = Gaussian::randomSample(dataMean, dataVar);
      mean = Gaussian::randomSample(dataMean, dataVar);
      ::std::cerr << "Mean : " << mean << ::std::endl;
      //    data.push_back(mean);
      vector< Vector< double> > samples = Gaussian::randomSamples(mean, cvar, nSamples);
      data.insert(data.end(), samples.begin(), samples.end());
   }

   vector< Reference< Gaussian> > initial, ds;
   for (size_t i = 0; i < data.size(); ++i) {
      cvar(1, 1) = cvar(0, 0) = .0001;
      ds.push_back(Gaussian::create(CoordinateFrame::createGenericFrame(data[i].dimension()),data[i], cvar));
   }
   rsamples = ds;

   cvar(0, 0) = cvar(1, 1) = 1;
   for (size_t i = 0; i < initialData.size(); ++i) {
      ::std::cerr << "Initial " << initialData[i] << ::std::endl;
      initial.push_back(
            Gaussian::create(CoordinateFrame::createGenericFrame(data[i].dimension()),initialData[i], cvar));
   }

   ExpectationMaximization algorithm;
   unique_ptr< ExpectationMaximization::Mixture> M;
   size_t iter = 0;
   for (bool done = false; !done;) {
      ::std::cerr << "Iteration " << ++iter << ::std::endl;
      M = algorithm.apply(ds, initial);
      if (M->size() > 1) {
         initial.clear();
         done = true;
         for (size_t i = 0; i < M->size(); ++i) {
            if (M->probability(i) > .10 / M->size()) {
               initial.push_back(M->gaussian(i));
            }
            else {
               done = false;
            }
         }
      }
      else {
         done = true;
      }
   }

   for (size_t i = 0; i < M->size(); ++i) {
      const Gaussian& ge = *M->gaussian(i);
      ::std::cerr << "cluster " << i << " : " << ::std::endl << "  weight : " << M->probability(i) << ::std::endl
            << "  mean " << ge.mean() << ::std::endl;
      double mx, mn, theta;
      Covariance::decompose(ge(0, 0), ge(1, 1), ge(0, 1), mx, mn, theta);
      ::std::cerr << "  cov  " << mx << " ; " << mn << " ; " << theta << ::std::endl;
   }

   clusters.clear();

   for (size_t i = 0; i < M->size(); ++i) {
      clusters.push_back(M->gaussian(i));
   }
}

static Reference< Group> generateGraphics(vector< Reference< Gaussian> >& _clusters,
      vector< Reference< Gaussian> >& _samples)
{
   Reference< Ellipses> gaussians = new Ellipses();
   Reference< Points> points = new Points();
   Reference< Ellipses> samples = new Ellipses();
   Reference< Points> samplePoints = new Points();

   double minX = _samples[0]->get(0);
   double maxX = minX;
   double minY = _samples[0]->get(1);
   double maxY = minY;

   for (size_t i = 1; i < _samples.size(); ++i) {
      minX = ::std::min(_samples[i]->get(0), minX);
      minY = ::std::min(_samples[i]->get(1), minY);
      maxX = ::std::max(_samples[i]->get(0), maxX);
      maxY = ::std::max(_samples[i]->get(1), maxY);
   }
   double scale = ::std::max(maxX - minX, maxY - minY) / 2;

   for (size_t i = 0; i < _clusters.size(); ++i) {
      const Gaussian& ge = *_clusters[i];
      double mx, mn, theta;
      Point pt((ge(0) - (minX + maxX) / 2) / scale, (ge(1) - (minY + maxY) / 2) / scale);
      Covariance::decompose(ge(0, 0), ge(1, 1), ge(0, 1), mx, mn, theta);
      points->add(pt);
      gaussians->add(Ellipse(pt, mx / scale, mn / scale, theta));
   }

   for (size_t i = 0; i < _samples.size(); ++i) {
      const Gaussian& ge = *_samples[i];
      double mx, mn, theta;
      Point pt((ge(0) - (minX + maxX) / 2) / scale, (ge(1) - (minY + maxY) / 2) / scale);
      Covariance::decompose(ge(0, 0), ge(1, 1), ge(0, 1), mx, mn, theta);
      samplePoints->add(pt);
      if (mx > 2 && mn > 2) {
         samples->add(Ellipse(pt, mx / scale, mn / scale, theta));
      }
   }

   Reference< Group> group = new Group();
   group->add(new StrokeNode(::indigo::Color::BLACK, 1.0 / scale));
   group->add(gaussians);
   group->add(points);
   group->add(new StrokeNode(::indigo::Color::GREEN, 1.0 / scale));
   group->add(samples);
   group->add(samplePoints);

   return group;
}

int main(int, const char**)
{
   vector< Reference< Gaussian> > theClusters;
   vector< Reference< Gaussian> > theSamples;

   ::std::cerr << "Generating clusters" << ::std::endl;
   generateClusters(theClusters, theSamples);

   ::std::cerr << "Creating graphics" << ::std::endl;
   auto group = generateGraphics(theClusters, theSamples);

   ::indigo::svg::write(group, ::std::cout);
   return 0;
}
