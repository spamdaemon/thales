#include <thales/probability/BivariateNormal.h>
#include <canopy/time/Time.h>

#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace ::std;
using namespace ::thales::probability;
using namespace ::canopy;
using namespace ::canopy::time;

int main(int argc, char** argv)
{
  // draw 10000 random numbers
  vector<double> X,Y;
  size_t N = 10000;
  for (size_t i=0;i<N;++i) {
    X.push_back(i/(N/10.0));
    Y.push_back(-X.back());
  }
  
  for (double rho=0;rho<=1;rho += .1) {
    BivariateNormal dist(1,1,5,9,rho);
    ::std::cerr << "RHO : " << rho << ::std::endl;
    {
      double avgPD = 0;
      Time start=Time::now();
      for (size_t i=0,sz=X.size();i<sz;++i) {
	avgPD += dist.approximateCDF(X[i],Y[i]);
      }
      Time end=Time::now();
      UInt64 elapsedTimeUS = (end.time()-start.time()).count() / 1000;
      UInt64 elapsedTimeMS = elapsedTimeUS / 1000;
      ::std::cerr << "\tTotal time           " << elapsedTimeMS << " ms" << endl
		  << "\tTime per calculation " << elapsedTimeUS/X.size() << " us/calculation" << endl;
      if (elapsedTimeMS>0) {
	::std::cerr  << "\tCalculation Rate     " << X.size() / elapsedTimeMS << " calculation/ms" << endl;
      }
      ::std::cerr << "\tAverage PD " << avgPD/X.size() << ::std::endl;
    }

  
    {
      double avgPD = 0;
      Time start=Time::now();
      for (size_t i=0,sz=X.size();i<sz;++i) {
	avgPD += dist.cdf(X[i],Y[i]);
      }
      Time end=Time::now();
      UInt64 elapsedTimeUS = (end.time()-start.time()).count() / 1000;
      UInt64 elapsedTimeMS = elapsedTimeUS / 1000;
      ::std::cerr << "\tTotal time           " << elapsedTimeMS << " ms" << endl
		  << "\tTime per calculation " << elapsedTimeUS/X.size() << " us/calculation" << endl;
      if (elapsedTimeMS>0) {
	::std::cerr  << "\tCalculation Rate     " << X.size() / elapsedTimeMS << " calculation/ms" << endl;
      }
      ::std::cerr << "\tAverage PD " << avgPD/X.size() << ::std::endl;
    }
  }

  return 0;
}
