#include <thales/probability/NormalDistribution.h>
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace ::std;
using namespace ::thales::probability;

int main(int argc, char** argv)
{
  size_t  N = 1024;
  if (argc>1) {
    istringstream in(argv[1]);
    in >> N;
    if (in.fail()) {
      cerr << "Could not parse non-negative number" << endl;
      return 1;
    }
  }

  if (N==0) {
    return 0;
  }
  cout << "static const size_t NORMAL_DISTRIBUTION_SIZE = " << N << ";" << endl;
  cout << "static const double NORMAL_DISTRIBUTION[NORMAL_DISTRIBUTION_SIZE] = { ";
  cout << setw(10) << setprecision(10);
  NormalDistribution dist;
  for (size_t i=0;i<N;++i) {
    if (i>0) {
      cout << ',';
    }
    if (i%8 == 0) {
      cout << endl;
    }
    else {
      cout << ' ';
    }
    cout << dist.drawRandom();
  }
  cout << endl << " };" << endl;
  return 0;
}
