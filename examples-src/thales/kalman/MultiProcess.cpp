#include <iostream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <cstdlib>

#include <timber/timber.h>
#include <timber/mt/JobQueue.h>
#include <canopy/mt/Thread.h>
#include <canopy/canopy.h>
#include <thales/thales.h>
#include <newton/newton.h>
#include <newton/Matrix.h>
#include <thales/kalman/Gaussian.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>
#include <canopy/time/Time.h>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::canopy::mt;
using namespace ::timber::mt;
using namespace ::thales;
using namespace ::newton;
using namespace ::thales::kalman;
using namespace ::thales::kalman::frame;

static size_t MAX_ITERATIONS = 1000;

static Reference< Gaussian> newGaussian4D(double x, double y, double vx, double vy, double xx, double yy, double vxvx,
      double vyvy, double xy = 0.0, double vxvy = 0)
{
   Vector< double> mean(x, y, vx, vy);
   Matrix< double> cvar(mean.dimension());
   cvar(0, 0) = xx;
   cvar(1, 1) = yy;
   cvar(2, 2) = vxvx;
   cvar(3, 3) = vyvy;
   cvar(0, 1) = cvar(1, 0) = xy;
   cvar(2, 3) = cvar(3, 2) = vxvy;

   Reference< Gaussian> res(Gaussian::create(Cartesian_2D_Inertial_PV::create(), ::std::move(mean), ::std::move(cvar)));
   return Gaussian::create(res->frame(), res->randomSample(), res->covariance());
}

struct PostUpdateTask : public Task
{
      PostUpdateTask(const vector< Reference< PreparedUpdate> >& data, size_t offset, size_t stride)
throws            ()
            : xPreUpdates(data),xOffset(offset),xStride(stride),xNumCalcs(0) {}

            ~PostUpdateTask() throws()
            {
            }

            void doTask(Job&) throws()
            {
               for (size_t i=xOffset;i<xPreUpdates.size();i+=xStride) {
                  xPreUpdates[i]->postUpdate();
                  ++xNumCalcs;
               }
            }

            const vector<Reference<PreparedUpdate> >& xPreUpdates;
            size_t xOffset;
            size_t xStride;
            size_t xNumCalcs;
         };

void doTest(JobQueue& queue, size_t nProcessors)
{
   ::std::cerr << "Measure the update with the innovation (4D -> 4D)" << endl << flush;
   vector < ::std::shared_ptr< Job> > jobs;

   vector< Reference< PreparedUpdate> > U;
   vector< Reference< Gaussian> > RESULT;

   for (size_t i = 0; i < MAX_ITERATIONS; ++i) {
      Reference< Gaussian> z(newGaussian4D(10 * i, 10 * i, -1, 1, 1, 1, .125, .125));
      Reference< Gaussian> p(newGaussian4D(10 * i + 1, 10 * i + 1, -1, 1, 1, 1, .125, .125));
      U.push_back(p->prepareKalmanUpdate(z));
   }
   ::std::cerr << "Prepared updates" << ::std::endl;

   size_t nCalc = 0;
   ::canopy::time::Time start = ::canopy::time::Time::now();
   size_t stride = nProcessors;
   for (size_t j = 0; j < 30000; ++j) {
      for (size_t i = 0; i < stride; ++i) {
         auto task = ::std::make_shared < PostUpdateTask > (U, i, stride);
         jobs.push_back(queue.scheduleTask(task));
      }
   }
   ::std::cerr << "Execute remaining jobs " << ::std::endl;
   queue.executeJobs();
   ::std::cerr << "Waiting for jobs to complete" << ::std::endl;

   for (size_t i = 0; i < jobs.size(); ++i) {
      if (jobs[i]->wait() == Job::FINISHED) {
         nCalc += static_cast< PostUpdateTask&>(*jobs[i]->task()).xNumCalcs;
      }
   }
   ::canopy::time::Time end = ::canopy::time::Time::now();
   ::std::cerr << "Number of calculations " << nCalc << ::std::endl;

   UInt64 elapsedTimeUS = (end.time() - start.time()).count() / 1000;
   UInt64 elapsedTimeMS = elapsedTimeUS / 1000;
   ::std::cerr << "Total time           " << elapsedTimeMS << " ms" << endl << "Time per calculation "
         << elapsedTimeUS / nCalc << " us/calculation" << endl;
   if (elapsedTimeMS > 0) {
      ::std::cerr << "Calculation Rate     " << nCalc / elapsedTimeMS << " calculation/ms" << endl;
   }
   ::std::cerr << flush;
}

int main(int argc, char** argv)
{
   size_t nThreads = 0;
   if (argc > 1) {
      istringstream sin(argv[1]);
      sin >> nThreads;
   }
   auto queue = JobQueue::create(nThreads);
   cerr << "Creating job queue with " << nThreads << " processors" << endl;
   doTest(*queue, 1 + nThreads);
   return 0;
}

