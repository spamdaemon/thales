
#include <thales/kalman/Filter.h>
#include <thales/kalman/GaussianTransform.h>
#include <thales/kalman/Prediction.h>
#include <thales/kalman/model/ConstantVelocityModel2D.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_P.h>
#include <thales/kalman/frame/Cartesian_2D_Inertial_PV.h>

#include <iostream>
#include <cmath>
#include <cstdlib>

using namespace std;
using namespace timber;
using namespace thales::kalman;
using namespace thales::kalman::frame;
using namespace ::newton;


int main(int argc, char** argv)
{
  // setup a filter
  Vector<double> mean(0.0,0.0,0.0,0.0);
  Matrix<double> cov = Matrix<double>::identity(mean.dimension());
  
  Reference<Gaussian> initial(Gaussian::create(Cartesian_2D_Inertial_PV::create(),mean, cov));
  Reference<Filter> filter = Filter::createFilter(0,initial,new ::thales::kalman::model::ConstantVelocityModel2D(3));
    
  for (double i=1;i<10;i += 1.0) {
    Vector<double> zMean(0,10*i);
    Matrix<double> zCov(Matrix<double>::identity(zMean.dimension()));
    Reference<Gaussian> z(Gaussian::create(Cartesian_2D_Inertial_P::create(),zMean,zCov));
   
   Reference<Prediction> p = filter->predict(i);
   filter = p->update(z);
   
   cerr << "Time " << i << endl
	<< " Prediction " << *p->state() << endl
	<< " Update     " << *filter->state() << endl
     ;
  }

  return 0;
}
